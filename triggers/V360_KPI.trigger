/* ----------------------------------------------------------------------------
* AB InBev :: 360 View
* ----------------------------------------------------------------------------
* Clase: V360_KPI.apxt
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                  				 Description
* 18/12/2018     g.martinez.cabral@accenture.com        Creation of Trigger which executes after delete, after insert, after update,
														 before delete, before insert, before update on the object ONTAP__KPI__c
*/


/**
* Constructor which call the V360_KPITriggerHandler in the following cases: after delete, after insert, after update, before delete, before insert, before update
* @author: g.martinez.cabral@accenture.com
* @param void
* @return Void
*/
trigger V360_KPI on ONTAP__KPI__c (after delete, after insert, after update, before delete, before insert, before update) {

    New V360_KPITriggerHandler().Run();
    
    if( Trigger.isInsert || Trigger.isUpdate ){
        
        if( Trigger.isBefore ){
            
            DRKPITriggerHandler handler = new DRKPITriggerHandler( Trigger.new );
            handler.run();
            
        }       
    }
}