trigger LocalCaseForceTrigger on ONTAP__Case_Force__c (before insert, before update,before delete,after insert,after update,after delete,  after undelete) {
	new LocalCaseForce_TriggerHandler().run();
}