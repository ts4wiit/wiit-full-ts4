trigger CS_TriggerContact on Contact (after delete, after insert, after update, before delete, before insert, before update) {
    new CS_TriggerContactHandler().run();
}