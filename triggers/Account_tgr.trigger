/*******************************************************************************
Desarrollado por: Avanxo México
Autor: Daniel Peñaloza
Proyecto: ISSM - DSD
Descripción: Trigger handler for Events related to Account records

------ ---------- -------------------------- -----------------------------------
No.    Fecha      Autor                      Descripción
------ ---------- -------------------------- -----------------------------------
1.0    22/08/2017 Daniel Peñaloza            Trigger created
*******************************************************************************/

trigger Account_tgr on Account  (after delete, after insert, after update, before delete, before insert, before update)  {
     
    New V360_AccountTriggerHandler().Run();    
   
    if( Trigger.isBefore ){   
        
        DRAccountTriggerHandler handler   = new DRAccountTriggerHandler(  Trigger.new  );
        DRAccountPublisher      publisher = new DRAccountPublisher( Trigger.newMap,Trigger.oldMap );
        
        if( Trigger.isInsert ){            
            handler.run();
            DRAccountTriggerHandler.setGeolocationField( Trigger.new ); 
        }
        
        if( Trigger.isUpdate ){
            handler.run();
            publisher.run();
        }
    }
    
    if( Trigger.isAfter ){   
        
        DRAccountTriggerHandler handler = new DRAccountTriggerHandler(  Trigger.new  );
        
        if( Trigger.isInsert ){    
            
            DRAccountTriggerHandler.setProspectClosestPocField( Trigger.new );
        }
    }
}