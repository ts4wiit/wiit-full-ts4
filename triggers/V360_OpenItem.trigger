/* ----------------------------------------------------------------------------
* AB InBev :: 360 View
* ----------------------------------------------------------------------------
* Clase: V360_OpenItem.apxt
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                  				 Description
* 18/12/2018     g.martinez.cabral@accenture.com        Creation of Trigger which executes after delete, after insert, after update,
													    before delete, before insert, before update on the object ONTAP__OpenItem__c
*/


/**
* Constructor which call the V360_OpenItemsTriggerHandler in the following cases: after delete, after insert, after update, before delete, before insert, before update
* @author: g.martinez.cabral@accenture.com
* @param void
* @return Void
*/
trigger V360_OpenItem on ONTAP__OpenItem__c (after delete, after insert, after update, before delete, before insert, before update) {
    //New V360_OpenItemsTriggerHandler().run(); -- This handler assigned the related account via apex. We changed it to use external ids and assigned the account from integration -- drs@globant
    if( Trigger.isInsert || Trigger.isUpdate ){      
        if( Trigger.isBefore ){         
            COPEC_OpenItemTriggerHandler handler = new COPEC_OpenItemTriggerHandler( Trigger.new );
            handler.run();           
        }       
    }
}