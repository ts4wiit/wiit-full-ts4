trigger AccountChangeEventTrigger on AccountChangeEvent ( after insert ) {
    System.debug('>>> Trigger is executing...');
    AccountChangeEventTriggerHandler accountChangeEventTriggerHandler = new accountChangeEventTriggerHandler( Trigger.new );
    accountChangeEventTriggerHandler.run();

}