/* ----------------------------------------------------------------------------
 * AB InBev ::
 * ----------------------------------------------------------------------------
 * Clase: ONTAP_Order_Item.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 01/02/2019     Gerardo Martinez        Creation of Trigger which executes  after insert, after update, 
														before insert, before update on the object ONTAP__Order_Item__c
 */



/**
* Constructor which call the ONTAP_OrderItemTriggerHandler in the following cases:after insert, after update, before insert, before update
* @author: g.martinez.cabral@accenture.com
* @param void
* @return Void
*/
trigger ONTAP_Order_Item on ONTAP__Order_Item__c( after insert,after update,before insert,  before update ) {
    New ONTAP_OrderItemTriggerHandler().run();
    
    if( Trigger.isUpdate || Trigger.isInsert ){
        
        if( Trigger.isBefore ){
            
            DROrderItemTriggerHandler handler = new DROrderItemTriggerHandler( Trigger.new );
            handler.run();
            
        }
        
    }
    
}