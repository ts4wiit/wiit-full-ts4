/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_CASEFORCE_TRIGGER.apxt
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User               Description
 * 03/12/2018      José Luis Vargas        Creacion del trigger que se ejecutara despues de una insercion
 *                                         o actualización en le objeto ONTAP__Case_Force__c
 */


/**
* Constructor which call the CS_TRIGGER_CASEFORCE_CLASS TriggerHandler in the following cases: after insert, after update, before insert, before update
* @author: José Luis Vargas
* @param void
* @return Void
*/
trigger CS_CaseForce_Trigger on ONTAP__Case_Force__c (after insert, after update, before insert, before update)  
{
    new CS_Trigger_CaseForce_Class().run();

    DRCaseForceTriggerHandler handler = new DRCaseForceTriggerHandler( Trigger.new );

    if( Trigger.isBefore ){
        handler.runBefore();
    }

    if( Trigger.isAfter ){
        handler.runAfter();
    }

    if(Trigger.isAfter && Trigger.isUpdate){
         Local_CaseForce_SendNotification.verifyStatusToSendNotifications((Map<Id, ONTAP__Case_Force__c>) Trigger.oldmap, (List<ONTAP__Case_Force__c>) Trigger.new);
    }
}