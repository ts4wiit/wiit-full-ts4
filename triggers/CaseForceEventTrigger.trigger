trigger CaseForceEventTrigger on ONTAP__Case_Force__ChangeEvent (after insert) {

    CaseForceEventTriggerHandler caseForceEventTriggerHandler = new CaseForceEventTriggerHandler( Trigger.new );
    caseForceEventTriggerHandler.run();
    
}