trigger LocalFeedItemTrigger on FeedItem (before insert, before update, after insert, after update) {
    new LocalFeedItem_TriggerHandler().run();
}