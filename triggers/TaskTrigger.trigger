trigger TaskTrigger on Task (after insert, after update) {
    
    TaskPublisher publisher = new TaskPublisher( Trigger.new );
    publisher.run();

}