trigger CaseForceCommentEventTrigger on ONTAP__Case_Force_Comment__ChangeEvent (after insert) {
    
   CaseForceCommentEventTriggerHandler caseForceCommentEventTriggerHandler = new CaseForceCommentEventTriggerHandler( Trigger.new );
   caseForceCommentEventTriggerHandler.run();

}