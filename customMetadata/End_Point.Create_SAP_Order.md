<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Create SAP Order</label>
    <protected>false</protected>
    <values>
        <field>Country__c</field>
        <value xsi:type="xsd:string">PA</value>
    </values>
    <values>
        <field>Timeout__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Token__c</field>
        <value xsi:type="xsd:string">noesnecesario</value>
    </values>
    <values>
        <field>URL__c</field>
        <value xsi:type="xsd:string">http://abi-pedidos-panama.de-c1.cloudhub.io/api/abipanama/pedidos</value>
    </values>
</CustomMetadata>
