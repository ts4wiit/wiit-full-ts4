<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>El Salvador</label>
    <protected>false</protected>
    <values>
        <field>Country__c</field>
        <value xsi:type="xsd:string">SV</value>
    </values>
    <values>
        <field>Min__c</field>
        <value xsi:type="xsd:string">SV_Good_Shape_Product,SV_Out_of_Regulation_Product,SV_Return_of_Empties</value>
    </values>
    <values>
        <field>Sum__c</field>
        <value xsi:type="xsd:string">SV_Empties_Introduction,SV_Finished_Product,SV_Gifts</value>
    </values>
</CustomMetadata>
