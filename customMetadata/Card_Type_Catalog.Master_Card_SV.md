<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Master Card SV</label>
    <protected>false</protected>
    <values>
        <field>IREP_CardDescription__c</field>
        <value xsi:type="xsd:string">Master Card</value>
    </values>
    <values>
        <field>IREP_CardId__c</field>
        <value xsi:type="xsd:string">MC</value>
    </values>
    <values>
        <field>IREP_Country__c</field>
        <value xsi:type="xsd:string">SV</value>
    </values>
</CustomMetadata>
