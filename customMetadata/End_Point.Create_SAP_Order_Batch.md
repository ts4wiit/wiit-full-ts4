<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Create SAP Order Batch</label>
    <protected>false</protected>
    <values>
        <field>Country__c</field>
        <value xsi:type="xsd:string">SV</value>
    </values>
    <values>
        <field>Timeout__c</field>
        <value xsi:type="xsd:double">10000.0</value>
    </values>
    <values>
        <field>Token__c</field>
        <value xsi:type="xsd:string">262fd69b-18c8-4172-9157-33d3de2917e4</value>
    </values>
    <values>
        <field>URL__c</field>
        <value xsi:type="xsd:string">https://wiit-hndesv-orders-sfdc-to-sap.de-c1.cloudhub.io/api/SV/generate-orders-batch</value>
    </values>
</CustomMetadata>
