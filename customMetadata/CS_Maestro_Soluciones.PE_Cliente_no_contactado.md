<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PE Cliente no contactado</label>
    <protected>false</protected>
    <values>
        <field>Pais__c</field>
        <value xsi:type="xsd:string">Peru</value>
    </values>
    <values>
        <field>Valor_Maestro_Solucion_Eng__c</field>
        <value xsi:type="xsd:string">Customer not cotacted</value>
    </values>
    <values>
        <field>Valor_Maestro_Solucion_SP__c</field>
        <value xsi:type="xsd:string">Cliente no contactado</value>
    </values>
</CustomMetadata>
