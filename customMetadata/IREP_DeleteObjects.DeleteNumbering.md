<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DeleteNumbering</label>
    <protected>false</protected>
    <values>
        <field>IREP_Days__c</field>
        <value xsi:type="xsd:string">60</value>
    </values>
    <values>
        <field>IREP_Object__c</field>
        <value xsi:type="xsd:string">IREP_Numbering__c</value>
    </values>
    <values>
        <field>IREP_RecordTypes__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>IREP_Step__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
</CustomMetadata>
