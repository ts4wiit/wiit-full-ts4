<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>HON_ClienteNoAplica</label>
    <protected>false</protected>
    <values>
        <field>Pais__c</field>
        <value xsi:type="xsd:string">Honduras</value>
    </values>
    <values>
        <field>Valor_Maestro_Solucion_Eng__c</field>
        <value xsi:type="xsd:string">Client not apply</value>
    </values>
    <values>
        <field>Valor_Maestro_Solucion_SP__c</field>
        <value xsi:type="xsd:string">Cliente no aplica</value>
    </values>
</CustomMetadata>
