<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Mayoristas</label>
    <protected>false</protected>
    <values>
        <field>HONES_Country__c</field>
        <value xsi:type="xsd:string">SV</value>
    </values>
    <values>
        <field>HONES_CreditGroup__c</field>
        <value xsi:type="xsd:string">0018</value>
    </values>
    <values>
        <field>HONES_CreditRating__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>HONES_Porcentaje_Tolerancia__c</field>
        <value xsi:type="xsd:double">10.0</value>
    </values>
    <values>
        <field>ISSM_AccountGroup__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
