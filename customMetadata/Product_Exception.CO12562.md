<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CO2-12562</label>
    <protected>false</protected>
    <values>
        <field>Country__c</field>
        <value xsi:type="xsd:string">SV</value>
    </values>
    <values>
        <field>IsReturnable__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>SKU__c</field>
        <value xsi:type="xsd:string">12562</value>
    </values>
    <values>
        <field>Unidad__c</field>
        <value xsi:type="xsd:string">UN</value>
    </values>
</CustomMetadata>
