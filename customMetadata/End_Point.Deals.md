<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Deals</label>
    <protected>false</protected>
    <values>
        <field>Country__c</field>
        <value xsi:type="xsd:string">SV</value>
    </values>
    <values>
        <field>Timeout__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Token__c</field>
        <value xsi:type="xsd:string">43125716217827821687216871698166719217933820471113209821380291928389</value>
    </values>
    <values>
        <field>URL__c</field>
        <value xsi:type="xsd:string">https://abco-priceengine.herokuapp.com/dealslist</value>
    </values>
</CustomMetadata>
