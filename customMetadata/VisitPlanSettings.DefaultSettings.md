<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Default Settings</label>
    <protected>false</protected>
    <values>
        <field>AssignedTourStatus__c</field>
        <value xsi:type="xsd:string">Assigned</value>
    </values>
    <values>
        <field>AssignedTourSubStatus__c</field>
        <value xsi:type="xsd:string">Tour vigente</value>
    </values>
    <values>
        <field>BatchSizeHK__c</field>
        <value xsi:type="xsd:double">5.0</value>
    </values>
    <values>
        <field>BatchSize__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>CreatedTourStatus__c</field>
        <value xsi:type="xsd:string">Created</value>
    </values>
    <values>
        <field>CreatedTourSubStatus__c</field>
        <value xsi:type="xsd:string">Tour creado en SFDC</value>
    </values>
    <values>
        <field>FilterServiceModels__c</field>
        <value xsi:type="xsd:string">Presales,Autosales</value>
    </values>
    <values>
        <field>InitialCallStatus__c</field>
        <value xsi:type="xsd:string">Pending to call</value>
    </values>
    <values>
        <field>InitialVisitStatus__c</field>
        <value xsi:type="xsd:string">Pending</value>
    </values>
    <values>
        <field>InitialVisitSubStatus__c</field>
        <value xsi:type="xsd:string">Visita no iniciada</value>
    </values>
    <values>
        <field>NAServiceModel__c</field>
        <value xsi:type="xsd:string">N/A</value>
    </values>
    <values>
        <field>NameOnCall__c</field>
        <value xsi:type="xsd:string">Levantar Pedido</value>
    </values>
    <values>
        <field>NameTourId__c</field>
        <value xsi:type="xsd:string">Name-Tour</value>
    </values>
    <values>
        <field>OwnerField__c</field>
        <value xsi:type="xsd:string">OwnerId</value>
    </values>
    <values>
        <field>OwnerObject__c</field>
        <value xsi:type="xsd:string">ONTAP__Route__c</value>
    </values>
    <values>
        <field>RTAutosales__c</field>
        <value xsi:type="xsd:string">Autosales</value>
    </values>
    <values>
        <field>RTBdr__c</field>
        <value xsi:type="xsd:string">BDR</value>
    </values>
    <values>
        <field>RTPresales__c</field>
        <value xsi:type="xsd:string">Presales</value>
    </values>
    <values>
        <field>RTTelesales__c</field>
        <value xsi:type="xsd:string">Telesales</value>
    </values>
    <values>
        <field>VisitPeriodConfig__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>VisitSubject__c</field>
        <value xsi:type="xsd:string">Visit</value>
    </values>
</CustomMetadata>
