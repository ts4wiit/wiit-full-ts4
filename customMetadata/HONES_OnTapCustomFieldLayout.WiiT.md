<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>WiiT</label>
    <protected>false</protected>
    <values>
        <field>Country__c</field>
        <value xsi:type="xsd:string">SV</value>
    </values>
    <values>
        <field>Definition__c</field>
        <value xsi:type="xsd:string">SAP_Number__c,Name</value>
    </values>
    <values>
        <field>Key__c</field>
        <value xsi:type="xsd:string">Account|Prospect_SV|V360_ReferenceClient__c</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Fields</value>
    </values>
</CustomMetadata>
