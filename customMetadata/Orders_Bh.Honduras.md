<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Honduras</label>
    <protected>false</protected>
    <values>
        <field>Country__c</field>
        <value xsi:type="xsd:string">HN</value>
    </values>
    <values>
        <field>Min__c</field>
        <value xsi:type="xsd:string">HN_Good_Shape_Product,HN_Out_of_Regulation_Product,HN_Return_of_Empties</value>
    </values>
    <values>
        <field>Sum__c</field>
        <value xsi:type="xsd:string">HN_Empties_Introduction,HN_Finished_Product,HN_Gifts</value>
    </values>
</CustomMetadata>
