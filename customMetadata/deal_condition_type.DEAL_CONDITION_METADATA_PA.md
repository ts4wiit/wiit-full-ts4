<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DEAL CONDITION METADATA PA</label>
    <protected>false</protected>
    <values>
        <field>Country__c</field>
        <value xsi:type="xsd:string">PA</value>
    </values>
    <values>
        <field>amount__c</field>
        <value xsi:type="xsd:string">YNK6</value>
    </values>
    <values>
        <field>percentage__c</field>
        <value xsi:type="xsd:string">YNK5</value>
    </values>
</CustomMetadata>
