<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ES_Comunicacion</label>
    <protected>false</protected>
    <values>
        <field>Pais__c</field>
        <value xsi:type="xsd:string">El Salvador</value>
    </values>
    <values>
        <field>Pregunta_Maestro_Solucion_Eng__c</field>
        <value xsi:type="xsd:string">¿Did you receive communication about the answer / solution from the responsible?</value>
    </values>
    <values>
        <field>Pregunta_Maestro_Solucion_SP__c</field>
        <value xsi:type="xsd:string">¿Recibió comunicación sobre la respuesta/solucion de parte del encargado?</value>
    </values>
</CustomMetadata>
