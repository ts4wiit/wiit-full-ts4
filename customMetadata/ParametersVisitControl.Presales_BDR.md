<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Presales-BDR</label>
    <protected>false</protected>
    <values>
        <field>CanCoexist__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>SameFrequency__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>SameWeeklyPeriod__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>ServiceModel2__c</field>
        <value xsi:type="xsd:string">BDR</value>
    </values>
    <values>
        <field>ServiceModel__c</field>
        <value xsi:type="xsd:string">Presales</value>
    </values>
    <values>
        <field>WhoPrevails__c</field>
        <value xsi:type="xsd:string">Presales</value>
    </values>
</CustomMetadata>
