<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Empties Introduction</label>
    <protected>false</protected>
    <values>
        <field>Add_Empties__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Atr_4__c</field>
        <value xsi:type="xsd:string">C1</value>
    </values>
    <values>
        <field>Available_Units__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Countries__c</field>
        <value xsi:type="xsd:string">SV</value>
    </values>
    <values>
        <field>Es__c</field>
        <value xsi:type="xsd:string">Siembra de envases</value>
    </values>
    <values>
        <field>Generate_Ticket__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>ID_SAP__c</field>
        <value xsi:type="xsd:string">ZSV9</value>
    </values>
    <values>
        <field>Is_Active__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Max_Weight__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Order_Item_Record_Type__c</field>
        <value xsi:type="xsd:string">SV_Order_Item</value>
    </values>
    <values>
        <field>Order_Record_Type__c</field>
        <value xsi:type="xsd:string">SV_Empties_Introduction</value>
    </values>
    <values>
        <field>Price_Engine_Apply_Deals__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Price_Engine_Request_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SAP_Reason__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SKU_Type__c</field>
        <value xsi:type="xsd:string">FERT</value>
    </values>
    <values>
        <field>Save_With_PO_Reference__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
