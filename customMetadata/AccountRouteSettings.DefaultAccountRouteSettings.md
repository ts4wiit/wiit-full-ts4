<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Default Account-Route Settings</label>
    <protected>false</protected>
    <values>
        <field>AccountRecordTypeToValidate__c</field>
        <value xsi:type="xsd:string">Account</value>
    </values>
    <values>
        <field>SalesAgentTeamMemberRole__c</field>
        <value xsi:type="xsd:string">Preventa</value>
    </values>
    <values>
        <field>ServiceModelsToFilter__c</field>
        <value xsi:type="xsd:string">Presales,Autosales</value>
    </values>
    <values>
        <field>SupervisorTeamMemberRole__c</field>
        <value xsi:type="xsd:string">Supervisor</value>
    </values>
    <values>
        <field>UserToAssignOrphanAccount__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
