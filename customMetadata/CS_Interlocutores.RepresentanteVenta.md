<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>RepresentanteVenta</label>
    <protected>false</protected>
    <values>
        <field>CS_Interlocutor_ENG__c</field>
        <value xsi:type="xsd:string">Sales Representative</value>
    </values>
    <values>
        <field>CS_Interlocutor_Nivel__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>CS_Interlocutor_SPN__c</field>
        <value xsi:type="xsd:string">Representante de Ventas</value>
    </values>
    <values>
        <field>CS_Tipo_Interlocutor__c</field>
        <value xsi:type="xsd:string">Preventa</value>
    </values>
</CustomMetadata>
