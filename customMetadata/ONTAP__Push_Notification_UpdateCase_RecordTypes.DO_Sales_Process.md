<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DO - Sales Process</label>
    <protected>true</protected>
    <values>
        <field>ONTAP__Enabled__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>ONTAP__Notification_Message__c</field>
        <value xsi:type="xsd:string">Se ha realizado un cambio en el caso.</value>
    </values>
    <values>
        <field>ONTAP__RecordTypeName__c</field>
        <value xsi:type="xsd:string">DO - Sales Process</value>
    </values>
    <values>
        <field>ONTAP__Record_Type__c</field>
        <value xsi:type="xsd:string">Asset Maintenance</value>
    </values>
</CustomMetadata>
