<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PA - Prices</label>
    <protected>false</protected>
    <values>
        <field>Country__c</field>
        <value xsi:type="xsd:string">PA</value>
    </values>
    <values>
        <field>Token__c</field>
        <value xsi:type="xsd:string">0871230981203987210938712093780192873098172036127638912591263198</value>
    </values>
    <values>
        <field>URL__c</field>
        <value xsi:type="xsd:string">https://abpape-priceengine-prod.herokuapp.com/priceorder</value>
    </values>
</CustomMetadata>
