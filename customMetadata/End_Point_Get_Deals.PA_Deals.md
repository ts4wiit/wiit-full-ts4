<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PA - Deals</label>
    <protected>false</protected>
    <values>
        <field>Country__c</field>
        <value xsi:type="xsd:string">PA</value>
    </values>
    <values>
        <field>Token__c</field>
        <value xsi:type="xsd:string">6542384656532768465476234723546352874283765472635726825375218186</value>
    </values>
    <values>
        <field>URL__c</field>
        <value xsi:type="xsd:string">https://abco-priceengine.herokuapp.com/dealslist</value>
    </values>
</CustomMetadata>
