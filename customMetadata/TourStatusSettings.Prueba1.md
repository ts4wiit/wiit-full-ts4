<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Prueba1</label>
    <protected>false</protected>
    <values>
        <field>AllowsEventDeletion__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>RecordTypesToExclude__c</field>
        <value xsi:type="xsd:string">IREP</value>
    </values>
    <values>
        <field>Status__c</field>
        <value xsi:type="xsd:string">Checking out</value>
    </values>
    <values>
        <field>Substatus__c</field>
        <value xsi:type="xsd:string">Descarga al dispositivo finalizada correctamente</value>
    </values>
</CustomMetadata>
