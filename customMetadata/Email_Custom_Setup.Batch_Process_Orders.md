<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Batch Process Orders</label>
    <protected>false</protected>
    <values>
        <field>Country__c</field>
        <value xsi:type="xsd:string">ALL</value>
    </values>
    <values>
        <field>Emails__c</field>
        <value xsi:type="xsd:string">rjimenez@ts4.mx</value>
    </values>
    <values>
        <field>Process__c</field>
        <value xsi:type="xsd:string">SendOrder</value>
    </values>
    <values>
        <field>Send_in_Error__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
