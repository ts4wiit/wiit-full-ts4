<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Gifts</label>
    <protected>false</protected>
    <values>
        <field>Add_Empties__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Atr_4__c</field>
        <value xsi:type="xsd:string">C1</value>
    </values>
    <values>
        <field>Available_Units__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Countries__c</field>
        <value xsi:type="xsd:string">PA</value>
    </values>
    <values>
        <field>Es__c</field>
        <value xsi:type="xsd:string">Obsequios</value>
    </values>
    <values>
        <field>Generate_Ticket__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>ID_SAP__c</field>
        <value xsi:type="xsd:string">ZNO1</value>
    </values>
    <values>
        <field>Is_Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Max_Weight__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Order_Item_Record_Type__c</field>
        <value xsi:type="xsd:string">PA_Order_Item</value>
    </values>
    <values>
        <field>Order_Record_Type__c</field>
        <value xsi:type="xsd:string">PA_Gifts</value>
    </values>
    <values>
        <field>Price_Engine_Apply_Deals__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Price_Engine_Request_Type__c</field>
        <value xsi:type="xsd:string">OBS</value>
    </values>
    <values>
        <field>SAP_Reason__c</field>
        <value xsi:type="xsd:string">D2-Activaciones,D1-Actividades dep,D3-Aniversarios,D4-Auspicios,D8-Compensación a,D5-Eventos Especia,
D6-Exclusividades,D7-Premio Promocio,D9-Promoción Dev E,D0-Campaña promocio,E8-Consolid. Dist</value>
    </values>
    <values>
        <field>SKU_Type__c</field>
        <value xsi:type="xsd:string">FERT</value>
    </values>
    <values>
        <field>Save_With_PO_Reference__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
