<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <description>Custom metadata with settings for Tour Status and SubStatus Fields. For example, to validate Event deletion</description>
    <fields>
        <fullName>AllowsEventDeletion__c</fullName>
        <defaultValue>false</defaultValue>
        <description>It indicates if the Status and Substatus combination allows to delete Event records created for the tour</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Allows Event Deletion</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>IsActive__c</fullName>
        <defaultValue>true</defaultValue>
        <description>It indicates if the Status and Substatus combination is active for validation</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Is Active</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>RecordTypesToExclude__c</fullName>
        <description>Comma separated Tour Record Type list (with Developer Name) to exclude from validations</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Record Types to Exclude</label>
        <required>false</required>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <description>Tour Status</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Status</label>
        <required>true</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Created</fullName>
                    <default>true</default>
                    <label>Created</label>
                </value>
                <value>
                    <fullName>Assigned</fullName>
                    <default>false</default>
                    <label>Assigned</label>
                </value>
                <value>
                    <fullName>Assignment error</fullName>
                    <default>false</default>
                    <label>Assignment error</label>
                </value>
                <value>
                    <fullName>Not Started</fullName>
                    <default>false</default>
                    <label>Not Started</label>
                </value>
                <value>
                    <fullName>Downloading</fullName>
                    <default>false</default>
                    <label>Downloading</label>
                </value>
                <value>
                    <fullName>Error Downloading</fullName>
                    <default>false</default>
                    <label>Error Downloading</label>
                </value>
                <value>
                    <fullName>Checking out</fullName>
                    <default>false</default>
                    <label>Checking out</label>
                </value>
                <value>
                    <fullName>Incorrect Data</fullName>
                    <default>false</default>
                    <label>Incorrect Data</label>
                </value>
                <value>
                    <fullName>In Progress</fullName>
                    <default>false</default>
                    <label>In Progress</label>
                </value>
                <value>
                    <fullName>Checking in</fullName>
                    <default>false</default>
                    <label>Checking in</label>
                </value>
                <value>
                    <fullName>Complete</fullName>
                    <default>false</default>
                    <label>Complete</label>
                </value>
                <value>
                    <fullName>Recovered</fullName>
                    <default>false</default>
                    <label>Recovered</label>
                </value>
                <value>
                    <fullName>Not Completed</fullName>
                    <default>false</default>
                    <label>Not Completed</label>
                </value>
                <value>
                    <fullName>Uploading</fullName>
                    <default>false</default>
                    <label>Uploading</label>
                </value>
                <value>
                    <fullName>Error Uploading</fullName>
                    <default>false</default>
                    <label>Error Uploading</label>
                </value>
                <value>
                    <fullName>Finished</fullName>
                    <default>false</default>
                    <label>Finished</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Substatus__c</fullName>
        <description>Tour Substatus</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Substatus</label>
        <required>false</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Tour creado en SFDC</fullName>
                    <default>true</default>
                    <label>Tour creado en SFDC</label>
                </value>
                <value>
                    <fullName>Tour vigente</fullName>
                    <default>false</default>
                    <label>Tour vigente</label>
                </value>
                <value>
                    <fullName>Tour listo para generar archivo download</fullName>
                    <default>false</default>
                    <label>Tour listo para generar archivo download</label>
                </value>
                <value>
                    <fullName>Descarga de SAP finalizada con errores</fullName>
                    <default>false</default>
                    <label>Descarga de SAP finalizada con errores</label>
                </value>
                <value>
                    <fullName>Listo para descargar al dispositivo</fullName>
                    <default>false</default>
                    <label>Listo para descargar al dispositivo</label>
                </value>
                <value>
                    <fullName>Descarga iniciada de BD al dispositivo</fullName>
                    <default>false</default>
                    <label>Descarga iniciada de BD al dispositivo</label>
                </value>
                <value>
                    <fullName>Descarga finalizada con errores, BD al dispositivo</fullName>
                    <default>false</default>
                    <label>Descarga finalizada con errores, BD al dispositivo</label>
                </value>
                <value>
                    <fullName>Descarga al dispositivo finalizada correctamente</fullName>
                    <default>false</default>
                    <label>Descarga al dispositivo finalizada correctamente</label>
                </value>
                <value>
                    <fullName>Usuario declara datos incorrectos</fullName>
                    <default>false</default>
                    <label>Usuario declara datos incorrectos</label>
                </value>
                <value>
                    <fullName>Salida a ruta iniciada</fullName>
                    <default>false</default>
                    <label>Salida a ruta iniciada</label>
                </value>
                <value>
                    <fullName>Cierre de ruta</fullName>
                    <default>false</default>
                    <label>Cierre de ruta</label>
                </value>
                <value>
                    <fullName>Subida iniciado del dispositivo a la BD.</fullName>
                    <default>false</default>
                    <label>Subida iniciado del dispositivo a la BD.</label>
                </value>
                <value>
                    <fullName>Subida del dispositivo a  BD finalizada con error</fullName>
                    <default>false</default>
                    <label>Subida del dispositivo a  BD finalizada con error</label>
                </value>
                <value>
                    <fullName>Tour recuperado del dispositivo</fullName>
                    <default>false</default>
                    <label>Tour recuperado del dispositivo</label>
                </value>
                <value>
                    <fullName>Error al recuperar tour desde el WS</fullName>
                    <default>false</default>
                    <label>Error al recuperar tour desde el WS</label>
                </value>
                <value>
                    <fullName>Subida de Jbridge a SAP</fullName>
                    <default>false</default>
                    <label>Subida de Jbridge a SAP</label>
                </value>
                <value>
                    <fullName>Error al subir de Jbridge a SAP</fullName>
                    <default>false</default>
                    <label>Error al subir de Jbridge a SAP</label>
                </value>
                <value>
                    <fullName>Subida del dispositivo a la BD terminada OK.</fullName>
                    <default>false</default>
                    <label>Subida del dispositivo a la BD terminada OK.</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Tour Status Settings</label>
    <pluralLabel>Tour Status Settings</pluralLabel>
    <visibility>Public</visibility>
</CustomObject>
