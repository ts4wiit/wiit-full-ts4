<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <description>Custom Metadata to gather Order Types</description>
    <fields>
        <fullName>Add_Empties__c</fullName>
        <defaultValue>false</defaultValue>
        <description>If checked, send X mark in Item to Add return of empties to the item created</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Add Empties</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Atr_4__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Atr 4</label>
        <length>5</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Available_Units__c</fullName>
        <description>If this Order Type supports other than CAJ, list here available units comma separated. Must adhere to ONTAP__Product__c.ONCALL_MeasureOfUnits__c picklist</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Available Units</label>
        <length>100</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Countries__c</fullName>
        <description>Saves the country initials to allow the users from this entity to create this type of orders</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Countries</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Es__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Es</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Generate_Ticket__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Generate Ticket</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ID_SAP__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>ID SAP</label>
        <length>5</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Is_Active__c</fullName>
        <defaultValue>true</defaultValue>
        <description>For management purposes. You can enable or disable configurations without deleting them</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Is Active</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Max_Weight__c</fullName>
        <description>Maximum weight per product.</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>Maximum weight per product.</inlineHelpText>
        <label>Max Weight</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Order_Item_Record_Type__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Order Item Record Type</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Order_Record_Type__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Order Record Type</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Price_Engine_Apply_Deals__c</fullName>
        <defaultValue>false</defaultValue>
        <description>If deals that Price Engine returns must be applided or displayed</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Price Engine Apply Deals</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Price_Engine_Request_Type__c</fullName>
        <defaultValue>&quot;ORD&quot;</defaultValue>
        <description>Type of the request to send to the Price Engine so it behaves accordingly (PFN, PBE, ORD, ENV)</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Price Engine Request Type</label>
        <length>3</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SAP_Reason__c</fullName>
        <description>For certain Order Types, a reason code must be sent to SAP with the item.</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>SAP_Reason</label>
        <length>10000</length>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>SKU_Type__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>SKU Type</label>
        <length>4</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Save_With_PO_Reference__c</fullName>
        <defaultValue>false</defaultValue>
        <description>When using the Reference Purchase Order field in OnCall Orders, this checkbox marks the Document Type that will have the purchase order. (Only one by country)</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Save With PO Reference</label>
        <type>Checkbox</type>
    </fields>
    <label>Order Type</label>
    <listViews>
        <fullName>Wiit</fullName>
        <columns>MasterLabel</columns>
        <columns>DeveloperName</columns>
        <columns>Es__c</columns>
        <columns>SKU_Type__c</columns>
        <columns>ID_SAP__c</columns>
        <columns>Countries__c</columns>
        <columns>Atr_4__c</columns>
        <columns>Order_Record_Type__c</columns>
        <columns>Order_Item_Record_Type__c</columns>
        <columns>Max_Weight__c</columns>
        <filterScope>Everything</filterScope>
        <label>Wiit</label>
        <language>en_US</language>
    </listViews>
    <pluralLabel>Order Types</pluralLabel>
    <visibility>Public</visibility>
</CustomObject>
