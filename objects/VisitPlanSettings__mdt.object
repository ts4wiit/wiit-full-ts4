<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <description>Tipo de metadatos personalizado para configuración del proceso de generación de Listas de Visitas, Eventos y Llamadas a partir de Planes de Visita</description>
    <fields>
        <fullName>AssignedTourStatus__c</fullName>
        <description>Assigned status for generated tours</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Assigned Tour Status</label>
        <length>30</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AssignedTourSubStatus__c</fullName>
        <description>Assigned Tour Sub Status</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Assigned Tour Sub Status</label>
        <length>30</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>BatchSizeHK__c</fullName>
        <description>Batch size to send tours and events to Heroku</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Batch Size HK</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>BatchSize__c</fullName>
        <description>Batch Size for DailyVisitplan_bch process</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Batch Size</label>
        <precision>18</precision>
        <required>true</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CreatedTourStatus__c</fullName>
        <description>Status for created tours</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Created Tour Status</label>
        <length>30</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CreatedTourSubStatus__c</fullName>
        <description>Created Tour Sub Status</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Created Tour Sub Status</label>
        <length>30</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>FilterServiceModels__c</fullName>
        <description>List of Service Models to filter in Triggers for Route and Account by Route</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Filter Service Models</label>
        <required>false</required>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>InitialCallStatus__c</fullName>
        <description>Initial status of generated calls</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Initial Call Status</label>
        <length>30</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>InitialVisitStatus__c</fullName>
        <description>Initial status of Visit Event</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Initial Visit Event Status</label>
        <length>30</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>InitialVisitSubStatus__c</fullName>
        <description>Initial sub status of Visit Event</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Initial Visit Event Sub Status</label>
        <length>30</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>NAServiceModel__c</fullName>
        <description>Text for non applicable service models</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>N/A Service Model</label>
        <length>30</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>NameOnCall__c</fullName>
        <description>Name on call</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Name on Call</label>
        <required>true</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Levantar Pedido</fullName>
                    <default>true</default>
                    <label>Levantar Pedido</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>NameTourId__c</fullName>
        <description>Name Tour Id</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Name Tour Id</label>
        <length>50</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OwnerField__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Owner Field</label>
        <metadataRelationshipControllingField>VisitPlanSettings__mdt.OwnerObject__c</metadataRelationshipControllingField>
        <referenceTo>FieldDefinition</referenceTo>
        <relationshipLabel>Visit_Plan_Settings</relationshipLabel>
        <relationshipName>Visit_Plan_Settings</relationshipName>
        <required>false</required>
        <type>MetadataRelationship</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OwnerObject__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Owner Object</label>
        <referenceTo>EntityDefinition</referenceTo>
        <relationshipLabel>Visit_Plan_Settings</relationshipLabel>
        <relationshipName>Visit_Plan_Settings</relationshipName>
        <required>true</required>
        <type>MetadataRelationship</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>RTAutosales__c</fullName>
        <description>Record Type Name for Autosales service model</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Record Type - Autosales</label>
        <length>30</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>RTBdr__c</fullName>
        <description>Record type name for BDR service model</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Record Type - BDR</label>
        <length>30</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>RTPresales__c</fullName>
        <description>Record type name for Presales service model</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Record Type - Presales</label>
        <length>30</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>RTTelesales__c</fullName>
        <description>Record type name for Telesales service model</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Record Type - Telesales</label>
        <length>30</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>VisitPeriodConfig__c</fullName>
        <description>Visit Period in days</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Visit Period Config</label>
        <precision>18</precision>
        <required>true</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>VisitSubject__c</fullName>
        <description>Initial Subject of generated Visits</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Visit Subject</label>
        <required>true</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Visit</fullName>
                    <default>true</default>
                    <label>Visit</label>
                </value>
                <value>
                    <fullName>Call</fullName>
                    <default>false</default>
                    <label>Call</label>
                </value>
                <value>
                    <fullName>Email</fullName>
                    <default>false</default>
                    <label>Email</label>
                </value>
                <value>
                    <fullName>Meeting</fullName>
                    <default>false</default>
                    <label>Meeting</label>
                </value>
                <value>
                    <fullName>Send Letter/Quote</fullName>
                    <default>false</default>
                    <label>Send Letter/Quote</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Visit Plan Settings</label>
    <pluralLabel>Visit Plan Settings</pluralLabel>
    <visibility>Public</visibility>
</CustomObject>
