public class DRPrimaryContactQueueable implements Queueable {
    
    private List<Contact> contacts = new List<Contact>();
    
    public DRPrimaryContactQueueable( List<Contact> contacts ){
        this.contacts = contacts;
    }
    
    public void execute(QueueableContext context) {

        if (! Test.isRunningTest() ){
       
            insert contacts;            
            
        }
   
    }     

}