@isTest
public class ApexSharingUtilsTest {
    
    @isTest
    static void testCaseSelectorMethods(){
        
        String recordTypeDeveloperName;
        
        Account account = DRFixtureFactory.newDRAccount();
        List<Id> caseIds = new List<Id>();
        
        //Create a case
 
        recordTypeDeveloperName = CaseForceEventTriggerHandler.DR_SALES_PROCESS_RECORD_TYPE;
        ONTAP__Case_Force__c caseSales = DRFixtureFactory.newDRCase( account.Id, recordTypeDeveloperName , 'No visita vendedor' );

        //Create a new user
        User bdr2 = DRFixtureFactory.newBDRUser( 'bozo@test.com','bozo' );
        Map<String,String> caseIdToBDRUserIdMap = new Map<String,String>();
        caseIdToBDRUserIdMap.put( caseSales.Id, bdr2.Id );
        
        Test.startTest();
        
        ApexSharingUtils sharing = new ApexSharingUtils( 'ONTAP__Case_Force__Share', 'ParentId', caseIdToBDRUserIdMap);
        sharing.share();
        
        List<ONTAP__Case_Force__Share> caseShares = [SELECT Id FROM ONTAP__Case_Force__Share WHERE UserOrGroupId = : bdr2.Id ];
        
        System.assertEquals( 1, caseShares.size(), 'Assertion error for ApexSharingUtils');
        
        Test.stopTest();
        
    }

}