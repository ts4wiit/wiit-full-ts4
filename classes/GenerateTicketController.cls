public with sharing class GenerateTicketController {

    public static final String SEPARATOR {get; private set;}

    private Id orderId;
    public ONTAP__Order__c order {get; private set;}
    public Map<String, List<ONTAP__Order_Item__c>> salesOrderItemMap {get; private set;}
    public Map<String, List<ONTAP__Order_Item__c>> emptiesOrderItemMap {get; private set;}
    public Map<String, List<ONTAP__Order_Item__c>> emptiesReturnOrderItemMap {get; private set;}
    public Account acc {get; private set;}
    public Event visit {get; private set;}

    public Decimal sumAmountBaseAndTax {get; private set;}
    public Decimal sumDisc2 {get; private set;}
    public Decimal sumAmountLine {get; private set;}

    static {
        SEPARATOR = '-------------------------------------';
    }

    public Boolean hidePrices {get; set;}

    public GenerateTicketController() {
        fetchParameters();
        fetchRecords();
        //hidePrices = getHidePrices(acc);
    }

    private void fetchParameters() {
        Map<String, String> params = ApexPages.currentPage().getParameters();

        if (params.containsKey('Id')) {
            orderId = params.get('Id');
        }
    }

    private void fetchRecords() {
        // Order
        try {
            order = [
                SELECT
                    Id, CreatedDate, ONTAP__OrderAccount__c, ONTAP__Amount_Sub_Total_Product__c, ONTAP__Amount_Taxes__c,
                    ONTAP__Amount_Automatic_Deals__c, ONTAP__Amount_Manual_Deals__c, ONTAP__Amount_Total__c,
                    ONTAP__Route_Id__c, ONTAP__Route_Description__c, ONTAP__Sequence_visit__c, ONTAP__Event_Id__c,
                    ONTAP__App_Version__c
//                    ONTAP__Amount_Base_Total_Full__c, ONTAP__Amount_Tax_Total_Full__c, ONTAP__Amount_Line_Total__c,
//                    ONTAP__Disc_Total_2__c
                FROM
                    ONTAP__Order__c
                WHERE
                    Id = :orderId
            ];

        } catch (Exception e) {
            System.debug('!!! ONTAP__Order__c query ERROR: ' + e.getMessage());
            return;
        }

        salesOrderItemMap = new Map<String, List<ONTAP__Order_Item__c>>();
        emptiesOrderItemMap = new Map<String, List<ONTAP__Order_Item__c>>();
        emptiesReturnOrderItemMap = new Map<String, List<ONTAP__Order_Item__c>>();
        Map<String, List<ONTAP__Order_Item__c>> tmpMap;
        List<ONTAP__Order_Item__c> tmpList;
        String productName;
        String lineType;
        sumAmountBaseAndTax = 0;
        sumDisc2 = 0;
        sumAmountLine = 0;
        for (ONTAP__Order_Item__c orderItem: [
            SELECT
                Id, ONTAP__ItemProduct__r.ONTAP__ProductShortName__c, ONTAP__ActualQuantity__c, ONTAP__Base_Price__c,
                ONTAP__Disc_Total__c, ONTAP__Disc_Total_2__c, ONTAP__Disc_Taxes__c, ONTAP__Amount_Base_Total_Full__c,
                ONTAP__Amount_Tax_Total__c, ONTAP__Order_Line_Type__c, ONTAP__UnitPriceFull__c,
                ONTAP__Amount_Line_Total__c, ONTAP__Base_Total__c, ONTAP__Tax_Total_Full__c, ONTAP__UnitPrice__c
            FROM
                ONTAP__Order_Item__c
            WHERE
                ONTAP__CustomerOrder__c = :order.Id
//                AND ONTAP__Order_Line_Type__c IN :ORDER_LINE_TYPES
        ]) {
            productName = orderItem.ONTAP__ItemProduct__r.ONTAP__ProductShortName__c;
            lineType = orderItem.ONTAP__Order_Line_Type__c;

            if (lineType == 'Sales Order') {
                tmpMap = salesOrderItemMap;

                sumAmountBaseAndTax += (orderItem.ONTAP__Amount_Base_Total_Full__c != null ? orderItem.ONTAP__Amount_Base_Total_Full__c.setScale(2,System.RoundingMode.HALF_UP) : 0.00)
                                        + (orderItem.ONTAP__Amount_Tax_Total__c != null ? orderItem.ONTAP__Amount_Tax_Total__c.setScale(2,System.RoundingMode.HALF_UP) : 0.00);
                sumDisc2 += orderItem.ONTAP__Disc_Total_2__c != null ? orderItem.ONTAP__Disc_Total_2__c.setScale(2,System.RoundingMode.HALF_UP) : 0.00;
                sumAmountLine += orderItem.ONTAP__Amount_Line_Total__c != null ? orderItem.ONTAP__Amount_Line_Total__c.setScale(2,System.RoundingMode.HALF_UP) : 0.00;

            } else if (lineType == 'Bonus Empties') {
                tmpMap = emptiesOrderItemMap;

            } else if (lineType == 'Empties Return Order') {
                tmpMap = emptiesReturnOrderItemMap;
            }

            if (tmpMap != null) {
                tmpList = tmpMap.containsKey(productName)
                    ? tmpMap.get(productName)
                    : new List<ONTAP__Order_Item__c>();
                tmpList.add(orderItem);

                tmpMap.put(productName, tmpList);
            }
        }

        // Account
        try {
            acc = [
                SELECT
                    Id, Name, ISSM_SalesOrg__r.Name, ONTAP__SAP_Number__c, ONTAP__Sales_Office__c, ONCALL__OnCall_Route_Code__c,
                    ONTAP__Street__c, ONTAP__District__c, ONTAP__CityName__c, Region__c, ONTAP__Email__c //, ISSM_Invoicingdocument__c
                FROM
                    Account
                WHERE
                    Id = :order.ONTAP__OrderAccount__c
            ];

        } catch (Exception e) {
            System.debug('!!! Account query ERROR: ' + e.getMessage());
        }

        // Event (Visit)
        try {
            visit = [
                SELECT
                    Id, VisitList__r.Name, VisitList__r.RouteDescription__c
                FROM
                    Event
                WHERE
                    Id = :order.ONTAP__Event_Id__c
            ];

        } catch (Exception e) {
            System.debug('!!! Event query ERROR: ' + e.getMessage());
        }
    }

    /** 
     ** @description    Validate if the prices of the order must be hiden because of the Invoicing Document of the Customer
     ** @param acc      The Account record to validate in the custom field 'ISSM_Invoicingdocument__c'
     ** @return         Returns true if the prices must be hidden or false if the prices must be displayed
     ** 
     ** @date           February 21, 2019
     ** @author         Carlos Pintor (Avanxo)
     **/
    /*
     * 
    private Boolean getHidePrices(Account acc){
        List<String> invoiceDocList = Label.ISSM_FilterInvoiceDocPrice.trim().split(';');
        return invoiceDocList.contains(acc.ISSM_Invoicingdocument__c);
    }
	*
	*/
    /*
    @Future(Callout=true)
    public static void sendEmails(List<Id> orderIdsToSendEmails) {
        Id templateId = [ 
            SELECT Id
            FROM EmailTemplate
            WHERE DeveloperName = 'Generate_Ticket'].Id;

        List<ONTAP__Order__c> orders = [
            SELECT Id, Name, ONTAP__OrderAccount__r.ONTAP__Email__c, CreatedDate
            FROM ONTAP__Order__c
            WHERE Id IN :orderIdsToSendEmails ];

        List<Attachment> attachments = [SELECT Id, Name, ParentId, Body FROM Attachment WHERE ParentId IN : orders AND Name LIKE 'O-%.pdf'];
        Map<Id, Attachment> attachmentMap = new Map<Id, Attachment>();
        for (Attachment a : attachments) {
            attachmentMap.put(a.ParentId, a);
        }

        // create missing attachments: W-011120
        Set<Id> orderIdsNoAttachment = new Set<Id>();
        for (Id orderId : orderIdsToSendEmails) {
            if (!attachmentMap.containsKey(orderId)) {
                orderIdsNoAttachment.add(orderId);
            }
        }
        createTicketsForOrdersImmediate(orderIdsNoAttachment);
        attachments = [SELECT Id, Name, ParentId, Body FROM Attachment WHERE ParentId IN : orderIdsNoAttachment AND Name LIKE 'O-%.pdf'];
        for (Attachment a : attachments) {
            attachmentMap.put(a.ParentId, a);
        }
        // \W-011120


        List<Messaging.SingleEmailMessage> messagesToSend = new List<Messaging.SingleEmailMessage>();

        Blob pdfBlob;

        Messaging.SingleEmailMessage message;
        for (ONTAP__Order__c order: orders) {
            if (attachmentMap.containsKey(order.Id)) {
                // get PDF blob
                pdfBlob = attachmentMap.get(order.Id).Body;

                // prepare Email
                message = new Messaging.SingleEmailMessage();
                message.setTargetObjectId(UserInfo.getUserId());
                message.setCcAddresses(new List<String> {order.ONTAP__OrderAccount__r.ONTAP__Email__c});
                message.setTemplateId(templateId);
                message.setWhatId(order.Id);
                message.setSaveAsActivity(false);
                message.setInReplyTo('noreply@gmodelo.com.mx');

                messagesToSend.add(message);

                Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
                attachment.setFileName(generateAttachmentFilename(order));
                attachment.setBody(pdfBlob);

                message.setFileAttachments(new Messaging.EmailFileAttachment[] {attachment});
            }
        }
        Messaging.sendEmail(messagesToSend);

    }
*/         
    @Future(Callout=true)
    public static void createTicketsForOrders(Set<Id> orderIds) {
        createTicketsForOrdersImmediate(orderIds);
    }

    public static void createTicketsForOrdersImmediate(Set<Id> orderIds) {
        Id templateId = [ 
            SELECT Id
            FROM EmailTemplate
            WHERE DeveloperName = 'TicketPanama'].Id;
        List<ONTAP__Order__c> orders = [
            SELECT Id, Name, ONTAP__OrderAccount__r.ONTAP__Email__c, ONTAP__Send_ticket_via_email__c, CreatedDate 
            FROM ONTAP__Order__c 
            WHERE Id IN : orderIds];
        Map<Id, ONTAP__Order__c> ordersMap = new Map<Id, ONTAP__Order__c>(orders);
        Map<Id, Attachment> attachmentsToUpsert = new Map<Id, Attachment>();
        Set<Id> setOrdersSendTicketByEmail = new Set<Id>();

        //check for existing attachments
        List<Attachment> duplicatedAttachments = [SELECT Id, Name, ParentId FROM Attachment WHERE ParentId IN : orders AND Name LIKE 'O-%.pdf'];

        for(Attachment a : duplicatedAttachments) {
            if (a.Name == generateAttachmentFilename(ordersMap.get(a.ParentId))) {
                attachmentsToUpsert.put(a.ParentId, a);
            }
        }

        PageReference pdf;
        Blob pdfBlob;
        String filename;

        for (ONTAP__Order__c order: orders) {
            // get PDF blob
            //June 11th, 2019: The Visualforce page "GenerateTicket" is not longer active to create the pdf file 
            //pdf = Page.GenerateTicket; 
            //June 11th, 2019: The Visualforce page "ISSM_OrderTicket_pag" is the new page to create the pdf file 
            //June 11th, 2019: This new requirement was requested by the PaP team in ABI México to get only one template of the order ticket for all sales applications
            //Oct 07th, 2019: For Panama, ISSM_OrderTicket_pag changed to OrderTicketPDF_pag
            pdf = Page.OrderTicketPDF_pag; 
            pdf.getParameters().put('Id', order.Id);
            pdf.setRedirect(true);
            try{
              pdfBlob = !Test.isRunningTest() ? pdf.getContent() : Blob.valueOf('UnitTest');
            }catch(Exception e){
                System.debug(LoggingLevel.INFO,e.getCause());
                System.debug(LoggingLevel.INFO,e.getMessage());
                System.debug(LoggingLevel.INFO,e.getStackTraceString());
            }
            if (attachmentsToUpsert.containsKey(order.Id)) {
                attachmentsToUpsert.get(order.Id).Body = pdfBlob;
            } else {
                filename = generateAttachmentFilename(order);

                // prepare Order Attachment
                attachmentsToUpsert.put(order.Id,
                        new Attachment(
                                ParentId = order.Id,
                                Name = filename,
                                Body = pdfBlob,
                                ContentType = 'application/pdf'
                        )
                );
            }
            if(order.ONTAP__OrderAccount__r.ONTAP__Email__c != NULL /*&& order.ONTAP__Send_ticket_via_email__c*/ ){
            	setOrdersSendTicketByEmail.add(order.Id);    
            }
        }
        upsert attachmentsToUpsert.values();
        
        List<Messaging.SingleEmailMessage> messagesToSend = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage message;
        User user = [
            SELECT email, firstName, lastName 
            FROM User 
            WHERE id = :UserInfo.getUserId()];
        //Cheap workaround for not using a VF template when sending email wiht a ContactId and whatId
        //https://salesforce.stackexchange.com/questions/1816/whatid-when-using-custom-object
        //Create a contact, delete it after
        Contact tempContact = new Contact(email = user.email, firstName = user.firstName, lastName = user.lastName);
        insert tempContact;

        for(Id orderId : setOrdersSendTicketByEmail){
            //pdfBlob = attachmentsToUpsert.get(orderId).Body;
            // prepare Email
            message = new Messaging.SingleEmailMessage();
            
            
            message.setTargetObjectId( tempContact.id );
            message.setCcAddresses(new List<String> {ordersMap.get(orderId).ONTAP__OrderAccount__r.ONTAP__Email__c});
            message.setTemplateId(templateId);
            message.setWhatId(orderId);
            message.setSaveAsActivity(false);
            message.setInReplyTo('noreply@gmodelo.com.mx');
            messagesToSend.add(message);
            
            Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
            attachment.setFileName( attachmentsToUpsert.get(orderId).Name );
            attachment.setBody( attachmentsToUpsert.get(orderId).Body );
            
            message.setFileAttachments(new Messaging.EmailFileAttachment[] {attachment});
        }
        Messaging.sendEmail(messagesToSend);
        delete tempContact;

    }

    private static String generateAttachmentFilename(ONTAP__Order__c order) {
        String month = String.valueOf(order.CreatedDate.month());
        if (month.length() == 1) {
            month = '0' + month;
        }
        String day = String.valueOf(order.CreatedDate.day());
        if (day.length() == 1) {
            day = '0' + day;
        }
        return order.Name + '_' + order.CreatedDate.year() + month + day + '.pdf';
    }
}