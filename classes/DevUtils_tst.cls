/* ----------------------------------------------------------------------------
* AB InBev :: 
* ----------------------------------------------------------------------------
* Clase: DevUtils_tst.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 18/01/2019        Heron Zurita           Creation of methods.
*/
@isTest public class DevUtils_tst {
    
    /**
    * Test method for team members and users
    * Created By: heron.zurita@accenture.com
    * @param void
    * @return void
    */
    @isTest static void testData(){
        Account acc = New Account(Name = 'test');
        insert acc;
        User u = [SELECT Id  from User Limit 1 ];
        
        DevUtils_cls.getFieldsMap('Account');
        DevUtils_cls.getVisitPlanSettings();
        DevUtils_cls.getAccountRouteSettings();
        DevUtils_cls.getTourStatusSettings();
        DevUtils_cls.getObjectValidationSettings('Account');
        
        Account[] accList = new Account[]{};
        Set<String> setList = new Set<String>();
        DevUtils_cls.filterSObjectList(accList,'Name',setList);
        
        Map<Id, Account> mapAcc = new Map<Id, Account>();
        String[] listStrings = new String[]{};
        DevUtils_cls.diffSObjectList(accList, mapAcc, listStrings);
        
        DevUtils_cls.getRecordTypes('Account', 'Name');
        
        Set<String> setMemberRoles = new Set<String>();
        DevUtils_cls.addUserToAccountTeam(acc.Id, u.Id, setMemberRoles);
        
        DEvUtils_cls.generateAccountTeamMember(acc.Id,u.id,'role');
        DevUtils_cls.getUserFromUsername('');
    }
}