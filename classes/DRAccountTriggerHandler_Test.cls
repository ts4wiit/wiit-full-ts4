@isTest
public class DRAccountTriggerHandler_Test {
    
    @isTest
    static void testAccounts(){    
        
       // Create users
       Id profileId = [ SELECT Id FROM PROFILE WHERE Name = 'DO BDR' ].get(0).Id;

	   User bdrUser1 = new User(FirstName='BDR1', LastName='Agent 1', UserName = 'bdr1@test.com', Email='bdr1@test.com',
				Alias='alias1', LocaleSidKey='es', LanguageLocaleKey='es', EmailEncodingKey='ISO-8859-1',ONTAP__Country_Alias__c = 'DO',
				ProfileId = profileId, TimeZoneSidKey='America/Puerto_Rico', Country='DO', isActive = true,User_Route__c='DO0001');

	   insert bdrUser1;        
       
	   User bdrUser2= new User(FirstName='BDR2', LastName='Agent 2', UserName = 'bdr2@test.com', Email='bdr2@test.com',
				Alias='alias2', LocaleSidKey='es', LanguageLocaleKey='es', EmailEncodingKey='ISO-8859-1',ONTAP__Country_Alias__c = 'DO',
				ProfileId = profileId, TimeZoneSidKey='America/Puerto_Rico', Country='DO', isActive = true,User_Route__c='DO0002');

	   insert bdrUser2; 
        
	   User bdrUser3= new User(FirstName='BDR3', LastName='Agent 3', UserName = 'bdr3@test.com', Email='bdr2@test.com',
				Alias='alias3', LocaleSidKey='es', LanguageLocaleKey='es', EmailEncodingKey='ISO-8859-1',ONTAP__Country_Alias__c = 'DO',
				ProfileId = profileId, TimeZoneSidKey='America/Puerto_Rico', Country='DO', isActive = true,User_Route__c='DO0003');

	   insert bdrUser3;         
    
        
       //Create accounts assigned to each bdr 
        
       Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account_DO').getRecordTypeId();
        
       List<Account> accountsBdr1 = new List<Account>();
        
       for( Integer i = 1 ; i <= 100 ; i++ ) {
            
            Account account = new Account( Name = 'Account BDR 1 ' + i, ONTAP__SAPCustomerId__c = 'CID1' + i, RecordTypeId = accountRecordTypeId, BDR_Route__c = bdrUser1.User_Route__c, 
                                           ONTAP__Contact_First_Name__c = 'Test', ONTAP__Contact_Last_Name__c = 'Test' );
            accountsBdr1.add( account );
            
       }     
        
       Test.startTest();
        
       insert accountsBdr1;
        
       List<Account> accountsBdr2 = new List<Account>();
        
       for( Integer i = 1 ; i <= 100 ; i++ ) {
            
            Account account = new Account( Name = 'Account BDR 2 ' + i, ONTAP__SAPCustomerId__c = 'CID2' + i, RecordTypeId = accountRecordTypeId, BDR_Route__c = bdrUser2.User_Route__c,
                                           ONTAP__Contact_First_Name__c = 'Test', ONTAP__Contact_Last_Name__c = 'Test');
            accountsBdr2.add( account );
            
       }     
        
       insert accountsBdr2;        

       List<Account> accountsBdr3 = new List<Account>();
        
       for( Integer i = 1 ; i <= 100 ; i++ ) {
            
            Account account = new Account( Name = 'Account BDR 3 ' + i, ONTAP__SAPCustomerId__c = 'CID3' + i, RecordTypeId = accountRecordTypeId, BDR_Route__c = bdrUser3.User_Route__c,
                                           ONTAP__Contact_First_Name__c = 'Test', ONTAP__Contact_Last_Name__c = 'Test');
            accountsBdr3.add( account );
            
       }     
        
       insert accountsBdr3;  
        
       // assertion
       
       List<Account> assignedAccounts = [ SELECT Id from Account WHERE OwnerId in ( : bdrUser1.Id, : bdrUser2.Id, : bdrUser3.Id) ];
        
       System.assertEquals( 300,assignedAccounts.size() , 'Assertion for accounts failed :(');
        
        
       Test.stopTest();
    
    }

}