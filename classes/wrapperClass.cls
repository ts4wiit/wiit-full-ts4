/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* -----------------------------------------------------------------------------
* Clase: HONES_ONCALL_MinPay_CONTRO.apxc
* Version: 1.0.0.0
*  
* Change History
* -----------------------------------------------------------------------------
* Date                 User                   Description
* 01/12/2018     Luis Parra        Creation of methods.
*
* @author: l.parra@accenture.com  -Luis arturo Parra Rosas
* @author: heron.zurita@accenture.com -Heron Zurita Vazquez
-------------------------------------------------------------------------------*/

/**
    * Structure of the wrapper class which contain this values with their setters and getters methods 
    * Created By: g.martinez.cabral@accenture.com
    * @param New values of Product modified
    * @return void
    */
public class wrapperClass {
@AuraEnabled public String description{set;get;}

@AuraEnabled public Boolean optional{set;get;} 
@AuraEnabled public String pay{set;get;}    
@AuraEnabled public Boolean selectedByUser{set;get;}    
@AuraEnabled public String pfntag{set;get;}
@AuraEnabled public String pbetag{set;get;}        
@AuraEnabled public String pronr{set;get;}    
@AuraEnabled public Double price{set;get;}    
@AuraEnabled public String addempties{set;get;}
@AuraEnabled public String measureCode{set;get;}    
@AuraEnabled public Double unitPrice{set;get;}
@AuraEnabled public String sku{set;get;} 
@AuraEnabled public Boolean freeGood{set;get;}
@AuraEnabled public String condcode{set;get;}
@AuraEnabled public String amount{set;get;}
@AuraEnabled public String posex{set;get;}
@AuraEnabled public String material{set;get;}      
@AuraEnabled public String cantidad{set;get;}         
@AuraEnabled public String tipo{set;get;}    
@AuraEnabled public Double subtotal{set;get;}         
@AuraEnabled public Double total{set;get;}     
@AuraEnabled public Double discount{set;get;}     
@AuraEnabled public Double tax{set;get;}      
@AuraEnabled public List<String> lista{set;get;}  
@AuraEnabled public List<String> listaPFN{set;get;} 
@AuraEnabled public List<String> listaPFNUnits{set;get;}    
@AuraEnabled public List<String> listaPBE{set;get;}    
@AuraEnabled public List<json_deals> listadeals{set;get;}
@AuraEnabled public String pfnpbe{set;get;} 
@AuraEnabled public String productCode{set;get;} 
@AuraEnabled public String materialProduct{set;get;}         
@AuraEnabled public String productType{set;get;}     
@AuraEnabled public String productRecord{set;get;} 
@AuraEnabled public String productMetadataType{set;get;}      
@AuraEnabled public Decimal quantity{set;get;}  
@AuraEnabled public String promosku{set;get;}
@AuraEnabled public String promoNamedescription{set;get;}       
@AuraEnabled public String promoDescTosend{set;get;}    
@AuraEnabled public String pbereason{set;get;}    
@AuraEnabled public String position{set;get;}
// Order Totals                                   
@AuraEnabled public Double orderTotal{set;get;}
@AuraEnabled public Decimal orderSubTotal{set;get;}
@AuraEnabled public Decimal orderTaxes{set;get;}
@AuraEnabled public Decimal orderDiscounts{set;get;}
//Reason code
@AuraEnabled public String reasonCode{set;get;}    
@AuraEnabled public String reasonDescription{set;get;}



/*
*rjimenez@ts4.mx
*Related products
*/
@AuraEnabled public List<wrapperClass> relatedProducts {set;get;}  
@AuraEnabled public Boolean isRelated {set;get;} 
@AuraEnabled public Boolean isReturnable{set;get;}
@AuraEnabled public Decimal weight{set;get;}
@AuraEnabled public String productTypeOrigin {get;set;}  
@AuraEnabled public String skuParent{set;get;} 
@AuraEnabled public List<String> relatedSku{set;get;}
// LLave de eliminacion o incremento en la interfaz ERG
@AuraEnabled public String KeyDltINC{set;get;} 
// LLave saber si puede agregar el nuevo producto seleccionado ERG
@AuraEnabled public String Keyadd{set;get;} 
// LLave para envio de orden al motor en la interfaz ERG
@AuraEnabled public String KeySendMotor{set;get;} 
// Envio de orden del motor tax3, tax4, tax5 ERG
@AuraEnabled public Double tax3{set;get;} 
@AuraEnabled public Double tax4{set;get;} 
@AuraEnabled public Double tax5{set;get;} 

 public wrapperClass() {
   pay = GlobalStrings.CREDIT;  
   isRelated = false;
   isReturnable = false;
   relatedProducts = new List<wrapperClass>();
   weight = 0;
 }   

    
}