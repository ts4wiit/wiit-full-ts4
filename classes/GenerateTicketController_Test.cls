/**
 * Created by Piotr Szagdaj on 25.04.2018.
 */

@IsTest
private class GenerateTicketController_Test {
    @IsTest
    static void shouldPrepareDataOnInit() {
        //GIVEN
        Account acc = buildAccount();
        ONTAP__Product__c product = buildProduct();
        ONTAP__Order__c ordr = buildOrder(acc.Id);
        List<ONTAP__Order_Item__c> orderItems = buildOrderItems(ordr.Id, product.Id);

        Test.setCurrentPageReference(new PageReference('Page.GenerateTicket'));
        System.currentPageReference().getParameters().put('Id', ordr.Id);

        //WHEN
        GenerateTicketController gtc = new GenerateTicketController();

        //THEN
        System.assertEquals(ordr.Id, gtc.order.Id);
        System.assertEquals(1, gtc.salesOrderItemMap.size());
        //System.assertEquals(1, gtc.emptiesOrderItemMap.size());
        //System.assertEquals(1, gtc.emptiesReturnOrderItemMap.size());
        System.assertEquals(acc.Id, gtc.acc.Id);
        System.assertEquals(null, gtc.visit);
        System.assertEquals(120.0, gtc.sumAmountBaseAndTax);
        System.assertEquals(-20.0, gtc.sumDisc2);
        System.assertEquals(500.0, gtc.sumAmountLine);
    }

    /*
    @IsTest
    static void shouldSendEmails() {
        //GIVEN
        Account acc = buildAccount();
        ONTAP__Product__c product = buildProduct();
        ONTAP__Order__c ordr = buildOrder(acc.Id);
        Attachment att = buildAttachment(ordr.Id);

        //WHEN
        Test.startTest();
        //GenerateTicketController.sendEmails(new List<Id> {ordr.Id});
        Test.stopTest();

        //THEN
        //No assert since emails are being sent from future call
    }
	*/
    
    @IsTest
    static void shouldCreateTicketsForOrder() {
        //GIVEN
        Account acc = buildAccount();
        ONTAP__Product__c product = buildProduct();
        ONTAP__Order__c ordr = buildOrder(acc.Id);

        //WHEN
        Test.startTest();
        GenerateTicketController.createTicketsForOrders(new Set<Id> {ordr.Id});
        Test.stopTest();

        //THEN
        List<Attachment> attachments = [SELECT Id FROM Attachment WHERE ParentId = :ordr.Id];
        System.assertEquals(1, attachments.size());
    }

    /*
    * Test data Builder methods
     */

    private static Account buildAccount() {
        Account acc = new Account();

        acc.Name = 'TestAccount';
        acc.ONTAP__Email__c = 'email@unittest.com';

        insert acc;

        return acc;
    }

    private static ONTAP__Product__c buildProduct() {
        ONTAP__Product__c product = new ONTAP__Product__c();

        product.ONTAP__ProductShortName__c = 'TestProductName';

        insert product;

        return product;
    }

    private static ONTAP__Order__c buildOrder(Id accountId) {
        ONTAP__Order__c ordr = new ONTAP__Order__c();

        ordr.ONTAP__OrderAccount__c = accountId;
        ordr.ONTAP__Amount_Sub_Total_Product__c = 100.0;
        ordr.ONTAP__Amount_Taxes__c = 20.0;
        ordr.ONTAP__Amount_Automatic_Deals__c = -50.0;
        ordr.ONTAP__Amount_Manual_Deals__c = 0.0;
        ordr.ONTAP__Amount_Total__c = 500.0;
        ordr.ONTAP__Route_Id__c = 'CODE12';
        ordr.ONTAP__Route_Description__c = 'RouteDescription';
        ordr.ONTAP__Sequence_visit__c = '01';
        ordr.ONTAP__Event_Id__c = null;
        ordr.ONTAP__App_Version__c = '1.0.00';

        insert ordr;

        return ordr;
    }

    private static List<ONTAP__Order_Item__c> buildOrderItems(Id orderId, Id productId) {
        List<ONTAP__Order_Item__c> orderItems = new List<ONTAP__Order_Item__c>();

        ONTAP__Order_Item__c item = new ONTAP__Order_Item__c();
        item.ONTAP__CustomerOrder__c = orderId;
        item.ONTAP__ItemProduct__c = productId;
        item.ONTAP__Order_Line_Type__c = 'Sales Order';
        item.ONTAP__ActualQuantity__c = 1.0;
        item.ONTAP__Base_Price__c = 100.0;
        item.ONTAP__Disc_Total__c = -10.0;
        item.ONTAP__Disc_Total_2__c = -20.0;
        item.ONTAP__Disc_Taxes__c = -4.0;
        item.ONTAP__Amount_Base_Total_Full__c = 100.0;
        item.ONTAP__Amount_Tax_Total__c = 20.0;
        item.ONTAP__UnitPricefull__c = 100.0;
        item.ONTAP__Amount_Line_Total__c = 500.0;
        orderItems.add(item);

        /*
        item = new ONTAP__Order_Item__c();
        item.ONTAP__CustomerOrder__c = orderId;
        item.ONTAP__Order_Line_Type__c = 'Bonus Empties';
        item.ONTAP__ActualQuantity__c = 1.0;
        orderItems.add(item);

        item = new ONTAP__Order_Item__c();
        item.ONTAP__CustomerOrder__c = orderId;
        item.ONTAP__Order_Line_Type__c = 'Empties Return Order';
        item.ONTAP__ActualQuantity__c = 1.0;
        orderItems.add(item);
		*/
        insert orderItems;

        return orderItems;
    }

    private static Attachment buildAttachment(Id orderId) {
        Attachment att = new Attachment();

        att.ParentId = orderId;
        att.Name = 'O-0001.pdf';
        att.Body = Blob.valueOf('UnitTest');
        att.ContentType = 'application/pdf';

        insert att;

        return att;
    }
}