/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: HONES_OPItemAmountController.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Gerardo Martinez        Creation of methods.
 */
public class HONES_OPItemAmountController {
    
    /**
    * Method to get all the data about an Open Item   
    * Created By: g.martinez.cabral@accenture.com
    * @param String recordId
    * @return List<String>
    */
    @AuraEnabled
    public static  List<String> getData (String recordId){
        List<String> listValues = new List<String>();
        List<ONTAP__OpenItem__c> dataConsult = [SELECT ISSM_Amount__c, ONTAP__Amount__c, ONTAP__DueDate__c FROM ONTAP__OpenItem__c WHERE ONTAP__Account__c =:recordId LIMIT 100];
        Double totalOpenItem = 0.0;
        Double expiredBalances = 0.0;
        Integer countOpenedItem = 0;
        
        System.debug('elemnts' + dataConsult.size());
        for(ONTAP__OpenItem__c openItemObject : dataConsult){
            System.debug('ars-> ' + openItemObject);
            
            if(openItemObject.ONTAP__Amount__c!=null){
                totalOpenItem += openItemObject.ONTAP__Amount__c;
            }
            if(openItemObject.ONTAP__DueDate__c<System.TODAY()){
                expiredBalances += openItemObject.ONTAP__Amount__c;
            }
            countOpenedItem++;
        }
        
        system.debug('totalOpenItem -> ' + totalOpenItem);
        system.debug('CounterOpenItem -> ' + expiredBalances);
        system.debug('expiredBalances -> ' + expiredBalances);
        
        listValues.add(''+totalOpenItem);
        listValues.add(''+countOpenedItem);
        listValues.add(''+expiredBalances);
        
        return listValues;
    }                                         
}