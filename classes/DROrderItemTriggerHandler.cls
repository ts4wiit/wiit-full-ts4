public with sharing class DROrderItemTriggerHandler extends DROrderTriggerHandler {

    @testvisible public static final String DR_ORDER_ITEM_RECORD_TYPE = 'DO_Order_Item';
    @testvisible public static final String CO_ORDER_ITEM_RECORD_TYPE = 'CO_Order_Item';
    @testvisible public static final String PE_ORDER_ITEM_RECORD_TYPE = 'PE_Order_Item';
    @testvisible public static final String EC_ORDER_ITEM_RECORD_TYPE = 'EC_Order_Item';
    private List<ONTAP__Order_Item__c> orderItems = new List<ONTAP__Order_Item__c>();
    private List<ONTAP__Order_Item__c> filteredOrderItems = new List<ONTAP__Order_Item__c>();
    private List<Id> orderIds = new List<Id>();
    private Map<Id,Id> orderToOrderItemMap = new Map<Id,Id>();
    private Map<Id,Id> orderIdToAccountIdMap = new Map<Id,Id>();
    private Id orderItemRecordTypeId;
    private Id orderItemRecordTypeId_CO;
    private Id orderItemRecordTypeId_PE;
    private Id orderItemRecordTypeId_EC;
    
    public DROrderItemTriggerHandler( List<ONTAP__Order_Item__c> orderItems ){
        
        this.orderItems = orderItems;
        
    } 
    
    public override void run(){

        this.setRecordType();
        this.filter();
        this.getOrders();
        super.getAccounts();
        this.setOwner();
        
    }

    public override void setRecordType(){

        try{

            orderItemRecordTypeId = Schema.SObjectType.ONTAP__Order_Item__c.getRecordTypeInfosByDeveloperName().get( DR_ORDER_ITEM_RECORD_TYPE ).getRecordTypeId();
            orderItemRecordTypeId_CO = Schema.SObjectType.ONTAP__Order_Item__c.getRecordTypeInfosByDeveloperName().get( CO_ORDER_ITEM_RECORD_TYPE ).getRecordTypeId();
            orderItemRecordTypeId_PE = Schema.SObjectType.ONTAP__Order_Item__c.getRecordTypeInfosByDeveloperName().get( PE_ORDER_ITEM_RECORD_TYPE ).getRecordTypeId();
            orderItemRecordTypeId_EC = Schema.SObjectType.ONTAP__Order_Item__c.getRecordTypeInfosByDeveloperName().get( EC_ORDER_ITEM_RECORD_TYPE ).getRecordTypeId();

        } catch( Exception e ){

            throw new OrderException('>>> Error while setting record type: ' + e.getMessage());

        }

    }

    private void getOrders(){
        
        for( ONTAP__Order_Item__c orderItem : filteredOrderItems ){
            
            orderIds.add( orderItem.ONTAP__CustomerOrder__c );
            orderToOrderItemMap.put( orderItem.Id, orderItem.ONTAP__CustomerOrder__c );
            
        }

        orders = [ SELECT Id, ONTAP__OrderAccount__c FROM ONTAP__Order__c WHERE Id in : orderIds ];
        
        for( ONTAP__Order__c order : orders ){
            
            accountIds.add( order.ONTAP__OrderAccount__c );
            orderIdToAccountIdMap.put( order.Id, order.ONTAP__OrderAccount__c );
            
        }
                
    }

    public override void filter(){

        for ( ONTAP__Order_Item__c orderItem : orderItems ){

            if( orderItem.RecordTypeId == orderItemRecordTypeId || orderItem.RecordTypeId == orderItemRecordTypeId_CO || orderItem.RecordTypeId == orderItemRecordTypeId_PE || orderItem.RecordTypeId == orderItemRecordTypeId_EC ){

                filteredOrderItems.add( orderItem );

            }
        }
    }

    public override void setOwner(){
        
        for ( ONTAP__Order_Item__c orderItem : filteredOrderItems ){

            try{

                Id accountId = orderIdToAccountIdMap.get( orderItem.ONTAP__CustomerOrder__c );
                Account account = accountIdToAccountMap.get( accountId );
                orderItem.OwnerId = account.OwnerId;

            } catch( Exception e ){

                throw new OrderException('>>> Related account not found: ' + e.getMessage());

            }

        }               
    }
}