/**
 * Created by Dejair.Junior on 10/17/2019.
 */
@isTest
public class DRAccountPublisher_Test {

    @isTest
    public static void testPublishing(){

        Test.startTest();

        Account account = DRFixtureFactory.newDRAccount();
        
        System.debug('>>> Account: ' + account);
        System.debug('>>> Shipping Address: ' + account.ShippingStreet);

        account.ONTAP__Secondary_Phone__c = '(552) 334-1233';
        update account;

        Test.getEventBus().deliver();

        Test.stopTest();

    }

}