@isTest
public class DRContactSelectorTest {
    
    @isTest
    static void testMethods(){

        Account account = DRFixtureFactory.newDRAccount();
        Contact contact = DRFixtureFactory.newDRContact( account.Id );
        List<Id> contactIds = new List<Id>();
        contactIds.add( contact.Id );
        
        Test.startTest();
        
        // Test getContactsByIds
        List<Contact> contacts = DRContactSelector.getContactsByIds( contactIds );
        
        System.assertEquals( 1 , contacts.size());
        
        // Test getContactIdToContactMap
        
        Map<Id,Contact> contactsMap = DRContactSelector.getContactIdToContactMap( contactIds );
        
        System.assertEquals( contact.FirstName, contactsMap.get( contact.Id ).FirstName );
        
        Test.stopTest();
        
    }

}