/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_DeleteAllAssetSchudeler_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 19/12/2018     Carlos Leal        Creation of methods.
 */
@isTest
private class V360_DeleteAllAssetSchudeler_Test{
    /**
   * Method for test theSchudeler for assets.
   * @author: c.leal.beltran@accenture.com
   * @param void
   * @return void
   */
  static testMethod void test_execute_UseCase1(){
    V360_DeleteAllAssetSchudeler obj01 = new V360_DeleteAllAssetSchudeler();
    SchedulableContext sc;
    obj01.execute(sc);
  }
}