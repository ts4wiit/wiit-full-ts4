public class SetProspectClosestPOCField {

    private static final String ACCOUNT_RECORD_TYPE_NAME  = 'Account_DO';
    
    public static map<id, account> 	  	 mapProspect = new map<id, account>();
    public static map<id, List<account>> mapAccounts = new map<id, List<account>>();

    public static void start(  List<account> listAcc ){  
        
        Boolean addOR = false;
        
        String queryString = ' SELECT id, Closest_POC_Code__c, ownerid, owner.name, Recordtype.DeveloperName, ' +
            				 ' ONTAP__Geolocation__c ' +
            				 ' FROM Account WHERE Recordtype.DeveloperName = \'' + ACCOUNT_RECORD_TYPE_NAME + '\' ';
        
        for(Account acc : listAcc){
            
            mapProspect.put(acc.Id, acc);
            
            Location loc = acc.ONTAP__Geolocation__c;
            
            Double lat = loc.latitude;
            Double log = loc.longitude;
            
            queryString += addOR ? ' OR ' : ' AND (';     
            
            addOR = true;
            
            queryString += ' ( DISTANCE(ONTAP__Geolocation__c, GEOLOCATION(' + lat + ',' + log + '), \'km\') < 10 ) ';
        }
        
        queryString += ' )';
            
        System.debug('queryString => ' + queryString);  

		List<Account> accList = new List<Account>();
        
        try{
            accList = Database.query( queryString );
            
            System.debug('accList => ' + [SELECT Id, ONTAP__Geolocation__c FROM Account WHERE Recordtype.DeveloperName = 'Account_DO']);
            
            execute( accList );     
            
        }catch(Exception ex){
            System.debug( 'Error => ' + ex.getMessage() );
        }             
    }
    
    private static void execute( List<Account> accList ){
        
        for(Account acc : accList){
            
            if(mapAccounts.containskey(acc.ownerid))
                mapAccounts.get(acc.ownerid).add(acc);   
                
            else
                mapAccounts.put(acc.ownerid, new list<account>{acc});                         			
        }
        finish();
    }
    
    private static void finish(){   
        
        for(account prospect : mapProspect.values()){
            
            Location locProspect = prospect.ONTAP__Geolocation__c;
            
            Double dist_temp = null;
            
            for( id ownerId : mapAccounts.keyset() ){
                
                for(account accClosest : mapAccounts.get(ownerId)){
                    
                    Location locClosest = accClosest.ONTAP__Geolocation__c;
                    
                    if( locClosest != null ){
                        Double dist = Location.getDistance(locProspect, locClosest, 'mi');
                        
                        if(dist_temp == null){
                            dist_temp = dist;
                        }
                        
                        if(dist <= dist_temp){
                            prospect.Closest_POC_Code__c = accClosest.id; 
                            dist_temp = dist;
                        }
                    }
                }
            }   
        }        
        update mapProspect.values();
    }
}