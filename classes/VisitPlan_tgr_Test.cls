/****************************************************************************************************
    General Information
    -------------------
	author: Aldair Leon Juarez
    email: aldair.leon@accenture.com
    company: Accenture
    Description: Test Trigger to control VisitPlan__c events

    Information about changes (versions)
    -------------------------------------
    Number    Dates             Author                       Description
    ------    --------          --------------------------   -----------
    1.0       22-01-2019        Aldair Leon                Test Class
****************************************************************************************************/

@isTest
public class VisitPlan_tgr_Test {
    @isTest static void ControlVisitPlanDelate(){
        ONTAP__Route__c route = new ONTAP__Route__c();  
        route.VisitControl_ByPass__c = true;
        insert route;
        VisitPlan__c visitP = new VisitPlan__c();
        visitP.Route__c=route.Id;
       
		insert visitP;
        update visitP;
        
        
        Test.startTest();
    	Database.DeleteResult result = Database.delete(visitP, false);
    	Test.stopTest();
    }
    

}