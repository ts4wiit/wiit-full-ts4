global  class DRDeleteFlexibleKPIs implements
                Database.Batchable<sObject>, Database.Stateful {


    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT Id FROM Flexible_KPI__c';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<Flexible_KPI__c> scope){

        List<Flexible_KPI__c> deleteList = new List<Flexible_KPI__c>();

        for (Flexible_KPI__c kpi : scope) {
            deleteList.add( kpi );
        }

        delete deleteList;

    }

    global void finish(Database.BatchableContext bc){
        System.debug('>>> Flexible KPIs successfull deleted!');
    }

}