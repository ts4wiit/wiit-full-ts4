/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: HONES_ONCALL_PartnerList_Test.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 18/12/2018           Luis Arturo Parra Rosas           Creation of methods.
*/

@isTest
private class HONES_ONCALL_PartnerList_Test{
     /**
    * Test method for set an Item List
    * Created By: luis.arturo.parra@accenture.com
    * @param void
    * @return void
    */
   @isTest static void HONES_ONCALL_PartnerList_UseCase1(){
      HONES_ONCALL_PartnerList tester=new HONES_ONCALL_PartnerList();
    	  tester.role='tester';
          tester.numb='tester';
          tester.itmNumber='tester';
   }
   
    @isTest static void HONES_ONCALL_PartnerList_Domicilio(){
      HONES_ONCALL_PartnerList.Domicilio DOM=new HONES_ONCALL_PartnerList.Domicilio();
      	DOM.NAME='tester';
        DOM.NAME_3='tester';
        DOM.STREET='tester';
        DOM.COUNTRY='tester';
        DOM.POSTL_CODE='tester';
        DOM.CITY='tester';
        DOM.DISTRICT='tester';
        DOM.REGION='tester';
        DOM.TELEPHONE='tester';
        DOM.TELEPHONE2='tester';
        DOM.TRANSPZONE='tester';
   }
}