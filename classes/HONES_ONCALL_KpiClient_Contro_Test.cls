/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: HONES_ONCALL_KpiClient_Contro_Test.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 18/12/2018           Luis Arturo Parra Rosas           Creation of methods.
*/

@isTest
private class HONES_ONCALL_KpiClient_Contro_Test{
       
    /**
    * Test method for get flexible data by id of user
    * Created By: luis.arturo.parra@accenture.com
    * @param void
    * @return void
    */
  	@isTest static void test_getFlexibleDataById_UseCase1(){
    	HONES_ONCALL_KpiClient_Contro obj01 = new HONES_ONCALL_KpiClient_Contro();
        Account acc =  new Account(Name='test',ISSM_CreditLimit__c=10,ONTAP__Credit_Amount__c=10);
        insert acc;
        ONCALL__Call__c cal =  new ONCALL__Call__c(ONCALL__POC__c=acc.Id); 
    	insert cal;
    	HONES_ONCALL_KpiClient_Contro.getFlexibleDataById(cal.Id);
 	}
    
  	/**
    * Test method for get infromation of users
    * Created By: luis.arturo.parra@accenture.com
    * @param void
    * @return void
    */
  	@isTest static void test_getInfoExample_UseCase1(){
        Account acc = new Account();
        acc.Name = 'Test9999';
        acc.V360_CallFrequency__c = 'LMRJVSD'; 
        acc.V360_VisitFrequency__c = 'LMRJV';
        acc.ONTAP__Delivery_Delay_Code__c = '24 hrs.';
        insert acc;
        List<Account> ct = [Select Id FROM Account WHERE Name = 'Test9999'];
        HONES_ONCALL_KpiClient_Contro obj01 = new HONES_ONCALL_KpiClient_Contro();
        HONES_ONCALL_KpiClient_Contro.getInfoExample(ct[0].Id);
  	}
    
    /**
    * Test method for get kpis
    * Created By: luis.arturo.parra@accenture.com
    * @param void
    * @return void
    */
  	@isTest static void test_calc_PC_UseCase1(){
        List<SObject>Obj=new List<SObject>();
        HONES_ONCALL_KpiClient_Contro obj01 = new HONES_ONCALL_KpiClient_Contro();
        HONES_ONCALL_KpiClient_Contro.calc_PC(10,Obj);
  	}
    
    /**
    * Test method for get the remain days
    * Created By: luis.arturo.parra@accenture.com
    * @param void
    * @return void
    */
  	@isTest static void test_calcRemainDays_UseCase1(){
        Account acc = new Account();
        acc.Name = 'Test9999';
        acc.V360_CallFrequency__c = 'LMRJVSD'; 
        acc.V360_VisitFrequency__c = 'LMRJV';
        acc.ONTAP__Delivery_Delay_Code__c = '24 hrs.';
        insert acc;
        List<Account>ct=[Select Id FROM Account WHERE Name = 'Test9999']; 
        HONES_ONCALL_KpiClient_Contro obj01 = new HONES_ONCALL_KpiClient_Contro();
        HONES_ONCALL_KpiClient_Contro.calcRemainDays(ct[0].Id);  
    }
	/**
    * Test method for get the remain days
    * Created By: luis.arturo.parra@accenture.com
    * @param void
    * @return void
    */
  	@isTest static void test_calcRemainDays_UseCase2(){
        Account acc = new Account();
        acc.Name = 'Test9999';
        acc.V360_CallFrequency__c = 'LMRJVSD'; 
        acc.ONTAP__Delivery_Delay_Code__c = '24 hrs.';
        insert acc;
        List<Account>ct=[Select Id FROM Account WHERE Name = 'Test9999']; 
        HONES_ONCALL_KpiClient_Contro obj01 = new HONES_ONCALL_KpiClient_Contro();
        HONES_ONCALL_KpiClient_Contro.calcRemainDays(ct[0].Id);  
    }
    
}