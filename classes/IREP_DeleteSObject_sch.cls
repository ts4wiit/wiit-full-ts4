global class IREP_DeleteSObject_sch implements Schedulable {

	Integer intStep;

	public IREP_DeleteSObject_sch(){
		this.intStep = 1;
	}

	public IREP_DeleteSObject_sch(Integer intStepParam){
		this.intStep = intStepParam;
	}

	global void execute(SchedulableContext sc) {

		list<IREP_DeleteObjects__mdt> lstConfig = [
			Select 	Id, IREP_Days__c, IREP_Object__c, IREP_RecordTypes__c, IREP_Step__c
			From	IREP_DeleteObjects__mdt
			Where	IREP_Step__c = :this.intStep
		];

		if(!lstConfig.isEmpty()){
			IREP_DeleteSObject_bch b = new IREP_DeleteSObject_bch(lstConfig[0].IREP_Object__c, lstConfig[0].IREP_Days__c, lstConfig[0].IREP_RecordTypes__c, this.intStep);
			database.executebatch(b, 2000);
		}
	}
}