/** 
* @author José Ismael on 17/05/19
* @company everis
* @description Test class to cover Local_CaseForce_SendNotification
*/
@isTest
public class Local_CaseForce_SendNotificationTest {
    @testSetup static void setup(){
        ONTAP__Triggers_Settings__c triggersSettings = new ONTAP__Triggers_Settings__c();
        triggersSettings.Name = 'AFR_CaseForce';
        triggersSettings.ONTAP__Active__c = true;
        insert triggersSettings;
        
        ONTAP__OnTapSettings__c ontapSettings = new ONTAP__OnTapSettings__c();
        ontapSettings.ONTAP__Connected_APP_for_ANDROID__c = 'TesteAndroid';
        ontapSettings.ONTAP__Connected_APP_for_IOS__c = 'TesteIOS';
        insert ontapSettings;
        
        list<ONTAP__Push_Notifications__c>  pushNotifications = new list<ONTAP__Push_Notifications__c>();
        pushNotifications.add( new ONTAP__Push_Notifications__c(Name = 'AFR_CaseForce', 
                                                                ONTAP__Android_Enabled__c = true,
        														ONTAP__iOS_Enabled__c = true));
        pushNotifications.add( new ONTAP__Push_Notifications__c(Name = 'ChatterPushNotification', 
                                                                ONTAP__Android_Enabled__c = true,
                                                                ONTAP__iOS_Enabled__c = true));
        insert pushNotifications;
        
         
        
    }
    @isTest
    static void caseForceCreationToUpdate(){
        /*Creating Case Force and updating it's Approval Status to Approved*/
        ONTAP__Case_Force__c cfTest = new ONTAP__Case_Force__c(
            ONTAP__Geolocation_Approval_Status__c = 'Pending',
            ONTAP__Geolocation_Update_Approved__c = false
        );
        Id devRecordTypeId  = Schema.SObjectType.ONTAP__Case_Force__c.getRecordTypeInfosByName().get('GPS Coordinates Update').getRecordTypeId();
		cfTest.RecordTypeId =  devRecordTypeId;       
        insert cfTest;
        
        cfTest.ONTAP__Geolocation_Approval_Status__c = 'Approved';
        cfTest.ONTAP__Description__c = 'Description';
        
        Test.startTest();
        update cfTest;
        
        
        system.assertEquals([Select Id, ONTAP__Geolocation_Approval_Status__c from ONTAP__Case_Force__c Where Id = :cfTest.Id].ONTAP__Geolocation_Approval_Status__c, 
                            'Approved');
        Test.stopTest();
    }
}