/**
* @author João Luiz
* @date 28/05/2019
* @description Class - Class for methods service on feed comment
*/
public class FeedCommentService  {
    
     /**
     * @description : Send push notification for the app about the user mention on comment
     * @param List<FeedComment> listFeed : new comment feed from trigger
     * @return void 
    */ 
    public static void mentionSegmentItem(List<FeedComment> listFeed){
        
        String communityId = null;	
        Set<Id> listFeedId = new Set<Id>();
        Map<Id, User> mUsersLoopkup = new Map<Id, User>();
        Map<Id, Set<Id>> mapIdUserByListFeedId = new Map<Id, Set<Id>>();
        Map<Id, String> mapIdUserByContent = new Map<Id, String>();

        for(FeedComment feeds : listFeed){
            if(feeds.insertedbyid!=null && !mUsersLoopkup.containskey(feeds.insertedbyid)){
                mUsersLoopkup.put(feeds.insertedbyid,new user());
            }            
        }
        if(mUsersLoopkup.size()>0){
            mUsersLoopkup = new map<Id,user>([SELECT id,name FROM User WHERE Id in: mUsersLoopkup.keyset()]);
        }

        for(FeedComment feeds : listFeed){
            String feedNowId = feeds.FeedItemId;
            
            if(feeds.CreatedById != feeds.ParentId){ 
                listFeedId.add(feeds.Id);
                mapIdUserByListFeedId.put(feeds.ParentId, listFeedId);
            }
         
            String strUser = mUsersLoopkup.containskey(feeds.insertedbyid) ? ' by '+ mUsersLoopkup.get(feeds.insertedbyid).name : '' ;
            String strContent = feeds.CommentBody.unescapeHtml4().unescapeJava() + strUser; 
           
            mapIdUserByContent.put(feeds.ParentId, strContent.normalizeSpace());

            ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.getFeedElement(communityId, feedNowId);				

            //Have all the comments
            List<ConnectApi.Comment> comments = feedElement.capabilities.comments.page.items; 
            
            List<ConnectApi.MessageSegment> messageSegments = comments.get(0).body.messageSegments;    

            for(ConnectApi.Comment comment : comments){
             
                for (ConnectApi.MessageSegment messageSegment : comment.body.messageSegments) {    
                
                    if (messageSegment instanceof ConnectApi.MentionSegment) {
                        ConnectApi.MentionSegment mentionSegment = (ConnectApi.MentionSegment) messageSegment;

                        listFeedId.add(feeds.Id);
                
                        if(!mapIdUserByContent.containsKey(mentionSegment.record.Id) && UserInfo.getUserId() != mentionSegment.record.Id){
                            mapIdUserByListFeedId.put(mentionSegment.record.Id, listFeedId);
                            mapIdUserByContent.put(mentionSegment.record.Id, strContent.normalizeSpace());
                        }
                    } 
                }
            }
        }

        System.debug(logginglevel.INFO, 'FeedCommmentService.mapIdUserByListFeedId: '+ mapIdUserByListFeedId); 
        System.debug(logginglevel.INFO, 'FeedCommmentService.mapIdUserByContent: '+ mapIdUserByContent); 

        FeedPushUtil feedUtil = new FeedPushUtil(mapIdUserByListFeedId, mapIdUserByContent);
        feedUtil.sendPushNotfication();
		
    }
  
}