/**
 * Company:             Avanxo México
 * Author:              Carlos Pintor / cpintor@avanxo.com
 * Project:             AbInbev 2019
 * Description:         Apex Unit Test Class of ISSM_OrderTicket_wpr
 *
 * Nu.    Date                Author                     Description
 * 1.0    June 11th, 2019     Carlos Pintor              Created
 * 2.0 	  Oct 2nd, 2019  	  Daniel Rosales		     Implementation for Panamá
 * 2.1    October 31th, 2019  Marco Zúñiga               Adjustment in constructor to add more parameters
 *
 */

@IsTest
public class OrderTicketPDF_wrapper_test {
	public static testMethod void testWrapper(){
        OrderTicketPDF_wrapper wrapper = new OrderTicketPDF_wrapper();
        
        OrderTicketPDF_wrapper.OrderItems orderItems = new OrderTicketPDF_wrapper.OrderItems();
        List<OrderTicketPDF_wrapper.OrderItem> orderItemList = new List<OrderTicketPDF_wrapper.OrderItem>();
        
        OrderTicketPDF_wrapper.OrderItem item = new OrderTicketPDF_wrapper.OrderItem();
        item = new OrderTicketPDF_wrapper.OrderItem();
        orderItemList.add(item);
        
        orderItems = new OrderTicketPDF_wrapper.OrderItems(orderItemList);
        
        wrapper = new OrderTicketPDF_wrapper('', '', '', '', '',
                                             '', '', '', '', '',
                                             2.3,4.3,3.3,32.3,42.3,
                                             '', '', '', '', '',
                                             orderItems, orderItemList);
    }
}