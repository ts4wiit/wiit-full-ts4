public class DRSchedulableDeleteItinerary implements Schedulable {
    
        public void execute(SchedulableContext sc) {

            DRDeleteItinerary deleteItinerary = new DRDeleteItinerary();
            Id batchId = Database.executeBatch( deleteItinerary, 5000 );

    }

}