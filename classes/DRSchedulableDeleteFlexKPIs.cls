/**
 * Created by Dejair.Junior on 9/19/2019.
 */

global with sharing class DRSchedulableDeleteFlexKPIs implements Schedulable  {

    global void execute(SchedulableContext sc) {

        DRDeleteFlexibleKPIs deleteKPIs = new DRDeleteFlexibleKPIs();
        Id batchId = Database.executeBatch( deleteKPIs, 5000 );

    }

}