@isTest
public class CaseForceEventTriggerHandlerTest {
    
    @isTest
    static void testSalesProcessCaseChangeEvent(){
        
        String recordTypeDeveloperName;
        
        Account account = DRFixtureFactory.newDRAccount();
        
        Test.startTest();
 
        Test.enableChangeDataCapture();   
        
        recordTypeDeveloperName = CaseForceEventTriggerHandler.DR_SALES_PROCESS_RECORD_TYPE;

        ONTAP__Case_Force__c caseSales = DRFixtureFactory.newDRCase( account.Id, recordTypeDeveloperName , 'No visita vendedor' );
        
        Test.getEventBus().deliver();
        
        Test.stopTest();
        
    }    
    
    @isTest
    static void testAssetInvestigationCaseChangeEvent(){
        
        String recordTypeDeveloperName;
        
        Account account = DRFixtureFactory.newDRAccount();
        
        Test.startTest();
 
        Test.enableChangeDataCapture();   
          
        recordTypeDeveloperName = CaseForceEventTriggerHandler.DR_ASSET_INVESTIGATION_RECORD_TYPE;

        ONTAP__Case_Force__c caseAssetInvestigation = DRFixtureFactory.newDRCase( account.Id, recordTypeDeveloperName , 'Asset investigation' );
        
        Test.getEventBus().deliver();
        
        Test.stopTest();
        
    }        

    @isTest
    static void testAssetMovementCaseChangeEvent(){
        
        String recordTypeDeveloperName;
        
        Account account = DRFixtureFactory.newDRAccount();
        
        Test.startTest();
 
        Test.enableChangeDataCapture();   
          
        recordTypeDeveloperName = CaseForceEventTriggerHandler.DR_ASSET_MOVEMENT_RECORD_TYPE;

        ONTAP__Case_Force__c caseMovement = DRFixtureFactory.newDRCase( account.Id, recordTypeDeveloperName , 'Movimiento' );
        
        Test.getEventBus().deliver();
        
        Test.stopTest();
        
    }    
    
    @isTest
    static void testAssetMaintenanceCaseChangeEvent(){
        
        String recordTypeDeveloperName;
        
        Account account = DRFixtureFactory.newDRAccount();
        
        Test.startTest();
 
        Test.enableChangeDataCapture();   
          
        recordTypeDeveloperName = CaseForceEventTriggerHandler.DR_ASSET_MAINTENANCE_RECORD_TYPE;

        ONTAP__Case_Force__c caseMovement = DRFixtureFactory.newDRCase( account.Id, recordTypeDeveloperName , 'Asset Maintenance' );
        
        Test.getEventBus().deliver();
        
        Test.stopTest();
        
    }        
    
    @isTest
    static void testPaymentMethodCaseChangeEvent(){
        
        String recordTypeDeveloperName;
        
        Account account = DRFixtureFactory.newDRAccount();
        
        Test.startTest();
 
        Test.enableChangeDataCapture();   
          
        recordTypeDeveloperName = CaseForceEventTriggerHandler.DR_PAYMENT_METHOD_CHANGE_RECORD_TYPE;

        ONTAP__Case_Force__c caseMovement = DRFixtureFactory.newDRCase( account.Id, recordTypeDeveloperName , 'Payments Method Change Request' );
        
        Test.getEventBus().deliver();
        
        Test.stopTest();
        
    }     
    
}