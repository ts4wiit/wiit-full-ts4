public with sharing class DRDeleteItinerary implements
                Database.Batchable<sObject>, Database.Stateful {

    public Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT Id FROM DOVisitPlan__c';
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext bc, List<DOVisitPlan__c> scope){

        List<DOVisitPlan__c> deleteList = new List<DOVisitPlan__c>();

        for (DOVisitPlan__c recordToDelete : scope) {
            deleteList.add( recordToDelete );
        }

        delete deleteList;

    }

    public void finish(Database.BatchableContext bc){
        System.debug('>>> Itineraries successfull deleted!');
    }
    
}