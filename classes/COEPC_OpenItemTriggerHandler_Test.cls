@isTest
public class COEPC_OpenItemTriggerHandler_Test {
    
    @isTest
    public static void testOpenItemSharing_CO(){

       Id profileId = [ SELECT Id FROM PROFILE WHERE Name = 'CO BDR' ].get(0).Id;
        
       Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account').getRecordTypeId(); 
        
	   User bdrUser1 = new User(FirstName='BDRCO', LastName='Agent 1', UserName = 'bdr1@test.com', Email='bdr1@test.com',
				Alias='alias1', LocaleSidKey='es', LanguageLocaleKey='es', EmailEncodingKey='ISO-8859-1',ONTAP__Country_Alias__c = 'CO',
				ProfileId = profileId, TimeZoneSidKey='America/Puerto_Rico', Country='CO', isActive = true,User_Route__c='CO9999');

	   insert bdrUser1;    
        
       Account account = new Account( Name = 'Account', ONTAP__SAPCustomerId__c = 'CID1', RecordTypeId = accountRecordTypeId, OwnerId = bdrUser1.Id,
                                           ONTAP__Contact_First_Name__c = 'Contact', ONTAP__Contact_Last_Name__c = 'Lastname', BDR_Route__c = 'CO9999' );
      
       insert account;

       String openItemRecordType = COPEC_OpenItemTriggerHandler.CO_OPEN_ITEM_RECORD_TYPE;
        system.debug('openItemRecordType:' + openItemRecordType);
       Id openItemRecordTypeId =  Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get( openItemRecordType ).getRecordTypeId();
        
       ONTAP__OpenItem__c openItem = new ONTAP__OpenItem__c(
           
       	   ONTAP__Account__c = account.Id, //           ONTAP__Route_Id__c = bdrUser1.User_Route__c,
           RecordTypeId = openItemRecordTypeId
       
       );
       
       Test.startTest();
        
       insert openItem;
        
       ONTAP__OpenItem__c newOpenItem = [ SELECT Id, OwnerId FROM ONTAP__OpenItem__c WHERE Id = : openItem.Id ];
        
       System.assertEquals( bdrUser1.Id , newOpenItem.OwnerId , '>>> Something went wrong :( assertion failed!');
         
       Test.stopTest();        

    }

    @isTest
    public static void testOpenItemSharing_PE(){

       Id profileId = [ SELECT Id FROM PROFILE WHERE Name = 'PE BDR' ].get(0).Id;
        
       Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account').getRecordTypeId(); 
        
	   User bdrUser1 = new User(FirstName='BDRPE', LastName='Agent 1', UserName = 'bdr1@test.com', Email='bdr1@test.com',
				Alias='alias1', LocaleSidKey='es', LanguageLocaleKey='es', EmailEncodingKey='ISO-8859-1',ONTAP__Country_Alias__c = 'PE',
				ProfileId = profileId, TimeZoneSidKey='America/Puerto_Rico', Country='PE', isActive = true,User_Route__c='PE9999');

	   insert bdrUser1;    
        
       Account account = new Account( Name = 'Account', ONTAP__SAPCustomerId__c = 'CID1', RecordTypeId = accountRecordTypeId, OwnerId = bdrUser1.Id,
                                           ONTAP__Contact_First_Name__c = 'Contact', ONTAP__Contact_Last_Name__c = 'Lastname', BDR_Route__c = 'PE9999' );
      
       insert account;

       String openItemRecordType = COPEC_OpenItemTriggerHandler.PE_OPEN_ITEM_RECORD_TYPE;
       Id openItemRecordTypeId =  Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get( openItemRecordType ).getRecordTypeId();
        
       ONTAP__OpenItem__c openItem = new ONTAP__OpenItem__c(
           
       	   ONTAP__Account__c = account.Id, //           ONTAP__Route_Id__c = bdrUser1.User_Route__c,
           RecordTypeId = openItemRecordTypeId
       
       );
       
       Test.startTest();
        
       insert openItem;
        
       ONTAP__OpenItem__c newOpenItem = [ SELECT Id, OwnerId FROM ONTAP__OpenItem__c WHERE Id = : openItem.Id ];
        
       System.assertEquals( bdrUser1.Id , newOpenItem.OwnerId , '>>> Something went wrong :( assertion failed!');
         
       Test.stopTest();        

    }


    @isTest
    public static void testOpenItemSharing_EC(){

       Id profileId = [ SELECT Id FROM PROFILE WHERE Name = 'EC BDR' ].get(0).Id;
        
       Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account').getRecordTypeId(); 
        
	   User bdrUser1 = new User(FirstName='BDREC', LastName='Agent 1', UserName = 'bdr1@test.com', Email='bdr1@test.com',
				Alias='alias1', LocaleSidKey='es', LanguageLocaleKey='es', EmailEncodingKey='ISO-8859-1',ONTAP__Country_Alias__c = 'EC',
				ProfileId = profileId, TimeZoneSidKey='America/Puerto_Rico', Country='EC', isActive = true,User_Route__c='EC9999');

	   insert bdrUser1;    
        
       Account account = new Account( Name = 'Account', ONTAP__SAPCustomerId__c = 'CID1', RecordTypeId = accountRecordTypeId, OwnerId = bdrUser1.Id,
                                           ONTAP__Contact_First_Name__c = 'Contact', ONTAP__Contact_Last_Name__c = 'Lastname', BDR_Route__c = 'EC9999' );
      
       insert account;

       String openItemRecordType = COPEC_OpenItemTriggerHandler.EC_OPEN_ITEM_RECORD_TYPE;
       Id openItemRecordTypeId =  Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get( openItemRecordType ).getRecordTypeId();
        
       ONTAP__OpenItem__c openItem = new ONTAP__OpenItem__c(
           
       	   ONTAP__Account__c = account.Id, //           ONTAP__Route_Id__c = bdrUser1.User_Route__c,
           RecordTypeId = openItemRecordTypeId
       
       );
       
       Test.startTest();
        
       insert openItem;
        
       ONTAP__OpenItem__c newOpenItem = [ SELECT Id, OwnerId FROM ONTAP__OpenItem__c WHERE Id = : openItem.Id ];
        
       System.assertEquals( bdrUser1.Id , newOpenItem.OwnerId , '>>> Something went wrong :( assertion failed!');
         
       Test.stopTest();        

    }

}