public class DRContactSelector {
    
    public static List<Contact> getContactsByIds( List<Id> contactIds ){
        
        return [ SELECT Id, FirstName, ONTAP__UserType__c, ONTAP__SAPUserId__c, LastName, Phone, Account.ONTAP__SAPCustomerId__c, Account.RecordType.DeveloperName FROM Contact WHERE Id in : contactIds ];

    }
    
    public static Map<Id,Contact> getContactIdToContactMap( List<Id> contactIds ){
        
        Map<Id,Contact> contactIdToContactMap = new Map<Id,Contact>();
        List<Contact> contacts = getContactsByIds( contactIds );
        
        for ( Contact contact : contacts ){
            
            contactIdToContactMap.put( contact.Id, contact );
            
        }   
        
        return contactIdToContactMap;
        
    }
    
}