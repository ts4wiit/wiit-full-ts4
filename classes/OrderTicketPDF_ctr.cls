/**
* Company:        Avanxo México
* Author:        Carlos Pintor / cpintor@avanxo.com
* Project:        AbInbev 2019
* Description:      Apex Controller of the Visualforce ISSM_OrderTicket_pag to create the Ticket PDF File
* Apex Test:      ISSM_OrderTicket_tst
*
* Nu.    Date                 Author                     Description
* 1.0    June 11th, 2019     Carlos Pintor              Created
* 1.1    June 19th, 2019     Marco Zuñiga               Modified the method "createOnCallOrderTicket"
* 1.2    June 19th, 2019     Carlos Pintor              Modified the method "createOnTapOrderTicket".
*                              The Logistic fee is now included in the unit price and total of each order item.
*                              The total of the order now also includes the logistic fee.
* 1.3    Oct 1st, 2019       Daniel Rosales             Implementation for Panamá
* 1.4    Octorber 30th, 2019 Marco Zúñiga               Added the total quantity of sold boxes
*                                                       and the total of discounts applied to the order
*
*/

public class OrderTicketPDF_ctr {
    public OrderTicketTemplate__mdt template {get; set;}
    public OrderTicketPDF_wrapper wrapper {get; set;}
    //public ISSM_OrderTicket_wpr.EmptyBalance empyBalancewrapper {get; set;}
    @TestVisible private Id orderId = null;
    /**
    * @description  Executes the code flow to retrieve the ticket template and the data to display
    */
    public OrderTicketPDF_ctr() {
        template = getTemplateSettings('OrderTicketTemplate__mdt');         //get the template setting in the Custom Metadata Type record
        getParameters(); //get the parameters of current page to retrieve the Id value
        if(orderId != null){ //if the Id value is not null, so orderId has a valid Id, call methods to get the information to display
            ONTAP__Order__c orderRecord = getOrderRecord(orderId); //get the order information
            wrapper = createTicket(orderRecord);
            //getOrderTicketWrapper(orderRecord); //create and set fiels of the information template in the wrapper class ISSM_OrderTicket_wpr
        }
    }
    
    /**
    * @description  Receives the object Type of the Custom Metadata Type to get the template (custom fields) of the ticket.
    *        If no record is found, the return value is a blank instance of the Custom Metadata Type.
    *
    * @param    objectType     sObject Type of the record to retrieve
    *
    * @return   Return a record of the Custom Metadata Type "ISSM_OrderTicketTemplateSettings__mdt" with the fields to create the template
    */
    @TestVisible
    private OrderTicketTemplate__mdt getTemplateSettings(String objectType){
        OrderTicketTemplate__mdt template = new OrderTicketTemplate__mdt();
        User u = [SELECT id, username, Country FROM User where Id= :UserInfo.getUserId()];
        String queryString = 'SELECT ';
        queryString += String.join(new List<String>(Schema.getGlobalDescribe().get(objectType).getDescribe().fields.getMap().keySet()),', ');
        queryString += ' FROM ' + objectType;
        queryString += ' WHERE IsActive__c = true';
        queryString += ' AND Country__c = \'';
        queryString += u.Country != null ? String.escapeSingleQuotes(u.Country) : 'PA';
        queryString += '\' LIMIT 1';
        
        try{
            template = Database.query(queryString);
        } catch(Exception e){ 
            template = new OrderTicketTemplate__mdt(); 
            }
        return !Test.isRunningTest() ? template : new OrderTicketTemplate__mdt();
    }
    
    /**
    * @description  Gets the parameters of the current page and reads the parameter Id to set an internal variable with the value
    */
    @TestVisible 
    private void getParameters() {
        Map<String, String> params = ApexPages.currentPage().getParameters();
        if (params.containsKey('Id')) {
            orderId = params.get('Id');
        }
    }
    
    /**
    * @description  Receives the Salesforce Id of the Order Record to create a query in order to retrieve all the information required by the ticket template.
    *
    * @param    orderId     Salesforce Order Record Id to retrieve
    *
    * @return   Return a record of type ONTAP__Order__c with all the fields to display in the ticket
    */
    @TestVisible 
    private ONTAP__Order__c getOrderRecord(Id orderId){
        ONTAP__Order__c orderRecord = [
            SELECT Id, ISSM_OriginText__c, ONCALL__SAP_Order_Number__c, CreatedDate, RecordType.DeveloperName,Name,
            ONTAP__OrderAccount__c,
            ONTAP__OrderAccount__r.Parent.Name,
            ONTAP__OrderAccount__r.ONTAP__SAP_Number__c,    
            ONTAP__OrderAccount__r.Name,                    
            ONTAP__OrderAccount__r.ONTAP__Street__c,        
            ONTAP__OrderAccount__r.ONTAP__District__c,      
            ONTAP__OrderAccount__r.Parent.ONTAP__SAP_Number__c,
            ONTAP__App_Version__c, ONTAP__Event_Id__c, ONTAP__BeginDate__c,
            ISSM_PaymentMethod__c, owner.name,
            ONTAP__Amount_Total__c, ONTAP__Amount_Sub_Total_Product__c, ONTAP__Amount_Taxes__c, 
            (SELECT Id, 
             ONTAP__ItemProduct__c, ONTAP__ItemProduct__r.ONTAP__ProductShortName__c, ONTAP__ItemProduct__r.ONTAP__MaterialProduct__c,
             ONTAP__ProductId__c, ISSM_OrderItemSKU__c,
             ONTAP__ActualQuantity__c, ONTAP__Amount_Line_Total__c, ONTAP__LineAmount__c, ONTAP__Discounts__c,
             ONTAP__TaxValue__c, ONTAP__Amount_Tax_Total__c, ONTAP__UnitPrice__c, ONTAP__UnitPriceFull__c, 
             ONCALL__OnCall_Product__c, ONCALL__OnCall_Product__r.ONTAP__ProductType__c, ONCALL__OnCall_Product__r.ONTAP__MaterialProduct__c,
             ONCALL__OnCall_Product__r.ONTAP__ProductShortName__c, 
             ISSM_Uint_Measure_Code__c
             FROM ONTAP__Order_Items1__r) // ONTAP__CustomerOrder__c
            FROM ONTAP__Order__c
            WHERE Id = :orderId ];
        return orderRecord;
    }
    
    /**
    * @description  Receives a string with the Salesforce Event Id in the Order to get the related Visit List name
    *
    * @param    eventId     String with Salesforce Event Id to get the related visit list
    *
    * @return   Return a Event record with the information of the related visit list
    */
    @TestVisible 
    private Event getEventRecord(String eventId){
        Event visit = null;
        try{ 
            visit = [ 
                SELECT Id, VisitList__r.Name, VisitList__r.RouteDescription__c
                FROM Event
                WHERE Id = :eventId];
        } catch(Exception e){}
        return visit;
    }
    
    @TestVisible 
    private OrderTicketPDF_wrapper createTicket(ONTAP__Order__c orderRecord){
        //field mapping of order information into the wrapper class attributes of ISSM_OrderTicket_wpr, valid only for OCAL Origin
        OrderTicketPDF_wrapper wrapper = new OrderTicketPDF_wrapper();
        List<OrderTicketPDF_wrapper.OrderItem> orderItemList = new List<OrderTicketPDF_wrapper.OrderItem>();
        
        wrapper.Visit_Transport = 'V' + DateTime.now().format('ddMMyyyy');
        wrapper.SAPOrderNumber = orderRecord.Name;
        wrapper.customerName = orderRecord.ONTAP__OrderAccount__r.Name;
        wrapper.customerSAPNumber = orderRecord.ONTAP__OrderAccount__r.ONTAP__SAP_Number__c;
        wrapper.customerAddress = (String.isNotBlank(orderRecord.ONTAP__OrderAccount__r.ONTAP__Street__c)? orderRecord.ONTAP__OrderAccount__r.ONTAP__Street__c: '') + ', ' + (String.isNotBlank(orderRecord.ONTAP__OrderAccount__r.ONTAP__District__c) ? orderRecord.ONTAP__OrderAccount__r.ONTAP__District__c :'' );
        wrapper.SectorVenta = orderRecord.ONTAP__OrderAccount__r.Parent.Name;
        wrapper.totalTax = orderRecord.ONTAP__Amount_Taxes__c;
        wrapper.totalWOTax = orderRecord.ONTAP__Amount_Sub_Total_Product__c;
        wrapper.totalWTax = orderRecord.ONTAP__Amount_Total__c;
        wrapper.CreatedDateFecha = orderRecord.CreatedDate.format('dd/MM/yyyy') + '';
        wrapper.CreatedDateHora =  orderRecord.CreatedDate.format('HH:mm') + '';
        wrapper.paymentMethod = orderRecord.ISSM_PaymentMethod__c;
        wrapper.vendedor = orderRecord.owner.name;
        wrapper.appVersion = orderRecord.ONTAP__App_Version__c;
        
        switch on orderRecord.ISSM_OriginText__c {
            when 'OCAL' {
            }
            when 'OTAP' {
            }
            when else {
            }
        }
        
        Decimal boxesCounter = 0.0;
        Decimal discounts = 0.0;
        for(ONTAP__Order_Item__c oi : orderRecord.ONTAP__Order_Items1__r){
            
            boxesCounter += oi.ONTAP__ItemProduct__c != null ? (oi.ONTAP__ActualQuantity__c != null ? oi.ONTAP__ActualQuantity__c : 0.0) : 0.0;
            discounts += oi.ONTAP__ItemProduct__c != null ? (oi.ONTAP__Discounts__c != null ? oi.ONTAP__Discounts__c : 0.0) : 0.0;
            
            System.debug('boxesCounter = ' + boxesCounter);
            System.debug('Discounts = ' + discounts);
            
            system.debug(oi.ONTAP__UnitPrice__c);
            if(oi.ONTAP__UnitPrice__c == 0.0 || oi.ONTAP__UnitPrice__c == null){
                continue;
            }
            OrderTicketPDF_wrapper.OrderItem item = new OrderTicketPDF_wrapper.OrderItem();
            item.sku = oi.ONTAP__ProductId__c;
            item.name = oi.ONTAP__ItemProduct__r.ONTAP__MaterialProduct__c;
            item.shortName = oi.ONTAP__ItemProduct__r.ONTAP__ProductShortName__c;
            item.udm = oi.ISSM_Uint_Measure_Code__c==NULL?'CAJ':oi.ISSM_Uint_Measure_Code__c;
            item.quantity = oi.ONTAP__ActualQuantity__c;
            item.price = oi.ONTAP__UnitPrice__c;
            item.discount = oi.ONTAP__Discounts__c;
            item.total =  oi.ONTAP__Amount_Line_Total__c ;
            switch on orderRecord.ISSM_OriginText__c {
                when 'OCAL' { 
                }
                when 'OTAP' {
                }
                when else {
                }
            }      
            orderItemList.add(item);
        }
        
        wrapper.totalSoldBoxes = boxesCounter;
        wrapper.totalDiscount = discounts;
        
        wrapper.orderItems =  new OrderTicketPDF_wrapper.OrderItems(orderItemList);    
        
        System.debug('wrapper = ' + wrapper);
        
        return wrapper;        
    }
 
}