/**
 * Created by Dejair.Junior on 10/3/2019.
 */

public with sharing class DRVisitHelper {
    
    private static final Date YEAR_START_DATE = Date.newInstance(2019, 12, 30);

    public static Integer getWeekNumber( Date dateToVerify ){
        
        return (YEAR_START_DATE.daysBetween(dateToVerify)/7) + 1;

    }

    public static Boolean isEven( Integer n ){

        Integer r;

        r = System.Math.mod(n, 2);
        if(r == 0) {
            return true;
        } else {
            return false;
        }

    }

}