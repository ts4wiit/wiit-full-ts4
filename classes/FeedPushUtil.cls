/**
* @author : Marcilio Souza
* @date : 15/10/2019
* @description : Util for feed chatter push notification
*/
public with sharing class FeedPushUtil {
    
    @TestVisible private static final String CUSTOM_PERMISSION = 'ChatterUserPushNotification';		
    @TestVisible private static final String CUSTOM_SETTING = 'ChatterPushNotification';

    private Map<Id, Set<Id>> mapIdUserByListFeedId {get;set;}
    private Map<Id, String> mapIdUserByContent {get;set;}
	
    public FeedPushUtil(){}
    
    public FeedPushUtil(Map<Id, Set<Id>> mapIdUserByListFeedId, Map<Id, String> mapIdUserByContent){
        
        Map<Id,User> mapUserAllowed = CustomPermissionService.getUsersWithCustomPermission(mapIdUserByListFeedId.keySet(), CUSTOM_PERMISSION);
        
        for (Id objUser : mapIdUserByListFeedId.keySet()){
            if(!mapUserAllowed.keySet().contains(objUser)){
                mapIdUserByListFeedId.remove(objUser);
            }
        }       
        
        this.mapIdUserByListFeedId = mapIdUserByListFeedId;
        this.mapIdUserByContent = mapIdUserByContent;
    }

    public void sendPushNotfication(){
        Map<Id, Map<String, Object>> mapIdUserByPayLoad = getUserIdbyPayload(this.mapIdUserByListFeedId, this.mapIdUserByContent);
        PushNotificationService.sendPushNotifications(mapIdUserByPayLoad, CUSTOM_SETTING); 
    }
    
    public Map<Id, Map<String, Object>> getUserIdbyPayload(Map<Id, Set<Id>> mapIdUserByListFeedId, Map<Id, String> mapIdUserByContent) {
        
        Map<Id, Map<String, Object>> mapIdUserByPayLoad = new Map<Id, Map<String, Object>>();
        system.debug('mapIdUserByListFeedId: ' + mapIdUserByListFeedId);
        for(Id obj : mapIdUserByListFeedId.keySet()){
            
            Map<String, Object> mapPayload = new Map<String, Object>();
            
            for(Id feeds : mapIdUserByListFeedId.get(obj)){                
                mapPayload.put('type', 'chatter_feed');
              
                if(mapIdUserByContent.containskey(obj)){
                    mapPayload.put('content', mapIdUserByContent.get(obj) );
                }else{
                    mapPayload.put('content', Label.PushNotification );
                }

                mapPayload.put('extra_fields', 'user_id,feed_id');
                mapPayload.put('user_id', obj);								 			
                mapPayload.put('feed_id', feeds);
                mapIdUserByPayLoad.put(obj, mapPayload);
            }
        }

        return mapIdUserByPayLoad;
    }

}