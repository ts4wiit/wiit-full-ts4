global class DRSchedulableVisits implements Schedulable {
   global void execute(SchedulableContext sc) {
       DRBatchVisits batchVisits = new DRBatchVisits();
       Id batchId = Database.executeBatch( batchVisits, 1 );
   }
}