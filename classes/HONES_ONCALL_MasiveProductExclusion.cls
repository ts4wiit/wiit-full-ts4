/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase:HONES_ONCALL_MasiveProductExclusion.apxc
* Versión: 1.0.0.0
* 
* 
* 
* Historial de Cambios
* ----------------------------------------------------------------------------
* Fecha           Usuario            Contacto      						Descripción
* 18/10/2019    Daniel Hernández	dhernandez@ts4.mx     			Creación de la clase  
*/
global class HONES_ONCALL_MasiveProductExclusion implements Database.Batchable<sObject>, Database.Stateful {
	private Id id_recordTypeByGroup;
	private String DistributionCenter; 
    private String Subchannel; 
    private String Attr3;
    private String criteria;
    private String country;
    private Map<Id, ONTAP__Product__c> selectedProducts;
    
    public HONES_ONCALL_MasiveProductExclusion(String searchC, String setCountry, Map<Id, ONTAP__Product__c> mapProducts) {
        criteria = searchC;
        country = setCountry;
        selectedProducts = mapProducts;
        id_recordTypeByGroup = [SELECT Id FROM RecordType WHERE DeveloperName='Grupo_Cliente'].Id;
        List<String> searchCriteria = searchC.split('[|]');
        DistributionCenter = searchCriteria[0];
        Subchannel = searchCriteria[1];
        Attr3 = searchCriteria[2];
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id, Name, ONTAP__SAP_Number__c FROM Account WHERE RecordType.Name = \'Account\' ';
        if(DistributionCenter != '--Todos--' ){
            query = query + ' AND IREP_DistributionCenter__c LIKE \'' + DistributionCenter.substring(0,4) + '%\'';
        }
        if(Subchannel != '--Todos--' ){
            query = query + ' AND ONCALL__KATR9__c LIKE \'' + Subchannel.substring(0,3) + '%\'';
        }
        if(Attr3 != '--Todos--' ){
            query = query + ' AND ONCALL__KATR3__c LIKE \'' + Attr3.substring(0,2) + '%\'';
        }
        query = query + ' AND ONTAP__ExternalKey__c LIKE \'' + country + '\'';
        
        /*
        if(DistributionCenter == '--Todos--' ){DistributionCenter = '_S__%';}
        else{DistributionCenter = DistributionCenter.substring(0,4) + '%';}
        if(Subchannel == '--Todos--' ){Subchannel = 'S__%';}
        else{Subchannel = Subchannel.substring(0,3) + '%';}
        if(Attr3 == '--Todos--' ){Attr3 = '__%';}
        else{Attr3 = Attr3.substring(0,2) + '%';}
        
        system.debug('>>>>>>> DistributionCenter: ' + DistributionCenter);
        system.debug('>>>>>>> Subchannel: ' + Subchannel);
        system.debug('>>>>>>> Attr3: ' + Attr3);
        
         query = 'SELECT Id, Name, ONTAP__SAP_Number__c FROM Account ' +
            ' WHERE IREP_DistributionCenter__c LIKE \'' + DistributionCenter +
            '\' AND ONCALL__KATR9__c LIKE \'' +  Subchannel + 
            '\' AND ONCALL__KATR3__c LIKE \'' +  Attr3 + '\'' + 
            'AND ONTAP__ExternalKey__c LIKE \'SV%\'';
        */
        system.debug('>>> query: ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Account> scope) {
        
        ID jobID = System.enqueueJob(new HONES_ONCALL_MasiveProductExclusionQueue(scope, selectedProducts, id_recordTypeByGroup, criteria));
        System.debug('>>>> jobID: ' + jobID);
        
        /***** This process has been moved to the Queue class
         * 
        Map<Id, HONES_ProductPerClient__c> exclusionsToDelete = new Map<Id, HONES_ProductPerClient__c>();
        Map<String, HONES_ProductPerClient__c> exclusionsPerClient = new Map<String, HONES_ProductPerClient__c>();
        Map<String, HONES_ProductPerClient__c> exclusionsToUpsert = new Map<String, HONES_ProductPerClient__c>();
        List<Id> idClients = new List<Id>();
        
        for (Account client : scope) {
            idClients.add(client.id);
        }
        
        Map<Id, HONES_ProductPerClient__c> currentExclusions = new Map<Id, HONES_ProductPerClient__c>([SELECT Id, HONES_RelatedClient__c, HONES_RelatedProduct__c, SAP_Account_ID__c, SAP_SKU_ID__c, RecordTypeId, HONES_MassiveKey__c 
                                                                                                       FROM HONES_ProductPerClient__c WHERE HONES_RelatedClient__c IN : idClients]);
        
        for (Account client : scope) {
            for(HONES_ProductPerClient__c currentExclusion : currentExclusions.values()){
                ONTAP__Product__c selectedProduct = selectedProducts.get(currentExclusion.HONES_RelatedProduct__c);
                if(selectedProduct == null){ // si una exclusión previa no ha sido seleccionada, es eliminada
                    exclusionsToDelete.put(currentExclusion.Id, currentExclusion);
                }else{ //si ha sido seleccionada, pero tuvo cambios, se actualiza
                    String new_key = String.valueOf(selectedProduct.Id)+String.valueOf(client.Id);
                    exclusionsPerClient.put(new_key, currentExclusion);
                    if(currentExclusion.RecordTypeId != id_recordTypeByGroup || currentExclusion.HONES_MassiveKey__c != criteria){
                        currentExclusion.RecordTypeId = id_recordTypeByGroup;
                        currentExclusion.HONES_MassiveKey__c = criteria;
                        exclusionsToUpsert.put(new_key, currentExclusion);
                    }
                }
            }
            
            for(ONTAP__Product__c prod : selectedProducts.values()){ // crea un nuevo registro si no se halló coincidencia
                HONES_ProductPerClient__c exclusion = exclusionsPerClient.get(String.valueOf(prod.Id)+String.valueOf(client.Id));
                if(exclusion == null){
                    exclusion = new HONES_ProductPerClient__c();
                    exclusion.RecordTypeId = id_recordTypeByGroup;
                    exclusion.HONES_MassiveKey__c = criteria;
                    exclusion.HONES_RelatedProduct__c = prod.Id;
                    exclusion.SAP_SKU_ID__c = prod.ONTAP__ProductCode__c;
                    exclusion.HONES_RelatedClient__c = client.Id;
                    exclusion.SAP_Account_ID__c = client.ONTAP__SAP_Number__c;
                    exclusionsToUpsert.put(String.valueOf(prod.Id)+String.valueOf(client.Id),exclusion);
                }
            }
        }
        
        system.debug('>>>> currentExclusions' + currentExclusions.size() + ' ' + currentExclusions);
        system.debug('>>>> exclusionsToUpsert' + exclusionsToUpsert.size() + ' ' + exclusionsToUpsert);
        system.debug('>>>> exclusionsToDelete' + exclusionsToDelete.size() + ' ' + exclusionsToDelete);
        
        try{
            delete exclusionsToDelete.values();
            upsert exclusionsToUpsert.values();
        } catch (DmlException e) {
            System.debug('==== Se produjo un error al modificar registros: ' + e.getMessage());
        }
        */
        
    }

    global void finish(Database.BatchableContext BC) {}
}