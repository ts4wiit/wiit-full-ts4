/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_DeleteInvoicesItemsSchudeler.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Carlos Leal        Creation of methods.
 */
global class V360_DeleteInvoicesBatch implements Database.Batchable<sObject>{
    
    global Boolean hasErrors {get; private set;}

   /**
   * Clean the class.
   * @author: c.leal.beltran@accenture.com 
   */
    global V360_DeleteInvoicesBatch()
    {  
        this.hasErrors = false;
    }
    
   /**
   * Execute the Query for the Invoice objects.
   * @author: c.leal.beltran@accenture.com
   * @param SchedulableContext sc
   * @return void
   */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        Id V360_RecordType = Schema.SObjectType.ONCALL__Invoice__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.INVOICE_V360_RECORDTYPE_NAME).getRecordTypeId();
        system.debug('Processing start method');
        Date todayDate = System.Today();

        // Query string for batch Apex
        String query = ''; 
        
        if(System.Test.isRunningTest()){
            query += 'SELECT Id FROM ONCALL__Invoice__c';
        }else{
            query += 'SELECT Id FROM ONCALL__Invoice__c WHERE RecordTypeId=:V360_RecordType AND LastModifiedDate<:todayDate';
        }
        Database.QueryLocator q = Database.getQueryLocator(query);
        return q;      
    }
    
   /**
   * Method for delete all the Invoice objects. 
   * @author: c.leal.beltran@accenture.com
   * @param Database.BatchableContext BC, List<ONCALL__Invoice__c> objectBatch
   * @return void
   */
    global void execute(Database.BatchableContext BC, List<ONCALL__Invoice__c> objectBatch)
    { 
        delete objectBatch;
        DataBase.emptyRecycleBin(objectBatch);
    }
    
    /**
   * Method finish for close all open process
   * @author: c.leal.beltran@accenture.com
   * @param Database.BatchableContext BC
   * @return void
   */
    global void finish(Database.BatchableContext BC)
    {   
    }
}