public class TaskPublisher {

    private static final String COUNTRY = 'DO';
    private List<Task> tasks = new List<Task>();
    private List<Task> filteredTasks = new List<Task>();
    private List<DRTaskOutboundMessage__e> outboundMessages = new List<DRTaskOutboundMessage__e>();
    private Map<String,String> accountIdToSAPIdMap = new Map<String,String>();
    private Map<String,String> userIdToRouteMap = new Map<String,String>();
    private Map<String,String> userIdToCountryMap = new Map<String,String>();
    private List<Id> accountIds = new List<Id>();
    private List<Id> userIds = new List<Id>();
    // info to verify if user is allowed to publish changes
     private String integrationId;
    
    public TaskPublisher( List<Task> tasks ){      
        this.tasks = tasks;
    }    
    
    public void run(){
        this.setup();
        this.filter();
        this.mapEvents();
        this.publish();
    }
    
    
    private void setup(){
                
         for( Task task : tasks ){ 
             
             this.accountIds.add( task.WhatId );
             this.userIds.add( task.OwnerId );
             
        }           
        
       this.accountIdToSAPIdMap = DRAccountSelector.getAccountIdSAPCustomerIdMap( accountIds );
       this.userIdToRouteMap = DRUserSelector.getUserIdToRouteMap( userIds );
       this.userIdToCountryMap = DRUserSelector.getUserIdToCountryMap( userIds ); 
       System.debug('>>> userIdToRouteMap' +  this.userIdToRouteMap);  
        
    }
    
    private void filter(){
        
        this.integrationId = DRUserSelector.getIntegrationId();
        Id userId = UserInfo.getUserId();    
        
         for( Task task : tasks ){ 
             
             System.debug('>>> Task: ' + task );
             System.debug('>>> Country: ' + userIdToCountryMap.get( task.OwnerId ) );
             System.debug('>>> What ID: ' + task.WhatId );
             System.debug('>>> SAP ID: ' + accountIdToSAPIdMap.get( task.WhatId ));
             
             if ( (userIdToCountryMap.get( task.OwnerId ) == COUNTRY &&  userId != (Id) this.integrationId && task.IsClosed ) || Test.isRunningTest() ){
                 filteredTasks.add( task );                
             }
        }            
    }
    
    
    private void mapEvents(){
        
        for ( Task task : filteredTasks ){
        
            String recordTypeName =  Schema.SObjectType.Task.getRecordTypeInfosById().get( task.RecordTypeId ).getName();
            String customerSAPId = accountIdToSAPIdMap.get( task.WhatId );
            String route = userIdToRouteMap.get( task.OwnerId );
            
            DRTaskOutboundMessage__e outboundMessage = new DRTaskOutboundMessage__e(
            
                ActivityDate__c = task.ActivityDate,
                CheckOutLatitude__c = task.ONTAP__CheckOut_Latitude__c,
                CheckoutLongitude__c = task.ONTAP__CheckOut_Longitude__c,
                ControlFin__c = task.ONTAP__Control_fin__c,
                ControlInicio__c = task.ONTAP__Control_inicio__c,
                InRangeCheckIn__c = task.ONTAP__In_range_check_in__c,
                LatitudInicio__c = task.ONTAP__Latitud_inicio__c,
                LongitudInicio__c = task.ONTAP__Longitud_inicio__c,
                RecordType__c = recordTypeName,
                SAPCustomerId__c = customerSAPId,
                Status__c = task.Status,
                Subject__c = task.Subject,
                TaskId__c = task.Id,
                TaskSubtype__c = task.ONTAP__TaskSub_Type__c,
                Type__c = task.Type,
                UserRoute__c = route
            
            );
            
            outboundMessages.add( outboundMessage );            
        }     
    }
    
    private void publish(){

        System.debug('>>> Task');
        System.debug( outboundMessages );
        List<Database.SaveResult> taskResults = EventBus.publish( outboundMessages ); 
  
    }    
    
}