@isTest
public class DROrderTriggerHandler_Test {
    
    @isTest
    public static void testOrderSharing(){

       Id profileId = [ SELECT Id FROM PROFILE WHERE Name = 'DO BDR' ].get(0).Id;
        
       Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account_DO').getRecordTypeId(); 
        
	   User bdrUser1 = new User(FirstName='BDR1', LastName='Agent 1', UserName = 'bdr1@test.com', Email='bdr1@test.com',
				Alias='alias1', LocaleSidKey='es', LanguageLocaleKey='es', EmailEncodingKey='ISO-8859-1',ONTAP__Country_Alias__c = 'DO',
				ProfileId = profileId, TimeZoneSidKey='America/Puerto_Rico', Country='DO', isActive = true,User_Route__c='DO9999');

	   insert bdrUser1;    
        
       Account account = new Account( Name = 'Account', ONTAP__SAPCustomerId__c = 'CID1', RecordTypeId = accountRecordTypeId, OwnerId = bdrUser1.Id,
                                           ONTAP__Contact_First_Name__c = 'Contact', ONTAP__Contact_Last_Name__c = 'Lastname', BDR_Route__c = 'DO9999' );
      
       insert account;

       String orderRecordType = DROrderTriggerHandler.DR_ORDER_RECORD_TYPE;
       Id orderRecordTypeId =  Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByDeveloperName().get( orderRecordType ).getRecordTypeId();
        
       ONTAP__Order__c order = new ONTAP__Order__c(
           
       	   ONTAP__OrderAccount__c = account.Id,
           ONTAP__Route_Id__c = bdrUser1.User_Route__c,
           RecordTypeId = orderRecordTypeId
       
       );
       
       Test.startTest();
        
       insert order;
        
       ONTAP__Order__c newOrder = [ SELECT Id,OwnerId FROM ONTAP__Order__c WHERE Id = : order.Id ];
        
       System.assertEquals( bdrUser1.Id , newOrder.OwnerId , '>>> Something went wrong :( assertion failed!');
         
       Test.stopTest();        

    }

}