/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_CASECLOSE_CLASS_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 08/01/2019           Jose Luis Vargas       Crecion de la clase para testing de la clase CS_CASECLOSE_CLASS 
 */

@isTest
private class CS_CaseClose_Class_Test
{  
    /**
    * Method for test the method GetDetailCase
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    @isTest static void test_GetDetailCase()
    {
        Account oNewAccount = new Account(Name = 'Account Test');   
        insert oNewAccount;
        
        Case oNewCase = new Case();
        oNewCase.Accountid = oNewAccount.Id;
        oNewCase.Description = 'Test Description';
        oNewCase.Status = 'New';
        oNewCase.Subject = 'Test Subject';
        insert oNewCase;
        CS_CASECLOSE_CLASS oClose = new CS_CASECLOSE_CLASS();
        CS_CASECLOSE_CLASS.GetDetailCase(oNewCase.Id);
    }
    
    /**
    * Method for test the method GetDetailAccount
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    @isTest static void test_GetDetailAccount()
    {
        Account oNewAccount = new Account(Name = 'Account Test');   
        insert oNewAccount;
        
        Case oNewCase = new Case();
        oNewCase.Accountid = oNewAccount.Id;
        oNewCase.Description = 'Test Description';
        oNewCase.Status = 'New';
        oNewCase.Subject = 'Test Subject';
        insert oNewCase;
        
        CS_CASECLOSE_CLASS oClose = new CS_CASECLOSE_CLASS();
        CS_CASECLOSE_CLASS.GetDetailAccount(oNewCase.Id);
    }
    
    /**
    * Method for test the method GetQuestionCloseCase
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    @isTest static void test_GetQuestionCloseCase()
    {
        Account oNewAccount = new Account(Name = 'Account Test');   
        insert oNewAccount;
        
        Case oNewCase = new Case();
        oNewCase.Accountid = oNewAccount.Id;
        oNewCase.Description = 'Test Description';
        oNewCase.Status = 'New';
        oNewCase.Subject = 'Test Subject';
        insert oNewCase;
        
        CS_CASECLOSE_CLASS oClose = new CS_CASECLOSE_CLASS();
        CS_CASECLOSE_CLASS.GetQuestionCloseCase(oNewCase.Id);
        
        /* Idioma Ingles */
        User oUpdateUser = new User();
        oUpdateUser.Id = UserInfo.getUserId();
        oUpdateUser.LanguageLocaleKey = 'en_US';
        update oUpdateUser;
            
        CS_CASECLOSE_CLASS.GetQuestionCloseCase(oNewCase.Id);      
        
        oUpdateUser = new User();
        oUpdateUser.Id = UserInfo.getUserId();
        oUpdateUser.Country__c = 'El Salvador';
         update oUpdateUser;
             
        CS_CASECLOSE_CLASS.GetQuestionCloseCase(oNewCase.Id);
        
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = :TestUtils_tst.PROFILE_NAME_ADMIN LIMIT 1].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Bogota',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'es',
            LocaleSidKey = 'es_MX',
            Country__c = 'Honduras',
            ManagerId = UserInfo.getUserId()
        );
        insert u;
        system.runAs(u){
			CS_CASECLOSE_CLASS.GetQuestionCloseCase(oNewCase.Id);
        }
    }
    
    /**
    * Method for test the method GetSolutionMasterOptions
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    @isTest static void test_GetSolutionMasterOptions()
    {
         CS_CASECLOSE_CLASS oClose = new CS_CASECLOSE_CLASS();
         CS_CASECLOSE_CLASS.GetSolutionMasterOptions();
         
        /* Idioma Ingles */
        User oUpdateUser = new User();
        oUpdateUser.Id = UserInfo.getUserId();
        oUpdateUser.LanguageLocaleKey = 'en_US';
        update oUpdateUser;
         
         CS_CASECLOSE_CLASS.GetSolutionMasterOptions();
        
          User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = :TestUtils_tst.PROFILE_NAME_ADMIN LIMIT 1].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Bogota',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'es',
            LocaleSidKey = 'es_MX',
            Country__c = 'Honduras',
            ManagerId = UserInfo.getUserId()
        );
        insert u;
        system.runAs(u){
			CS_CASECLOSE_CLASS.GetSolutionMasterOptions();
        }
     }
     
    /**
    * Method for test the method CloseCase
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    
         @isTest static void test_CloseCasenoo()
     {
        Account oNewAccount = new Account(Name = 'Account Test');   
        insert oNewAccount;
        
        ONTAP__Case_Force__c oNewCaseForce = new ONTAP__Case_Force__c();
        oNewCaseForce.ONTAP__Status__c = 'New';
        oNewCaseForce.ONTAP__Subject__c = 'Test Case Force';
        oNewCaseForce.ONTAP__Description__c = 'Test Description';
        insert oNewCaseForce;
        
        Case oNewCase = new Case();
        oNewCase.Accountid = oNewAccount.Id;
        oNewCase.Description = 'Test Description';
        oNewCase.Status = 'New';
        oNewCase.Subject = 'Test Subject';
        insert oNewCase;
         
        CS_CASECLOSE_CLASS oClose = new CS_CASECLOSE_CLASS();
        CS_CASECLOSE_CLASS.CloseCase(oNewCase.Id, oNewCaseForce.Id , 'Close Test', 'Registro Incompleto','CS_RT_Tracing_Case__c');
     }
    
     @isTest static void test_CloseCase()
     {
        Account oNewAccount = new Account(Name = 'Account Test');   
        insert oNewAccount;
        
        ONTAP__Case_Force__c oNewCaseForce = new ONTAP__Case_Force__c();
        oNewCaseForce.ONTAP__Status__c = 'New';
        oNewCaseForce.ONTAP__Subject__c = 'Test Case Force';
        oNewCaseForce.ONTAP__Description__c = 'Test Description';
        insert oNewCaseForce;
        
        Case oNewCase = new Case();
        oNewCase.Accountid = oNewAccount.Id;
        oNewCase.Description = 'Test Description';
        oNewCase.Status = 'New';
        oNewCase.Subject = 'Test Subject';
        insert oNewCase;
         
        CS_RO_Cooler_Settings__c Coolset = new CS_RO_Cooler_Settings__c ();
        Coolset.CS_RT_Cooler_Repair__c='CS_RT_Cooler_Repair__c';
		insert Coolset;

        CS_CASECLOSE_CLASS oClose = new CS_CASECLOSE_CLASS();
        CS_CASECLOSE_CLASS.CloseCase(oNewCase.Id, oNewCaseForce.Id , 'Close Test', 'Registro Incompleto','CS_RT_Cooler_Repair__c');
     }
  
     @isTest static void test_CloseCaseGeneral()
     {
        Account oNewAccount = new Account(Name = 'Account Test');   
        insert oNewAccount;
        
        ONTAP__Case_Force__c oNewCaseForce = new ONTAP__Case_Force__c();
        oNewCaseForce.ONTAP__Status__c = 'New';
        oNewCaseForce.ONTAP__Subject__c = 'Test Case Force';
        oNewCaseForce.ONTAP__Description__c = 'Test Description';
        insert oNewCaseForce;
        
        Case oNewCase = new Case();
        oNewCase.Accountid = oNewAccount.Id;
        oNewCase.Description = 'Test Description';
        oNewCase.Status = 'New';
        oNewCase.Subject = 'Test Subject';
        insert oNewCase;
         
        CS_RO_Cooler_Settings__c Coolset = new CS_RO_Cooler_Settings__c ();
        Coolset.CS_RT_General_Case__c='CS_RT_General_Case__c';
		insert Coolset;

        CS_CASECLOSE_CLASS oClose = new CS_CASECLOSE_CLASS();
        CS_CASECLOSE_CLASS.CloseCase(oNewCase.Id, oNewCaseForce.Id , 'Close Test', 'Registro Incompleto','CS_RT_General_Case__c');
    
     
     }
    
     @isTest static void test_CloseCaseGenerate()
     {
        Account oNewAccount = new Account(Name = 'Account Test');   
        insert oNewAccount;
        
        ONTAP__Case_Force__c oNewCaseForce = new ONTAP__Case_Force__c();
        oNewCaseForce.ONTAP__Status__c = 'New';
        oNewCaseForce.ONTAP__Subject__c = 'Test Case Force';
        oNewCaseForce.ONTAP__Description__c = 'Test Description';
        insert oNewCaseForce;
        
        Case oNewCase = new Case();
        oNewCase.Accountid = oNewAccount.Id;
        oNewCase.Description = 'Test Description';
        oNewCase.Status = 'New';
        oNewCase.Subject = 'Test Subject';
        insert oNewCase;
         
        CS_RO_Cooler_Settings__c Coolset = new CS_RO_Cooler_Settings__c ();
        Coolset.CS_RT_Generate_Client__c='CS_RT_Generate_Client__c';
		insert Coolset;

        CS_CASECLOSE_CLASS oClose = new CS_CASECLOSE_CLASS();

        CS_CASECLOSE_CLASS.CloseCase(oNewCase.Id, oNewCaseForce.Id , 'Close Test', 'Registro Incompleto','CS_RT_Generate_Client__c');
     }

     @isTest static void test_CloseCaseInstal()
     {
        Account oNewAccount = new Account(Name = 'Account Test');   
        insert oNewAccount;
        
        ONTAP__Case_Force__c oNewCaseForce = new ONTAP__Case_Force__c();
        oNewCaseForce.ONTAP__Status__c = 'New';
        oNewCaseForce.ONTAP__Subject__c = 'Test Case Force';
        oNewCaseForce.ONTAP__Description__c = 'Test Description';
        insert oNewCaseForce;
        
        Case oNewCase = new Case();
        oNewCase.Accountid = oNewAccount.Id;
        oNewCase.Description = 'Test Description';
        oNewCase.Status = 'New';
        oNewCase.Subject = 'Test Subject';
        insert oNewCase;
         
        CS_RO_Cooler_Settings__c Coolset = new CS_RO_Cooler_Settings__c ();
        Coolset.CS_RT_Installation_Cooler__c='CS_RT_Installation_Cooler__c';
		insert Coolset;

        CS_CASECLOSE_CLASS oClose = new CS_CASECLOSE_CLASS();
        CS_CASECLOSE_CLASS.CloseCase(oNewCase.Id, oNewCaseForce.Id , 'Close Test', 'Registro Incompleto','CS_RT_Installation_Cooler__c');
     }
    
     @isTest static void test_CloseCaseRetire()
     {
        Account oNewAccount = new Account(Name = 'Account Test');   
        insert oNewAccount;
        
        ONTAP__Case_Force__c oNewCaseForce = new ONTAP__Case_Force__c();
        oNewCaseForce.ONTAP__Status__c = 'New';
        oNewCaseForce.ONTAP__Subject__c = 'Test Case Force';
        oNewCaseForce.ONTAP__Description__c = 'Test Description';
        insert oNewCaseForce;
        
        Case oNewCase = new Case();
        oNewCase.Accountid = oNewAccount.Id;
        oNewCase.Description = 'Test Description';
        oNewCase.Status = 'New';
        oNewCase.Subject = 'Test Subject';
        insert oNewCase;
         
        CS_RO_Cooler_Settings__c Coolset = new CS_RO_Cooler_Settings__c ();
        Coolset.CS_RT_Retirement_Cooler__c='CS_RT_Retirement_Cooler__c';
		insert Coolset;

        CS_CASECLOSE_CLASS oClose = new CS_CASECLOSE_CLASS();
        CS_CASECLOSE_CLASS.CloseCase(oNewCase.Id, oNewCaseForce.Id , 'Close Test', 'Registro Incompleto','CS_RT_Retirement_Cooler__c');
     }
    
     @isTest static void test_CloseCaseTraci()
     {
        Account oNewAccount = new Account(Name = 'Account Test');   
        insert oNewAccount;
        
        ONTAP__Case_Force__c oNewCaseForce = new ONTAP__Case_Force__c();
        oNewCaseForce.ONTAP__Status__c = 'New';
        oNewCaseForce.ONTAP__Subject__c = 'Test Case Force';
        oNewCaseForce.ONTAP__Description__c = 'Test Description';
        insert oNewCaseForce;
        
        Case oNewCase = new Case();
        oNewCase.Accountid = oNewAccount.Id;
        oNewCase.Description = 'Test Description';
        oNewCase.Status = 'New';
        oNewCase.Subject = 'Test Subject';
        insert oNewCase;
         
        CS_RO_Cooler_Settings__c Coolset = new CS_RO_Cooler_Settings__c ();
        Coolset.CS_RT_Tracing_Case__c='CS_RT_Tracing_Case__c';
		insert Coolset;

        CS_CASECLOSE_CLASS oClose = new CS_CASECLOSE_CLASS();
        CS_CASECLOSE_CLASS.CloseCase(oNewCase.Id, oNewCaseForce.Id , 'Close Test', 'Registro Incompleto','CS_RT_Tracing_Case__c');
     }
}