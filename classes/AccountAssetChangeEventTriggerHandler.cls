public class AccountAssetChangeEventTriggerHandler {
    
    public static final String DR_ACCOUNT_RECORD_TYPE = 'Account_DO';
    List<ONTAP__Account_Asset__ChangeEvent> accountAssetChangeEvents = new List<ONTAP__Account_Asset__ChangeEvent>();
    List<Id> accountIds = new List<Id>();
    Map<String,String> accountIdRecordTypeDeveloperNameMap = new Map<String,String>();
    List<DRAccountAssetOutboundMessage__e> accountAssetOutboundMessages = new List<DRAccountAssetOutboundMessage__e>();
    
    public AccountAssetChangeEventTriggerHandler( List<ONTAP__Account_Asset__ChangeEvent> accountAssetChangeEvents ){   
        
        this.accountAssetChangeEvents = accountAssetChangeEvents;
        
    }
    
    public void run(){
        
        this.setAccountList();
        this.mapAccounts();
		this.mapEvents();
        this.publish();      
        
    }
    
    private void mapEvents(){     
        
        for ( ONTAP__Account_Asset__ChangeEvent accountAssetChangeEvent : accountAssetChangeEvents ){
            
            if( verify( accountAssetChangeEvent ) ){
                
  				accountAssetOutboundMessages.add( newAccountAssetOutboundMessage( accountAssetChangeEvent ) );
                
            }
            
        }       
    }

    private Boolean verify( ONTAP__Account_Asset__ChangeEvent accountAssetChangeEvent ){
        
        return true;
        
    }    
    
    private void setAccountList(){
        
        for( ONTAP__Account_Asset__ChangeEvent accountAssetChangeEvent : accountAssetChangeEvents ){
            if ( accountAssetChangeEvent.ChangeEventHeader.getChangeType() == 'UPDATE' ){
                this.accountIds.add( accountAssetChangeEvent.ONTAP__Account__c );              
            } 
        }     
        
    }
    
    private void mapAccounts(){   
        
        this.accountIdRecordTypeDeveloperNameMap = DRAccountSelector.getAccountIdRecordTypeDeveloperNameMap( accountIds );
        
    }
    
    private DRAccountAssetOutboundMessage__e newAccountAssetOutboundMessage( ONTAP__Account_Asset__ChangeEvent accountAssetChangeEvent ){   
        
        DRAccountAssetOutboundMessage__e newAccountAssetOutboundMessage = new DRAccountAssetOutboundMessage__e(
        	AssetCode__c = accountAssetChangeEvent.ONTAP__Asset_Code__c,
            AssetStatus__c = accountAssetChangeEvent.ONTAP__Asset_Status__c,
            SAPCustomerID__c = accountAssetChangeEvent.Account_SAP_ID__c,
            UndetectedReason__c = accountAssetChangeEvent.ONTAP__Undetected_Reason__c,
            UndetectedReasonComment__c = accountAssetChangeEvent.ONTAP__Undetected_Reason_Comment__c
        );
        
        return newAccountAssetOutboundMessage;
    }
    
    private void publish(){
        
         List<Database.SaveResult> accountAssetResults = EventBus.publish( accountAssetOutboundMessages );  
        
    }
    
}