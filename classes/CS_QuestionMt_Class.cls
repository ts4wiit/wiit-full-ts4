/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_QUESTIONMT_CLASS.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 01/02/2019     Jose Luis Vargas             Creacion de la clase para el guardado de preguntas de las tipificaciones
 */

public class CS_QuestionMt_Class 
{
    public CS_QuestionMt_Class() { }
    
     /**
     * Method for consulting the level 1 for typification for country
     * @author: jose.l.vargas.lara@accenture.com
     * @param Void
     * @return List with the typification level 1 for country
     */
     @AuraEnabled
     public static List<CS_CaseTypification_Class.PicklistValues> GetTypificationN1()
     {
         return CS_CaseTypification_Class.GetTypificationN1();
     }
     
     /**
     * Method for consulting the level 2 for typification for country
     * @author: jose.l.vargas.lara@accenture.com
     * @param Typification selected for level 1
     * @return List with the typification level 2 for country
     */
     @AuraEnabled
     public static List<CS_CaseTypification_Class.PicklistValues> GetTypificationN2(string typificationNivel1)
     {
       return CS_CaseTypification_Class.GetTypificationN2(typificationNivel1);
     }
     
     /**
     * Method for consulting the level 3 for typification for country
     * @author: jose.l.vargas.lara@accenture.com
     * @param Typification selected for level 1, typification selected for level 2
     * @return List with the typification level 3 for country
     */
     @AuraEnabled
     public static List<CS_CaseTypification_Class.PicklistValues> GetTypificationN3(string typificationNivel1, string typificationNivel2)
     {
         return CS_CaseTypification_Class.GetTypificationN3(typificationNivel1, typificationNivel2);
     }
     
     /**
     * Method for consulting the level 4 for typification for country
     * @author: jose.l.vargas.lara@accenture.com
     * @param Typification selected for level 1, typification selected for level 2, typification selected for level 3
     * @return List with the typification level 4 for country
     */
     @AuraEnabled
     public static List<CS_CaseTypification_Class.PicklistValues> GetTypificationN4(string typificationNivel1, string typificationNivel2, string typificationNivel3)
     {
         return CS_CaseTypification_Class.GetTypificationN4(typificationNivel1, typificationNivel2, typificationNivel3);
     }
    
    /**
     * Method for consulting values of question type 
     * @author: d.zacarias.cantillo@accenture.com
     * @param void
     * @return List with the picklist values
     */
    @AuraEnabled
    public static List<String> getPickListValuesIntoList()
    {
        List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = CS_Question_Typification_Matrix__c.CS_Question_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        pickListValuesList.add('');
        
        for(Schema.PicklistEntry pickListVal : ple)
        {
            pickListValuesList.add(pickListVal.getLabel());
        }
        return pickListValuesList;
    }
    
    /**
     * Method for create a new Question
     * @author: d.zacarias.cantillo@accenture.com
     * @param typification selected for level 1, level 2, level 3, level 4, question, question type and options 
     * @return Boolean to help us know the status(Question created or not)
     */
    @AuraEnabled
    public static boolean createNewQuestion(String typificationN1, String typificationN2, String typificationN3, String typificationN4, String question, String qType, String qOptions)
    {
        string userCountry = '';
        boolean statusProceso = true;
        ISSM_TypificationMatrix__c oDetailTypification = new ISSM_TypificationMatrix__c();
        
        try
        {
            User currentUser = [SELECT Country__c FROM USER WHERE Id =: UserInfo.getUserId()];
            userCountry = currentUser.Country__c;
            
            oDetailTypification = [SELECT Id, ISSM_TypificationLevel1__c, ISSM_TypificationLevel2__c, ISSM_TypificationLevel3__c, ISSM_TypificationLevel4__c,  ISSM_Countries_ABInBev__c 
                                   FROM ISSM_TypificationMatrix__c 
                                   WHERE ISSM_TypificationLevel1__c =: typificationN1 
                                   AND ISSM_TypificationLevel2__c =: typificationN2 
                                   AND ISSM_TypificationLevel3__c =: typificationN3 
                                   AND  ISSM_TypificationLevel4__c =: typificationN4 
                                   AND ISSM_Countries_ABInBev__c =: userCountry 
                                   LIMIT 1];
            
            qType =  GetPickListValue(qType);                  
            CS_Question_Typification_Matrix__c newQuestion = new CS_Question_Typification_Matrix__c
            (
                CS_Country__c = oDetailTypification.ISSM_Countries_ABInBev__c,
                CS_Question__c= question,
                CS_Question_Type__c = qType,
                CS_Options__c = qOptions,
                CS_Typification_Level1__c = oDetailTypification.ISSM_TypificationLevel1__c,
                CS_Typification_Level2__c = oDetailTypification.ISSM_TypificationLevel2__c,
                CS_Typification_Level3__c = oDetailTypification.ISSM_TypificationLevel3__c,
                CS_Typification_Level4__c = oDetailTypification.ISSM_TypificationLevel4__c,
			    CS_Typification_Matrix_Number__c = oDetailTypification.Id
            );
            
            Insert newQuestion;
        }
        catch(Exception ex)
        {
            statusProceso = false;
            System.debug('CS_QUESTIONMT_CLASS.createNewQuestion  Message: ' + ex.getMessage());   
            System.debug('CS_QUESTIONMT_CLASS.createNewQuestion  Cause: ' + ex.getCause());   
            System.debug('CS_QUESTIONMT_CLASS.createNewQuestion  Line number: ' + ex.getLineNumber());   
            System.debug('CS_QUESTIONMT_CLASS.createNewQuestion  Stack trace: ' + ex.getStackTraceString());
        }
        
        return statusProceso;    
    }
    
    /**
    * Method for create a new Question
    * @author: d.zacarias.cantillo@accenture.com
    * @param typification selected for level 1, level 2, level 3, level 4, question, question type and options 
    * @return Boolean to help us know the status(Question created or not)
    */
    private static string GetPickListValue(string qType)
    {
        string pickListValue = '';
        Schema.DescribeFieldResult oFieldPickList = CS_Question_Typification_Matrix__c.CS_Question_Type__c.getDescribe();
        List<Schema.PicklistEntry> lstPickListValues = oFieldPickList.getPickListValues();     
        for(Schema.PicklistEntry oValue : lstPickListValues)
        {
            if(oValue.getLabel() == qType)
                pickListValue = oValue.getValue();
        }
        
        return pickListValue;
    }
}