/* ----------------------------------------------------------------------------
* AB InBev :: 360 View
* ----------------------------------------------------------------------------
* Clase: V360_InterlocutorTools.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 12/02/2018     Gerardo Martinez        Creation of methods.
* 12/12/2019     Daniel Hernandez.       Assign the BDR user as owner if the account don't have a presales relationship
*/

public class V360_InterlocutorTools {
    
    
  /**
    * Method wich assing an interlocutor by customer
    * Created By: g.martinez.cabral@accenture.com
    * @param new customers modified or inserted
    * @return void
    */
    public static void AssignInterlocutorBeforeUpdate(List<account> newList){
        
        Set<Id> zones = new Set<Id>();
        
        for (Account acc : newList){
            
            if (acc.V360_SalesZoneAssignedBDR__c != NULL){
                zones.add(acc.V360_SalesZoneAssignedBDR__c);
            }
            if( acc.V360_SalesZoneAssignedTelesaler__c != NULL){
                zones.add(acc.V360_SalesZoneAssignedTelesaler__c);
            }
            if( acc.V360_SalesZoneAssignedPresaler__c != NULL){
                zones.add(acc.V360_SalesZoneAssignedPresaler__c);
            }
            if( acc.V360_SalesZoneAssignedCredit__c != NULL){   
                zones.add(acc.V360_SalesZoneAssignedCredit__c);
            }
            if( acc.V360_SalesZoneAssignedCoolers__c != NULL){         
                zones.add(acc.V360_SalesZoneAssignedCoolers__c);
            }
            if( acc.V360_SalesZoneAssignedTellecolector__c != NULL){ 
                zones.add(acc.V360_SalesZoneAssignedTellecolector__c);
            }
            
        }
        
        Map<Id,V360_SalerPerZone__c> mapSalerPerZone = new Map<Id, V360_SalerPerZone__c>([SELECT Id, V360_User__c, V360_SalesZone__r.Parent.V360_TeamLead__c, V360_SalesZone__r.Parent.Parent.V360_Manager__c,V360_SalesZone__r.Parent.Parent.Parent.V360_Director__c FROM V360_SalerPerZone__c WHERE Id IN:zones]);
       
        for (Account acc :  newList){
            /** Agents Asgination **/ 
            if( acc.V360_SalesZoneAssignedBDR__c != NULL && mapSalerPerZone.containsKey(acc.V360_SalesZoneAssignedBDR__c) && mapSalerPerZone.get(acc.V360_SalesZoneAssignedBDR__c).V360_User__c != NULL){
                acc.V360_BDR__c = mapSalerPerZone.get(acc.V360_SalesZoneAssignedBDR__c).V360_User__c;
                if((acc.V360_SalesZoneAssignedPresaler__c == NULL && acc.ONTAP__ExternalKey__c.startsWith(Label.CS_CountryCodeSV)) || !acc.ONTAP__ExternalKey__c.startsWith(Label.CS_CountryCodeSV)){ // TS4. DHD: se modifica esta línea junto a la validación de abajo para presales
                    acc.OwnerId = mapSalerPerZone.get(acc.V360_SalesZoneAssignedBDR__c).V360_User__c;
                }
            }
            if( acc.V360_SalesZoneAssignedTelesaler__c != NULL && mapSalerPerZone.containsKey(acc.V360_SalesZoneAssignedTelesaler__c) && mapSalerPerZone.get(acc.V360_SalesZoneAssignedTelesaler__c).V360_User__c != NULL){
                acc.V360_TelesalesAgent__c = mapSalerPerZone.get(acc.V360_SalesZoneAssignedTelesaler__c).V360_User__c;
            }
            if( acc.V360_SalesZoneAssignedPresaler__c != NULL && mapSalerPerZone.containsKey(acc.V360_SalesZoneAssignedPresaler__c) && mapSalerPerZone.get(acc.V360_SalesZoneAssignedPresaler__c).V360_User__c != NULL){                     
                acc.V360_Presaler__c = mapSalerPerZone.get(acc.V360_SalesZoneAssignedPresaler__c).V360_User__c;
                if(// TS4. DHD: Modificación realizada el 13/02/2019 a solicutud de Hilario Medina. Otros países pueden tener un BDR y Presales a la vez, pero el BDR es el único owner. En El Salvador no coexisten los 2 modelos de servicio.
                    acc.ONTAP__ExternalKey__c != null && acc.ONTAP__ExternalKey__c.startsWith(Label.CS_CountryCodeSV)
                ){
                	acc.OwnerId = mapSalerPerZone.get(acc.V360_SalesZoneAssignedPresaler__c).V360_User__c;
                }
            }
            if( acc.V360_SalesZoneAssignedCredit__c != NULL && mapSalerPerZone.containsKey(acc.V360_SalesZoneAssignedCredit__c) && mapSalerPerZone.get(acc.V360_SalesZoneAssignedCredit__c).V360_User__c != NULL){                     
                acc.ISSM_BillingManager__c = mapSalerPerZone.get(acc.V360_SalesZoneAssignedCredit__c).V360_User__c;
            }
            if( acc.V360_SalesZoneAssignedCoolers__c != NULL && mapSalerPerZone.containsKey(acc.V360_SalesZoneAssignedCoolers__c) && mapSalerPerZone.get(acc.V360_SalesZoneAssignedCoolers__c).V360_User__c != NULL){                     
                acc.ISSM_BossRefrigeration__c = mapSalerPerZone.get(acc.V360_SalesZoneAssignedCoolers__c).V360_User__c;
            }
            if( acc.V360_SalesZoneAssignedTellecolector__c != NULL && mapSalerPerZone.containsKey(acc.V360_SalesZoneAssignedTellecolector__c) && mapSalerPerZone.get(acc.V360_SalesZoneAssignedTellecolector__c).V360_User__c != NULL){                     
                acc.V360_Tellecolector__c = mapSalerPerZone.get(acc.V360_SalesZoneAssignedTellecolector__c).V360_User__c;
            }
            
            /** Supervisor Assignation **/ 
            
            if( acc.V360_SalesZoneAssignedTelesaler__c != NULL && mapSalerPerZone.containsKey(acc.V360_SalesZoneAssignedTelesaler__c) && mapSalerPerZone.get(acc.V360_SalesZoneAssignedTelesaler__c).V360_SalesZone__r.Parent.V360_TeamLead__c != NULL){
                acc.V360_TelesalesSupervisor__c = mapSalerPerZone.get(acc.V360_SalesZoneAssignedTelesaler__c).V360_SalesZone__r.Parent.V360_TeamLead__c;
            }
            if( acc.V360_SalesZoneAssignedPresaler__c != NULL && mapSalerPerZone.containsKey(acc.V360_SalesZoneAssignedPresaler__c) && mapSalerPerZone.get(acc.V360_SalesZoneAssignedPresaler__c).V360_SalesZone__r.Parent.V360_TeamLead__c != NULL){                     
                acc.V360_TeamLead__c = mapSalerPerZone.get(acc.V360_SalesZoneAssignedPresaler__c).V360_SalesZone__r.Parent.V360_TeamLead__c;
            }
            if( acc.V360_SalesZoneAssignedCredit__c != NULL && mapSalerPerZone.containsKey(acc.V360_SalesZoneAssignedCredit__c) && mapSalerPerZone.get(acc.V360_SalesZoneAssignedCredit__c).V360_SalesZone__r.Parent.V360_TeamLead__c != NULL){                     
               acc.V360_CreditBoss__c = mapSalerPerZone.get(acc.V360_SalesZoneAssignedCredit__c).V360_SalesZone__r.Parent.V360_TeamLead__c;
            }
            
            /** Manager Assignation **/ 
            
            if( acc.V360_SalesZoneAssignedTelesaler__c != NULL && mapSalerPerZone.containsKey(acc.V360_SalesZoneAssignedTelesaler__c) && mapSalerPerZone.get(acc.V360_SalesZoneAssignedTelesaler__c).V360_SalesZone__r.Parent.Parent.V360_Manager__c != NULL){
                acc.V360_TelesalesManager__c = mapSalerPerZone.get(acc.V360_SalesZoneAssignedTelesaler__c).V360_SalesZone__r.Parent.Parent.V360_Manager__c;
            }
            if( acc.V360_SalesZoneAssignedPresaler__c  != NULL && mapSalerPerZone.containsKey(acc.V360_SalesZoneAssignedPresaler__c) && mapSalerPerZone.get(acc.V360_SalesZoneAssignedPresaler__c).V360_SalesZone__r.Parent.Parent.V360_Manager__c != NULL){                     
                acc.V360_Manager__c = mapSalerPerZone.get(acc.V360_SalesZoneAssignedPresaler__c).V360_SalesZone__r.Parent.Parent.V360_Manager__c;
            }
            
            /** Director Assignation **/ 
            
            if( acc.V360_SalesZoneAssignedTelesaler__c != NULL && mapSalerPerZone.containsKey(acc.V360_SalesZoneAssignedTelesaler__c) && mapSalerPerZone.get(acc.V360_SalesZoneAssignedTelesaler__c).V360_SalesZone__r.Parent.Parent.Parent.V360_Director__c != NULL){
                acc.V360_TelesalesDirector__c = mapSalerPerZone.get(acc.V360_SalesZoneAssignedTelesaler__c).V360_SalesZone__r.Parent.Parent.Parent.V360_Director__c;
            }
            if( acc.V360_SalesZoneAssignedPresaler__c != NULL && mapSalerPerZone.containsKey(acc.V360_SalesZoneAssignedPresaler__c) && mapSalerPerZone.get(acc.V360_SalesZoneAssignedPresaler__c).V360_SalesZone__r.Parent.Parent.Parent.V360_Director__c!= NULL){                     
                acc.V360_Director__c = mapSalerPerZone.get(acc.V360_SalesZoneAssignedPresaler__c).V360_SalesZone__r.Parent.Parent.Parent.V360_Director__c;
            }
        }
    }
}