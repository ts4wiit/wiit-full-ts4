/*******************************************************************************
Developed by: TS4
Author: Daniel Hernández
Proyect: HONES
Description: Apex class to test methods for HONES_GetStatusBatch_LT functionality

------ ---------- -------------------------- -----------------------------------
No.    Date       Autor                      Description
------ ---------- -------------------------- -----------------------------------
1.0    09/12/2019 Daniel Hernández           Created Class
*******************************************************************************/

@isTest
public class HONES_GetStatusBatch_LT_Test {
    @testSetup static void setup(){
        Account cta = new Account(Name = 'Pupuseria', ONTAP__ExternalKey__c='SVCS0112345678911');
        insert cta;
        RecordType recordTypeOnCall= [SELECT Id FROM RecordType WHERE Name = 'ONCALL' LIMIT 1][0];
        ONCALL__Call__c llamada = new ONCALL__Call__c(Name = 'levantar pedido', RecordTypeId = recordTypeOnCall.Id, ONCALL__POC__c = cta.Id);
        insert llamada;
        ONTAP__Order__c order_withCall = new ONTAP__Order__c(ONCALL__Call__c = llamada.Id, ONCALL__OnCall_Status__c='Closed', ONTAP__OrderAccount__c= cta.Id,
                                                             ONCALL__SAP_Order_Response__c='S - SALES_HEADER_IN procesado con éxito', 
                                                             ONTAP__SAP_Order_Number__c='6202143408');
        insert order_withCall;
    }
    
    
    @isTest static void sendOrder(){
        ONTAP__Order__c orden = [SELECT Id, Name, ONCALL__Call__c, ONTAP__Event_Id__c FROM ONTAP__Order__c LIMIT 10][0];
        
        HONES_GetStatusBatch_LT.getStatusBatch(orden.Id);
        
        HONES_GetStatusBatch_LT.ResponseElement re = new HONES_GetStatusBatch_LT.ResponseElement();
        List<HONES_GetStatusBatch_LT.ResponseElement> li_re = new List<HONES_GetStatusBatch_LT.ResponseElement>();
        re.Id='123';
        re.orderResponse = 'abc';
        re.sapNumber = '0000';
        re.parse('{"make":"SFDC","year":"2020"}');
        li_re.add(re);
        HONES_GetStatusBatch_LT.ResponseClass rc = new HONES_GetStatusBatch_LT.ResponseClass();
        rc.result = li_re;
        rc.parse('{"make":"SFDC","year":"2020"}');
    }
    
    @isTest static void callout(){
        ONTAP__Order__c orden = [SELECT Id, Name, ONCALL__Call__c, ONTAP__Event_Id__c FROM ONTAP__Order__c LIMIT 10][0];
        
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('GetResource');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json;charset=UTF-8');
        Test.setMock(HttpCalloutMock.class, new HONES_BlockOrder_HttpCalloutMock());
        Test.startTest();
        HONES_GetStatusBatch_LT.CustomResponse response = HONES_GetStatusBatch_LT.getStatusBatch(orden.Id);
        Test.stopTest();
    }
}