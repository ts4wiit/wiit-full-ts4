@isTest
public class CaseForceCommentEventTriggerHandlerTest {
    
    @isTest
    static void testCaseCommentEvent(){
        
        String recordTypeDeveloperName;
        
        Account account = DRFixtureFactory.newDRAccount();
        
        Test.startTest();
 
        Test.enableChangeDataCapture();   
        
        recordTypeDeveloperName = CaseForceEventTriggerHandler.DR_SALES_PROCESS_RECORD_TYPE;

        ONTAP__Case_Force__c caseSales = DRFixtureFactory.newDRCase( account.Id, recordTypeDeveloperName , 'No visita vendedor' );
        
        ONTAP__Case_Force_Comment__c caseComment = DRFixtureFactory.newDRCaseComment( caseSales.Id );
        
        Test.getEventBus().deliver();
        
        Test.stopTest();
        
    }     

}