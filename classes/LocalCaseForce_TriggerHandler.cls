public with sharing class LocalCaseForce_TriggerHandler extends Local_TriggerHandler {
	
    public override void afterUpdate() {     
 
        Local_CaseForce_SendNotification.verifyStatusToSendNotifications((Map<Id, ONTAP__Case_Force__c>)trigger.oldmap, newList);
    }
}