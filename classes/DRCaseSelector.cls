public class DRCaseSelector {

    private List<Id> caseIds = new List<Id>();
    public List<ONTAP__Case_Force__c> cases { get; set; }
    private Set<Id> recordTypeIds = new Set<Id>();
    private Map<String,String> caseIdTOOwnerIdMap = new Map<String,String>();
    private List<Id> userIds = new List<Id>();
    
    public DRCaseSelector( List<Id> caseIds ){
        this.caseIds = caseIds;
        this.setRecordTypeIds();
        this.setCases();
    }
    
    private void setRecordTypeIds(){
        
        Set<Id> recordTypeIds = new Set<Id>();
        
        List<RecordType> recordTypes = [ SELECT Id,IsActive,Name,SobjectType FROM RecordType WHERE SobjectType = 'ONTAP__Case_Force__c' AND DeveloperName like 'DO%'  ];
                
        for( RecordType recorType : recordTypes ){
            
            this.recordTypeIds.add( recorType.Id );
            
        }
        
    }
    
    private void setCases(){
               
        this.cases = [ SELECT Id, ONTAP__Case_Number__c, ONTAP__Case_Origin__c, CreatedDate, ONTAP__Account__r.OwnerId,Payment_Method__c,Payment_Terms__c,
                              ONTAP__Description__c, OwnerId, Account_SAP_ID__c, ONTAP__Status__c, Case_Type__c, RecordType.DeveloperName,
                              Account_Asset__r.ONTAP__Asset_Code__c,ONTAP__Account__r.Owner.ONTAP__Country_Alias__c,LastModifiedDate,ONTAP__Account__r.ONTAP__SAPCustomerId__c
                         FROM ONTAP__Case_Force__c
                        WHERE Id in : caseIds 
                          AND RecordTypeId in : recordTypeIds ];
        
        for ( ONTAP__Case_Force__c forceCase : this.cases ){
            
            caseIdTOOwnerIdMap.put( forceCase.Id, forceCase.OwnerId );
            userIds.add( forceCase.OwnerId );
            
        }
                  
    }
    
    public Map<Id,ONTAP__Case_Force__c> filterByRecordTypeName( String recordTypeDeveloperName ){
        
        Map<Id,ONTAP__Case_Force__c> filteredCasesMap = new Map<Id,ONTAP__Case_Force__c>();
        
        for ( ONTAP__Case_Force__c forceCase : cases ){
            
            if ( forceCase.RecordType.DeveloperName == recordTypeDeveloperName ){
                
                filteredCasesMap.put( forceCase.Id, forceCase );
                
            }
            
        }
        
        return filteredCasesMap;
        
    }
    
    public Map<String,String> getCaseIdToBRDIdMap(){
        
        Map<String,String> caseIdToBDRUserIdMap = new Map<String,String>();
        
        for ( ONTAP__Case_Force__c forceCase : this.cases ){
            
            caseIdToBDRUserIdMap.put( forceCase.Id, forceCase.ONTAP__Account__r.OwnerId );
            
        }
        
        return caseIdToBDRUserIdMap;
        
    }    
    
    public Map<String,String> getCaseIdToRouteMap(){
        
        Map<String,String> caseIdToRouteMap = new Map<String,String>();
        
        Map<String,String> userIdToRouteMap = DRUserSelector.getUsersIdToRouteMap( userIds );
        
        for ( ONTAP__Case_Force__c forceCase : this.cases ){
            
            caseIdToRouteMap.put( forceCase.Id, userIdToRouteMap.get( forceCase.OwnerId ) );
            
        }
        
        return caseIdToRouteMap;
        
    }    
    
}