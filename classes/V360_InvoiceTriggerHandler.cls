/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_InvoiceTriggerHandler.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Gerardo Martinez        Creation of methods.
 */
public class V360_InvoiceTriggerHandler extends TriggerHandlerCustom{
	private Map<Id, ONCALL__Invoice__c> newMap;
    private Map<Id, ONCALL__Invoice__c> oldMap;
    private List<ONCALL__Invoice__c> newList;
    private List<ONCALL__Invoice__c> oldList;
    
    /**
    * Constructor of the class
    * Created By: g.martinez.cabral@accenture.com
    * @param New values of Product modified
    * @return void
    */
    public V360_InvoiceTriggerHandler() {
        
        this.newMap = (Map<Id, ONCALL__Invoice__c>) Trigger.newMap;
        this.oldMap = (Map<Id, ONCALL__Invoice__c>) Trigger.oldMap;
        this.newList = (List<ONCALL__Invoice__c>) Trigger.new;
        this.oldList = (List<ONCALL__Invoice__c>) Trigger.old;
    }
    
    /**
    * Method wich is executed every time a product is inserted
    * Created By: g.martinez.cabral@accenture.com
    * @param New values of Product modified
    * @return void
    */
    public override void beforeInsert(){
        /*
	 	* No need for code if accounts are linked using an External Id-Unique
        * To relate the account use instead ONTAP__Codigo_del_cliente__c
        * ONTAP__SAP_Number__c is not indexed in current ONTAP Package and will cause a non-selective query with +200,000 records
        * drs@avx
		*/
		/*
         List<ONCALL__Invoice__c> invoicelist = PreventExecutionUtil.validateInvoiceWithOutId(this.newList);
         relateAccountsToInvoice(invoicelist);
		*/
     }
    
    /**
    * Method wich is executed every time a product is updated
    * Created By: g.martinez.cabral@accenture.com
    * @param New values of Product modified
    * @return void
    */
    public override void beforeUpdate(){
        /*
		 * No need for code if accounts are linked using an External Id-Unique
 		* To relate the account use instead ONTAP__Codigo_del_cliente__c
         * ONTAP__SAP_Number__c is not indexed in current ONTAP Package and will cause a non-selective query with +200,000 records
         * drs@avx
         */
		/*
        List<ONCALL__Invoice__c> invoicelist = PreventExecutionUtil.validateInvoiceWithId(this.newMap);
        relateAccountsToInvoice(invoicelist);
        */
    }
    
    /**
    * Method for relate an Account whit their invoice
    * Created By: g.martinez.cabral@accenture.com
    * @param New values of Product modified
    * @return void
    */

    /*
	 * No need for code if accounts are linked using an External Id-Unique
     * To relate the account use instead ONTAP__Codigo_del_cliente__c
     * ONTAP__SAP_Number__c is not indexed in current ONTAP Package and will cause a non-selective query with +200,000 records
     * drs@avx
     */

    /*
    public static void relateAccountsToInvoice(List<ONCALL__Invoice__c> newlist){
        Set<String> SAPNumsToRelate = new Set<String>();
        Map<String,Account> accReference = new Map<String,Account>();
        
        for (ONCALL__Invoice__c accInvoice:newlist){
            if(accInvoice.V360_CustomerSAPId__c!=NULL){
                SAPNumsToRelate.add(accInvoice.V360_CustomerSAPId__c);
            }
        }
        
        for (List<Account> accs : [SELECT Id, ONTAP__SAP_Number__c FROM Account WHERE ONTAP__SAP_Number__c IN:SAPNumsToRelate]){
            for (Account acc : accs){
                 accReference.put(acc.ONTAP__SAP_Number__c,acc);
            }            
        }
        
        System.debug('accReference'+accReference);
      
        
        for (ONCALL__Invoice__c finalInvoice:newlist){
           Account acc = accReference.get(finalInvoice.V360_CustomerSAPId__c);
            if(acc!= NULL){           
                finalInvoice.ONCALL__POC_Account__c = acc.Id;
			}
           System.debug('Flex Data: '+finalInvoice);
        }
    }
    */
}