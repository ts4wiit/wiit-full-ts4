@isTest
public class DRVisitsHelper_Test {
    
    @isTest
    public static void testWeekNumber(){
        
        Date myDate = Date.newInstance(2020, 1, 8);
        Integer weekNumber = DRVisitHelper.getWeekNumber( myDate );
        System.assertEquals(2, weekNumber, 'Assertion failed on week number :(');
        
    }
    
    @isTest
    public static void testIsEven(){
        
        Integer i = 2;
        Boolean isEven = DRVisitHelper.isEven(i);
        System.assertEquals(true, isEven, 'Assertion failed on isEven evaluation');
        
        i = 3;
        isEven = DRVisitHelper.isEven(i);
        System.assertEquals(false, isEven, 'Assertion failed on isEven evaluation');
        
    }

}