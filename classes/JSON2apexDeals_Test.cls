/* ----------------------------------------------------------------------------
 * AB InBev :: Oncall
 * ----------------------------------------------------------------------------
 * Clase: Applieddeals_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 10/01/2018        Luis Parra            Creation of methods.
 */
@isTest
private class JSON2apexDeals_Test{
     
    /**
    * Test method for parse the json
    * Created By: luis.arturo.parra@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void test_parse_UseCase1(){
    	JSON2apexDeals dealss= new JSON2apexDeals();
     	//dealss.accountId='acc';
        //dealss.salesOrg='org';
        //dealss.deals=new List<JSON2apexDeals.Deals>();
        //JSON2ApexDeals.parse('{"outputMessages":[],"order":{"country":"SV","accountId":"0011820881","salesOrg":"CS01","paymentMethod":"CASH","total":1.5,"subtotal":1.5,"tax":0,"discount":-0.27,"empties":{"totalAmount":0,"minimumRequired":0,"extraAmount":0},"items":[{"sku":"000000000000009888","quantity":1,"freeGood":false,"discount":-0.27,"tax":0,"subtotal":1.5,"total":1.5,"price":1.5,"unitPrice":1.5}]}}');
        
        //Added drs@avx 
        
        JSON2ApexDeals.parse('{"promotionNumber":"249032","title":"PRUEBA MALTA DC","validfrom":"2019-02-13T00:00:00.000Z","validto":"9999-12-31T00:00:00.000Z"}');
     }
}