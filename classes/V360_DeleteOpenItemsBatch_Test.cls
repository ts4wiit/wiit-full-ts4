/* ----------------------------------------------------------------------------
* AB InBev :: 360 View
* ----------------------------------------------------------------------------
* Clase: V360_DeleteOpenItemsBatch_Test.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 19/12/2018     Carlos Leal        Creation of methods.
*/
@isTest
public class V360_DeleteOpenItemsBatch_Test {
    /**
* Method test for delete all the open items items data in V360_DeleteOpenItemsBatch.
* @author: c.leal.beltran@accenture.com
* @param void
* @return void
*/
    @isTest static void testBatch(){
        Id V360_OpenItemRT = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_V360_RECORDTYPE_NAME).getRecordTypeId();
        ONTAP__OpenItem__c open = new ONTAP__OpenItem__c(RecordTypeId =V360_OpenItemRT); 
        insert open;
        V360_DeleteOpenItemsBatch obj01 = new V360_DeleteOpenItemsBatch();
        Database.executeBatch(obj01, 200);
    }
}