@isTest
public class DRAccountChangeHelper_Test {
    
    @isTest
    public static void testSmallFiles(){
        
        String attachmentContent = '';
        
        for( Integer i = 0; i <= 50000; i++ ){
            
            attachmentContent = attachmentContent + '$';
            
        }
        
        List<String> parentIds = new List<String>();
        Map<String,List<Attachment>> attachmentMap  = new Map<String,List<Attachment>>();
        Account prospectAccount = DRFixtureFactory.newDRProspect();
        parentIds.add( prospectAccount.Id );
                
        Attachment attachment = new Attachment();   	
    	attachment.Name = 'Unit Test Attachment';
    	Blob bodyBlob = Blob.valueOf( attachmentContent );
    	attachment.body = bodyBlob;
        attachment.parentId = prospectAccount.id;
        insert attachment; 
        
        Attachment attachment2 = new Attachment();   	
    	attachment2.Name = 'Unit Test Attachment 2';
    	Blob bodyBlob2 = Blob.valueOf( attachmentContent );
    	attachment2.body = bodyBlob2;
        attachment2.parentId = prospectAccount.id;
        insert attachment2;     
  
        Attachment attachment3 = new Attachment();   	
    	attachment3.Name = 'Unit Test Attachment 3';
    	Blob bodyBlob3 = Blob.valueOf( attachmentContent );
    	attachment3.body = bodyBlob3;
        attachment3.parentId = prospectAccount.id;
        insert attachment3;           
        
        Account account  = [ SELECT Id,ONTAP__Contact_First_Name__c, ONTAP__Contact_Last_Name__c,ONTAP__District__c, Phone,
                                    ONTAP__Province__c, ONTAP__LegalName__c, Name, RNC_Number__c,SIC, RecordTypeId
                               FROM Account WHERE Id = : prospectAccount.Id ];
        
        DRProspectOutboundMessage__e outboundMessage = new DRProspectOutboundMessage__e( 
        	      AccountId__c = account.Id,
                  ClosestPOCCode__c = 'XXXXXXXXXX',
                  ContactFirstName__c = account.ONTAP__Contact_First_Name__c,
                  ContactLastName__c = account.ONTAP__Contact_Last_Name__c,
                  District__c = account.ONTAP__District__c,
                  Phone__c = account.Phone,
                  Province__c = account.ONTAP__Province__c,
                  LegalName__c = account.ONTAP__LegalName__c,
                  Name__c = account.Name,
                  RNCNumber__c = account.RNC_Number__c,
                  SIC__c = account.SIC,
                  ShippingAddress__c = 'Address'
        );
        
        
        AttachmentSelector selector = new AttachmentSelector( parentIds );
        attachmentMap = selector.getParentIdToAttachmentsMap();        
        
        Test.startTest();
        
        DRProspectOutboundMessage__e prospectEvent = new DRProspectOutboundMessage__e();
        DRAccountChangeHelper accountChangeHelper = new  DRAccountChangeHelper( outboundMessage );
        accountChangeHelper.accountIdToAttachmentsMap = attachmentMap;
        prospectEvent = accountChangeHelper.enrichProspectEvent();  
        System.assertEquals(  EncodingUtil.base64Encode( bodyBlob ).length() , prospectEvent.frontImageURL__c.length() , 'Assertion failed :(');
        
        Test.stopTest();
        
    }

    @isTest
    public static void testMediumFiles(){
        
        String attachmentContent = '';
        
        for( Integer i = 0; i <= 150000; i++ ){
            
            attachmentContent = attachmentContent + '$';
            
        }
        
        List<String> parentIds = new List<String>();
        Map<String,List<Attachment>> attachmentMap  = new Map<String,List<Attachment>>();
        Account prospectAccount = DRFixtureFactory.newDRProspect();
        parentIds.add( prospectAccount.Id );
                
        Attachment attachment = new Attachment();   	
    	attachment.Name = 'Unit Test Attachment';
    	Blob bodyBlob = Blob.valueOf( attachmentContent );
    	attachment.body = bodyBlob;
        attachment.parentId = prospectAccount.id;
        insert attachment; 
        
        Attachment attachment2 = new Attachment();   	
    	attachment2.Name = 'Unit Test Attachment 2';
    	Blob bodyBlob2 = Blob.valueOf( attachmentContent );
    	attachment2.body = bodyBlob2;
        attachment2.parentId = prospectAccount.id;
        insert attachment2;     
  
        Attachment attachment3 = new Attachment();   	
    	attachment3.Name = 'Unit Test Attachment 3';
    	Blob bodyBlob3 = Blob.valueOf( attachmentContent );
    	attachment3.body = bodyBlob3;
        attachment3.parentId = prospectAccount.id;
        insert attachment3;      
        
        
        Account account  = [ SELECT Id,ONTAP__Contact_First_Name__c, ONTAP__Contact_Last_Name__c,ONTAP__District__c, Phone,
                                    ONTAP__Province__c, ONTAP__LegalName__c, Name, RNC_Number__c,SIC, RecordTypeId
                               FROM Account WHERE Id = : prospectAccount.Id ];
        
        DRProspectOutboundMessage__e outboundMessage = new DRProspectOutboundMessage__e( 
        	      AccountId__c = account.Id,
                  ClosestPOCCode__c = 'XXXXXXXXXX',
                  ContactFirstName__c = account.ONTAP__Contact_First_Name__c,
                  ContactLastName__c = account.ONTAP__Contact_Last_Name__c,
                  District__c = account.ONTAP__District__c,
                  Phone__c = account.Phone,
                  Province__c = account.ONTAP__Province__c,
                  LegalName__c = account.ONTAP__LegalName__c,
                  Name__c = account.Name,
                  RNCNumber__c = account.RNC_Number__c,
                  SIC__c = account.SIC,
                  ShippingAddress__c = 'Address'
        );
        
        
        AttachmentSelector selector = new AttachmentSelector( parentIds );
        attachmentMap = selector.getParentIdToAttachmentsMap();        
        
        Test.startTest();
        
        DRProspectOutboundMessage__e prospectEvent = new DRProspectOutboundMessage__e();
        DRAccountChangeHelper accountChangeHelper = new  DRAccountChangeHelper( outboundMessage );
        accountChangeHelper.accountIdToAttachmentsMap = attachmentMap;
        prospectEvent = accountChangeHelper.enrichProspectEvent();   
        System.assertEquals(  EncodingUtil.base64Encode( bodyBlob ).length() , prospectEvent.frontImageURL__c.length() + prospectEvent.frontImageURL2__c.length() , 'Assertion failed :(');
        
        Test.stopTest();
        
    }    
        
    @isTest
    public static void testLargeFiles(){
        
        String attachmentContent = '';
        
        for( Integer i = 0; i <= 250000; i++ ){
            
            attachmentContent = attachmentContent + '$';
            
        }
        
        List<String> parentIds = new List<String>();
        Map<String,List<Attachment>> attachmentMap  = new Map<String,List<Attachment>>();
        Account prospectAccount = DRFixtureFactory.newDRProspect();
        parentIds.add( prospectAccount.Id );
                
        Attachment attachment = new Attachment();   	
    	attachment.Name = 'Unit Test Attachment';
    	Blob bodyBlob = Blob.valueOf( attachmentContent );
    	attachment.body = bodyBlob;
        attachment.parentId = prospectAccount.id;
        insert attachment; 
        
        Attachment attachment2 = new Attachment();   	
    	attachment2.Name = 'Unit Test Attachment 2';
    	Blob bodyBlob2 = Blob.valueOf( attachmentContent );
    	attachment2.body = bodyBlob2;
        attachment2.parentId = prospectAccount.id;
        insert attachment2;     
  
        Attachment attachment3 = new Attachment();   	
    	attachment3.Name = 'Unit Test Attachment 3';
    	Blob bodyBlob3 = Blob.valueOf( attachmentContent );
    	attachment3.body = bodyBlob3;
        attachment3.parentId = prospectAccount.id;
        insert attachment3;      
        
        
        Account account  = [ SELECT Id,ONTAP__Contact_First_Name__c, ONTAP__Contact_Last_Name__c,ONTAP__District__c, Phone,
                                    ONTAP__Province__c, ONTAP__LegalName__c, Name, RNC_Number__c,SIC, RecordTypeId
                               FROM Account WHERE Id = : prospectAccount.Id ];
        
        DRProspectOutboundMessage__e outboundMessage = new DRProspectOutboundMessage__e( 
        	      AccountId__c = account.Id,
                  ClosestPOCCode__c = 'XXXXXXXXXX',
                  ContactFirstName__c = account.ONTAP__Contact_First_Name__c,
                  ContactLastName__c = account.ONTAP__Contact_Last_Name__c,
                  District__c = account.ONTAP__District__c,
                  Phone__c = account.Phone,
                  Province__c = account.ONTAP__Province__c,
                  LegalName__c = account.ONTAP__LegalName__c,
                  Name__c = account.Name,
                  RNCNumber__c = account.RNC_Number__c,
                  SIC__c = account.SIC,
                  ShippingAddress__c = 'Address'
        );
        
        
        AttachmentSelector selector = new AttachmentSelector( parentIds );
        attachmentMap = selector.getParentIdToAttachmentsMap();        
        
        Test.startTest();
        
        DRProspectOutboundMessage__e prospectEvent = new DRProspectOutboundMessage__e();
        DRAccountChangeHelper accountChangeHelper = new  DRAccountChangeHelper( outboundMessage );
        accountChangeHelper.accountIdToAttachmentsMap = attachmentMap;
        prospectEvent = accountChangeHelper.enrichProspectEvent();   
        System.assertEquals(  EncodingUtil.base64Encode( bodyBlob ).length() , prospectEvent.frontImageURL__c.length() + prospectEvent.frontImageURL2__c.length() + prospectEvent.frontImageURL3__c.length() , 'Assertion failed :(');
        
        Test.stopTest();
        
    } 
    
    @isTest
    public static void testTooLargeFiles(){
        
        String attachmentContent = '$$$$$$$$$$$$$$$$$$$$$$$$';
        
        for( Integer i = 0; i <= 40000; i++ ){
            
            attachmentContent = attachmentContent + '$$$$$$$$$$$$$$$$$$$$$$$$';
            
        }
        
        List<String> parentIds = new List<String>();
        Map<String,List<Attachment>> attachmentMap  = new Map<String,List<Attachment>>();
        Account prospectAccount = DRFixtureFactory.newDRProspect();
        parentIds.add( prospectAccount.Id );
                
        Attachment attachment = new Attachment();   	
    	attachment.Name = 'Unit Test Attachment';
    	Blob bodyBlob = Blob.valueOf( attachmentContent );
    	attachment.body = bodyBlob;
        attachment.parentId = prospectAccount.id;
        insert attachment; 
        
        Attachment attachment2 = new Attachment();   	
    	attachment2.Name = 'Unit Test Attachment 2';
    	Blob bodyBlob2 = Blob.valueOf( attachmentContent );
    	attachment2.body = bodyBlob2;
        attachment2.parentId = prospectAccount.id;
        insert attachment2;     
  
        Attachment attachment3 = new Attachment();   	
    	attachment3.Name = 'Unit Test Attachment 3';
    	Blob bodyBlob3 = Blob.valueOf( attachmentContent );
    	attachment3.body = bodyBlob3;
        attachment3.parentId = prospectAccount.id;
        insert attachment3; 
        
        Account account  = [ SELECT Id,ONTAP__Contact_First_Name__c, ONTAP__Contact_Last_Name__c,ONTAP__District__c, Phone,
                                    ONTAP__Province__c, ONTAP__LegalName__c, Name, RNC_Number__c,SIC, RecordTypeId
                               FROM Account WHERE Id = : prospectAccount.Id ];
        
        DRProspectOutboundMessage__e outboundMessage = new DRProspectOutboundMessage__e( 
        	      AccountId__c = account.Id,
                  ClosestPOCCode__c = 'XXXXXXXXXX',
                  ContactFirstName__c = account.ONTAP__Contact_First_Name__c,
                  ContactLastName__c = account.ONTAP__Contact_Last_Name__c,
                  District__c = account.ONTAP__District__c,
                  Phone__c = account.Phone,
                  Province__c = account.ONTAP__Province__c,
                  LegalName__c = account.ONTAP__LegalName__c,
                  Name__c = account.Name,
                  RNCNumber__c = account.RNC_Number__c,
                  SIC__c = account.SIC,
                  ShippingAddress__c = 'Address'
        );
        
        
        AttachmentSelector selector = new AttachmentSelector( parentIds );
        attachmentMap = selector.getParentIdToAttachmentsMap();        
        
        Test.startTest();
        
        DRProspectOutboundMessage__e prospectEvent = new DRProspectOutboundMessage__e();
        DRAccountChangeHelper accountChangeHelper = new  DRAccountChangeHelper( outboundMessage );
        accountChangeHelper.accountIdToAttachmentsMap = attachmentMap;
        prospectEvent = accountChangeHelper.enrichProspectEvent();   
        System.assertEquals( 'Image too large' , prospectEvent.frontImageURL__c , 'Assertion failed :(');
        
        Test.stopTest();
        
    }    
    
}