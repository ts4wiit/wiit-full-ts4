/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_TaskCtrl.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Gerardo Martinez        Creation of methods.
 */
public class V360_TaskCtrl {

    /**
    * Method to get all the tasks related to an account
    * Created By: g.martinez.cabral@accenture.com
    * @param String accId
    * @return List<Task>
    */
    @AuraEnabled
    public static List<Task> getTasksByAccount(String accId) {
        List<Task> lstTask =  [SELECT Id, Subject, ActivityDate, Owner.Alias FROM Task WHERE WhatId =:accId Order By ActivityDate DESC LIMIT 2000];
        return lstTask;
    }
    
}