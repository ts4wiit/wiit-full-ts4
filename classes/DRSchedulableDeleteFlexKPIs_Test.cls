/**
 * Created by Dejair.Junior on 9/19/2019.
 */
@isTest
public class DRSchedulableDeleteFlexKPIs_Test {

    public static String CRON_EXP = '0 0 0 15 3 ? 2022';

    @testSetup
    static void setupFlexKPIs() {

        Account account = DRFixtureFactory.newDRAccount();
        User bdr = DRFixtureFactory.newBDRUser( 'bdr@xyz.com.do', 'bdrtest' );
        List<Flexible_KPI__c> kpis = new List<Flexible_KPI__c>();

        for ( Integer i = 0; i < 2000; i++ ){

            Flexible_KPI__c flexKPI = new Flexible_KPI__c( Account__c = account.Id, Country_Code__c = 'DO', Name = 'Test ' + i, Value__c = 'Value ' + i , Route__c = bdr.User_Route__c);
            kpis.add( flexKPI );

        }

        insert kpis;

    }

    @isTest
    static void testScheduledJob(){

        Test.startTest();

        String jobId = System.schedule('DRSchedulableDeleteFlexKPIs',CRON_EXP , new DRSchedulableDeleteFlexKPIs());

        Test.stopTest();

    }

}