/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: MockHttpResponseGeneratorDealOrder.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 07/01/2019           Heron Zurita           Creation of methods.
*/

@isTest
global class MockHttpResponseGeneratorDealOrder implements HttpCalloutMock {
    // Implement this interface method
    /*
     * Method that create HTTPResponse where verify the correct answer 
     * Created By:heron.zurita@accenture.com
     * @param HTTPRequest req
     * @return HTTPResponse 
*/
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        System.assertEquals('https://abco-priceengine.herokuapp.com/dealslist', req.getEndpoint());
        System.assertEquals('POST', req.getMethod());
        
        // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"deals":[{"pronr":"248856","proportional":false,"optional":false,"validfrom":"2018-12-17T00:00:00.000Z","validto":"2018-12-31T00:00:00.000Z","description":"2% Descuento por 9891"},{"pronr":"248854","proportional":false,"optional":true,"validfrom":"2018-12-17T00:00:00.000Z","validto":"2019-12-31T00:00:00.000Z","description":"9888 CADA 15 UND 1 UNIDAD GRATIS ADICIONAL"}]}');
        response.setStatusCode(200);
        return response;
    }
    
}