/* ----------------------------------------------------------------------------
* AB InBev :: 
* ----------------------------------------------------------------------------
* Clase: HONES_ONCALL_PartnerListHONES_Test.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 16/01/2019    Ricardo Ortega Zuñiga      Creation of methods.
*/

@isTest
public class HONES_ONCALL_PartnerListHONES_Test {
    
	@isTest static void testDomicilio(){
       
        HONES_ONCALL_PartnerListHONES part = new HONES_ONCALL_PartnerListHONES();
        
        part.role = '1';
        part.numb = '10';
        part.itmNumber = '3232';
        
        HONES_ONCALL_PartnerListHONES.Domicilio d = new HONES_ONCALL_PartnerListHONES.Domicilio();
        d.NAME = 'Calle benito juarez';
        
        part.domicilios = d;
        
    }
}