/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: VisitControlRetraceToursCtrl_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 19/12/2018     Gerardo Martinez        Creation of methods.
 */
@isTest public class VisitControlRetraceToursCtrl_Test {
    
    /**
    * Test method for load the information requiered in the page
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    @isTest static void loadPage(){
        PageReference pageRef = Page.VisitControlRetraceToursPage;
        Test.setCurrentPage(pageRef);
        Id salesOrgRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ORG_RECORDTYPE_NAME).getRecordTypeId();
        Id salesOfficeRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_OFFICE_RECORDTYPE_NAME).getRecordTypeId();
        Id clientGroupdevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.CLIENT_GROUP_RECORDTYPE_NAME).getRecordTypeId();
        Id salesGroupdevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_GROUP_RECORDTYPE_NAME).getRecordTypeId();
        Id salesZonedevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ZONE_RECORDTYPE_NAME).getRecordTypeId();    
        
        Account og =  new Account(Name='OG',ONTAP__ExternalKey__c='OG',RecordTypeId=salesOrgRecordTypeId);
        insert og;
        Account off =  new Account(Name='OF',ONTAP__ExternalKey__c='OF',RecordTypeId=salesOfficeRecordTypeId, ParentId=og.Id);
        insert off;
        Account cg =  new Account(Name='CG',ONTAP__ExternalKey__c='CG',RecordTypeId=clientGroupdevRecordTypeId, ParentId=off.Id);
        insert cg;
        Account sg =  new Account(Name='SG',ONTAP__ExternalKey__c='SG',RecordTypeId=salesGroupdevRecordTypeId, ParentId=cg.Id);
        insert sg;
        Account szz =  new Account(Name='SZ',ONTAP__ExternalKey__c='SZ',RecordTypeId=salesZonedevRecordTypeId, ParentId=sg.Id);
        insert szz;
        
        V360_SalerPerZone__c sz = new V360_SalerPerZone__c(V360_SalesZone__c=szz.id);
        insert sz;
        
        Account acc = new Account(Name='1',V360_SalesZoneAssignedBDR__c=sz.Id,V360_SalesZoneAssignedTelesaler__c=sz.Id,V360_SalesZoneAssignedPresaler__c=sz.Id,V360_SalesZoneAssignedCredit__c=sz.Id,V360_SalesZoneAssignedTellecolector__c=sz.Id,V360_SalesZoneAssignedCoolers__c=sz.Id);           
        
        User u = [Select Id FROM User Limit 1];  

        ONTAP__Route__c rt = New ONTAP__Route__c(RouteManager__c = u.Id,VisitControl_AgentByZone__c =sz.Id, ServiceModel__c='Presales',VisitControl_Bypass__c = true);
        insert rt;
        List<ONTAP__Route__c> listr=  New List<ONTAP__Route__c> ();
        listr.add(rt);
        
        VisitControl_RetraceAndAnticipateRoute__c ob = new VisitControl_RetraceAndAnticipateRoute__c(VisitControl_Route__c=rt.Id,VisitControl_Deleted__c=TRUE,VisitControl_DateToSkip__c=System.today(), VisitControl_RetraceOrAnticipate__c='Anticipate');
        insert ob;
        VisitControl_RetraceAndAnticipateRoute__c ob2 = new VisitControl_RetraceAndAnticipateRoute__c(VisitControl_Route__c=rt.Id,VisitControl_Deleted__c=TRUE,VisitControl_DateToSkip__c=System.today(), VisitControl_RetraceOrAnticipate__c='Anticipate');
		insert ob2;

        
        Set<Id>Ids = new Set <Id>();
        Ids.add(rt.Id);
        
        ApexPages.StandardSetController routes = new ApexPages.StandardSetController(listr);
        routes.setSelected(listr);
        
        VisitControlRetraceToursCtrl controller = new VisitControlRetraceToursCtrl(routes);
        controller.selectedIds =Ids;
        controller.dateSelected = System.today();
        controller.typeOfSkiptSelected = 'Retrace';
        
        ApexPages.StandardSetController routesList2 = controller.routes;
        controller.getRoutesToRetrace();
        controller.refresh();
        controller.first();
        controller.last();
        controller.next();
        controller.previous();
        Boolean hasNext=controller.hasNext;
        Boolean hasPrev=controller.hasPrevious;
        Integer x =controller.noOfRecords;
        Integer y =controller.pageNumber;
        
        controller.saveList();
        
        VisitControlRetraceToursCtrl.WrapperError wp = new  VisitControlRetraceToursCtrl.WrapperError('error','error');
        wp.route='test';
        wp.route='test';
    }
    
    /**
    * Test method for save information in the execute of the page
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    @isTest static void SaveError(){
        PageReference pageRef = Page.VisitControlRetraceToursPage;
        Test.setCurrentPage(pageRef);
        Id salesOrgRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ORG_RECORDTYPE_NAME).getRecordTypeId();
        Id salesOfficeRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_OFFICE_RECORDTYPE_NAME).getRecordTypeId();
        Id clientGroupdevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.CLIENT_GROUP_RECORDTYPE_NAME).getRecordTypeId();
        Id salesGroupdevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_GROUP_RECORDTYPE_NAME).getRecordTypeId();
        Id salesZonedevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ZONE_RECORDTYPE_NAME).getRecordTypeId();
        
        Account og =  new Account(Name='OG',ONTAP__ExternalKey__c='OG',RecordTypeId=salesOrgRecordTypeId);
        insert og;
        Account off =  new Account(Name='OF',ONTAP__ExternalKey__c='OF',RecordTypeId=salesOfficeRecordTypeId, ParentId=og.Id);
        insert off;
        Account cg =  new Account(Name='CG',ONTAP__ExternalKey__c='CG',RecordTypeId=clientGroupdevRecordTypeId, ParentId=off.Id);
        insert cg;
        Account sg =  new Account(Name='SG',ONTAP__ExternalKey__c='SG',RecordTypeId=salesGroupdevRecordTypeId, ParentId=cg.Id);
        insert sg;
        Account szz =  new Account(Name='SZ',ONTAP__ExternalKey__c='SZ',RecordTypeId=salesZonedevRecordTypeId, ParentId=sg.Id);
        insert szz;
        
        V360_SalerPerZone__c sz = new V360_SalerPerZone__c(V360_SalesZone__c=szz.id);
        insert sz;
        
        Account acc = new Account(Name='1',V360_SalesZoneAssignedBDR__c=sz.Id,V360_SalesZoneAssignedTelesaler__c=sz.Id,V360_SalesZoneAssignedPresaler__c=sz.Id,V360_SalesZoneAssignedCredit__c=sz.Id,V360_SalesZoneAssignedTellecolector__c=sz.Id,V360_SalesZoneAssignedCoolers__c=sz.Id);        
        
        User u = [Select Id FROM User Limit 1];  
                
        ONTAP__Route__c rt = New ONTAP__Route__c(RouteManager__c = u.Id,VisitControl_AgentByZone__c =sz.Id, ServiceModel__c='Presales',VisitControl_Bypass__c = true);
        insert rt;
        List<ONTAP__Route__c> listr=  New List<ONTAP__Route__c> ();
        listr.add(rt);
        
        Set<Id>Ids = new Set <Id>();
        Ids.add(rt.Id);
        
        ApexPages.StandardSetController routes = new ApexPages.StandardSetController(listr);
        routes.setSelected(listr);
        
        VisitControlRetraceToursCtrl controller = new VisitControlRetraceToursCtrl(routes);
        controller.selectedIds =Ids;
        controller.typeOfSkiptSelected = 'Retrace';
        
        ApexPages.StandardSetController routesList2 = controller.routes;
        controller.getRoutesToRetrace();
        controller.refresh();
        controller.first();
        controller.last();
        controller.next();
        controller.previous();
        Boolean hasNext=controller.hasNext;
        Boolean hasPrev=controller.hasPrevious;
        Integer x =controller.noOfRecords;
        Integer y =controller.pageNumber;
        
        controller.saveList();
    }
}