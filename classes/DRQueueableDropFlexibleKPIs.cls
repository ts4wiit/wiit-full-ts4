public class DRQueueableDropFlexibleKPIs implements Queueable {
    
    private Long sObjectRowCount;
    private List<Flexible_KPI__c> deleteList = new List<Flexible_KPI__c>();
    
    public DRQueueableDropFlexibleKPIs(){
        
        this.setSObjectRowCount();
        
    }
    
 	public void execute( QueueableContext context ) {
       
        while( this.sObjectRowCount > 0 ){
            
            deleteCurrentList();
            setDeleteList();
            setSObjectRowCount();
            
        }
        
    }
    
    private void setSObjectRowCount(){
        
        AggregateResult[] groupedResults = [SELECT count(Id) recordsCount FROM Flexible_KPI__c];
        this.sObjectRowCount = (Long) groupedResults[0].get('recordsCount');             
        
    }
    
    private void setDeleteList(){
        
		this.deleteList = [ SELECT Id FROM Flexible_KPI__c LIMIT 10000];
        this.sObjectRowCount = this.deleteList.size();
        
    }
    
    private void deleteCurrentList(){
        
        delete this.deleteList;
        
    }
    
}