public class DRUserSelector {
    
    public static Map<String,String> getRouteToUserIdMap( List<String> routesSet ){
        
        List<User> users = new List<User>();
        users = [ SELECT Id,User_Route__c  FROM User where User_Route__c in : routesSet AND User_Route__c like 'DO%'];
        Map<String,String> routeToUserIdMap = new Map<String,String>();
        
        for( User user : users ){
            
            routeToUserIdMap.put( user.User_Route__c, user.Id );
            
        }
        
        return routeToUserIdMap;
        
    }
    
    public static Map<String,String> getUserIdToRouteMap( List<Id> userIds ){
        
        List<User> users = getUsersByIds( userIds );
        Map<String,String> userIdToRouteMap = new Map<String,String>();
        
        for( User user : users ){
            
            userIdToRouteMap.put( user.Id, user.User_Route__c );
            
        }
        
        return userIdToRouteMap;
        
    }
    
    public static Map<String,String> getUserIdToCountryMap( List<Id> userIds ){
        
        List<User> users = getUsersByIds( userIds );
        
        Map<String,String> userIdToRouteMap = new Map<String,String>();
        
        for( User user : users ){
            
            userIdToRouteMap.put( user.Id, user.ONTAP__Country_Alias__c );
            
        }
        
        return userIdToRouteMap;
        
    }    
    
    public static List<User> getUsersByIds( List<Id> userIds ){
        
        return[ SELECT Id, User_Route__c, ONTAP__Country_Alias__c,VisitsScheduledUntil__c,ProfileId FROM User WHERE Id in : userIds AND IsActive = true ];
        
    }
    
    public static Map<String,String> getUsersIdToRouteMap( List<Id> userIds ){
        
        Map<String,String> userIdToRouteMap = new Map<String,String>();
        
        List<User> users = getUsersByIds( userIds );
        
        for ( User user : users ){
            
            userIdToRouteMap.put( user.Id,user.User_Route__c );
            
        }
        
        return userIdToRouteMap;
        
    }    
    
     
    public static String getIntegrationId(){
        
        return DRCustomSettingsHelper.getIntegrationUserId();
        
    }
}