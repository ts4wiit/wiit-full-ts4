global class BatchPopulateProspectClosestPOCField implements Database.Stateful, Database.AllowsCallouts, Schedulable, Database.Batchable<sObject>  {
   
    public BatchPopulateProspectClosestPOCField(){ }
    private static final String PROSPECT_RECORD_TYPE_NAME = 'Prospect_DO';
    private static final String ACCOUNT_RECORD_TYPE_NAME = 'Account_DO';
    global map<id, account> 	  mapProspect = new map<id, account>();
    global map<id, list<account>> mapAccounts = new map<id, list<account>>();
    
    public static id Schedule(){
        String cron_exp   = '0 0 1 ? * MON,TUE,WED,THU,FRI,SAT,SUN *'; //Weekly at 1AM
        string strnjobame = 'Populate Prospect Closest POC Field';
        
        List<CronTrigger> surveyAutoStatusJobs = [SELECT CronJobDetail.Name, TimesTriggered, NextFireTime 
                                                    FROM CronTrigger 
                                                   WHERE CronJobDetail.Name =: strnjobame];
        if (surveyAutoStatusJobs.isEmpty()) {
            return System.schedule(strnjobame, cron_exp, new BatchPopulateProspectClosestPOCField());
            
        }else{
            return null;
        }
    }
    
    public void execute(SchedulableContext SC) {        
        database.executeBatch(new BatchPopulateProspectClosestPOCField());
    }
     
    public Database.QueryLocator start(Database.BatchableContext BC){
        
        return Database.getQueryLocator([SELECT id, Closest_POC_Code__c, ownerid, owner.name, ONTAP__Longitude__c,
                                         		ONTAP__Latitude__c, Recordtype.DeveloperName 
                                           FROM Account                                          
                                          WHERE (ONTAP__Longitude__c != null AND ONTAP__Latitude__c != null)
                                            AND ( Recordtype.DeveloperName = : PROSPECT_RECORD_TYPE_NAME
                                             OR   Recordtype.DeveloperName = : ACCOUNT_RECORD_TYPE_NAME )
                                       ORDER BY ownerid, Recordtypeid, ONTAP__Longitude__c, ONTAP__Latitude__c ]);
    }
    
    public void execute(Database.BatchableContext BC, List<Account> scope){
        
        for(Account acc : scope){

            if(!mapAccounts.containskey(acc.id) && acc.Recordtype.DeveloperName == PROSPECT_RECORD_TYPE_NAME && acc.Closest_POC_Code__c == null){
                mapProspect.put(acc.id, acc);
            }
            if(!mapAccounts.containskey(acc.ownerid) && acc.Recordtype.DeveloperName != PROSPECT_RECORD_TYPE_NAME){
                mapAccounts.put(acc.ownerid, new list<account>{acc});
            }else{
                if(mapAccounts.containskey(acc.ownerid) && acc.Recordtype.DeveloperName != PROSPECT_RECORD_TYPE_NAME ){
                    mapAccounts.get(acc.ownerid).add(acc);
                }
            }
        }
    }
    
    public void finish(Database.BatchableContext BC){       
        
        for(account prospect : mapProspect.values()){
            
            Location locProspect = Location.newInstance(decimal.valueof(prospect.ONTAP__Latitude__c), decimal.valueof(prospect.ONTAP__Longitude__c));
            Double dist_temp = null;
            
            for( id acid : mapAccounts.keyset() ){
                
                for(account accClosest : mapAccounts.get(acid)){
                    
                    Location locClosest = Location.newInstance(decimal.valueof(accClosest.ONTAP__Latitude__c), decimal.valueof(accClosest.ONTAP__Longitude__c));
                    Double 	 dist 		= Location.getDistance(locProspect, locClosest, 'mi');
                    
                    if(dist_temp == null){
                        dist_temp = dist;
                    }
                    
                    if(dist <= dist_temp){
                        prospect.Closest_POC_Code__c 			  = accClosest.id; 
                        prospect.ontap__geolocation__Latitude__s  = locClosest.getLatitude();
                        prospect.ontap__geolocation__Longitude__s = locClosest.getLongitude();
                        dist_temp = dist;
                    }
                }
            }   
        }
        update mapProspect.values();
    }
    
    public static id getRecordtype(String devname){
        Schema.DescribeSObjectResult 	   resSchema 	  = account.sObjectType.getDescribe();        
        Map<String,Schema.RecordTypeInfo>  recordTypeInfo = resSchema.getRecordTypeInfosByDeveloperName(); 
        
        if(recordTypeInfo.containskey(devname) ){
            return recordTypeInfo.get(devname).getRecordTypeId();
            
        }else{
            return null;
        }
    }
}