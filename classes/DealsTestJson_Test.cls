/* ----------------------------------------------------------------------------
 * AB InBev :: Oncall
 * ----------------------------------------------------------------------------
 * Clase: Applieddeals_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 10/01/2018        Luis Parra            Creation of methods.
 */
@isTest
public class DealsTestJson_Test {
    
   /**
    * Test method for deals json
    * Created By: luis.arturo.parra@accenture.com
    * @param void
    * @return void
    */
   @isTest static void setupTestData(){
    DealsTestJson tester = new DealsTestJson();
		tester.concode=1.0;
    
		tester.amount=1.0;

  		tester.percentage=1.0;

  		tester.pronr=1.0;
   }
}