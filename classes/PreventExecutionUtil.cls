public without sharing class PreventExecutionUtil {
    /* ----------------------------------------------------------------------------
* AB InBev :: 360 View
* ----------------------------------------------------------------------------
* Clase: V360_ProductTriggerHandler.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 03/01/2019     Gerardo Martinez             Creation of methods.
*/
    
    /*public static List<ONTAP__Order__c> validateOrderByUser(List<ONTAP__Order__c> orders){
        List<ONTAP__Order__c> ordersFilt = new List<ONTAP__Order__c>();
        List<USER> idIntegrUserList = [select id from USER where username=:GlobalStrings.INTEGRATION_USER Limit 1];
        USER idIntegrUser = idIntegrUserList.get(0);
        for(ONTAP__Order__c order : orders){
            if(order.CreatedById ==idIntegrUser.ID){
                order.addError('El perfil de integracion no puede insertar ordenes');
            }
		ordersFilt.add(order);
        }        
        return ordersFilt;
    }*/
    
    public static List<ONTAP__Product__c> validateProductWithOutId (List<ONTAP__Product__c> objList){
        List<ONTAP__Product__c> result = new List<ONTAP__Product__c>();
        for(ONTAP__Product__c prd : objList){
            if(prd.ISSM_Organitation_Product__c!=NULL && (prd.ISSM_Organitation_Product__c == GlobalStrings.HONDURAS_ORG_CODE || prd.ISSM_Organitation_Product__c == GlobalStrings.EL_SALVADOR_ORG_CODE)){
                result.add(prd);
            }
        }
        return result;
    }
    
    public static List<ONTAP__Product__c> validateProductWithId (Map<Id,ONTAP__Product__c> objMap){
        List<ONTAP__Product__c> result = new List<ONTAP__Product__c>();
        
        for(List<ONTAP__Product__c> lst : [SELECT Id, ISSM_Organitation_Product__c FROM ONTAP__Product__c WHERE Id IN:objMap.keySet()]){
            for(ONTAP__Product__c prd : lst){
                if(prd.ISSM_Organitation_Product__c!=NULL && (prd.ISSM_Organitation_Product__c == GlobalStrings.HONDURAS_ORG_CODE || prd.ISSM_Organitation_Product__c == GlobalStrings.EL_SALVADOR_ORG_CODE)){
                    result.add(objMap.get(prd.Id));
                }
            }
        }
        return result;
    }
    
    public static List<ONTAP__OpenItem__c> validateOpenItemWithOutId (List<ONTAP__OpenItem__c> objList){
        List<ONTAP__OpenItem__c> result = new List<ONTAP__OpenItem__c>();
        
        Id V360_OpenItemRT = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_V360_RECORDTYPE_NAME).getRecordTypeId();
     	
        for(ONTAP__OpenItem__c op : objList){
            if(op.RecordTypeId!=NULL && V360_OpenItemRT!=NULL && op.RecordTypeId==V360_OpenItemRT){
                result.add(op);
            }
        }
        return result;
    }
    
    public static List<ONTAP__OpenItem__c> validateOpenItemWithId (Map<Id,ONTAP__OpenItem__c> objMap){
        List<ONTAP__OpenItem__c> result = new List<ONTAP__OpenItem__c>();
               
        Id V360_OpenItemRT = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_V360_RECORDTYPE_NAME).getRecordTypeId();
        
        for(List<ONTAP__OpenItem__c> lst : [SELECT Id, RecordTypeId FROM ONTAP__OpenItem__c WHERE Id IN:objMap.keySet()]){
            for(ONTAP__OpenItem__c op : lst){
                if(op.RecordTypeId!=NULL && V360_OpenItemRT!=NULL && op.RecordTypeId==V360_OpenItemRT){
                    result.add(objMap.get(op.Id));
            	}
            }
        }
        return result;
    }    

    public static List<ONTAP__KPI__c> validateKPIWithOutId (List<ONTAP__KPI__c> objList){
       
        List<ONTAP__KPI__c> result = new List<ONTAP__KPI__c>();
        
        Id V360_KPIRT = Schema.SObjectType.ONTAP__KPI__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.KPI_V360_RECORDTYPE_NAME).getRecordTypeId();
     	
        for(ONTAP__KPI__c kpi : objList){
            if(kpi.RecordTypeId!=NULL && V360_KPIRT!=NULL &&kpi.RecordTypeId==V360_KPIRT){
                result.add(kpi);
            }
        }
        return result;
    }
    
    public static List<ONTAP__KPI__c> validateKPIWithId (Map<Id,ONTAP__KPI__c> objMap){
        List<ONTAP__KPI__c> result = new List<ONTAP__KPI__c>();
        
        Id V360_KPIRT = Schema.SObjectType.ONTAP__KPI__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.KPI_V360_RECORDTYPE_NAME).getRecordTypeId();
        
        for(List<ONTAP__KPI__c> lst : [SELECT Id, RecordTypeId FROM ONTAP__KPI__c WHERE Id IN:objMap.keySet()]){
            for(ONTAP__KPI__c kpi : lst){
                if(kpi.RecordTypeId!=NULL && V360_KPIRT!=NULL && kpi.RecordTypeId==V360_KPIRT){
                	result.add(objMap.get(kpi.Id));
            	}
            }
        }
        return result;
    }
    
    public static List<ONCALL__Invoice__c> validateInvoiceWithOutId(List<ONCALL__Invoice__c> objList){
        List<ONCALL__Invoice__c> result = new List<ONCALL__Invoice__c>();
        Id V360_InvoiceRT = Schema.SObjectType.ONCALL__Invoice__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.INVOICE_V360_RECORDTYPE_NAME).getRecordTypeId();
     	
        for(ONCALL__Invoice__c invoice : objList){
            if(invoice.RecordTypeId!=NULL && V360_InvoiceRT!=NULL && invoice.RecordTypeId==V360_InvoiceRT){
                result.add(invoice);
            }
        }
        return result;
    }
    
    public static List<ONCALL__Invoice__c> validateInvoiceWithId (Map<Id,ONCALL__Invoice__c> objMap){
        List<ONCALL__Invoice__c> result = new List<ONCALL__Invoice__c>();
        
        Id V360_InvoiceRT = Schema.SObjectType.ONCALL__Invoice__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.INVOICE_V360_RECORDTYPE_NAME).getRecordTypeId();
        
        for(List<ONCALL__Invoice__c> lst : [SELECT Id, RecordTypeId FROM ONCALL__Invoice__c WHERE Id IN:objMap.keySet()]){
            for(ONCALL__Invoice__c invoice : lst){
                if(invoice.RecordTypeId!=NULL && V360_InvoiceRT!=NULL && invoice.RecordTypeId==V360_InvoiceRT){
                	result.add(objMap.get(invoice.Id));
            	}
            }
        }
        return result;
    }
    
    public static List<ONTAP__EmptyBalance__c> validateEmptyWithOutId(List<ONTAP__EmptyBalance__c> objList){
        List<ONTAP__EmptyBalance__c> result = new List<ONTAP__EmptyBalance__c>();
        Id V360_EmptyRT = Schema.SObjectType.ONTAP__EmptyBalance__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.EMPTY_BALANCE_V360_RECORDTYPE_NAME).getRecordTypeId();
     	
        for(ONTAP__EmptyBalance__c emp : objList){
            if(emp.RecordTypeId!=NULL && V360_EmptyRT!=NULL && emp.RecordTypeId==V360_EmptyRT){
                result.add(emp);
            }
        }
        return result;
    }
    
    public static List<ONTAP__EmptyBalance__c> validateEmptyWithId (Map<Id,ONTAP__EmptyBalance__c> objMap){
        List<ONTAP__EmptyBalance__c> result = new List<ONTAP__EmptyBalance__c>();
        
        Id V360_EmptyRT = Schema.SObjectType.ONTAP__EmptyBalance__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.EMPTY_BALANCE_V360_RECORDTYPE_NAME).getRecordTypeId();
        
        for(List<ONTAP__EmptyBalance__c> lst : [SELECT Id, RecordTypeId FROM ONTAP__EmptyBalance__c WHERE Id IN:objMap.keySet()]){
            for(ONTAP__EmptyBalance__c rmp : lst){
                if(rmp.RecordTypeId!=NULL && V360_EmptyRT!=NULL && rmp.RecordTypeId==V360_EmptyRT){
                	result.add(objMap.get(rmp.Id));
            	}
            }
        }
        return result;
    }
    
    public static List<ONTAP__Account_Asset__c> validateAccountAssetWithOutId(List<ONTAP__Account_Asset__c> objList){
        List<ONTAP__Account_Asset__c> result = new List<ONTAP__Account_Asset__c>();
        Id V360_AccountAssetRT = Schema.SObjectType.ONTAP__Account_Asset__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.ASSETS_V360_RECORDTYPE_NAME).getRecordTypeId();
     	
        for(ONTAP__Account_Asset__c aa : objList){
            if(aa.RecordTypeId!=NULL && V360_AccountAssetRT!=NULL && aa.RecordTypeId==V360_AccountAssetRT){
                result.add(aa);
            }
        }
        return result;
    }
    
    public static List<ONTAP__Account_Asset__c> validateAccountAssetWithId (Map<Id,ONTAP__Account_Asset__c> objMap){
        List<ONTAP__Account_Asset__c> result = new List<ONTAP__Account_Asset__c>();
        
        Id V360_AccountAssetRT = Schema.SObjectType.ONTAP__Account_Asset__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.ASSETS_V360_RECORDTYPE_NAME).getRecordTypeId();
        
        for(List<ONTAP__Account_Asset__c> lst : [SELECT Id, RecordTypeId FROM ONTAP__Account_Asset__c WHERE Id IN:objMap.keySet()]){
            for(ONTAP__Account_Asset__c aa : lst){
                if(aa.RecordTypeId!=NULL && V360_AccountAssetRT!=NULL && aa.RecordTypeId==V360_AccountAssetRT){
                	result.add(objMap.get(aa.Id));
            	}
            }
        }
        return result;
    }
    
	public static List<ONCALL__Call__c> validateCallWithOutId(List<ONCALL__Call__c> objList){
        List<ONCALL__Call__c> result = new List<ONCALL__Call__c>();
        Id V360_RouteRT = Schema.SObjectType.ONTAP__Route__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.RECORDTYPE_VISITCONTROL_ROUTE).getRecordTypeId();
     	
        for(ONCALL__Call__c call : objList){
            if(call.VisitControl_RouteRecordType__c!=NULL && V360_RouteRT!=NULL && call.VisitControl_RouteRecordType__c==V360_RouteRT){
                result.add(call);
            }
        }
        return result;
    }
    
    public static List<ONTAP__Tour__c> validateTourWithOutId(List<ONTAP__Tour__c> objList){
        List<ONTAP__Tour__c> result = new List<ONTAP__Tour__c>();
        
        Id V360_TourBDR = Schema.SObjectType.ONTAP__Tour__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.BDR).getRecordTypeId();
     	Id V360_TourTelesales = Schema.SObjectType.ONTAP__Tour__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.TELESALES).getRecordTypeId();
     	Id V360_TourPresales = Schema.SObjectType.ONTAP__Tour__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.PRESALES).getRecordTypeId();

        for(ONTAP__Tour__c tour : objList){
            if(tour.RecordTypeId!=NULL && V360_TourBDR!=NULL && V360_TourTelesales!=NULL && V360_TourPresales!=NULL && 
               (tour.RecordTypeId==V360_TourPresales||tour.RecordTypeId==V360_TourBDR||tour.RecordTypeId==V360_TourTelesales)){
                result.add(tour);
            }
        }
        return result;
    }
    
    public static List<ONTAP__Route__c> validateRouteWithOutId(List<ONTAP__Route__c> objList){
        List<ONTAP__Route__c> result = new List<ONTAP__Route__c>();
        
        Id V360_RouteRT = Schema.SObjectType.ONTAP__Route__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.RECORDTYPE_VISITCONTROL_ROUTE).getRecordTypeId();

        for(ONTAP__Route__c route : objList){
            if(route.RecordTypeId!=NULL && V360_RouteRT!=NULL && route.RecordTypeId==V360_RouteRT){
                result.add(route);
            }
        }
        return result;
    }
    
    public static List<ONTAP__Route__c> validaterouteWithId (Map<Id,ONTAP__Route__c> objMap){
        List<ONTAP__Route__c> result = new List<ONTAP__Route__c>();
        
        Id V360_RouteRT = Schema.SObjectType.ONTAP__Route__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.RECORDTYPE_VISITCONTROL_ROUTE).getRecordTypeId();
        
        for(List<ONTAP__Route__c> lst : [SELECT Id, RecordTypeId FROM ONTAP__Route__c WHERE Id IN:objMap.keySet()]){
            for(ONTAP__Route__c route : lst){
                if(route.RecordTypeId!=NULL && V360_RouteRT!=NULL && route.RecordTypeId==V360_RouteRT){
                	result.add(objMap.get(route.Id));
            	}
            }
        }
        return result;
    }
    
    //===================================== ACCOUNT ===================================================
    //Added LABELS and CONSTANTS to GlobalStrings Class to support new countries. (PA) drs@avx
    //Added LABELS and CONSTANTS to GlobalStrings Class to support new countries. (CO, PE, EC) drs@globant
    //In order to avoid future code modifications, logic must be enhanced to take countries from custom metadata (or somewhere)
    //===================================== ACCOUNT ===================================================
    //
    //
     public static Map<Id,Account> validateAccountWithIdByContry (Map<Id,Account> mapObj){
        Map<Id,Account> newMapRes = new Map<Id,Account>();
        for(List<Account> lst :  [SELECT Id 
                                  FROM Account 
                                  WHERE Id IN:mapObj.keySet() 
                                  AND (ONCALL__Country__c=:GlobalStrings.HONDURAS_COUNTRY_CODE 
                                       OR ONCALL__Country__c=:GlobalStrings.EL_SALVADOR_COUNTRY_CODE
                                       OR ONCALL__Country__c=:GlobalStrings.PANAMA_COUNTRY_CODE
                                       OR ONCALL__Country__c=:GlobalStrings.COLOMBIA_COUNTRY_CODE
                                       OR ONCALL__Country__c=:GlobalStrings.PERU_COUNTRY_CODE
                                       OR ONCALL__Country__c=:GlobalStrings.ECUADOR_COUNTRY_CODE
                                      )]){ //added drs@avx
                      for(Account acc : lst){
                          newMapRes.put(acc.Id,mapObj.get(acc.Id));
                      }
        }
        for(Account acc : mapObj.values()){
            if(!newMapRes.containsKey(acc.Id) 
               && acc.ONCALL__Country__c!=NULL 
               && (acc.ONCALL__Country__c==GlobalStrings.HONDURAS_COUNTRY_CODE 
                   || acc.ONCALL__Country__c==GlobalStrings.EL_SALVADOR_COUNTRY_CODE 
                   || acc.ONCALL__Country__c==GlobalStrings.PANAMA_COUNTRY_CODE
                    || acc.ONCALL__Country__c==GlobalStrings.COLOMBIA_COUNTRY_CODE
                    || acc.ONCALL__Country__c==GlobalStrings.PERU_COUNTRY_CODE
                    || acc.ONCALL__Country__c==GlobalStrings.ECUADOR_COUNTRY_CODE)){ //added drs@avx
                newMapRes.put(acc.Id,mapObj.get(acc.Id));
            }
        } 
        return newMapRes;
    }    
    
    public static Map<Id,Account> validateAccountWithIdByORG (Map<Id,Account> mapObj){
        Map<Id,Account> newMapRes = new Map<Id,Account>();
               
        for(List<Account> lst : [SELECT Id 
                                 FROM Account 
                                 WHERE Id IN:mapObj.keySet() 
                                 AND (ONTAP__SalesOgId__c=:GlobalStrings.HONDURAS_ORG_CODE
                                      OR ONTAP__SalesOgId__c=:GlobalStrings.EL_SALVADOR_ORG_CODE
                                      OR ONTAP__SalesOgId__c=:GlobalStrings.PANAMA_ORG_CODE //added drs@
                                      OR ONTAP__SalesOgId__c=:GlobalStrings.COLOMBIA_ORG_CODE
                                      OR ONTAP__SalesOgId__c=:GlobalStrings.PERU_ORG_CODE
                                      OR ONTAP__SalesOgId__c=:GlobalStrings.PERU_ORG_CODE2
                                      OR ONTAP__SalesOgId__c=:GlobalStrings.ECUADOR_ORG_CODE
                                     )]){ //added drs@avx
            for(Account acc : lst){
               newMapRes.put(acc.Id,mapObj.get(acc.Id));
            }
        }
        
        for(Account acc : mapObj.values()){
            if(!newMapRes.containsKey(acc.Id) 
               && acc.ONTAP__SalesOgId__c != NULL 
               && (acc.ONTAP__SalesOgId__c==GlobalStrings.HONDURAS_ORG_CODE 
                   || acc.ONTAP__SalesOgId__c==GlobalStrings.EL_SALVADOR_ORG_CODE
                   || acc.ONTAP__SalesOgId__c==GlobalStrings.PANAMA_ORG_CODE
                   || acc.ONTAP__SalesOgId__c==GlobalStrings.COLOMBIA_ORG_CODE
                   || acc.ONTAP__SalesOgId__c==GlobalStrings.PERU_ORG_CODE
                   || acc.ONTAP__SalesOgId__c==GlobalStrings.PERU_ORG_CODE2
                   || acc.ONTAP__SalesOgId__c==GlobalStrings.ECUADOR_ORG_CODE
                  )){ //added drs@avx
                newMapRes.put(acc.Id,mapObj.get(acc.Id));
            }
        }
        
        
        return newMapRes;
    }
    
    public static List<Account> validateAccountWithIdByORGBeforeInsert (List<Account> mapObj){
        List<Account> newMapRes = new List<Account>();
        
        for(Account acc : mapObj){
            if( acc.ONTAP__SalesOgId__c != NULL 
               && (acc.ONTAP__SalesOgId__c==GlobalStrings.HONDURAS_ORG_CODE 
                   || acc.ONTAP__SalesOgId__c==GlobalStrings.EL_SALVADOR_ORG_CODE
                   || acc.ONTAP__SalesOgId__c==GlobalStrings.PANAMA_ORG_CODE //added drs@avx
                   || acc.ONTAP__SalesOgId__c==GlobalStrings.COLOMBIA_ORG_CODE
                   || acc.ONTAP__SalesOgId__c==GlobalStrings.PERU_ORG_CODE
                   || acc.ONTAP__SalesOgId__c==GlobalStrings.PERU_ORG_CODE2
                   || acc.ONTAP__SalesOgId__c==GlobalStrings.ECUADOR_ORG_CODE )){ 
                newMapRes.add(acc);
            }
        }
        
        
        return newMapRes;
    }
    
     
    public static List<Account> validateAccountWithoutIdByContry(List<Account> objList){
        List<Account> result = new List<Account>();
        
        for(Account acc : objList){
            if(acc.ONCALL__Country__c!=NULL 
               && (acc.ONCALL__Country__c==GlobalStrings.HONDURAS_COUNTRY_CODE 
                   || acc.ONCALL__Country__c==GlobalStrings.EL_SALVADOR_COUNTRY_CODE
                   || acc.ONCALL__Country__c==GlobalStrings.PANAMA_COUNTRY_CODE		//added drs@avx
                   || acc.ONCALL__Country__c==GlobalStrings.COLOMBIA_COUNTRY_CODE	//added drs@globant
                   || acc.ONCALL__Country__c==GlobalStrings.PERU_COUNTRY_CODE		//added drs@globant
                   || acc.ONCALL__Country__c==GlobalStrings.ECUADOR_COUNTRY_CODE )){ //added drs@globant
                result.add(acc);
            }
        }
        return result;
    }
    
    public static List<Account> validateAccountWithoutIdByORG(List<Account> objList){
        List<Account> result = new List<Account>();
        
        for(Account acc : objList){
            if(acc.ONTAP__SalesOgId__c!=NULL 
               && (acc.ONTAP__SalesOgId__c==GlobalStrings.HONDURAS_ORG_CODE 
                   || acc.ONTAP__SalesOgId__c==GlobalStrings.EL_SALVADOR_ORG_CODE
                   || acc.ONTAP__SalesOgId__c==GlobalStrings.PANAMA_ORG_CODE
                   || acc.ONTAP__SalesOgId__c==GlobalStrings.COLOMBIA_ORG_CODE
                   || acc.ONTAP__SalesOgId__c==GlobalStrings.PERU_ORG_CODE
                   || acc.ONTAP__SalesOgId__c==GlobalStrings.PERU_ORG_CODE2
                   || acc.ONTAP__SalesOgId__c==GlobalStrings.ECUADOR_ORG_CODE
                  )){ //added drs@avx
                result.add(acc);
            }
        }
        return result;
    }
 
  /**
    * method wich validate each account that recieves and returns only the accounts that a value has changed and its recordtype is Account
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return Void
    */ 
        
    public static List<Account> prevalidateBeforeToTranslate(Map<id,Account> newMap,Map<id,Account> oldMap){
        List<Account> result = new List<Account>();
        
        Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.ACCOUNT_RECORDTYPE_NAME).getRecordTypeId();
        
        for(Account acc : newMap.values()){
            if (acc.RecordTypeId!=NULL && acc.RecordTypeId == accountRecordTypeId && newMap.get(acc.Id).HONES_BusinessType__c != NULL && (newMap.get(acc.Id).HONES_BusinessType__c != oldMap.get(acc.Id).HONES_BusinessType__c || !newMap.get(acc.Id).HONES_BusinessType__c.contains('-') ) ) {
             	result.add(acc);
            }else if(acc.RecordTypeId!=NULL && acc.RecordTypeId == accountRecordTypeId && newMap.get(acc.Id).HONES_Creditsuppressionreason__c != NULL && (newMap.get(acc.Id).HONES_Creditsuppressionreason__c != oldMap.get(acc.Id).HONES_Creditsuppressionreason__c || !newMap.get(acc.Id).HONES_Creditsuppressionreason__c.contains('-') )){
                result.add(acc);
            }else if(acc.RecordTypeId!=NULL && acc.RecordTypeId == accountRecordTypeId && newMap.get(acc.Id).HONES_Suppressionreason__c != NULL && (newMap.get(acc.Id).HONES_Suppressionreason__c != oldMap.get(acc.Id).HONES_Suppressionreason__c || !newMap.get(acc.Id).HONES_Suppressionreason__c.contains('-'))){
                result.add(acc);
            }else if(acc.RecordTypeId!=NULL && acc.RecordTypeId == accountRecordTypeId && newMap.get(acc.Id).HONES_Term__c != NULL && (newMap.get(acc.Id).HONES_Term__c != oldMap.get(acc.Id).HONES_Term__c || !newMap.get(acc.Id).HONES_Term__c.contains('-'))){
                result.add(acc);
            }else if(acc.RecordTypeId!=NULL && acc.ONTAP__ExternalKey__c!=NULL && !acc.ONTAP__ExternalKey__c.startsWith('SV') && acc.RecordTypeId == accountRecordTypeId && newMap.get(acc.Id).ONCALL__KATR1__c != NULL && (newMap.get(acc.Id).ONCALL__KATR1__c != oldMap.get(acc.Id).ONCALL__KATR1__c || !newMap.get(acc.Id).ONCALL__KATR1__c.contains('-'))){
                result.add(acc); //No se añade una descripción al código sólo para El Salvador
            }else if(acc.RecordTypeId!=NULL && acc.RecordTypeId == accountRecordTypeId && newMap.get(acc.Id).ONCALL__KATR10__c != NULL && (newMap.get(acc.Id).ONCALL__KATR10__c != oldMap.get(acc.Id).ONCALL__KATR10__c || !newMap.get(acc.Id).ONCALL__KATR10__c.contains('-'))){
                result.add(acc);
            }else if(acc.RecordTypeId!=NULL && acc.ONTAP__ExternalKey__c!=NULL && !acc.ONTAP__ExternalKey__c.startsWith('SV') && acc.RecordTypeId == accountRecordTypeId && newMap.get(acc.Id).ONCALL__KATR2__c != NULL && (newMap.get(acc.Id).ONCALL__KATR2__c != oldMap.get(acc.Id).ONCALL__KATR2__c || !newMap.get(acc.Id).ONCALL__KATR2__c.contains('-'))){
                result.add(acc); //No se añade una descripción al código sólo para El Salvador
            }else if(acc.RecordTypeId!=NULL && acc.RecordTypeId == accountRecordTypeId && newMap.get(acc.Id).ONCALL__KATR3__c != NULL && (newMap.get(acc.Id).ONCALL__KATR3__c != oldMap.get(acc.Id).ONCALL__KATR3__c || !newMap.get(acc.Id).ONCALL__KATR3__c.contains('-'))){
                result.add(acc);
            }else if(acc.RecordTypeId!=NULL && acc.RecordTypeId == accountRecordTypeId && newMap.get(acc.Id).ONCALL__KATR4__c != NULL && (newMap.get(acc.Id).ONCALL__KATR4__c != oldMap.get(acc.Id).ONCALL__KATR4__c || !newMap.get(acc.Id).ONCALL__KATR4__c.contains('-'))){
                result.add(acc);
            }else if(acc.RecordTypeId!=NULL && acc.RecordTypeId == accountRecordTypeId && newMap.get(acc.Id).ONCALL__KATR6__c != NULL && (newMap.get(acc.Id).ONCALL__KATR6__c != oldMap.get(acc.Id).ONCALL__KATR6__c || !newMap.get(acc.Id).ONCALL__KATR6__c.contains('-'))){
                result.add(acc);
            }else if(acc.RecordTypeId!=NULL && acc.RecordTypeId == accountRecordTypeId && newMap.get(acc.Id).ONCALL__KATR9__c != NULL && (newMap.get(acc.Id).ONCALL__KATR9__c != oldMap.get(acc.Id).ONCALL__KATR9__c || !newMap.get(acc.Id).ONCALL__KATR9__c.contains('-'))){
                result.add(acc);
            }else if(acc.RecordTypeId!=NULL && acc.RecordTypeId == accountRecordTypeId && newMap.get(acc.Id).ONTAP__PaymentMethod__c != NULL && (newMap.get(acc.Id).ONTAP__PaymentMethod__c != oldMap.get(acc.Id).ONTAP__PaymentMethod__c || !newMap.get(acc.Id).ONTAP__PaymentMethod__c.contains('-'))){
                result.add(acc);
            }else if(acc.RecordTypeId!=NULL && acc.RecordTypeId == accountRecordTypeId && newMap.get(acc.Id).V360_CreditSubGroup__c != NULL && (newMap.get(acc.Id).V360_CreditSubGroup__c != oldMap.get(acc.Id).V360_CreditSubGroup__c)){
                result.add(acc);
            }else if(acc.RecordTypeId!=NULL && acc.RecordTypeId == accountRecordTypeId && newMap.get(acc.Id).V360_TransportationZone__c != NULL && (newMap.get(acc.Id).V360_TransportationZone__c != oldMap.get(acc.Id).V360_TransportationZone__c || !newMap.get(acc.Id).V360_TransportationZone__c.contains('-'))){
                result.add(acc);
            }
        }
        return result;
    }
    
}