/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_DeleteKPISBatch_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 19/12/2018     Carlos Leal        Creation of methods.
 */
@isTest
private class V360_DeleteAssetsBatch_Test{
    
   /**
   * Method test for delete all the assets in V360_DeleteAssetsBatch.
   * @author: c.leal.beltran@accenture.com
   * @param void
   * @return void
   */
    @isTest static void testBatch(){
        Id V360_Account_AssetRT = Schema.SObjectType.ONTAP__Account_Asset__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.ASSETS_V360_RECORDTYPE_NAME).getRecordTypeId();
        
        ONTAP__Account_Asset__c asset= NEW ONTAP__Account_Asset__c(RecordTypeId =V360_Account_AssetRT );
        insert asset;
        V360_DeleteAssetsBatch obj01 = new V360_DeleteAssetsBatch();
        Database.executeBatch(obj01, 200);
    }
}