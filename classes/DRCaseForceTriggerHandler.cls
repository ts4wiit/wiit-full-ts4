public with sharing class DRCaseForceTriggerHandler {

    private List<ONTAP__Case_Force__c> cases = new List<ONTAP__Case_Force__c>();
    private List<ONTAP__Case_Force__c> filteredCases = new List<ONTAP__Case_Force__c>();
    private Set<Id> recordTypeIds = new Set<Id>();
    private List<Id> accountIds = new List<Id>();
    private List<Account> accounts = new List<Account>();
    private Map<Id,Account> accountIdToAccountMap = new Map<Id,Account>();
    private Map<String,String> objectIdToUserIdMap = new Map<String,String>();

    public DRCaseForceTriggerHandler( List<ONTAP__Case_Force__c> cases ){

        this.cases = cases;
    }

    public void runBefore(){
        
        this.getDRRecordTypes();
        this.filter();
        this.getAccounts();
        this.setBDRs();
    }

    public void runAfter(){

        this.getDRRecordTypes();
        this.filter();
        this.getAccounts();
        this.setMap();
        this.share();
    }

    private void getDRRecordTypes(){

        List<RecordType> recordTypes = [ SELECT Id FROM RecordType 
                                          WHERE DeveloperName like 'DO%' 
                                            AND sObjectType = 'ONTAP__Case_Force__c' ];
        
        if( !recordTypes.isEmpty() ){
            for( RecordType recordType : recordTypes ){
                
                recordTypeIds.add( recordType.Id );                
            }
        }else{
            System.debug('recordTypes is Empty ');
        }
    }
        
    private void filter(){
        
        if( !cases.isEmpty() ){
            
            for( ONTAP__Case_Force__c caseForce : cases ){
                
                if( recordTypeIds.contains( caseForce.RecordTypeId ) ){
    
                    filteredCases.add( caseForce );
                    accountIds.add( caseForce.ONTAP__Account__c );                    
                }   
            }      
        }else{
            System.debug('Cases is Empty');
        }
    }
    
    private void getAccounts(){
        
        this.accounts = DRAccountSelector.getAccountsByIds( accountIds );
        
        for( Account account: accounts ){
            this.accountIdToAccountMap.put( account.Id, account );                
        }      
    }

    private void setBDRs(){

        for( ONTAP__Case_Force__c caseForce : filteredCases ){
            
            if( caseForce.ONTAP__Account__c != null ){
                Account account = accountIdToAccountMap.get( caseForce.ONTAP__Account__c );
                caseForce.BDR_Route__c = account.OwnerId;
            }else{
                trigger.new[0].addError( 'El campo de cuenta no puede ser nulo' );
            }
        }        
    }
    
    private void setMap(){
            
        for( ONTAP__Case_Force__c caseForce : filteredCases ){
            
            Account account = new Account();
            
            if( caseForce.ONTAP__Account__c != null ){
                account = accountIdToAccountMap.get( caseForce.ONTAP__Account__c );
                
            }else{
                //trigger.new[0].addError( 'El campo de cuenta no puede ser nulo' );
            }
            
            if( caseForce.OwnerId != account.OwnerId ){
                objectIdToUserIdMap.put( caseForce.Id, account.OwnerId );     
            }
        }   
    }
    
    private void share(){
        
         ApexSharingUtils shareInstance = new ApexSharingUtils( 
             'ONTAP__Case_Force__Share', 
              'ParentId', 
              objectIdToUserIdMap );

		 shareInstance.share();       
    }
}