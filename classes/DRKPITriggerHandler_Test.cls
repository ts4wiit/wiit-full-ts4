@isTest
public class DRKPITriggerHandler_Test {
    
    @isTest
    static void testKPIs(){    
        
       // Create users
       Id profileId = [ SELECT Id FROM PROFILE WHERE Name = 'DO BDR' ].get(0).Id;

	   User bdrUser1 = new User(FirstName='BDR1', LastName='Agent 1', UserName = 'bdr1@test.com', Email='bdr1@test.com',
				Alias='alias1', LocaleSidKey='es', LanguageLocaleKey='es', EmailEncodingKey='ISO-8859-1',ONTAP__Country_Alias__c = 'DO',
				ProfileId = profileId, TimeZoneSidKey='America/Puerto_Rico', Country='DO', isActive = true,User_Route__c='DO0001');

	   insert bdrUser1;        
       
	   User bdrUser2= new User(FirstName='BDR2', LastName='Agent 2', UserName = 'bdr2@test.com', Email='bdr2@test.com',
				Alias='alias2', LocaleSidKey='es', LanguageLocaleKey='es', EmailEncodingKey='ISO-8859-1',ONTAP__Country_Alias__c = 'DO',
				ProfileId = profileId, TimeZoneSidKey='America/Puerto_Rico', Country='DO', isActive = true,User_Route__c='DO0002');

	   insert bdrUser2; 
        
	   User bdrUser3= new User(FirstName='BDR3', LastName='Agent 3', UserName = 'bdr3@test.com', Email='bdr2@test.com',
				Alias='alias3', LocaleSidKey='es', LanguageLocaleKey='es', EmailEncodingKey='ISO-8859-1',ONTAP__Country_Alias__c = 'DO',
				ProfileId = profileId, TimeZoneSidKey='America/Puerto_Rico', Country='DO', isActive = true,User_Route__c='DO0003');

	   insert bdrUser3;         
    
        
       //Create accounts assigned to each bdr 
        
       Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account_DO').getRecordTypeId();
        
       List<Account> accountsBdr1 = new List<Account>();
        
       for( Integer i = 1 ; i <= 100 ; i++ ) {
            
            Account account = new Account( Name = 'Account BDR 1 ' + i, ONTAP__SAPCustomerId__c = 'CID1' + i, RecordTypeId = accountRecordTypeId, OwnerId = bdrUser1.Id,
                                           ONTAP__Contact_First_Name__c = 'Contact', ONTAP__Contact_Last_Name__c = 'Lastname');
            accountsBdr1.add( account );
            
       }     
        
       insert accountsBdr1;
        
       List<Account> accountsBdr2 = new List<Account>();
        
       for( Integer i = 1 ; i <= 100 ; i++ ) {
            
            Account account = new Account( Name = 'Account BDR 2 ' + i, ONTAP__SAPCustomerId__c = 'CID2' + i, RecordTypeId = accountRecordTypeId, OwnerId = bdrUser2.Id,
                                           ONTAP__Contact_First_Name__c = 'Contact', ONTAP__Contact_Last_Name__c = 'Lastname');
            accountsBdr2.add( account );
            
       }     
        
       insert accountsBdr2;        

       List<Account> accountsBdr3 = new List<Account>();
        
       for( Integer i = 1 ; i <= 100 ; i++ ) {
            
            Account account = new Account( Name = 'Account BDR 3 ' + i, ONTAP__SAPCustomerId__c = 'CID3' + i, RecordTypeId = accountRecordTypeId, OwnerId = bdrUser3.Id,
                                           ONTAP__Contact_First_Name__c = 'Contact', ONTAP__Contact_Last_Name__c = 'Lastname');
            accountsBdr3.add( account );
            
       }     
        
       insert accountsBdr3;                               
              
       Test.startTest();     

       String cid;         
       List<ONTAP__KPI__c> kpis = new List<ONTAP__KPI__c>();
        
       for( Integer i = 1 ; i <= 4 ; i++ ) {
           
           for( Integer j = 1 ; j <= 100 ; j++ ) {
               
               cid = 'CID';
               cid = cid + i;
               cid = cid + j;
               
               ONTAP__KPI__c kpi = new ONTAP__KPI__c( ONTAP__KPI_name__c = 'Test', 
                                                      BDR_Route__c = 'DO000' + i, 
                                                      Country_Code__c = 'DO', 
                                                      Account_Sap_Id__c = cid,
                                                      ONTAP__Categorie__c = 'Volume' );  
               
               kpis.add( kpi );               
               
           }
 
       }
        
      for( Integer i = 1 ; i <= 4 ; i++ ) {
           
           for( Integer j = 1 ; j <= 100 ; j++ ) {
               
               cid = 'CID';
               cid = cid + i;
               cid = cid + j;
               
               ONTAP__KPI__c kpi = new ONTAP__KPI__c( ONTAP__KPI_name__c = 'Test', 
                                                      BDR_Route__c = 'DO000' + i, 
                                                      Country_Code__c = 'DO', 
                                                      Account_Sap_Id__c = '-',
                                                      ONTAP__Categorie__c = 'Volume' );  
               
               kpis.add( kpi );               
               
           }
 
       }
                   
       insert kpis;
        
       // assertion
       
       List<ONTAP__KPI__c> assignedKPIs = [ SELECT Id FROM ONTAP__KPI__c WHERE OwnerId in ( :bdrUser1.Id,:bdrUser2.Id,:bdrUser3.Id ) AND Account_Sap_Id__c != '-' AND ONTAP__User_id__c = null];
        
       System.assertEquals( 300 , assignedKPIs.size(), 'Assertion failed; assigned PoC KPIs do not match the expected size.');
        
       List<ONTAP__KPI__c> assignedKPIs2 = [ SELECT Id FROM ONTAP__KPI__c WHERE OwnerId in ( :bdrUser1.Id,:bdrUser2.Id,:bdrUser3.Id ) AND Account_Sap_Id__c = '-' AND ONTAP__User_id__c != null ];
        
       System.assertEquals( 300 , assignedKPIs2.size(), 'Assertion failed; assigned User KPIs do not match the expected size.');
                
       Test.stopTest();
    
    }    
    
    @isTest
    static void testNoUser(){  
 
       Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account_DO').getRecordTypeId();
        
       // Create users
       Id profileId = [ SELECT Id FROM PROFILE WHERE Name = 'DO BDR' ].get(0).Id;

	   User bdrUser1 = new User(FirstName='BDR1', LastName='Agent 1', UserName = 'bdr1@test.com', Email='bdr1@test.com',
				Alias='alias1', LocaleSidKey='es', LanguageLocaleKey='es', EmailEncodingKey='ISO-8859-1',ONTAP__Country_Alias__c = 'DO',
				ProfileId = profileId, TimeZoneSidKey='America/Puerto_Rico', Country='DO', isActive = true,User_Route__c='DO0001');

	   insert bdrUser1;    
        
       Account account = new Account( Name = 'Account BDR 1 ', ONTAP__SAPCustomerId__c = 'CID1', RecordTypeId = accountRecordTypeId, OwnerId = bdrUser1.Id,
                                           ONTAP__Contact_First_Name__c = 'Contact', ONTAP__Contact_Last_Name__c = 'Lastname'); 
        
        
       insert account;
        
       Test.startTest(); 
        
       ONTAP__KPI__c kpi = new ONTAP__KPI__c( ONTAP__KPI_name__c = 'Test', 
                                                      BDR_Route__c = 'DO9999', 
                                                      Country_Code__c = 'DO', 
                                                      Account_Sap_Id__c = 'CID1',
                                                      ONTAP__Categorie__c = 'Volume' );      
        
       insert kpi; 
        
       Id currentUserId = UserInfo.getUserId();
        
       kpi = [ SELECT OwnerId FROM ONTAP__KPI__c ].get(0);
        
       System.assertEquals( currentUserId , kpi.OwnerId, 'Assertion failed :(') ;
        
       Test.stopTest(); 
        
    }    

}