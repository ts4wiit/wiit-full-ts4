/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: Apex2JsonDeals_Test.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 24/01/2018        Carlos Leal           Creation of methods.
*/
@isTest
private class Apex2JsonDeals_Test {
    
   /**
   * Method for test for apex to json.
   * @author: c.leal.beltran@accenture.com
   * @param void
   * @return void
   */
    @isTest static void checkJson_Test(){
        //Test.startTest();
        Apex2JsonDeals obj = new Apex2JsonDeals();
        obj.token = '';
		obj.accountId = '';
		obj.salesOrg = '';
        //Test.stopTest();
    }

}