public with sharing class HONES_ONCALL_PartnerListHONES {
     /**
    * Methods getters and setters for entity HONES_ONCALL_PartnerList
    * Created By: heron.zurita@accenture.com
    * @param void
    * @return void
    */
    public String role{get; set;}
    public String numb{get; set;}
    public String itmNumber{get; set;}
    public Domicilio domicilios {get;set;}

    public class Domicilio{
        public String NAME {get;set;}
    }
}