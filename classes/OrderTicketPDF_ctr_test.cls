/**
 * Company:             Avanxo México
 * Author:              Carlos Pintor / cpintor@avanxo.com
 * Project:             AbInbev 2019
 * Description:         Apex Unit Test Class of OrderTicketPDF_ctr
 *
 * Nu.    Date                Author                     Description
 * 1.0    June 11th, 2019     Carlos Pintor              Created
 * 2.0    Oct 2nd, 2019		  Daniel Rosales			Implementation for Panamá
 *
 */

@IsTest
public class OrderTicketPDF_ctr_test {
    @testSetup static void createTestData(){
        ONTAP__Tour__c tour = new ONTAP__Tour__c();
        tour.ONTAP__TourId__c = null;
        //Insert tour;
        
        Event visit = new Event();
        visit.DurationInMinutes = 10;
        visit.ActivityDateTime = DateTime.now();
        Insert visit;
        
        String RecordTypeAccount = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByDeveloperName().get('Account').getRecordTypeId();
        String RecordTypeAccountSalesOffice = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByDeveloperName().get('SalesOffice').getRecordTypeId();
        String RecordTypeAccountSalesOrg = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByDeveloperName().get('SalesOrg').getRecordTypeId();
        
        //Creamos Cuenta con SalesOffice Parent
        Account  objAccountSalesOfficeParent = new Account();
            objAccountSalesOfficeParent.Name = 'Name SalesOffice Parent';
            objAccountSalesOfficeParent.RecordTypeId = RecordTypeAccountSalesOffice;
            insert objAccountSalesOfficeParent;
            
        //Creamos Cuenta con SalesOffice
        Account  objAccountSalesOffice = new Account();
            objAccountSalesOffice.Name = 'Name SalesOffice';
            objAccountSalesOffice.RecordTypeId = RecordTypeAccountSalesOffice;
            objAccountSalesOffice.ONTAP__SalesOffId__c = 'SO01';
            objAccountSalesOffice.ParentId = objAccountSalesOfficeParent.Id;
            insert objAccountSalesOffice;
        
            
        //Creamos Cuenta con SalesOrg
        Account  objAccountSalesOrg = new Account();
            objAccountSalesOrg.Name = 'Name Sales Org';
            objAccountSalesOrg.RecordTypeId = RecordTypeAccountSalesOrg;
            objAccountSalesOrg.ONTAP__SalesOgId__c = 'OR01';
            insert objAccountSalesOrg;
        
        //Creamos Cuenta con Tipo de registro Account
        Account  objAccount = new Account();
            objAccount.Name = 'Name Account';
          objAccount.ONTAP__SAP_Number__c = '0123456789';
          objAccount.ONTAP__ExternalKey__c = '0123456789';
            objAccount.RecordTypeId = RecordTypeAccount;
            objAccount.ONTAP__SalesOffId__c = 'SO01';
            objAccount.ONTAP__SalesOgId__c ='OR01';
            insert objAccount;
        
        
        ONTAP__Order__c order = new ONTAP__Order__c();
        order.RecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByDeveloperName().get('Order_with_Price').getRecordTypeId();
        order.ONTAP__OrderAccount__c = objAccount.Id;
        order.ONTAP__Route_Id__c = '123456';
        order.ONTAP__Route_Description__c = 'Route Description';
        order.ONTAP__PurchaseOrder__c = 'yyyymmdd012340123';
        order.ONTAP__App_Version__c = '1.2';
        order.ONTAP__Event_Id__c = visit.Id;
        Insert order;
        
        ONTAP__Order_Item__c item1Order1 = new ONTAP__Order_Item__c();
        item1Order1.ONTAP__CustomerOrder__c = order.Id;
        item1Order1.ONTAP__Order_Line_Type__c = 'Sales Order';
        item1Order1.ONTAP__ActualQuantity__c = 1;
        item1Order1.ONTAP__UnitPriceFull__c = 1.0;
        item1Order1.ONTAP__Disc_Total_2__c = 1.0;
        item1Order1.ONTAP__Amount_Line_Total__c = 1.0;
        item1Order1.ONTAP__UnitPrice__c = 1.0;
        Insert item1Order1;
        /*
        ONTAP__Order_Item__c item2Order1 = new ONTAP__Order_Item__c();
        item2Order1.ONTAP__CustomerOrder__c = order.Id;
        item2Order1.ONTAP__Order_Line_Type__c = 'Empties Return Order';
        item2Order1.ONTAP__ActualQuantity__c = 1;
        item2Order1.ONTAP__UnitPriceFull__c = 0.0;
        item2Order1.ONTAP__Disc_Total_2__c = 0.0;
        item2Order1.ONTAP__Amount_Line_Total__c = 0.0;
        Insert item2Order1;
*/
        
        ONTAP__Order__c order2 = new ONTAP__Order__c();
        order2.RecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByDeveloperName().get('OnCall_Order').getRecordTypeId();
        order.ONTAP__OrderAccount__c = objAccount.Id;
        order2.ONTAP__Route_Id__c = '123456';
        order2.ONTAP__Route_Description__c = 'Route Description';
        order2.ONTAP__PurchaseOrder__c = 'yyyymmdd012340123';
        order2.ONTAP__App_Version__c = '1.2';
        Insert order2;
        
        ONTAP__Product__c prod1 = new ONTAP__Product__c();
        prod1.ONTAP__ProductType__c = 'FERT';
        Insert prod1;
        
        ONTAP__Order_Item__c item1Order2 = new ONTAP__Order_Item__c();
        item1Order2.ONTAP__CustomerOrder__c = order2.Id;
        item1Order2.ONCALL__OnCall_Product__c = prod1.Id;
        item1Order2.ONTAP__Order_Line_Type__c = 'Sales Order';
        item1Order2.ONTAP__ActualQuantity__c = 1;
        item1Order2.ONTAP__UnitPriceFull__c = 1.0;
        item1Order2.ONTAP__Disc_Total_2__c = 1.0;
        item1Order2.ONTAP__Amount_Line_Total__c = 1.0;
        item1Order2.ONTAP__UnitPrice__c = 1.0;
        Insert item1Order2;
        
        ONTAP__Product__c prod2 = new ONTAP__Product__c();
        prod2.ONTAP__ProductType__c = 'LEER';
        Insert prod2;
        
        ONTAP__Order__c order3 = new ONTAP__Order__c();
        order3.RecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByDeveloperName().get('Standard').getRecordTypeId();
        order3.ONTAP__OrderAccount__c = objAccount.Id;
        order3.ONTAP__Route_Id__c = '123456';
        order3.ONTAP__Route_Description__c = 'Route Description';
        order3.ONTAP__PurchaseOrder__c = 'yyyymmdd012340123';
        order3.ONTAP__App_Version__c = '1.2';
        Insert order3;
    }
    
    public static testMethod void testOnTapOrderTicket(){
        Test.startTest();
        Id orderWithPriceId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByDeveloperName().get('Order_with_Price').getRecordTypeId();
        ONTAP__Order__c order = [SELECT Id FROM ONTAP__Order__c WHERE RecordTypeId = :orderWithPriceId LIMIT 1];
        PageReference pageRef = Page.OrderTicketPDF_pag;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', order.Id);
        OrderTicketPDF_ctr ctr = new OrderTicketPDF_ctr();
        Test.stopTest();
    }
    
    public static testMethod void testOnCallOrderTicket(){
        Test.startTest();
        Id onCallorderId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByDeveloperName().get('OnCall_Order').getRecordTypeId();
        ONTAP__Order__c order = [SELECT Id FROM ONTAP__Order__c WHERE RecordTypeId = :onCallorderId LIMIT 1];
        PageReference pageRef = Page.OrderTicketPDF_pag;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', order.Id);
        OrderTicketPDF_ctr ctr = new OrderTicketPDF_ctr();
        Test.stopTest();
    }
    
    public static testMethod void testOrderTicketDifferenteRecordType(){
        /*List<User> lstUser =  new list<User>();
      
      	Id p = [select id from profile where name='PAN Presales'].id;

      	User U = new User(alias = 'alias123', email='test1232112@noemail.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p, country='PA',IsActive =true,
        timezonesidkey='America/Los_Angeles', username='test1232112@noemail.com');       
       	lstUser.add(U);      
      	
        insert lstUser;   
      	
        System.runAs(U){*/
            
            Test.startTest();
            Id onCallorderId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByDeveloperName().get('Standard').getRecordTypeId();
            ONTAP__Order__c order = [SELECT Id FROM ONTAP__Order__c WHERE RecordTypeId = :onCallorderId LIMIT 1];
            PageReference pageRef = Page.OrderTicketPDF_pag;
            Test.setCurrentPage(pageRef);
        
        
            ApexPages.currentPage().getParameters().put('Id', order.Id);
            OrderTicketPDF_ctr ctr = new OrderTicketPDF_ctr();
            Test.stopTest();
            
        //}      	         
    }
}