public class DRPrimaryContactService {
    
    public static final String CONTACT_ROLE = 'Administrator';
    
    public static void createPrimaryContacts( List<Account> accounts ){
        
        List<Contact> contacts = new List<Contact>();
        Integer counter = 0;

        for( Account account : accounts ){
            
            Contact contact = new Contact( AccountId = account.Id, 
                                           ONTAP__Contact_Function__c = CONTACT_ROLE, 
                                           FirstName = account.ONTAP__Contact_First_Name__c, 
                                           LastName = account.ONTAP__Contact_Last_Name__c,
                                           Phone = account.Phone,
                                           Email = account.ONTAP__Email__c);
            
            contacts.add( contact );      
            
            counter++;
            
            if ( counter == 200 ){
                
                System.enqueueJob( new DRPrimaryContactQueueable( contacts ));
                contacts.clear();
                counter = 0;
                
            }
            
        }
        
        //finally
		System.enqueueJob( new DRPrimaryContactQueueable( contacts ));
        
    }

}