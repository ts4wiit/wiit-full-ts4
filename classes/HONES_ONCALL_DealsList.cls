/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: HONES_ONCALL_DealsList.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 18/12/2018           Luis Arturo Parra Rosas           Creation of methods.
*/

public class HONES_ONCALL_DealsList {
    
    /**
    * Methods getters and setters for entity HONES_ONCALL_DealsList
    * Created By: heron.zurita@accenture.com
    * @param void
    * @return void
    */
    public String condcode{get; set;}
	public String amount{get; set;}
    public String pronr{get; set;}
    public String posex{get;set;}
}