/* ----------------------------------------------------------------------------
 * AB InBev :: Trigger Question Typification Matrix Handler
 * ----------------------------------------------------------------------------
 * Clase: CS_TriggerQuestionTMHandler.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 08/02/2019      Gabriel García          Hnadler .
 */
public class CS_TriggerQuestionTMHandler extends TriggerHandlerCustom{
    
    private Map<Id, CS_Question_Typification_Matrix__c> newMap;
    private Map<Id, CS_Question_Typification_Matrix__c> oldMap;
    private List<CS_Question_Typification_Matrix__c> newList;
    private List<CS_Question_Typification_Matrix__c> oldList;
    
    public CS_TriggerQuestionTMHandler() {
        
        this.newMap = (Map<Id, CS_Question_Typification_Matrix__c>) Trigger.newMap;
        this.oldMap = (Map<Id, CS_Question_Typification_Matrix__c>) Trigger.oldMap;
        this.newList = (List<CS_Question_Typification_Matrix__c>) Trigger.new;
        this.oldList = (List<CS_Question_Typification_Matrix__c>) Trigger.old;
    }
    
    /**
    * method wich start every time before record is insert 
    * @author: gabriel.e.garcia@accenture.com
    * @param void
    * @return Void
    */
    public override void beforeInsert(){
        addTypificationMatrix(this.newlist);
    }
    
    public void addTypificationMatrix(List<CS_Question_Typification_Matrix__c> newList){
        List<ISSM_TypificationMatrix__c> listTM = new List<ISSM_TypificationMatrix__c>();
        Map<String, String> mapTM = new Map<String, String>();
        
        listTM = [SELECT Id, ISSM_TypificationLevel1__c, ISSM_TypificationLevel2__c, ISSM_TypificationLevel3__c, ISSM_TypificationLevel4__c, ISSM_Countries_ABInBev__c
                  FROM ISSM_TypificationMatrix__c];
        
        for(ISSM_TypificationMatrix__c tm : listTM){
            String key = '' + tm.ISSM_TypificationLevel1__c + '|' + tm.ISSM_TypificationLevel2__c + '|' + tm.ISSM_TypificationLevel3__c + '|' + tm.ISSM_TypificationLevel4__c + '|' +  tm.ISSM_Countries_ABInBev__c;
            mapTM.put(key, tm.Id);
        }
        
        if(!mapTM.IsEmpty()){
            for(CS_Question_Typification_Matrix__c question : newList){
                String keyQ = '' + question.CS_Typification_Level1__c + '|' + question.CS_Typification_Level2__c + '|' + question.CS_Typification_Level3__c + '|' + question.CS_Typification_Level4__c + '|' +  question.CS_Country__c;	            
                question.CS_Typification_Matrix_Number__c = mapTM.containsKey(keyQ) ? mapTM.get(keyQ) : null;
            }
        }
        
        
        
    }

}