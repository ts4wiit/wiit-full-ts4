/* ----------------------------------------------------------------------------
* AB InBev :: Customer Service
* ----------------------------------------------------------------------------
* Clase: CS_SyncProspect_Scheduler_Class_Test.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 30/04/2019     Jose Luis Vargas          Creation of test class
*/

@isTest
public class CS_SyncProspect_Scheduler_Class_Test 
{
    /**
    * method to test Execute method
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    @isTest
    static void test_execute()
    {
        CS_SyncProspect_Scheduler_Class oScheduler = new CS_SyncProspect_Scheduler_Class();
        SchedulableContext sc = null;
        oScheduler.execute(sc);
    }
}