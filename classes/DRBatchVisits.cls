global class DRBatchVisits implements 
    Database.Batchable<sObject>, Database.Stateful {
    
    global Integer processedUsers = 0;
        
    global Database.QueryLocator start(Database.BatchableContext bc) {
       String query = 'SELECT User_Route__c,VisitsScheduledUntil__c FROM User WHERE ONTAP__Country_Alias__c = \'DO\' AND User_Route__c like \'DO%\'' + (Test.isRunningTest()? + 'LIMIT 1':'');
       return Database.getQueryLocator(query);
    }
        
    global void execute(Database.BatchableContext bc, List<User> scope){

        Datetime executionDateTime = Datetime.now();
        
        for (User user : scope) {
          DOVisitsExecution__c execution = new DOVisitsExecution__c( Name = user.User_Route__c + '|' + executionDateTime, User__c = user.Id );
          insert execution;
        }

    }    
        
    global void finish(Database.BatchableContext bc){
        System.debug(processedUsers + ' users processed.');
    }    
        
}