public class DRAccountTriggerHandler {
    
    public static final 		String DR_ACCOUNT_RECORD_TYPE = 'Account_DO';
    private List<Account> 		accounts 			= new List<Account>();
    private List<Account> 		filteredAccounts 	= new List<Account>();
    private Map<String,String> 	routeToUserIdMap 	= new Map<String,String>();
    private List<String> 		routes 				= new List<String>();
    private Boolean             isInsert;

    Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get( DR_ACCOUNT_RECORD_TYPE ).getRecordTypeId();

    public DRAccountTriggerHandler( List<Account> accounts ){

        this.accounts = accounts;
    }

    public DRAccountTriggerHandler( List<Account> accounts, Boolean isInsert ){

        this.accounts = accounts;
        this.isInsert = isInsert;
    }
    
    public void run(){
        
        this.filterAccounts();
        this.getRoutes();
        this.getRouteToUserIdMap();
        this.setBDRRoutes();        
    }
    
    private void filterAccounts(){
        
        for( Account account : accounts ){
            
            if( account.RecordTypeId == accountRecordTypeId ){
                filteredAccounts.add( account );                
            }            
        }        
    }
    
    private void getRoutes(){
        
        for ( Account account : filteredAccounts ){
            
            routes.add( account.BDR_Route__c );            
        }        
    }
    
    private void getRouteToUserIdMap(){
        
        this.routeToUserIdMap = DRUserSelector.getRouteToUserIdMap( routes );        
    }
    
    private void setBDRRoutes(){
        
        for ( Account account : filteredAccounts ){
            
			Id ownerId = routeToUserIdMap.get( account.BDR_Route__c );
            
            if ( OwnerId == null ){                
                ownerId = UserInfo.getUserId();                
            }    
            
            account.OwnerId = ownerId;            
        }                
    }
    

    public static void setGeolocationField( List<Account> listAccounts ){
        
        for( Account acc : listAccounts ){
                
            if(String.isNotBlank(acc.ONTAP__Latitude__c) && String.isNotBlank(acc.ONTAP__Longitude__c)){
                
                try{
                    acc.ONTAP__Geolocation__Latitude__s  = Double.valueof(acc.ONTAP__Latitude__c);
                    acc.ONTAP__Geolocation__Longitude__s = Double.valueof(acc.ONTAP__Longitude__c);
                    
                }catch(Exception ex){
                    System.debug('Latitude Or longitude not valid: ' + ex.getMessage());
                }
            }
        }
    }
    
    public static void setProspectClosestPocField( List<Account> listAccounts ){
        
        Set<Id> idsAccounts = new Set<Id>();
        
        for(Account conta : listAccounts){
            idsAccounts.add(conta.Id);
        }
        
        List<Account> listProspects = new List<Account>();
        
        for( Account acc : [SELECT Id, Recordtype.DeveloperName, ONTAP__Geolocation__c, Closest_POC_Code__c, ONTAP__Geolocation__Latitude__s 
                              FROM Account WHERE Id IN : idsAccounts] ){
            
            if(   acc.Recordtype.DeveloperName == 'Prospect_DO' 
               && acc.Closest_POC_Code__c 	   == null 
               && acc.ONTAP__Geolocation__c    != null ){
                
                listProspects.add( acc );
            }
        }
        if( listProspects.size() > 0 ){
        	SetProspectClosestPOCField.start( listProspects );            
        }
    }
}