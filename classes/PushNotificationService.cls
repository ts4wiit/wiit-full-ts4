/**
* @author Leonardo Santos
* @date 05/28/2019
* @description service - This class sends Chatter push notifications comments to Mobile(Android & IOS)
*/
public with sharing class PushNotificationService {

    /**
     * @description : Method void that send chatter feed notifications.
	 * @param Map<Id, Map<String, Object>> mapIdUserByPayLoad : Map id of users and id of feeds
     * @param String pushSettings : settings for the push
    */
	public static void sendPushNotifications(Map<Id, Map<String, Object>> mapIdUserByPayLoad, String pushSettings){
		
		ONTAP__OnTapSettings__c lOnTapSettings = ONTAP__OnTapSettings__c.getInstance();
                   
		ONTAP__Push_Notifications__c lPushNotificationSettings = ONTAP__Push_Notifications__c.getValues(pushSettings); 
	
		Messaging.PushNotification lMsgToBeSend = new Messaging.PushNotification();
		Set<String> setUserId = new Set<String>();

		for(Id objUser : mapIdUserByPayLoad.keySet()){

			Map<String, Object> payload = mapIdUserByPayLoad.get(objUser);
			
			if( lPushNotificationSettings != null){
			
				if( lPushNotificationSettings.ONTAP__Android_Enabled__c ){ 
						
					lMsgToBeSend.setPayload(payload);
						
					setUserId.add(objUser);
					
					if(!Test.isRunningTest()){
						lMsgToBeSend.send(lOnTapSettings.ONTAP__Connected_APP_for_ANDROID__c, setUserId);
					}
				}

				if( lPushNotificationSettings.ONTAP__iOS_Enabled__c ){
					
					Map<String, Object> payloadApple = Messaging.PushNotificationPayload.apple((String)payload.get('content'), 'default', null, payload);
					
					lMsgToBeSend.setPayload(payloadApple);
					
					Set<String> lSetBDRToSend = new Set<String>();	

					lSetBDRToSend.add(objUser);

					if(!Test.isRunningTest()){
						lMsgToBeSend.send(lOnTapSettings.ONTAP__Connected_APP_for_IOS__c, lSetBDRToSend);
					}	
				}
			}
		}
	}	
}