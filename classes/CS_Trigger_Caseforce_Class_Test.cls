/* ----------------------------------------------------------------------------
* AB InBev :: Customer Service
* ----------------------------------------------------------------------------
* Clase: CS_TRIGGER_CASEFORCE_CLASS_Test.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 08/01/2019           Jose Luis Vargas       Crecion de la clase para testing de la clase CS_TRIGGER_CASEFORCE_CLASS 
*/
@isTest
private class CS_Trigger_Caseforce_Class_Test 
{
    /**
    * Method Config Setup
    * @author: gabriel.e.garcia@accenture.com
    * @param Void
    * @return Void
    */
    @testSetup static void setup() 
    {
        Account accountTest = new Account(Name = 'Account Test');
        insert accountTest;
        
        ONTAP__Product__c product = new ONTAP__Product__c(ONTAP__MaterialProduct__c = 'Product Test', ONTAP__ProductCode__c = '3005686');
        insert product;
        ONTAP__Account_Asset__c assetAcc = new ONTAP__Account_Asset__c(ONTAP__Asset_Description__c = 'Description', HONES_EquipmentNumber__c='1234567890', 
                                                                       HONES_Asset_Model__c='model', ONTAP__Serial_Number__c='0987654321',V360_MaterialNumber__c='1029384756', 
                                                                       ONTAP__Brand__c='Brand', ONTAP__Account__c = accountTest.id);
        Insert assetAcc;
        
        List<ISSM_MappingFieldCase__c> listmapField = new List<ISSM_MappingFieldCase__c>();
        
        ISSM_MappingFieldCase__c mapfieldcase_cs1 = new ISSM_MappingFieldCase__c(
            Name = 'ISSM_TypificationLevel1__c',ISSM_APICaseForce__c = 'ISSM_TypificationLevel1__c', Active__c = true);
        listmapField.add(mapfieldcase_cs1);
        ISSM_MappingFieldCase__c mapfieldcase_cs2 = new ISSM_MappingFieldCase__c(
            Name = 'ISSM_TypificationLevel2__c',ISSM_APICaseForce__c = 'ISSM_TypificationLevel2__c', Active__c = true);
        listmapField.add(mapfieldcase_cs2);
        ISSM_MappingFieldCase__c mapfieldcase_cs3 = new ISSM_MappingFieldCase__c(
            Name = 'ISSM_TypificationLevel3__c',ISSM_APICaseForce__c = 'ISSM_TypificationLevel3__c', Active__c = true);
        listmapField.add(mapfieldcase_cs3);
        ISSM_MappingFieldCase__c mapfieldcase_cs4 = new ISSM_MappingFieldCase__c(
            Name = 'ISSM_TypificationLevel4__c',ISSM_APICaseForce__c = 'ISSM_TypificationLevel4__c', Active__c = true);
        listmapField.add(mapfieldcase_cs4);
        ISSM_MappingFieldCase__c mapfieldcase_cs5 = new ISSM_MappingFieldCase__c(
            Name = 'HONES_Case_Country__c',ISSM_APICaseForce__c = 'CS_Country__c', Active__c = true);   
        listmapField.add(mapfieldcase_cs5);
        ISSM_MappingFieldCase__c mapfieldcase_cs6 = new ISSM_MappingFieldCase__c(
            Name = 'ISSM_OwnerCaseForce__c',ISSM_APICaseForce__c = 'OwnerId', Active__c = true);   
        listmapField.add(mapfieldcase_cs6);
        ISSM_MappingFieldCase__c mapfieldcase_cs7 = new ISSM_MappingFieldCase__c(
            Name = 'AccountId',ISSM_APICaseForce__c = 'ONTAP__Account__c', Active__c = true);   
        listmapField.add(mapfieldcase_cs7);
        ISSM_MappingFieldCase__c mapfieldcase_cs8 = new ISSM_MappingFieldCase__c(
            Name = 'Status',ISSM_APICaseForce__c = 'ONTAP__Status__c', Active__c = true);   
        listmapField.add(mapfieldcase_cs8);
        
        insert listmapField;
        
        //SELECT Name, ISSM_APICaseForce__c FROM ISSM_MappingFieldCase__c WHERE Active__c  = true
        List<ISSM_MappingFieldCase__c> listISSM_MappingFieldCase = new List<ISSM_MappingFieldCase__c>();
        ISSM_MappingFieldCase__c mappingFieldCS = new ISSM_MappingFieldCase__c(Name = 'HONES_Case_Country__c', ISSM_APICaseForce__c = 'CS_Country__c', Active__c = true);
        ISSM_MappingFieldCase__c mappingFieldCS2 = new ISSM_MappingFieldCase__c(Name = 'ISSM_Productcode__c', ISSM_APICaseForce__c = 'CS_Material__c', Active__c = true);
        ISSM_MappingFieldCase__c mappingFieldCS3 = new ISSM_MappingFieldCase__c(Name = 'CS_Account_Asset__c', ISSM_APICaseForce__c = 'CS_Account_Asset__c', Active__c = true);
        listISSM_MappingFieldCase.add(mappingFieldCS);
        listISSM_MappingFieldCase.add(mappingFieldCS2);
        listISSM_MappingFieldCase.add(mappingFieldCS3);
        insert listISSM_MappingFieldCase;
        
        CS_Preguntas_CaseForce__c preguntasCF  = new CS_Preguntas_CaseForce__c();
        preguntasCF.CS_Pregunta_CF__c = '¿Es la primera vez que solicita crédito?';
        preguntasCF.CS_Api_Name_Field__c = 'CS_OT_first_time_request_credit__c';
        preguntasCF.Name = 'SV_Credito_P1'; 
        preguntasCF.CS_RecordType_Name__c = 'CS_RT_OT_SV_Coolers';
        
        insert preguntasCF;
    }
    
    
    @isTest static void test_afterInsertAudit() {
  
        
        ISSM_TypificationMatrix__c objISSM_TypificationMatrix = new ISSM_TypificationMatrix__c(
            ISSM_TypificationLevel1__c = 'Hallazgo',
            ISSM_TypificationLevel2__c = 'Equipo Frío',
            ISSM_TypificationLevel3__c = 'NEF - Equipo no encontrado en cliente',
            ISSM_TypificationLevel4__c = '',
            CS_OT_Type_of_request__c = 'Equipo No Encontrado',
            ISSM_Countries_ABInBev__c = 'El Salvador',
            ISSM_CaseRecordType__c = 'HONES_Audit_Equipment_NotFound_Customer',
            HONES_CF_RecordType__c = 'HONES_Audit_Equipment_NotFound_Customer',
            CS_Days_to_End__c = 1
        );
        insert objISSM_TypificationMatrix;
        
        Account oNewAccount = new Account(Name = 'Account Test', ONTAP__ExternalKey__c='SVCS010012345678');   
        insert oNewAccount;                
        
        Schema.DescribeSObjectResult R = ONTAP__Case_Force__c.SObjectType.getDescribe();
        Map<String,Schema.RecordTypeInfo> rtMapByName = R.getRecordTypeInfosByName();
        Schema.RecordTypeInfo recordType = rtMapByName.get('HONES_Audit_Equipment_NotFound_Customer');
        
            
            CS_TRIGGER_CASEFORCE_CLASS oTrigger = new CS_TRIGGER_CASEFORCE_CLASS();
            
            ONTAP__Case_Force__c oNewCaseForce = new ONTAP__Case_Force__c();
            oNewCaseForce.ONTAP__Status__c = 'Closed';
            oNewCaseForce.ONTAP__Subject__c = 'Test Case Force';
            oNewCaseForce.ONTAP__Description__c = 'Test Description';
            oNewCaseForce.RecordTypeId = recordType.getRecordTypeId();
            oNewCaseForce.OwnerId = UserInfo.getUserId();
            oNewCaseForce.CS_OT_Type_of_request__c = 'Equipo No Encontrado';
            oNewCaseForce.CS_Country__c = 'El Salvador';
            oNewCaseForce.CS_Country_Code__c = 'SV';
            oNewCaseForce.ONTAP__Account__c = oNewAccount.Id;
            insert oNewCaseForce;
            
            Case oCase = new Case( ISSM_OwnerCaseForce__c = UserInfo.getUserId(), AccountId = oNewAccount.Id, ISSM_LocalStreet__c = 'Calle1', ISSM_LocalNumber__c = '90', 
                                  ISSM_TypificationLevel1__c = 'Hallazgo', ISSM_TypificationLevel2__c = 'Equipo Frío', ISSM_TypificationLevel3__c = 'NEF - Equipo no encontrado en cliente', 
                                  ISSM_TypificationLevel4__c = '', ISSM_PostalCode__c = '12345', ISSM_LocalColony__c ='Colonia1',  
                                  HONES_Case_Country__c = 'El Salvador',ISSM_CaseForceNumber__c = oNewCaseForce.Id);
            insert oCase;
            
            oTrigger.afterInsert(); 
    }
    
    /**
    * Method for test the method afterInsert for assigned queue
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    /*
    @isTest static void test_afterInsertQueue()
    {
        CS_CASE_USER_ESCALATION_CLASS testEscalation= new CS_CASE_USER_ESCALATION_CLASS();
        List<Profile> prof = [Select Name,Id from Profile];
        
        Schema.DescribeFieldResult fieldResult = ONTAP__Case_Force__c.CS_OT_Type_of_request__c.getDescribe();
        
        Schema.DescribeSObjectResult R = ONTAP__Case_Force__c.SObjectType.getDescribe();
        Map<String,Schema.RecordTypeInfo> rtMapByName = R.getRecordTypeInfosByName();
        Schema.RecordTypeInfo recordType = rtMapByName.get('OnTap SV Credits');
        
        User ownerU = new User(Email = 'user12345@email.com', Username = 'user12345@email.com', LastName = 'Test', Alias = 'Test', TimeZoneSidKey = 'America/Bogota',
                               LocaleSidKey = 'es', EmailEncodingKey = 'ISO-8859-1', ProfileId = prof[0].id,LanguageLocaleKey = 'es');
        insert ownerU;  
        
        ISSM_TypificationMatrix__c objISSM_TypificationMatrix = new ISSM_TypificationMatrix__c(
            ISSM_TypificationLevel1__c = 'SAC',
            ISSM_TypificationLevel2__c = 'Solicitudes',
            ISSM_TypificationLevel3__c = 'Equipo Frío',
            ISSM_TypificationLevel4__c = 'Reparación',
            CS_OT_Type_of_request__c = fieldResult.getPicklistValues()[0].value,
            ISSM_Countries_ABInBev__c = 'El Salvador',
            ISSM_CaseRecordType__c = 'HONES_RT_Maintance_Notice',
            ISSM_AssignedTo__c = 'Queue',
            ISSM_OwnerQueue__c = 'SV_WithoutOwner',
            ISSM_OwnerUser__c = ownerU.Id,
            ISSM_Email1CommunicationLevel3__c = 'test@test.com',
            CS_Days_to_End__c = 7
        );
        
        insert objISSM_TypificationMatrix;
        
        Account oNewAccount = new Account(Name = 'Account Test', OwnerId = ownerU.Id, ONTAP__ExternalKey__c='SVCS010012345678');   
        insert oNewAccount;
        
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueuesObject testQueue = new QueueSObject(QueueId = testGroup.id, SObjectType = 'Case');
            insert testQueue;
            
            ISSM_AppSetting_cs__c settings = ISSM_AppSetting_cs__c.getOrgDefaults();
            settings.ISSM_IdQueueWithoutOwner__c = testQueue.QueueId;
            insert settings; 
            
            CS_TRIGGER_CASEFORCE_CLASS oTrigger = new CS_TRIGGER_CASEFORCE_CLASS();
            
            ONTAP__Product__c prod = [SELECT Id, ONTAP__MaterialProduct__c, ONTAP__ProductCode__c FROM ONTAP__Product__c LIMIT 1][0];
            ONTAP__Account_Asset__c asset = [SELECT Id, ONTAP__Asset_Description__c, HONES_EquipmentNumber__c, HONES_Asset_Model__c, 
                                             ONTAP__Serial_Number__c,V360_MaterialNumber__c, ONTAP__Brand__c, ONTAP__Account__c 
                                             FROM ONTAP__Account_Asset__c LIMIT 1][0];
            
            ONTAP__Case_Force__c oNewCaseForce = new ONTAP__Case_Force__c();
            oNewCaseForce.ONTAP__Status__c = 'Assigned';
            oNewCaseForce.CS_Account_Asset__c = asset.Id;
            oNewCaseForce.CS_Material__c = prod.Id;
            oNewCaseForce.ONTAP__Subject__c = 'Test Case Force';
            oNewCaseForce.ONTAP__Description__c = 'Test Description';
            oNewCaseForce.RecordTypeId = recordType.getRecordTypeId();
            oNewCaseForce.CS_OT_Type_of_request__c = fieldResult.getPicklistValues()[0].value;
            oNewCaseForce.CS_Country__c = 'El Salvador';
            oNewCaseForce.CS_Country_Code__c = 'SV';
            oNewCaseForce.ISSM_TypificationLevel1__c = 'SAC';
            oNewCaseForce.ISSM_TypificationLevel2__c = 'Solicitudes';
            oNewCaseForce.ISSM_TypificationLevel3__c = 'Equipo Frío';
            oNewCaseForce.ISSM_TypificationLevel4__c = 'Reparación';
            oNewCaseForce.ONTAP__Account__c = oNewAccount.Id;
            insert oNewCaseForce;
            
            Case oCase = new Case( OwnerId = UserInfo.getUserId(), AccountId = oNewAccount.Id, ISSM_LocalStreet__c = 'Calle1', ISSM_LocalNumber__c = '90', 
                                  ISSM_TypificationLevel1__c = 'SAC', ISSM_TypificationLevel2__c = 'Solicitudes', ISSM_TypificationLevel3__c = 'Equipo Frío', 
                                  ISSM_TypificationLevel4__c = 'Reparación', ISSM_PostalCode__c = '12345', ISSM_LocalColony__c ='Colonia1',  
                                  HONES_Case_Country__c = 'El Salvador', ISSM_CaseForceNumber__c = oNewCaseForce.Id);
            insert oCase;
            
            update oNewCaseForce;
            oTrigger.afterInsert();
        }
    }
*/
    
    /**
    * Method for test the method afterInsert for assigned user
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    
    /*
    @isTest static void test_afterInsertUser()
    {
        CS_CASE_USER_ESCALATION_CLASS testEscalation= new CS_CASE_USER_ESCALATION_CLASS();
        List<Profile> prof = [Select Name,Id from Profile];
        
        User ownerU = new User(Email = 'user12345@email.com', Username = 'user12345@email.com', LastName = 'Test', Alias = 'Test', TimeZoneSidKey = 'America/Bogota',
                               LocaleSidKey = 'es', EmailEncodingKey = 'ISO-8859-1', ProfileId = prof[0].id,LanguageLocaleKey = 'es');
        insert ownerU;  
        
        Schema.DescribeFieldResult fieldResult = ONTAP__Case_Force__c.CS_OT_Type_of_request__c.getDescribe();
        
        Schema.DescribeSObjectResult R = ONTAP__Case_Force__c.SObjectType.getDescribe();
        Map<String,Schema.RecordTypeInfo> rtMapByName = R.getRecordTypeInfosByName();
        Schema.RecordTypeInfo recordType = rtMapByName.get('OnTap SV_Coolers');
        
        ISSM_TypificationMatrix__c objISSM_TypificationMatrix = new ISSM_TypificationMatrix__c(
            ISSM_TypificationLevel1__c = 'SAC',
            ISSM_TypificationLevel2__c = 'Solicitudes',
            ISSM_TypificationLevel3__c = 'Equipo Frío',
            ISSM_TypificationLevel4__c = 'Reparación',
            CS_OT_Type_of_request__c = 'Mantenimiento',
            ISSM_Countries_ABInBev__c = 'El Salvador',
            ISSM_CaseRecordType__c = 'HONES_RT_Maintance_Notice',
            ISSM_AssignedTo__c = 'Queue',
            ISSM_OwnerQueue__c = 'SV_WithoutOwner',
            ISSM_OwnerUser__c = ownerU.Id,
            ISSM_Email1CommunicationLevel3__c = 'test@test.com',
            CS_Days_to_End__c = 7,
            CS_Dias_Primer_Contacto__c = 1
        );
        
        insert objISSM_TypificationMatrix;
        
        Account oNewAccount = new Account(Name = 'Account Test', OwnerId = ownerU.Id, ONTAP__ExternalKey__c='SVCS010012345678');   
        insert oNewAccount;                
        
        CS_TRIGGER_CASEFORCE_CLASS oTrigger = new CS_TRIGGER_CASEFORCE_CLASS();
        
        ONTAP__Case_Force__c oNewCaseForce = new ONTAP__Case_Force__c();
        oNewCaseForce.ONTAP__Status__c = 'Assigned';
        oNewCaseForce.ISSM_TypificationLevel1__c = 'SAC';
        oNewCaseForce.ISSM_TypificationLevel2__c = 'Solicitudes';
        oNewCaseForce.ISSM_TypificationLevel3__c = 'Equipo Frío';
        oNewCaseForce.ISSM_TypificationLevel4__c = 'Reparación';
        oNewCaseForce.ONTAP__Subject__c = 'Test Case Force';
        oNewCaseForce.ONTAP__Description__c = 'Test Description';
        oNewCaseForce.RecordTypeId = recordType.getRecordTypeId();
        oNewCaseForce.CS_OT_Type_of_request__c = 'Mantenimiento';
        oNewCaseForce.CS_Country__c = 'El Salvador';
        oNewCaseForce.CS_Country_Code__c = 'SV';
        oNewCaseForce.ONTAP__Account__c = oNewAccount.Id;
        insert oNewCaseForce;
        
        Case oCase = new Case( OwnerId = UserInfo.getUserId(), AccountId = oNewAccount.Id, ISSM_LocalStreet__c = 'Calle1', ISSM_LocalNumber__c = '90', 
                              ISSM_TypificationLevel1__c = 'SAC', ISSM_TypificationLevel2__c = 'Solicitudes', ISSM_TypificationLevel3__c = 'Equipo Frío', 
                              ISSM_TypificationLevel4__c = 'Reparación', ISSM_PostalCode__c = '12345', ISSM_LocalColony__c ='Colonia1',  
                              HONES_Case_Country__c = 'El Salvador', ISSM_CaseForceNumber__c = oNewCaseForce.Id);
        insert oCase;
        
        oTrigger.afterInsert();        
    }
    */
    /**
    * Method for test the method afterInsert for assigned user
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    
    @isTest static void test_afterInsertInterlocutor()
    {
        CS_CASE_USER_ESCALATION_CLASS testEscalation= new CS_CASE_USER_ESCALATION_CLASS();
        List<Profile> prof = [Select Name,Id from Profile];
        Schema.DescribeFieldResult fieldResult = ONTAP__Case_Force__c.CS_OT_Type_of_request__c.getDescribe();
        User ownerU = new User(Email = 'user12345@email.com', Username = 'user12345@email.com', LastName = 'Test', Alias = 'Test', TimeZoneSidKey = 'America/Bogota',
                               LocaleSidKey = 'es', EmailEncodingKey = 'ISO-8859-1', ProfileId = prof[0].id,LanguageLocaleKey = 'es');
        insert ownerU;  
        
        ISSM_TypificationMatrix__c objISSM_TypificationMatrix = new ISSM_TypificationMatrix__c(
            ISSM_TypificationLevel1__c = 'Solicitudes',
            ISSM_TypificationLevel2__c = 'Equipo Frío',
            ISSM_TypificationLevel3__c = 'EFI - Instalación',
            ISSM_TypificationLevel4__c = '',
            //CS_OT_Type_of_request__c = fieldResult.getPicklistValues()[0].value,
            CS_OT_Type_of_request__c = 'Instalación',
            ISSM_Countries_ABInBev__c = 'El Salvador',
            ISSM_CaseRecordType__c = 'CS_RT_Installation_Cooler',
            HONES_CF_RecordType__c = 'CS_RT_OT_SV_Coolers',
            ISSM_AssignedTo__c = 'Telesales Manager',
            ISSM_OwnerQueue__c = 'SV_WithoutOwner',
            ISSM_OwnerUser__c = ownerU.Id,
            ISSM_Email1CommunicationLevel3__c = 'test@test.com',
            CS_Days_to_End__c = 7,
            CS_Dias_Primer_Contacto__c = 1
        );
        
        insert objISSM_TypificationMatrix;
        
        Account oNewAccount = new Account(Name = 'Account Test', OwnerId = ownerU.Id, ONTAP__ExternalKey__c='SVCS010012345678');   
        insert oNewAccount;                
        
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        
		ONTAP__Account_Asset__c assetAcc = new ONTAP__Account_Asset__c(ONTAP__Asset_Description__c = 'Description', HONES_EquipmentNumber__c='1234567890', 
                                                                       HONES_Asset_Model__c='model', ONTAP__Serial_Number__c='0987654321',V360_MaterialNumber__c='1029384756', 
                                                                       ONTAP__Brand__c='Brand', ONTAP__Account__c = oNewAccount.id);
        Insert assetAcc;
        
        Schema.DescribeSObjectResult R = ONTAP__Case_Force__c.SObjectType.getDescribe();
        Map<String,Schema.RecordTypeInfo> rtMapByName = R.getRecordTypeInfosByName();
        Schema.RecordTypeInfo recordType = rtMapByName.get('OnTap SV_Coolers');
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueuesObject testQueue = new QueueSObject(QueueId = testGroup.id, SObjectType = 'Case');
            insert testQueue;
            
            ISSM_AppSetting_cs__c settings = ISSM_AppSetting_cs__c.getOrgDefaults();
            settings.ISSM_IdQueueWithoutOwner__c = testQueue.QueueId;
            insert settings;
            
            CS_TRIGGER_CASEFORCE_CLASS oTrigger = new CS_TRIGGER_CASEFORCE_CLASS();
            
            ONTAP__Case_Force__c oNewCaseForce = new ONTAP__Case_Force__c();
            oNewCaseForce.ISSM_TypificationLevel1__c = 'Solicitudes';
            oNewCaseForce.ISSM_TypificationLevel2__c = 'Equipo Frío';
            oNewCaseForce.ISSM_TypificationLevel3__c = 'EFI - Instalación';
            oNewCaseForce.ISSM_TypificationLevel4__c = '';
            oNewCaseForce.ONTAP__Status__c = 'Open';
            oNewCaseForce.ONTAP__Subject__c = 'Test Case Force';
            oNewCaseForce.ONTAP__Description__c = 'Test Description';
            oNewCaseForce.RecordTypeId = recordType.getRecordTypeId();
            oNewCaseForce.CS_OT_Type_of_request__c = 'Instalación';
            oNewCaseForce.CS_Country__c = 'El Salvador';
            oNewCaseForce.CS_Country_Code__c = 'SV';
            oNewCaseForce.CS_Account_Asset__c = assetAcc.Id;
            oNewCaseForce.ONTAP__Account__c = oNewAccount.Id;
            insert oNewCaseForce;
            
            Case oCase = new Case( OwnerId = UserInfo.getUserId(), AccountId = oNewAccount.Id, ISSM_LocalStreet__c = 'Calle1', ISSM_LocalNumber__c = '90', 
                                  ISSM_TypificationLevel1__c = 'Solicitudes', ISSM_TypificationLevel2__c = 'Equipo Frío', ISSM_TypificationLevel3__c = 'EFI - Instalación', 
                                  ISSM_TypificationLevel4__c = '', ISSM_PostalCode__c = '12345', ISSM_LocalColony__c ='Colonia1',  
                                  HONES_Case_Country__c = 'El Salvador', ISSM_CaseForceNumber__c = oNewCaseForce.Id);
            insert oCase;
            
            oTrigger.afterInsert();
        }       
    }

    @isTest static void test_afterInsertInterlocutorProspect()
    {
        CS_CASE_USER_ESCALATION_CLASS testEscalation= new CS_CASE_USER_ESCALATION_CLASS();
        List<Profile> prof = [Select Name,Id from Profile];
        Schema.DescribeFieldResult fieldResult = ONTAP__Case_Force__c.CS_OT_Type_of_request__c.getDescribe();
        User ownerU = new User(Email = 'user12345@email.com', Username = 'user12345@email.com', LastName = 'Test', Alias = 'Test', TimeZoneSidKey = 'America/Bogota',
                               LocaleSidKey = 'es', EmailEncodingKey = 'ISO-8859-1', ProfileId = prof[0].id,LanguageLocaleKey = 'es');
        insert ownerU;  
        
        ISSM_TypificationMatrix__c objISSM_TypificationMatrix = new ISSM_TypificationMatrix__c(
            ISSM_TypificationLevel1__c = 'Solicitudes',
            ISSM_TypificationLevel2__c = 'Visita',
            ISSM_TypificationLevel3__c = 'SVP - Cliente Potencial',
            ISSM_TypificationLevel4__c = '',
            //CS_OT_Type_of_request__c = fieldResult.getPicklistValues()[0].value,
            CS_OT_Type_of_request__c = 'Cliente Nuevo',
            ISSM_Countries_ABInBev__c = 'El Salvador',
            ISSM_CaseRecordType__c = 'CS_RT_Generate_Client ',
            HONES_CF_RecordType__c = 'CS_RT_Prospect',
            ISSM_AssignedTo__c = 'Telesales Manager',
            ISSM_OwnerQueue__c = 'SV_WithoutOwner',
            ISSM_OwnerUser__c = ownerU.Id,
            ISSM_Email1CommunicationLevel3__c = 'test@test.com',
            CS_Days_to_End__c = 7,
            CS_Dias_Primer_Contacto__c = 1
        );
        
        insert objISSM_TypificationMatrix;
        
        Account oNewAccount = new Account(Name = 'Account Test', OwnerId = ownerU.Id, ONTAP__ExternalKey__c='SVCS010012345678');   
        insert oNewAccount;                
        
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        
		ONTAP__Account_Asset__c assetAcc = new ONTAP__Account_Asset__c(ONTAP__Asset_Description__c = 'Description', HONES_EquipmentNumber__c='1234567890', 
                                                                       HONES_Asset_Model__c='model', ONTAP__Serial_Number__c='0987654321',V360_MaterialNumber__c='1029384756', 
                                                                       ONTAP__Brand__c='Brand', ONTAP__Account__c = oNewAccount.id);
        Insert assetAcc;
        
        Schema.DescribeSObjectResult R = ONTAP__Case_Force__c.SObjectType.getDescribe();
        Map<String,Schema.RecordTypeInfo> rtMapByName = R.getRecordTypeInfosByName();
        Schema.RecordTypeInfo recordType = rtMapByName.get('CS_RT_Prospect');
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueuesObject testQueue = new QueueSObject(QueueId = testGroup.id, SObjectType = 'Case');
            insert testQueue;
            
            ISSM_AppSetting_cs__c settings = ISSM_AppSetting_cs__c.getOrgDefaults();
            settings.ISSM_IdQueueWithoutOwner__c = testQueue.QueueId;
            insert settings;
            
            CS_TRIGGER_CASEFORCE_CLASS oTrigger = new CS_TRIGGER_CASEFORCE_CLASS();
            
            ONTAP__Case_Force__c oNewCaseForce = new ONTAP__Case_Force__c();
            oNewCaseForce.ISSM_TypificationLevel1__c = 'Solicitudes';
            oNewCaseForce.ISSM_TypificationLevel2__c = 'Visita';
            oNewCaseForce.ISSM_TypificationLevel3__c = 'SVP - Cliente Potencial';
            oNewCaseForce.ISSM_TypificationLevel4__c = '';
            oNewCaseForce.ONTAP__Status__c = 'Open';
            oNewCaseForce.ONTAP__Subject__c = 'Test Case Force';
            oNewCaseForce.ONTAP__Description__c = 'Test Description';
            oNewCaseForce.RecordTypeId = recordType.getRecordTypeId();
            oNewCaseForce.CS_OT_Type_of_request__c = 'Cliente Nuevo';
            oNewCaseForce.CS_Country__c = 'El Salvador';
            oNewCaseForce.CS_Country_Code__c = 'SV';
            oNewCaseForce.CS_Account_Asset__c = assetAcc.Id;
            oNewCaseForce.ONTAP__Account__c = oNewAccount.Id;
            insert oNewCaseForce;
            
            Case oCase = new Case( OwnerId = UserInfo.getUserId(), AccountId = oNewAccount.Id, ISSM_LocalStreet__c = 'Calle1', ISSM_LocalNumber__c = '90', 
                                  ISSM_TypificationLevel1__c = 'Solicitudes', ISSM_TypificationLevel2__c = 'Visita', ISSM_TypificationLevel3__c = 'SVP - Cliente Potencial', 
                                  ISSM_TypificationLevel4__c = '', ISSM_PostalCode__c = '12345', ISSM_LocalColony__c ='Colonia1',  
                                  HONES_Case_Country__c = 'El Salvador', ISSM_CaseForceNumber__c = oNewCaseForce.Id);
            insert oCase;
            
            oTrigger.afterInsert();
        }       
    }
    @isTest static void test_afterInsertInterlocutorNewData()
    {
        CS_CASE_USER_ESCALATION_CLASS testEscalation= new CS_CASE_USER_ESCALATION_CLASS();
        List<Profile> prof = [Select Name,Id from Profile];
        Schema.DescribeFieldResult fieldResult = ONTAP__Case_Force__c.CS_OT_Type_of_request__c.getDescribe();
        User ownerU = new User(Email = 'user12345@email.com', Username = 'user12345@email.com', LastName = 'Test', Alias = 'Test', TimeZoneSidKey = 'America/Bogota',
                               LocaleSidKey = 'es', EmailEncodingKey = 'ISO-8859-1', ProfileId = prof[0].id,LanguageLocaleKey = 'es');
        insert ownerU;  
        
        ISSM_TypificationMatrix__c objISSM_TypificationMatrix = new ISSM_TypificationMatrix__c(
            ISSM_TypificationLevel1__c = 'Solicitudes',
            ISSM_TypificationLevel2__c = 'Equipo Frío',
            ISSM_TypificationLevel3__c = 'EFI - Instalación',
            ISSM_TypificationLevel4__c = '',
            //CS_OT_Type_of_request__c = fieldResult.getPicklistValues()[0].value,
            CS_OT_Type_of_request__c = 'Instalación',
            ISSM_Countries_ABInBev__c = 'El Salvador',
            ISSM_CaseRecordType__c = 'CS_RT_Installation_Cooler',
            //ISSM_CaseRecordType__c = 'HONES_RT_Maintance_Notice',
            HONES_CF_RecordType__c = 'CS_RT_OT_SV_Coolers',
            ISSM_AssignedTo__c = 'Telesales Manager',
            ISSM_OwnerQueue__c = 'SV_WithoutOwner',
            ISSM_OwnerUser__c = ownerU.Id,
            ISSM_Email1CommunicationLevel3__c = 'test@test.com',
            CS_Days_to_End__c = 7,
            CS_Dias_Primer_Contacto__c = 1
        );
        
        insert objISSM_TypificationMatrix;
        
        Account oNewAccount = new Account(Name = 'Account Test', OwnerId = ownerU.Id, ONTAP__ExternalKey__c='SVCS010012345678');   
        insert oNewAccount;                
        
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        
		ONTAP__Account_Asset__c assetAcc = new ONTAP__Account_Asset__c(ONTAP__Asset_Description__c = 'Description', HONES_EquipmentNumber__c='1234567890', 
                                                                       HONES_Asset_Model__c='model', ONTAP__Serial_Number__c='0987654321',V360_MaterialNumber__c='1029384756', 
                                                                       ONTAP__Brand__c='Brand', ONTAP__Account__c = oNewAccount.id);
        Insert assetAcc;
        
        Schema.DescribeSObjectResult R = ONTAP__Case_Force__c.SObjectType.getDescribe();
        Map<String,Schema.RecordTypeInfo> rtMapByName = R.getRecordTypeInfosByName();
        Schema.RecordTypeInfo recordType = rtMapByName.get('OnTap SV_Coolers');
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueuesObject testQueue = new QueueSObject(QueueId = testGroup.id, SObjectType = 'Case');
            insert testQueue;
            
            ISSM_AppSetting_cs__c settings = ISSM_AppSetting_cs__c.getOrgDefaults();
            settings.ISSM_IdQueueWithoutOwner__c = testQueue.QueueId;
            insert settings;
            
            CS_TRIGGER_CASEFORCE_CLASS oTrigger = new CS_TRIGGER_CASEFORCE_CLASS();
            
            ONTAP__Case_Force__c oNewCaseForce = new ONTAP__Case_Force__c();
            oNewCaseForce.ISSM_TypificationLevel1__c = 'Solicitudes';
            oNewCaseForce.ISSM_TypificationLevel2__c = 'Equipo Frío';
            oNewCaseForce.ISSM_TypificationLevel3__c = 'EFI - Instalación';
            oNewCaseForce.ISSM_TypificationLevel4__c = '';
            oNewCaseForce.ONTAP__Status__c = 'Open';
            oNewCaseForce.ONTAP__Subject__c = 'Test Case Force';
            oNewCaseForce.ONTAP__Description__c = 'Test Description';
            oNewCaseForce.RecordTypeId = recordType.getRecordTypeId();
            oNewCaseForce.CS_OT_Type_of_request__c = 'Instalación';
            oNewCaseForce.CS_Country__c = 'El Salvador';
            oNewCaseForce.CS_Country_Code__c = 'SV';
            oNewCaseForce.CS_Account_Asset__c = assetAcc.Id;
            oNewCaseForce.ONTAP__Account__c = oNewAccount.Id;
            insert oNewCaseForce;
            
            Case oCase = new Case( OwnerId = UserInfo.getUserId(), AccountId = oNewAccount.Id, ISSM_LocalStreet__c = 'Calle1', ISSM_LocalNumber__c = '90', 
                                  ISSM_TypificationLevel1__c = 'Solicitudes', ISSM_TypificationLevel2__c = 'Equipo Frío', ISSM_TypificationLevel3__c = 'EFI - Instalación', 
                                  ISSM_TypificationLevel4__c = '', ISSM_PostalCode__c = '12345', ISSM_LocalColony__c ='Colonia1',  
                                  HONES_Case_Country__c = 'El Salvador', ISSM_CaseForceNumber__c = oNewCaseForce.Id);
            insert oCase;
            
            oTrigger.afterInsert();
        }       
    }
    
    
    
    /**
    * Method for test the method afterInsert for assigned user
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    **/
    /*
    @isTest static void test_afterInsertInterlocutorTypeRequest()
    {
        CS_CASE_USER_ESCALATION_CLASS testEscalation= new CS_CASE_USER_ESCALATION_CLASS();
        List<Profile> prof = [Select Name,Id from Profile];
        Schema.DescribeFieldResult fieldResult = ONTAP__Case_Force__c.CS_OT_Type_of_request__c.getDescribe();
        User ownerU = new User(Email = 'user12345@email.com', Username = 'user12345@email.com', LastName = 'Test', Alias = 'Test', TimeZoneSidKey = 'America/Bogota',
                               LocaleSidKey = 'es', EmailEncodingKey = 'ISO-8859-1', ProfileId = prof[0].id,LanguageLocaleKey = 'es');
        insert ownerU;  
        
        ISSM_TypificationMatrix__c objISSM_TypificationMatrix = new ISSM_TypificationMatrix__c(
            ISSM_TypificationLevel1__c = 'Solicitudes',
            ISSM_TypificationLevel2__c = 'Equipo Frío',
            ISSM_TypificationLevel3__c = 'EFR - Reparación',
            ISSM_TypificationLevel4__c = '',
            CS_OT_Type_of_request__c = fieldResult.getPicklistValues()[0].value,
            ISSM_Countries_ABInBev__c = 'El Salvador',
            ISSM_CaseRecordType__c = 'HONES_RT_Maintance_Notice',
            ISSM_AssignedTo__c = 'Telesales Manager',
            ISSM_OwnerQueue__c = 'SV_WithoutOwner',
            ISSM_OwnerUser__c = ownerU.Id,
            ISSM_Email1CommunicationLevel3__c = 'test@test.com',
            CS_Days_to_End__c = 7,
            CS_Dias_Primer_Contacto__c = 1
        );
        
        insert objISSM_TypificationMatrix;
        
        Account oNewAccount = new Account(Name = 'Account Test', OwnerId = ownerU.Id, ONTAP__ExternalKey__c='SVCS010012345678');   
        insert oNewAccount;                
        
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        
        
        
        Schema.DescribeSObjectResult R = ONTAP__Case_Force__c.SObjectType.getDescribe();
        Map<String,Schema.RecordTypeInfo> rtMapByName = R.getRecordTypeInfosByName();
        Schema.RecordTypeInfo recordType = rtMapByName.get('OnTap SV_Coolers');
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueuesObject testQueue = new QueueSObject(QueueId = testGroup.id, SObjectType = 'Case');
            insert testQueue;
            
            ISSM_AppSetting_cs__c settings = ISSM_AppSetting_cs__c.getOrgDefaults();
            settings.ISSM_IdQueueWithoutOwner__c = testQueue.QueueId;
            insert settings;
            
            CS_TRIGGER_CASEFORCE_CLASS oTrigger = new CS_TRIGGER_CASEFORCE_CLASS();
            
            ONTAP__Case_Force__c oNewCaseForce = new ONTAP__Case_Force__c();
            oNewCaseForce.ISSM_TypificationLevel1__c = 'Solicitudes';
            oNewCaseForce.ISSM_TypificationLevel2__c = 'Equipo Frío';
            oNewCaseForce.ISSM_TypificationLevel3__c = 'EFR - Reparación';
            oNewCaseForce.ISSM_TypificationLevel4__c = '';
            oNewCaseForce.ONTAP__Status__c = 'Open';
            oNewCaseForce.ONTAP__Subject__c = 'Test Case Force';
            oNewCaseForce.ONTAP__Description__c = 'Test Description';
            oNewCaseForce.RecordTypeId = recordType.getRecordTypeId();
            oNewCaseForce.CS_OT_Type_of_request__c = 'Mantenimiento';
            oNewCaseForce.CS_Country__c = 'El Salvador';
            oNewCaseForce.CS_Country_Code__c = 'SV';
            oNewCaseForce.ONTAP__Account__c = oNewAccount.Id;
            insert oNewCaseForce;
            
            Case oCase = new Case( OwnerId = UserInfo.getUserId(), AccountId = oNewAccount.Id, ISSM_LocalStreet__c = 'Calle1', ISSM_LocalNumber__c = '90', 
                                  ISSM_TypificationLevel1__c = 'Solicitudes', ISSM_TypificationLevel2__c = 'Equipo Frío', ISSM_TypificationLevel3__c = 'EFR - Reparación', 
                                  ISSM_TypificationLevel4__c = '', ISSM_PostalCode__c = '12345', ISSM_LocalColony__c ='Colonia1',  
                                  HONES_Case_Country__c = 'El Salvador', ISSM_CaseForceNumber__c = oNewCaseForce.Id);
            insert oCase;
            
            oTrigger.afterInsert(); 
        }
        
    }
    */
    /**
    * Method for test the method afterUpdate
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    
    
    @isTest static void test_afterUpdate()
    {
        CS_TRIGGER_CASEFORCE_CLASS oTrigger = new CS_TRIGGER_CASEFORCE_CLASS();
        
        ONTAP__Case_Force__c oNewCaseForce = new ONTAP__Case_Force__c();
        oNewCaseForce.ONTAP__Status__c = 'New';
        oNewCaseForce.ONTAP__Subject__c = 'Test Case Force';
        oNewCaseForce.ONTAP__Description__c = 'Test Description';
        insert oNewCaseForce;
        
        ONTAP__Case_Force__c oUpdateCaseForce = new ONTAP__Case_Force__c();
        oUpdateCaseForce.ONTAP__Subject__c = 'Test Case Force';
        oUpdateCaseForce.Id = oNewCaseForce.Id;
        ISSM_TriggerManager_cls.Activate();
        Update oUpdateCaseForce;
        
        oTrigger.afterUpdate();   
    }

    @isTest static void Test_SendRequestCoolers()
    {
        List<String>  oltsCase = new List<String> ();
        ONTAP__Case_Force__c  oNewCaseForce = new ONTAP__Case_Force__c ();
        oNewCaseForce.ONTAP__Status__c = 'New';
        oNewCaseForce.ONTAP__Subject__c = 'Test Case Force';
        oNewCaseForce.ONTAP__Description__c = 'Test Description';
        insert oNewCaseForce;

        oltsCase.add(oNewCaseForce.id);
        
	    CS_Trigger_Caseforce_Class.GetQuestionsFirstContact();
	    CS_Trigger_Caseforce_Class.SendRequestCoolers(oltsCase);
	}

    @isTest static void test_GetIdQueueWithOutOwnerSV()       
    {
		ISSM_AppSetting_cs__c appsett = new ISSM_AppSetting_cs__c();
        appsett.ISSM_IdQueueWithoutOwner__c = '00G1k000002KK4uEAG';
		insert appsett;
        
        CS_Trigger_Caseforce_Class.GetIdQueueWithOutOwner('El Salvador', '', appsett);
    }        
    @isTest static void test_GetIdQueueWithOutOwnerSVAG()       
    {
		ISSM_AppSetting_cs__c appsett = new ISSM_AppSetting_cs__c();
        appsett.ISSM_IdQueueWithoutOwner__c = '00G1k000002KK4uEAG';
		insert appsett;
        
        CS_Trigger_Caseforce_Class.GetIdQueueWithOutOwner('El Salvador', 'SV_SacAgent', appsett);
    }
    @isTest static void test_GetIdQueueWithOutOwnerHN()       
    {
		ISSM_AppSetting_cs__c appsett = new ISSM_AppSetting_cs__c();
        appsett.ISSM_IdQueueWithoutOwner__c = '00G1k000002KK4uEAG';
		insert appsett;
        
        CS_Trigger_Caseforce_Class.GetIdQueueWithOutOwner('Honduras', '', appsett);
    }
    @isTest static void test_GetIdQueueWithOutOwnerPA()       
    {
		ISSM_AppSetting_cs__c appsett = new ISSM_AppSetting_cs__c();
        appsett.ISSM_IdQueueWithoutOwner__c = '00G1k000002KK4uEAG';
		insert appsett;
        
        CS_Trigger_Caseforce_Class.GetIdQueueWithOutOwner('Panama', '', appsett);
    }
}