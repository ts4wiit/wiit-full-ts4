/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: HONES_ONCALL_CreditStatus_Contro_Test.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 18/12/2018           Luis Arturo Parra Rosas           Creation of methods.
*/

@isTest
private class HONES_ONCALL_CreditStatus_Contro_Test{
    
    /* * Test to get creditLimitUseCase1 By UserId
*Created By: Luis Arturo Parra 
* Suport contact:  g.martinez.cabral@accenture.com 
* @param void
* @return void
*/
    @isTest static void test_creditLimit_UseCase1(){
        HONES_ONCALL_CreditStatus_Contro obj01 = new HONES_ONCALL_CreditStatus_Contro();
        Account acc =  new Account(Name='test',ISSM_CreditLimit__c=10,ONTAP__Credit_Amount__c=10); 
        
        insert acc;
        
        HONES_ONCALL_CreditStatus_Contro.creditLimit(acc.Id);
        
        
    }
    /* * Test to get creditLimitUseCase2 By UserId
*Created By: Luis Arturo Parra 
* Suport contact:  g.martinez.cabral@accenture.com 
* @param void
* @return void
*/
    
    @isTest static void test_creditLimit_UseCase2(){
        HONES_ONCALL_CreditStatus_Contro obj01 = new HONES_ONCALL_CreditStatus_Contro();
        HONES_ONCALL_CreditStatus_Contro.creditLimit('test data');
    }
    
    /* * Test to get creditLimitUseCase3 By UserId
*Created By: Luis Arturo Parra 
* Suport contact:  g.martinez.cabral@accenture.com 
* @param void
* @return void
*/
    @isTest static void test_creditLimit_UseCase3(){
        HONES_ONCALL_CreditStatus_Contro obj01 = new HONES_ONCALL_CreditStatus_Contro();
        HONES_ONCALL_CreditStatus_Contro.creditLimit(null);
    }
    
    /* * Test to get getById_UseCase1
*Created By: Luis Arturo Parra 
* Suport contact:  g.martinez.cabral@accenture.com 
* @param void
* @return void
*/
    @isTest static void test_getById_UseCase1(){
        Test.startTest();
        HONES_ONCALL_CreditStatus_Contro obj01 = new HONES_ONCALL_CreditStatus_Contro();
        HONES_ONCALL_CreditStatus_Contro.getById('test data');
        Test.stopTest();
    }
    
    /* * Test to get ById_UseCase2
*Created By: Luis Arturo Parra 
* Suport contact:  g.martinez.cabral@accenture.com 
* @param void
* @return void
*/
    
    @isTest static void test_getById_UseCase2(){
        Test.startTest();
        HONES_ONCALL_CreditStatus_Contro obj01 = new HONES_ONCALL_CreditStatus_Contro();
        Account acc =  new Account(Name='test',ISSM_CreditLimit__c=10,ONTAP__Credit_Amount__c=10);
        insert acc;
        ONCALL__Call__c cal =  new ONCALL__Call__c(ONCALL__POC__c=acc.Id); 
        insert cal;
        HONES_ONCALL_CreditStatus_Contro.getById(acc.Id);
        Test.stopTest();
    }
    /* * Test to get FlexibleData By UserId Case1
* *Created By: Luis Arturo Parra 
* Suport contact:  g.martinez.cabral@accenture.com 
* @param void
* @return void
*/
    @isTest static void test_getFlexibleDataById_UseCase1(){
        HONES_ONCALL_CreditStatus_Contro obj01 = new HONES_ONCALL_CreditStatus_Contro();
        HONES_ONCALL_CreditStatus_Contro.getFlexibleDataById('test data');
    }
    /* * Test to get FlexibleData By UserId Case2
* *Created By: Luis Arturo Parra 
* Suport contact:  g.martinez.cabral@accenture.com 
* @param void
* @return void
*/
    @isTest static void test_getFlexibleDataById_UseCase2(){
        HONES_ONCALL_CreditStatus_Contro obj01 = new HONES_ONCALL_CreditStatus_Contro();
        Account acc =  new Account(Name='test',ISSM_CreditLimit__c=10,ONTAP__Credit_Amount__c=10); 
        insert acc;
        ONCALL__Call__c cal =  new ONCALL__Call__c(ONCALL__POC__c=acc.Id); 
        insert cal;
        HONES_ONCALL_CreditStatus_Contro.getFlexibleDataById(acc.Id);
    }
    
    /* * Test to get disponible  Case1
* *Created By: Luis Arturo Parra 
* Suport contact:  g.martinez.cabral@accenture.com 
* @param void
* @return void
*/
    @isTest static void test_disponible_UseCase1(){
        HONES_ONCALL_CreditStatus_Contro obj01 = new HONES_ONCALL_CreditStatus_Contro();
        Account acc =  new Account(Name='test',ONTAP__Credit_Amount__c=10, ONTAP__ExternalKey__c = 'SV');
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();
        insert acc;
        
        ONTAP__OpenItem__c op= new ONTAP__OpenItem__c();
        op.ONTAP__Amount__c=999.05;
        op.ISSM_Debit_Credit__c='H';
        op.ONTAP__Account__c=acc.Id;
        op.ONTAP__SAPCustomerId__c='0010365605';
        op.RecordTypeId = rt_open_items;
        insert op;
        ONTAP__OpenItem__c open=[SELECT ONTAP__Amount__c,ISSM_Debit_Credit__c,ONTAP__SAPCustomerId__c FROM ONTAP__OpenItem__c WHERE ONTAP__SAPCustomerId__c='0010365605' AND RecordTypeId =: rt_open_items LIMIT 1];
        HONES_ONCALL_CreditStatus_Contro.disponible(acc.Id);
    }
    
    /* * Test to get disponible  Case2
* *Created By: Luis Arturo Parra 
* Suport contact:  g.martinez.cabral@accenture.com 
* @param void
* @return void
*/
    
    @isTest static void test_disponible_UseCase2(){
        HONES_ONCALL_CreditStatus_Contro obj01 = new HONES_ONCALL_CreditStatus_Contro();
        Account acc =  new Account(Name='test',ISSM_CreditLimit__c=10,ONTAP__Credit_Amount__c=10, ONTAP__ExternalKey__c = 'SV');
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();
        insert acc;
        
        ONTAP__OpenItem__c op= new ONTAP__OpenItem__c();
        op.ONTAP__Amount__c=999.05;
        op.ISSM_Debit_Credit__c='S';
        op.ONTAP__Account__c=acc.Id;
        op.ONTAP__SAPCustomerId__c='0010365605';
        op.RecordTypeId=rt_open_items;
        insert op;
        ONTAP__OpenItem__c open=[SELECT ONTAP__Amount__c,ISSM_Debit_Credit__c,ONTAP__SAPCustomerId__c FROM ONTAP__OpenItem__c WHERE ONTAP__SAPCustomerId__c='0010365605' AND RecordTypeId =:rt_open_items LIMIT 1];
        HONES_ONCALL_CreditStatus_Contro.disponible(acc.Id);
    }
    /* * Test to get outOfDateCredits
* *Created By: Luis Arturo Parra 
* Suport contact:  g.martinez.cabral@accenture.com 
* @param void
* @return void
*/
    @isTest static void test_outOfDateCredits_UseCase1(){
        HONES_ONCALL_CreditStatus_Contro obj01 = new HONES_ONCALL_CreditStatus_Contro();
        Account acc =  new Account(Name='test',ONTAP__Credit_Amount__c=10, ONTAP__ExternalKey__c = 'SV');
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();
        insert acc;
        
        ONTAP__OpenItem__c op= new ONTAP__OpenItem__c();
        op.ONTAP__Amount__c=999.05;
        op.ISSM_Debit_Credit__c='S';
        op.ONTAP__Account__c=acc.Id;
        op.ONTAP__SAPCustomerId__c='0010365605';
        op.ONTAP__DueDate__c=System.today()-1;
        //System.debug(op.ONTAP__DueDate__c);
        op.RecordTypeId= rt_open_items;
        insert op;
        ONTAP__OpenItem__c open=[SELECT ONTAP__Amount__c,ISSM_Debit_Credit__c,ONTAP__SAPCustomerId__c FROM ONTAP__OpenItem__c WHERE ONTAP__SAPCustomerId__c='0010365605' AND RecordTypeId =: rt_open_items LIMIT 1];
        HONES_ONCALL_CreditStatus_Contro.outOfDateCredits(acc.Id);
    }
    /* * Test to get outOfDateCredits
* *Created By: Luis Arturo Parra 
* Suport contact:  g.martinez.cabral@accenture.com 
* @param void
* @return void*/
    @isTest static void test_outOfDateCredits_UseCase2(){
        HONES_ONCALL_CreditStatus_Contro obj01 = new HONES_ONCALL_CreditStatus_Contro();
        Account acc =  new Account(Name='test',ONTAP__Credit_Amount__c=10, ONTAP__ExternalKey__c = 'SV');
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();
        insert acc;
        
        ONTAP__OpenItem__c op= new ONTAP__OpenItem__c();
        op.ONTAP__Amount__c=999.05;
        op.ISSM_Debit_Credit__c='H';
        op.ONTAP__Account__c=acc.Id;
        op.ONTAP__SAPCustomerId__c='0010365605';
        op.ONTAP__DueDate__c=System.today()-1;
        //System.debug(op.ONTAP__DueDate__c);
        op.RecordTypeId= rt_open_items;
        insert op;
        ONTAP__OpenItem__c open=[SELECT ONTAP__Amount__c,ISSM_Debit_Credit__c,ONTAP__SAPCustomerId__c FROM ONTAP__OpenItem__c WHERE ONTAP__SAPCustomerId__c='0010365605' AND RecordTypeId =: rt_open_items LIMIT 1];
        HONES_ONCALL_CreditStatus_Contro.outOfDateCredits(acc.Id);
    }
    /* * Test to get outOfDateCreditsLiquid 
* *Created By: Luis Arturo Parra 
* Suport contact:  g.martinez.cabral@accenture.co
* @param void
* @return void
*/
    @isTest static void test_outOfDateCreditsLiquid_UseCase1(){
        HONES_ONCALL_CreditStatus_Contro obj01 = new HONES_ONCALL_CreditStatus_Contro();
        Account acc =  new Account(Name='test',ONTAP__Credit_Amount__c=10, ONTAP__ExternalKey__c = 'SV');
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();
        insert acc;
        
        ONTAP__OpenItem__c op= new ONTAP__OpenItem__c();
        op.ONTAP__Amount__c=999.05;
        op.ISSM_Debit_Credit__c='S';
        op.ONTAP__Account__c=acc.Id;
        op.ONTAP__SAPCustomerId__c='0010365605';
        op.ONTAP__DueDate__c=System.today()-1;
        //System.debug(op.ONTAP__DueDate__c);
        op.RecordTypeId=rt_open_items;
        op.V360_Reference__c='L';
        insert op;
        ONTAP__OpenItem__c open=[SELECT ONTAP__Amount__c,ISSM_Debit_Credit__c,ONTAP__SAPCustomerId__c FROM ONTAP__OpenItem__c WHERE ONTAP__SAPCustomerId__c='0010365605' AND V360_Reference__c= 'L' AND RecordTypeId =: rt_open_items LIMIT 1];
        HONES_ONCALL_CreditStatus_Contro.outOfDateCreditsLiquid(acc.Id);
    }
      /* * Test to get outOfDateCreditsLiquid 
* *Created By: Luis Arturo Parra 
* Suport contact:  g.martinez.cabral@accenture.co
* @param void
* @return void
*/
    @isTest static void test_outOfDateCreditsLiquid_UseCase2(){
        HONES_ONCALL_CreditStatus_Contro obj01 = new HONES_ONCALL_CreditStatus_Contro();
        Account acc =  new Account(Name='test',ONTAP__Credit_Amount__c=10, ONTAP__ExternalKey__c = 'SV');
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();
        insert acc;
        
        ONTAP__OpenItem__c op= new ONTAP__OpenItem__c();
        op.ONTAP__Amount__c=999.05;
        op.ISSM_Debit_Credit__c='H';
        op.ONTAP__Account__c=acc.Id;
        op.ONTAP__SAPCustomerId__c='0010365605';
        op.ONTAP__DueDate__c=System.today()-1;
        //System.debug(op.ONTAP__DueDate__c);
        op.RecordTypeId= rt_open_items;
        op.V360_Reference__c='L';
        insert op;
        ONTAP__OpenItem__c open=[SELECT ONTAP__Amount__c,ISSM_Debit_Credit__c,ONTAP__SAPCustomerId__c FROM ONTAP__OpenItem__c WHERE ONTAP__SAPCustomerId__c='0010365605'  AND V360_Reference__c= 'L' AND RecordTypeId =: rt_open_items LIMIT 1];
        HONES_ONCALL_CreditStatus_Contro.outOfDateCreditsLiquid(acc.Id);
    }
    /* * Test to get outOfDateCreditsContainer 
* *Created By: Luis Arturo Parra 
* Suport contact:  g.martinez.cabral@accenture.com 
* @param void
* @return void
*/
    @isTest static void test_outOfDateCreditsContainer_UseCase1(){
        HONES_ONCALL_CreditStatus_Contro obj01 = new HONES_ONCALL_CreditStatus_Contro();
        Account acc =  new Account(Name='test',ONTAP__Credit_Amount__c=10, ONTAP__ExternalKey__c = 'SV');
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();
        insert acc;
        
        ONTAP__OpenItem__c op= new ONTAP__OpenItem__c();
        op.ONTAP__Amount__c=999.05;
        op.ISSM_Debit_Credit__c='S';
        op.ONTAP__Account__c=acc.Id;
        op.ONTAP__SAPCustomerId__c='0010365605';
        op.ONTAP__DueDate__c=System.today()-1;
        //System.debug(op.ONTAP__DueDate__c);
        op.RecordTypeId=rt_open_items;
        op.V360_Reference__c='L';
        insert op;
        ONTAP__OpenItem__c open=[SELECT ONTAP__Amount__c,ISSM_Debit_Credit__c,ONTAP__SAPCustomerId__c FROM ONTAP__OpenItem__c WHERE ONTAP__SAPCustomerId__c='0010365605'  AND V360_Reference__c= 'L' AND RecordTypeId =: rt_open_items LIMIT 1];
        HONES_ONCALL_CreditStatus_Contro.outOfDateCreditsContainer(acc.Id);
    }
    /* * Test to get dateCredits 
* *Created By: Luis Arturo Parra 
* Suport contact:  g.martinez.cabral@accenture.com 
* @param void
* @return void
*/
    @isTest static void test_dateCredits_UseCase1(){
       	HONES_ONCALL_CreditStatus_Contro obj01 = new HONES_ONCALL_CreditStatus_Contro();
        Account acc =  new Account(Name='test',ONTAP__Credit_Amount__c=10, ONTAP__ExternalKey__c = 'SV');
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();
        insert acc;
        
        ONTAP__OpenItem__c op= new ONTAP__OpenItem__c();
        op.ONTAP__Amount__c=999.05;
        op.ISSM_Debit_Credit__c='H';
        op.ONTAP__Account__c=acc.Id;
        op.ONTAP__SAPCustomerId__c='0010365605';
        op.ONTAP__DueDate__c=System.today()+1;
        //System.debug(op.ONTAP__DueDate__c);
        op.RecordTypeId= rt_open_items;
        
        insert op;
        List<ONTAP__OpenItem__c> open=[SELECT ONTAP__Amount__c,ISSM_Debit_Credit__c,ONTAP__SAPCustomerId__c FROM ONTAP__OpenItem__c WHERE ONTAP__SAPCustomerId__c='0010365605'  AND RecordTypeId =: rt_open_items LIMIT 1];
        if(open.size() > 0 ){
        HONES_ONCALL_CreditStatus_Contro.dateCredits(acc.Id);
        }
    }
    /* * Test to get Total 
* *Created By: Luis Arturo Parra 
* Suport contact:  g.martinez.cabral@accenture.com 
* @param void
* @return void
*/
    @isTest static void test_total_UseCase1(){
        HONES_ONCALL_CreditStatus_Contro obj01 = new HONES_ONCALL_CreditStatus_Contro();
        Account acc =  new Account(Name='test',ONTAP__Credit_Amount__c=10, ONTAP__ExternalKey__c = 'SV');
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();
        insert acc;
        
        ONTAP__OpenItem__c op= new ONTAP__OpenItem__c();
        op.ONTAP__Amount__c=999.05;
        op.ISSM_Debit_Credit__c='H';
        op.ONTAP__Account__c=acc.Id;
        op.ONTAP__SAPCustomerId__c='0010365605';
        op.ONTAP__DueDate__c=System.today()+1;
        //System.debug(op.ONTAP__DueDate__c);
        op.RecordTypeId=rt_open_items;
        
        insert op;
        ONTAP__OpenItem__c open=[SELECT ONTAP__Amount__c,ISSM_Debit_Credit__c,ONTAP__SAPCustomerId__c FROM ONTAP__OpenItem__c WHERE ONTAP__SAPCustomerId__c='0010365605'   AND RecordTypeId =: rt_open_items LIMIT 1];
        HONES_ONCALL_CreditStatus_Contro.total(acc.Id);
    }
     @isTest static void test_total_UseCase2(){
        HONES_ONCALL_CreditStatus_Contro obj01 = new HONES_ONCALL_CreditStatus_Contro();
        Account acc =  new Account(Name='test',ONTAP__Credit_Amount__c=10, ONTAP__ExternalKey__c = 'SV');
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();
        insert acc;
        
        ONTAP__OpenItem__c op= new ONTAP__OpenItem__c();
        op.ONTAP__Amount__c=999.05;
        op.ISSM_Debit_Credit__c='H';
        op.ONTAP__Account__c=acc.Id;
        op.ONTAP__SAPCustomerId__c='0010365605';
        op.ONTAP__DueDate__c=System.today()-1;
        //System.debug(op.ONTAP__DueDate__c);
        op.RecordTypeId=rt_open_items;
        
        insert op;
        ONTAP__OpenItem__c open=[SELECT ONTAP__Amount__c,ISSM_Debit_Credit__c,ONTAP__SAPCustomerId__c FROM ONTAP__OpenItem__c WHERE ONTAP__SAPCustomerId__c='0010365605'   AND RecordTypeId =: rt_open_items LIMIT 1];
        HONES_ONCALL_CreditStatus_Contro.total(acc.Id);
    }
    /* * Test to get onTimeDateCreditsLiquid 
* *Created By: Luis Arturo Parra 
* Suport contact:  g.martinez.cabral@accenture.com 
* @param void
* @return void
*/
    @isTest static void test_onTimeDateCreditsLiquid_UseCase1(){
        HONES_ONCALL_CreditStatus_Contro obj01 = new HONES_ONCALL_CreditStatus_Contro();
        Account acc =  new Account(Name='test',ONTAP__Credit_Amount__c=10, ONTAP__ExternalKey__c = 'SV');
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();
        insert acc;
        
        ONTAP__OpenItem__c op= new ONTAP__OpenItem__c();
        op.ONTAP__Amount__c=999.05;
        op.ISSM_Debit_Credit__c='H';
        op.ONTAP__Account__c=acc.Id;
        op.ONTAP__SAPCustomerId__c='0010365605';
        op.ONTAP__DueDate__c=System.today()+1;
        //System.debug(op.ONTAP__DueDate__c);
        op.RecordTypeId= rt_open_items;
        op.V360_Reference__c='L';
        
        insert op;
        ONTAP__OpenItem__c open=[SELECT ONTAP__Amount__c,ISSM_Debit_Credit__c,ONTAP__SAPCustomerId__c FROM ONTAP__OpenItem__c WHERE ONTAP__SAPCustomerId__c='0010365605'  AND V360_Reference__c= 'L'  AND RecordTypeId =: rt_open_items LIMIT 1];
                
        HONES_ONCALL_CreditStatus_Contro.onTimeDateCreditsLiquid(acc.Id);
    }
    /* * Test to get onTimeDateCreditsContainer
* *Created By: Luis Arturo Parra 
* Suport contact:  g.martinez.cabral@accenture.com 
* @param void
* @return void
*/
    @isTest static void test_onTimeDateCreditsContainer_UseCase1(){
       	HONES_ONCALL_CreditStatus_Contro obj01 = new HONES_ONCALL_CreditStatus_Contro();
        Account acc =  new Account(Name='test',ONTAP__Credit_Amount__c=10, ONTAP__ExternalKey__c = 'SV');
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();
        insert acc;
        
        ONTAP__OpenItem__c op= new ONTAP__OpenItem__c();
        op.ONTAP__Amount__c=999.05;
        op.ISSM_Debit_Credit__c='H';
        op.ONTAP__Account__c=acc.Id;
        op.ONTAP__SAPCustomerId__c='0010365605';
        op.ONTAP__DueDate__c=System.today()-1;
        //System.debug(op.ONTAP__DueDate__c);
        op.RecordTypeId=rt_open_items;
        op.V360_Reference__c='R';
        insert op;
        ONTAP__OpenItem__c open=[SELECT ONTAP__Amount__c,ISSM_Debit_Credit__c,ONTAP__SAPCustomerId__c FROM ONTAP__OpenItem__c WHERE ONTAP__SAPCustomerId__c='0010365605'  AND V360_Reference__c= 'R' AND RecordTypeId =: rt_open_items LIMIT 1];
        HONES_ONCALL_CreditStatus_Contro.onTimeDateCreditsContainer(acc.Id);
    }
}