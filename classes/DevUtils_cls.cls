/*******************************************************************************
Desarrollado por: Avanxo México
Autor: Daniel Peñaloza
Proyecto: Modelo - DSD
Descripción: Clase con métodos globales utilitarios para otros desarrollos

------ ---------- -------------------------- -----------------------------------
No.    Fecha      Autor                      Descripción
------ ---------- -------------------------- -----------------------------------
1.0    18/08/2017 Daniel Peñaloza            Clase creada
*******************************************************************************/

public class DevUtils_cls {
    public static final String DATA_TYPE_BOOLEAN = 'BOOLEAN';
    public static final String DATA_TYPE_DOUBLE  = 'DOUBLE';
    public static final String DATA_TYPE_INTEGER = 'INTEGER';
    public static final String DATA_TYPE_STRING  = 'STRING';

    public static Map<String, SObjectField> getFieldsMap(String sObjectName) {
        return Schema.getGlobalDescribe().get(sObjectName).getDescribe().fields.getMap();
    }

    /**
     * Get visit plan settings record with the specified developer name
     * @param  developerName Developer name of the visit plan settings record
     * @return               Visit Plan Settings record
     */
    public static VisitPlanSettings__mdt getVisitPlanSettings(String developerName) {
        VisitPlanSettings__mdt settings = [
            SELECT
                Id
                ,MasterLabel
                ,DeveloperName
                ,BatchSize__c
                ,BatchSizeHK__c
                ,FilterServiceModels__c
                ,InitialCallStatus__c
                ,AssignedTourStatus__c
                ,CreatedTourStatus__c
                ,AssignedTourSubStatus__c
                ,CreatedTourSubStatus__c
                ,NameTourId__c
                ,InitialVisitStatus__c
                ,InitialVisitSubStatus__c
                ,NAServiceModel__c
                ,NameOncall__c
                ,RTAutosales__c
                ,RTBdr__c
                ,RTPresales__c
                ,RTTelesales__c
                ,VisitPeriodConfig__c
                ,VisitSubject__c
                ,OwnerObject__c
                ,OwnerField__r.DataType
                ,OwnerField__r.DeveloperName
                ,OwnerField__r.Label
            FROM VisitPlanSettings__mdt
            WHERE DeveloperName = :developerName
            LIMIT 1
        ];

        return settings;
    }

    /**
     * * Get default visit plan settings record
     * @return Visit Plan Settings record
     */
    public static VisitPlanSettings__mdt getVisitPlanSettings() {
        String defaultSettingsName = 'DefaultSettings';
        return getVisitPlanSettings(defaultSettingsName);
    }

    /**
     * Get account-route settings record with the specified developer name
     * @param  developerName Developer name of the settings record
     * @return               Account-Route Settings record
     */
    public static AccountRouteSettings__mdt getAccountRouteSettings(String developerName) {
        AccountRouteSettings__mdt settings = [
            SELECT
                Id
                ,MasterLabel
                ,DeveloperName
                ,AccountRecordTypeToValidate__c
                ,ServiceModelsToFilter__c
                ,SuperVisorTeamMemberRole__c
                ,SalesAgentTeamMemberRole__c
            	,UserToAssignOrphanAccount__c
            FROM AccountRouteSettings__mdt
            WHERE DeveloperName = :developerName
            LIMIT 1
        ];

        return settings;
    }

    /**
     * Get default Account-Route settings record
     * @return Account-Route Settings record
     */
    public static AccountRouteSettings__mdt getAccountRouteSettings() {
        String defaultSettingsName = 'DefaultAccountRouteSettings';
        return getAccountRouteSettings(defaultSettingsName);
    }

    /**
     * Get list of Tour Status Settings for different validations y dependent code
     * @return TourStatusSettings__mdt records list
     */
    public static TourStatusSettings__mdt[] getTourStatusSettings() {
        return [
            SELECT
                Id
                ,MasterLabel
                ,AllowsEventDeletion__c
                ,IsActive__c
                ,Status__c
                ,Substatus__c
                ,RecordTypesToExclude__c
            FROM TourStatusSettings__mdt
            WHERE IsActive__c = true
        ];
    }

    /**
     * Get list of Tour Status Settings for different validations y dependent code
     * @return TourStatusSettings__mdt records list
     */
    public static ObjectValidationSettings__mdt[] getObjectValidationSettings(String objectName) {
        return [
            SELECT
                Id
                ,MasterLabel
                ,DeleteFromTour__c
                ,DeleteFromVisitPlan__c
                ,FieldToValidate__c
                ,FieldToValidate__r.DataType
                ,FieldToValidate__r.DeveloperName
                ,FieldToValidate__r.Label
                ,ObjectToValidate__c
                ,ValueToCompare__c
            FROM ObjectValidationSettings__mdt
            WHERE IsActive__c = true
                AND ObjectToValidate__c = :objectName
        ];
    }

    /**
     * Filter a list of records based on a field and a list of possible values
     * @param  lstObjects     List of records to filter
     * @param  fieldName      API Name of the text field to compare
     * @param  setValidValues Set of valid values to filter records
     * @return                List of filtered records
     */
    public static SObject[] filterSObjectList(SObject[] lstObjects, String fieldName, Set<String> setValidValues) {
        SObject[] lstFilteredObjects = new List<SObject>();

        for (SObject obj : lstObjects) {
            if (setValidValues.contains(String.valueOf(obj.get(fieldName)))) {
                lstFilteredObjects.add(obj);
            }
        }

        return lstFilteredObjects;
    }

    /**
       @method Filter a list of records based on a field and a list of possible values
     * @param  newLstObjects      List of records to filter
     * @param  oldMapObjects      Map containing triggerOld values
     * @param  fieldsToCompare    Fields to compare API names 
     * @return SObject[] lstFilteredObjects  List of filtered records
     */
    public static SObject[] diffSObjectList(SObject[] newLstObjects, Map<Id,SObject> oldMapObjects, String[] fieldsToCompare) {
    	System.debug('***************** ENTRO   DevUtils_cls.diffSObjectList* ***************+');
        SObject[] lstFilteredObjects = new List<SObject>();
        Integer differencesFound = 0;

        for (SObject obj : newLstObjects) {
            for(String fieldName : fieldsToCompare){
                if(obj.get(fieldName) != oldMapObjects.get((Id)obj.get('Id')).get(fieldName)){
                    differencesFound++;
                }
            }
            if (differencesFound == fieldsToCompare.size()) {
                lstFilteredObjects.add(obj);
            }
            differencesFound = 0;
        }

        return lstFilteredObjects;
    }

    /**
     * Obtener mapa de tipos de registro de un objeto usando un campo especificado como llave
     * @param      sObjectName  Nombre del objeto para obtener tipos de registro
     * @param      keyField     Campo llave a utilizar en el mapa.
     * @return                  Mapa de tipos de registro
     */
    public static Map<String, RecordType> getRecordTypes(String sObjectName, String keyField) {
        Map<String, RecordType> mapRecordTypes = new Map<String, RecordType>();

        // Obtener lista de tipos de registro
        RecordType[] lstRecordTypes = [
            SELECT Id, DeveloperName, Name, SobjectType, Description
            FROM RecordType
            WHERE SobjectType = :sObjectName AND IsActive = true
        ];

        // Agregar resultados al mapa usando el tipo de objeto y el campo clave especificado
        for (RecordType recType : lstRecordTypes) {
            mapRecordTypes.put( (String)recType.get(keyField), recType );
        }

        return mapRecordTypes;
    }

    /**
     * Generate an AccountTeamMember record
     * @param  accountId Account Id
     * @param  userId    User Id
     * @param  roleName  Role name
     * @return           AccountTeamMember record
     */
    public static AccountTeamMember generateAccountTeamMember(Id accountId, Id userId, String roleName) {
        AccountTeamMember teamMember = new AccountTeamMember(
            AccountId      = accountId,
            UserId         = userId,
            TeamMemberRole = roleName
        );

        return teamMember;
    }

    /**
     * Adds an user to account team.
     * @param      teamMemberRole  The team member role
     */
    public static void addUserToAccountTeam(String accountId, String userId, Set<String> setMemberRoles) {
        AccountTeamMember[] lstTeamMembers = new List<AccountTeamMember>();

        if (setMemberRoles == null || setMemberRoles.isEmpty()) {
            return;
        }

        for (String roleName: setMemberRoles) {
            AccountTeamMember atm = DevUtils_cls.generateAccountTeamMember(accountId, userId, roleName);
            lstTeamMembers.add(atm);
        }

        insert lstTeamMembers;
    }

    /**
      @method getUserFromUsername: get user object from Username
    * @param String username
    * @return User
    **/
    public static User getUserFromUsername(String username){
        User returnUser;
        for(User u : [SELECT Id FROM User WHERE Username =: username LIMIT 1]){
            returnUser = u;
        }
        return returnUser;
    }
}