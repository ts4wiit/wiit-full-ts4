/**
* @author João Luiz
* @date 28/05/2019
* @description Class - This class cover the test for FeedCommentTrigger ; FeedCommentTriggerHandler ; FeedCommentService
*                      * SeeAllData needed because ConnectApi
*/
@isTest(SeeAllData=true)
public class FeedCommentServiceTest {
	
    @isTest
	private static void setup() {
        Profile p = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
        
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', Firstname='UserTest' , LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='abitestpermissionsetuser@testorg.com');
        insert u;
        u = [select id,username,name from user where id=: u.id];
        
        //Create Push Notification
        ONTAP__Push_Notifications__c pushNotifications = new ONTAP__Push_Notifications__c();
        pushNotifications.Name = 'ChatterPushNotificationTest';
        pushNotifications.ONTAP__Android_Enabled__c = true;
        pushNotifications.ONTAP__iOS_Enabled__c = true;
        insert pushNotifications;
        		       
		ONTAP__Triggers_Settings__c triggersSettings = new ONTAP__Triggers_Settings__c();
        triggersSettings.Name = 'Triggers_Settings__c';
        triggersSettings.ONTAP__Active__c = true;
        insert triggersSettings;        
 
 
		//Create a new FeedItem
		FeedItem fItem = new FeedItem();
		//fItem.Body = 'Hello!!! @' + User.Name;
        fItem.Body = 'Hello!!!  @Manager ' +u.id;
		fItem.ParentId = u.id;
		fItem.Title = 'FileName';
		insert fItem;
     	System.debug('Item = '+ fItem);    
       
		//Create a new FeedItem
		FeedComment fComment = new FeedComment();
		fComment.feedItemId = fItem.Id;
		fComment.commentBody = 'Hello 2!!! @Manage'+  u.Id  ;
        
		insert fComment;
 		
        List<FeedComment> listFeedComment = new List<FeedComment>{fComment};
        FeedCommentService.mentionSegmentItem(listFeedComment);
         
	}

     @isTest
    static void createMention(){
        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
        ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
        
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        
        mentionSegmentInput.id = Userinfo.getUserId();
        messageBodyInput.messageSegments.add(mentionSegmentInput);
        
        textSegmentInput.text = 'Could you take a look?';
        messageBodyInput.messageSegments.add(textSegmentInput);
        
        feedItemInput.body = messageBodyInput;
        feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
        feedItemInput.subjectId = Userinfo.getUserId();
        
        ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
    }

}