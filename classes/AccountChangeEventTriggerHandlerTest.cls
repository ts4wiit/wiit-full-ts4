@isTest
public class AccountChangeEventTriggerHandlerTest {


    @isTest
    static void testAccountChangeEvent(){
               
        Test.startTest();
        
        Test.enableChangeDataCapture();   
            
        Account account = DRFixtureFactory.newDRAccount();
        
        account.Phone = '(552) 334-1233';
        update account;
        
        Test.getEventBus().deliver();
        
        account.ONTAP__Secondary_Phone__c = '(552) 334-1239';
        update account;
        
        Test.getEventBus().deliver();
        
        account.ONTAP__Neighborhood__c = 'Capao Redondo';
        update account;
        
        Test.getEventBus().deliver();
        
        account.ShippingStreet = 'Rua dos Xaverianos';
        update account;
        
        Test.getEventBus().deliver();
        
        account.ONTAP__Latitude__c = '29.2222';
        update account;
        
        Test.getEventBus().deliver();
        
        account.ONTAP__Longitude__c = '45.4545';
        update account;
        
        Test.getEventBus().deliver();
        
        account.ONTAP__Province__c = 'Chiuaua';
        update account;
        
        Test.getEventBus().deliver();
        
        account.ONTAP__District__c = 'Itatinga';
        update account;
        
        Test.getEventBus().deliver();
        
        account.ONTAP__Contact_Last_Name__c = 'Killminster';
        update account;
        
        Test.getEventBus().deliver();
        
        Test.stopTest();
        
    }    
    
}