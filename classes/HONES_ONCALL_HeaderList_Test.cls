/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: HONES_ONCALL_HeaderList_Test.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                          Description
* 01/10/2019           Michelle Valderrama           Creation of methods.
*/

/*Headers  for OnCall Application Test Class*/

@isTest
public class HONES_ONCALL_HeaderList_Test {
   
	/**
    * Test method for a header list
    * Created By: m.valderrama.yapor@accenture.com
    * @param void
    * @return void
    */   
 	@isTest static void HONES_ONCALL_HEAD(){
     
        HONES_ONCALL_HeaderList tester =new HONES_ONCALL_HeaderList();
         
        tester.salesOrg ='test1';
        tester.sfdcId = 'test2';
        tester.deliveryDate = 'test3';
        tester.paymentMethod = 'test4';
        tester.orderType = 'test5';
        tester.orderReason = 'test6';
        tester.customerOrder = 'test7';
   }
}