@isTest
public class FlexibleKPI_Test {
    
    @isTest
    static void testFlexibleKPIs(){    
    
       Account account = DRFixtureFactory.newDRAccount();
       User bdr = DRFixtureFactory.newBDRUser( 'bdr@xyz.com.do', 'bdrtest' );
 
       Test.startTest(); 
        
       Flexible_KPI__c flexKPI = new Flexible_KPI__c( Account__c = account.Id, Country_Code__c = 'DO', Name = 'Test', Value__c = '5' , Route__c = bdr.User_Route__c);
           
       insert flexKPI;
        
       Flexible_KPI__c flexKPI2 = new Flexible_KPI__c( Account__c = account.Id, Country_Code__c = 'DO', Name = 'Test', Value__c = '5' , Route__c = 'DO9999'); 
        
       insert flexKPI2; 
        
       Test.stopTest();
    
    }

}