/**
* @author João Luiz
* @date 28/05/2019
* @description Class - Class for methods service on feed item
*/
public class FeedItemService  {
    
     /**
     * @description : Send push notification for the app about the user mention on item
     * @param List<FeedItem> listFeed : list of new feed item from trigger
     * @return void
    */
    public static void mentionSegmentItem(List<FeedItem> listFeed){
        
        String communityId = null;
        Set<Id> listUserIds = new Set<Id>();
        Set<Id> listFeedId = new Set<Id>();
        Map<Id, Set<Id>> mapIdUserByListFeedId = new Map<Id, Set<Id>>();        
        Map<Id, User> mUsersLoopkup = new Map<Id, User>();
        Map<Id, String> mapIdUserByContent = new Map<Id, String>();
        
        for(FeedItem feeds : listFeed){
            if(feeds.insertedbyid != null && !mUsersLoopkup.containskey(feeds.insertedbyid)){
                mUsersLoopkup.put(feeds.insertedbyid,new user());
            }            
        }

        if(!mUsersLoopkup.isEmpty()){
            mUsersLoopkup = new Map<Id, User>([SELECT Id, Name FROM User WHERE Id in:mUsersLoopkup.keyset()]);
        }
       
        for(FeedItem feeds : listFeed){				
            String feedNowId = feeds.Id;
            
            if(feeds.CreatedById != feeds.ParentId){
                listUserIds.add(feeds.ParentId);
                listFeedId.add(feeds.Id);
                mapIdUserByListFeedId.put(feeds.ParentId, listFeedId);
            }

            ConnectApi.FeedElement feedItem = ConnectApi.ChatterFeeds.getFeedElement(communityId, feedNowId);
            ConnectApi.MentionSegment mentionSegment = null;
            
            List<ConnectApi.MessageSegment> messageSegments = feedItem.body.messageSegments;
            
            System.debug(logginglevel.INFO,'FeedItemServices.mentionSegmentItem.feedItem.body.text: '+ feedItem.body.text); 
            System.debug(logginglevel.INFO,'FeedItemServices.mentionSegmentItem.messageSegments: '+ messageSegments); 

            String strUser = mUsersLoopkup.containskey(feeds.insertedbyid) ? ' by '+ mUsersLoopkup.get(feeds.insertedbyid).name : ' ' ;
             System.debug(logginglevel.INFO,'FeedItemServices.mentionSegmentItem.strUser: '+ strUser); 
            String strContent = feedItem.body.text.unescapeHtml4().unescapeJava() + strUser; 

            for (ConnectApi.MessageSegment messageSegment : messageSegments) {               
                
                if (messageSegment instanceof ConnectApi.MentionSegment) {
                    mentionSegment = (ConnectApi.MentionSegment) messageSegment;
                    listUserIds.add(mentionSegment.record.Id);
                    listFeedId.add(feeds.Id);
                    mapIdUserByListFeedId.put(mentionSegment.record.Id, listFeedId);

                    if(!mapIdUserByContent.containsKey(mentionSegment.record.Id)){
                        mapIdUserByContent.put(mentionSegment.record.Id, strContent.normalizeSpace());
                    }
                } 
                
                if (messageSegment instanceof ConnectApi.TextSegment) {
                    ConnectApi.TextSegment textSegment = (ConnectApi.TextSegment) messageSegment;

                    if(!mapIdUserByContent.containsKey(feeds.parentId)){
                        mapIdUserByContent.put(feeds.parentId, strContent.normalizeSpace());
                    }

                }
            }
        }

        System.debug(logginglevel.INFO, 'FeedItemService.mapIdUserByContent: '+ mapIdUserByContent); 
        System.debug(logginglevel.INFO, 'FeedItemServices.mentionSegmentItem.listUserIds: '+ listUserIds);

        FeedPushUtil feedUtil = new FeedPushUtil(mapIdUserByListFeedId, mapIdUserByContent);
        feedUtil.sendPushNotfication();

    }

}