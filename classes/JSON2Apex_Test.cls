/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: JSON2Apex_Test.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 18/12/2018           Luis Arturo Parra Rosas           Creation of methods.
*/

@isTest
private class JSON2Apex_Test{
  @isTest static void test_parse_UseCase1(){
    JSON2Apex obj01 = new JSON2Apex();
    JSON2Apex.Order objOrd = new JSON2Apex.Order();
    objOrd.country = 'test data';
    objOrd.accountId = 'test data';
    objOrd.salesOrg = 'test data';
    objOrd.paymentMethod = 'test data';
    objOrd.orderType = 'test data';
    objOrd.total = 1.1;
    objOrd.subtotal = 1.1;
    objOrd.tax = 1.1;
    objOrd.discount = 1.1;
    JSON2Apex.Empties objEmp = new JSON2Apex.Empties();
    objOrd.empties = objEmp;
    JSON2Apex.Items objItems = new JSON2Apex.Items();
    List<JSON2Apex.Items> lstIt = new List<JSON2Apex.Items>();
    lstIt.add(objItems);
    JSON2Apex.OutputMessages objOutMess = new JSON2Apex.OutputMessages();
    List<JSON2Apex.OutputMessages> lstOutMess = new List<JSON2Apex.OutputMessages>();
    lstOutMess.add(objOutMess);
    obj01.outputMessages = new List<JSON2Apex.OutputMessages>();
    obj01.order = new JSON2Apex.Order();
    JSON2Apex.Empties obj151 = new JSON2Apex.Empties();
    obj151.totalAmount = '10';
    obj151.minimumRequired = '10';
    obj151.extraAmount = 10;
    JSON2Apex.Items obj201 = new JSON2Apex.Items();

    obj201.sku = 'test data';
    obj201.quantity = 'test data';
    obj201.freeGood = false;
    obj201.discount = 1.1;
    obj201.tax = 1.1;
    obj201.subtotal = 1.1;
    obj201.total = 1.1;
    obj201.price = 1.1;
    obj201.unitPrice = 1.1;
    JSON2Apex.OutputMessages obj311 = new JSON2Apex.OutputMessages();
    
     JSON2Apex.parse('{"outputMessages":[],"order":{"country":"SV","accountId":"0011820881","salesOrg":"CS01","paymentMethod":"CASH","total":1.5,"subtotal":1.5,"tax":0,"discount":-0.27,"empties":{"totalAmount":0,"minimumRequired":0,"extraAmount":0},"items":[{"sku":"000000000000009888","quantity":1,"freeGood":false,"discount":-0.27,"tax":0,"subtotal":1.5,"total":1.5,"price":1.5,"unitPrice":1.5}]}}');
  }
}