/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase:ONCALL_SendOrder_Batch.apxc
* Versión: 1.0.0.0
* 
* 
* 
* Historial de Cambios
* ----------------------------------------------------------------------------
* Fecha           Usuario            Contacto                           Descripción
* 11/05/2019    Gabriel Garcia    gabriel.e.garcia@accenture.com     Envio de ordenes OnTap  
*/
public class ONCALL_SendOrder_Batch implements Database.Batchable<sObject>, Database.AllowsCallouts{
    /*
* Method start return all data which is selected in the query
* Created By:gabriel.e.garcia@accenture.com
* @params  Database.BatchableContext c
* @return List<ONTAP__Order__c>
*/
    Set<Id> setIdOrders = new Set<Id>();
    
    public Database.QueryLocator start(Database.BatchableContext c){       
        String query = 'SELECT Id,ONCALL__SAP_Order_Item_Counter__c,ONCALL__Total_Order_Item_Quantity__c, '+
                                         'ONTAP__DocumentationType__c,ONTAP__OrderAccount__r.ONTAP__SalesOgId__c,ONTAP__DeliveryDate__c, '+
                                         'ISSM_PaymentMethod__c,Order_Reason__c,ISSM_OriginText__c '+
                                         'FROM ONTAP__Order__c '+
                                         'WHERE ONCALL__SAP_Order_Item_Counter__c != NULL ' +
                                         'AND ONCALL__SAP_Order_Item_Counter__c > 0 '+
                                         'AND ONCALL__Total_Order_Item_Quantity__c != NULL ' +
                                         'AND ONCALL__Total_Order_Item_Quantity__c> 0 '+
                                         'AND ONCALL__SAP_Order_Number__c = NULL '+
                                         'AND ONCALL__OnCall_Status__c = \'Closed\' '+
                                         'AND ISSM_PaymentMethod__c != NULL ' +
                                         'AND ISSM_OriginText__c != NULL ' + /* 'AND ISSM_OriginText__c != \'OCAL\' '+ */
                                         'AND ISSM_OriginText__c != \'OCAL\' ' +
                                         'AND ONTAP__DocumentationType__c != NULL ' +
                                         'AND ONTAP__OrderAccount__c != NULL ' +
                                         'AND ONTAP__OrderAccount__r.ONTAP__SalesOgId__c != NULL ' +
                                         'AND ONTAP__DeliveryDate__c != NULL ' +                                         
                                         //'AND (ONCALL__SAP_Order_Response__c = null OR ONCALL__SAP_Order_Response__c = \'\') ' +
                                         'AND (NOT ONCALL__SAP_Order_Response__c  LIKE  \'SEND BY BATCH WITH RESPONSE%\') ' +
                                         'AND ONTAP__OrderAccount__r.ONTAP__ExternalKey__c LIKE \'SV%\' ' +
                                         'AND CreatedDate >= YESTERDAY ';
                                         //'AND ID = \'a121k000000bpfdAAA\'';
        System.debug('ONCALL_SendOrder_Batch::query::' + query);                
        return DataBase.getQueryLocator(query);
    }

    /*
* Method Execute
* Created By: gabriel.e.garcia@accenture.com
* @params Database.BatchableContext c,List<ONTAP__Order__c> scope
* @return void
*/
    public void execute(Database.BatchableContext c, List<ONTAP__Order__c> scope){
        SettingsBatchSendOrder__c sbs = SettingsBatchSendOrder__c.getOrgDefaults();

        for(ONTAP__Order__c order: scope){                                                      
            //if(order.ONCALL__SAP_Order_Item_Counter__c==order.ONCALL__Total_Order_Item_Quantity__c){                  
                setIdOrders.add(order.Id);
            //}
        }
        
        System.debug('Total Ids : ' + setIdOrders.size());
        ONCALL_orderToJSON sjsn = new ONCALL_orderToJSON();
        User user = [SELECT id, username, Country FROM User where Id= :UserInfo.getUserId()];
        ONCALL_orderToJSON.countryAccount = user.Country;
        sjsn.isBatchProcess = sbs.UseBatchService__c;
        sjsn.createJson(setIdOrders);  
               
    }
    /*
    * Method that finalize the batch
    * Created By:gabriel.e.garcia@accenture.com
    * @params Database.BatchableContext c
    * @return void 
    */

    public void finish(Database.BatchableContext BC){
       String country = 'WITHOUT_COUNTRY_USER';
       try{
           country =  [Select ONTAP__Country_Alias__c From User Where Id = :UserInfo.getUserId()][0].ONTAP__Country_Alias__c;
       }catch(Exception e){
           System.debug('ONCALL_SendOrder_Batch::finish::' + e.getMessage());
       }
      
        SettingsBatchSendOrder__c sbs = SettingsBatchSendOrder__c.getOrgDefaults();
        Integer stophour = 20; 
        Integer stopminute = 59;
        Integer nextExecute = 2;
        if(sbs.Hour_Stop__c !=null && sbs.Hour_Stop__c < 25 && sbs.Hour_Stop__c > -1) stophour = Integer.valueOf(sbs.Hour_Stop__c);
        if(sbs.Minute_Stop__c !=null && sbs.Minute_Stop__c < 60 && sbs.Minute_Stop__c > -1) stopminute = Integer.valueOf(sbs.Minute_Stop__c);
        if(sbs.Next_Execute__c !=null && sbs.Next_Execute__c < 14001 && sbs.Next_Execute__c > -1) nextExecute = Integer.valueOf(sbs.Next_Execute__c);
           
        Datetime hoy =  System.now();
        
        hoy = hoy.addMinutes(nextExecute);      
        String cronExpression = '0 ' + String.valueOf(hoy.minute()) + ' ' + String.valueOf(hoy.hour()) + ' ' + String.valueOf(hoy.day()) + ' ' + String.valueOf(hoy.month()) + ' ? ' + String.valueOf(hoy.year());       
        System.debug('cronExpression ' + cronExpression);
      
        System.Debug('hour ' + stophour + ' minute '+ stopminute );
        System.debug('hora ' + hoy.hour() + ' minute ' + hoy.minute());
        if(hoy.hour() <= stophour && hoy.minute() <= stopminute ){
            String SCHEDULE_NAME =  'SendOrder:'+ country + String.valueOf(hoy.getTime());
            System.schedule(SCHEDULE_NAME, cronExpression, new ONCALL_SchedulerSendOrder());             
            //System.enqueueJob(new ONCALL_QueueableSendOrder());
        }
        AsyncApexJob asyncAJ = [Select Id, Status,ExtendedStatus,NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email    from AsyncApexJob where Id =: BC.getJobId()];
        Email_Custom_Setup__mdt listEmailCustom = [SELECT Id, MasterLabel, Emails__c, Label, Send_in_Error__c FROM Email_Custom_Setup__mdt WHERE Country__c = 'ALL' AND Label =: GlobalStrings.BATCH_ORDER_EMAILS LIMIT 1];
        if(listEmailCustom.Send_in_Error__c && asyncAJ.NumberOfErrors > 0){
            list<String> emails = listEmailCustom.Emails__c.split(';');
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = emails;
            mail.setToAddresses(toAddresses);
            mail.setSubject('ONCALL_SendOrder_Batch estatus: ' + asyncAJ.Status);
            mail.setPlainTextBody('ONCALL_SendOrder_Batch Jobs processed ' + asyncAJ.TotalJobItems +   'with '+ asyncAJ.NumberOfErrors + ' failures.');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
}