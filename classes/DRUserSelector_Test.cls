@isTest
public class DRUserSelector_Test {
    
    @isTest
    static void testMethods(){
        
        Test.startTest();
  
        User bdr =  DRFixtureFactory.newBDRUser('test-dr@test.com', 'test');
        
        List<String> routesSet = new List<String>();
        
        List<Id> userIds = new List<Id>();
        
        Map<String,String> userIdToCountryMap = new Map<String,String>(); 
        
        Map<String,String> routeToUserIdMap = new Map<String,String>();  
        
        Map<String,String> userIdToRouteMap = new Map<String,String>();  
        
        Map<String,String> usersIdToCountryMap = new Map<String,String>();
        
        routesSet.add( bdr.User_Route__c );
        
        userIds.add( bdr.Id );
               
        routeToUserIdMap = DRUserSelector.getRouteToUserIdMap( routesSet );
        
        System.assertEquals(1, routeToUserIdMap.size(), 'Empty map!');
        
        routeToUserIdMap = DRUserSelector.getUserIdToRouteMap( userIds );
        
        System.assertEquals(1, routeToUserIdMap.size(), 'Empty map!');
        
        userIdToRouteMap = DRUserSelector.getUserIdToCountryMap( userIds );
        
        System.assertEquals(1, userIdToRouteMap.size(), 'Empty map!');
        
        usersIdToCountryMap = DRUserSelector.getUsersIdToRouteMap( userIds );
        
        System.assertEquals(1, usersIdToCountryMap.size(), 'Empty map!');   
        
        DRUserSelector.getIntegrationId();
        
        Test.stopTest();
        
    }       

}