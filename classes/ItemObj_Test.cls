/* ----------------------------------------------------------------------------
* AB InBev :: 360 View
* ----------------------------------------------------------------------------
* Clase: ItemObj_Test.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 16/01/2019     Carlos Leal            Creation of methods.
*/

@isTest
public class ItemObj_Test {
    /**
    * Test method for set an ItemObj
    * Created By:heron.zurita@accenture.com
    * @param void
    * @return void
    */
	@isTest 
    static void ItemObj_Test_UseCase1(){
    	ItemObj item = new ItemObj();
        item.sku= 'Test';
        item.unitPrice= 0.0;
        item.discount= 0.0;
        item.freeGood= false;
        item.price= 0.0;
        item.subtotal= 0.0;
        item.tax= 0.0;
        item.total= 0.0;
        item.unit = 'Test';
        item.quantity = 0;
        List<Applieddeals> applieddeals = new List<Applieddeals>();
        item.applieddeals = applieddeals;
    }
}