/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: HONES_ONCALL_ProductsJson.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 17/01/2018           Luis Arturo Parra Rosas           Creation of methods.
*/

public class HONES_ONCALL_ProductsJson {
    
     /**
    * Methods getters and setters for entity HONES_ONCALL_ProductJson
    * Created By: heron.zurita@accenture.com
    * @param void
    * @return void
    */
    
	public HONES_ONCALL_HeaderList iwHeader {get; set;}
    public List<HONES_ONCALL_DealsList> deals {get; set;}
    public List<HONES_ONCALL_ItemsList> items {get; set;}
    public List<HONES_ONCALL_PartnerList> partner  {get; set;}

}