/**
 * Created by Dejair.Junior on 9/23/2019.
 */

global with sharing class DRBatchableProspectPublisher implements Database.Batchable<sObject> {

    private List<DRProspectOutboundMessage__e> prospectOutboundMessages = new List<DRProspectOutboundMessage__e>();
    private Map<String,List<Attachment>> accountIdToAttachmentsMap = new Map<String,List<Attachment>>();
    private List<Account> affectedAccounts = new List<Account>();
    List<String> prospectIds = new List<String>();

    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT Id,'+
                'Closest_POC_Code__r.ONTAP__SAPCustomerId__c,' +
                'ONTAP__Contact_First_Name__c,' +
                'ONTAP__Contact_Last_Name__c,' +
                'ONTAP__District__c,' +
                'ONTAP__Latitude__c,' +
                'ONTAP__Longitude__c,' +
                'Phone,' +
                'ONTAP__Province__c,' +
                'ONTAP__LegalName__c,' +
                'Name,' +
                'RNC_Number__c,' +
                'SIC,' +
                'ShippingAddress, ' +
                'DROutboundMessageProcessed__c ' +
                'FROM Account ' +
                'WHERE RecordType.DeveloperName = \'Prospect_DO\' ' +
                'AND DROutboundMessageProcessed__c = false';

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<Account> scope){

        //fisrt iteration: sets affected records and initializes IDs set
        for (Account account : scope) {

            affectedAccounts.add( account );
            prospectIds.add( account.Id );

        }

        setAttachmentsMap();

        //second iteration: map and prepares to publish events
        for (Account account : scope) {

            mapProspect( account );

        }

        publish();

    }

    global void finish(Database.BatchableContext bc){

        for ( Account account : affectedAccounts ) {

            account.DROutboundMessageProcessed__c = true;

        }

        update affectedAccounts;

    }

    private void mapProspect( Account account ) {

        String shippingAddress = '';

        try {

            shippingAddress = account.ShippingAddress.getStreet() + ', ' +
                              account.ShippingAddress.getCity() ;

        } catch (Exception e) {

            system.debug('>>> Exception: ' + e.getMessage());

        }

        DRProspectOutboundMessage__e outboundMessage = new DRProspectOutboundMessage__e(
                AccountId__c = account.Id,
                ClosestPOCCode__c = account.Closest_POC_Code__r.ONTAP__SAPCustomerId__c,
                ContactFirstName__c = account.ONTAP__Contact_First_Name__c,
                ContactLastName__c = account.ONTAP__Contact_Last_Name__c,
                District__c = account.ONTAP__District__c,
                Latitude__c = account.ONTAP__Latitude__c,
                Longitude__c = account.ONTAP__Longitude__c,
                Phone__c = account.Phone,
                Province__c = account.ONTAP__Province__c,
                LegalName__c = account.ONTAP__LegalName__c,
                Name__c = account.Name,
                RNCNumber__c = account.RNC_Number__c,
                SIC__c = account.SIC,
                ShippingAddress__c = shippingAddress
        );

        DRProspectOutboundMessage__e enrichedMessage = enrichProspectEvent( outboundMessage );

        if( enrichedMessage != null ){

            outboundMessage = enrichedMessage;

        }

        System.debug('----------------------------------------------------------------------------------------');
        System.debug('>>> Outbound message (after enrichment)');
        System.debug( outboundMessage );
        System.debug('----------------------------------------------------------------------------------------');
        prospectOutboundMessages.add( outboundMessage );

    }

    private void publish(){
        System.debug('>>> Publishing prospects...');
        System.debug( prospectOutboundMessages );
        List<Database.SaveResult> prospectResults = EventBus.publish( prospectOutboundMessages );
    }

    private DRProspectOutboundMessage__e enrichProspectEvent( DRProspectOutboundMessage__e message ){

        DRProspectOutboundMessage__e prospectEvent = new DRProspectOutboundMessage__e();
        DRAccountChangeHelper accountChangeHelper = new  DRAccountChangeHelper( message );
        accountChangeHelper.accountIdToAttachmentsMap = this.accountIdToAttachmentsMap;
        prospectEvent = accountChangeHelper.enrichProspectEvent();
        return prospectEvent;

    }

    private void setAttachmentsMap(){

        AttachmentSelector attachmentSelector = new AttachmentSelector( prospectIds );
        accountIdToAttachmentsMap = AttachmentSelector.getParentIdToAttachmentsMap();

    }

}