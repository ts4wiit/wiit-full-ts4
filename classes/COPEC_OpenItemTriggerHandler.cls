public with sharing virtual class COPEC_OpenItemTriggerHandler {
    
    @testvisible public static final String CO_OPEN_ITEM_RECORD_TYPE = 'CO_Open_Item';
    @testvisible public static final String PE_OPEN_ITEM_RECORD_TYPE = 'PE_Open_Item';
    @testvisible public static final String EC_OPEN_ITEM_RECORD_TYPE = 'EC_Open_Item';
    public List<ONTAP__OpenItem__c> openItems = new List<ONTAP__OpenItem__c>();
    public List<ONTAP__OpenItem__c> filteredOpenItems = new List<ONTAP__OpenItem__c>();
    public List<Account> accounts = new List<Account>();
    public Map<Id,Account> accountIdToAccountMap = new Map<Id,Account>();
    public Map<Id,Id> openItemIdToAccountIdMap = new Map<Id,Id>();
    public List<Id> accountIds = new List<Id>();
    private Id openItemRecordTypeId_CO;
    private Id openItemRecordTypeId_PE;
    private Id openItemRecordTypeId_EC;

    public class OpenItemException extends Exception {}

    public COPEC_OpenItemTriggerHandler(){}

    public COPEC_OpenItemTriggerHandler( List<ONTAP__OpenItem__c> openItems ){
        
        this.openItems = openItems;
        
    }

    public virtual void run(){

        this.setRecordType();
        this.filter();
        this.getAccounts();
        this.setOwner();       
        
    }

    public virtual void setRecordType(){

        try{

            openItemRecordTypeId_CO = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get( CO_OPEN_ITEM_RECORD_TYPE ).getRecordTypeId();
            openItemRecordTypeId_PE = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get( PE_OPEN_ITEM_RECORD_TYPE ).getRecordTypeId();
            openItemRecordTypeId_EC = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get( EC_OPEN_ITEM_RECORD_TYPE ).getRecordTypeId();

        } catch( Exception e ){

            throw new OpenItemException('>>> Error while setting record type: ' + e.getMessage());

        }

    }

     public virtual void filter(){

        for ( ONTAP__OpenItem__c openItem : openItems ){

            if( openItem.RecordTypeId == openItemRecordTypeId_CO || openItem.RecordTypeId == openItemRecordTypeId_PE || openItem.RecordTypeId == openItemRecordTypeId_EC ){

                filteredOpenItems.add( openItem );
                openItemIdToAccountIdMap.put( openItem.Id, openItem.ONTAP__Account__c );
                accountIds.add( openItem.ONTAP__Account__c );

            }
        }
    }

    public virtual void getAccounts(){

        accounts = DRAccountSelector.getAccountsByIds( accountIds );

        for( Account account : accounts ){

            accountIdToAccountMap.put(account.Id, account);

        }

    }
    
    public virtual void setOwner(){
        
        for ( ONTAP__OpenItem__c openItem : filteredOpenItems ){

            try{

                openItem.OwnerId = accountIdToAccountMap.get( openItem.ONTAP__Account__c ).OwnerId;

            } catch( Exception e ){

                throw new OpenItemException('>>> Related account not found: ' + e.getMessage());

            }

        }
    }

}