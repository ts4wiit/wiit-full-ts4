@isTest
public class DRSchedulableDeleteItinerary_Test {
    
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';

    @testSetup
    static void setupItineraries() {
        
        List<DOVisitPlan__c> visitPlans = new List<DOVisitPlan__c>();

        for ( Integer i = 0; i < 2000; i++ ){

            String externalId = 'KEY' + i;
            DOVisitPlan__c visitPlan = new DOVisitPlan__c( External_ID__c = externalId );
            visitPlans.add( visitPlan );

        }

        insert visitPlans;

    }

    @isTest
    static void testScheduledJob(){

        Test.startTest();

        String jobId = System.schedule('DRSchedulableDeleteItinerary',CRON_EXP , new DRSchedulableDeleteItinerary());

        Test.stopTest();

    }
}