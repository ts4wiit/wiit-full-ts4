/* ----------------------------------------------------------------------------
* AB InBev :: iRep
* ----------------------------------------------------------------------------
* Clase: ECPE_DeleteOpenItemsEmptiesBalanceBatch.apxc
* Versión: 1.0.0.0
* Descripción: Calendarizable que invoca el Batch ECPE_DeleteOpenItemsEmptiesBalanceBatch
* 
* 
* Historial de Cambios
* ----------------------------------------------------------------------------
* Fecha           Usuario            Contacto      						Descripción
* 07/10/2019    Carlos Álvarez	  carlos.alvarezs@gmodelo.com.mx     Creación de la clase  
*/
public class ECPE_DeleteOpenItemsEmptiesBalanceSch implements Schedulable {
    
    IREP_BatchControlExecution__mdt ibc_days;
    
	public void execute(SchedulableContext sc) {
        
        List<IREP_BatchControlExecution__mdt> listIBDays = [SELECT Id, DeveloperName, MasterLabel, Label, IREP_BatchSize__c FROM IREP_BatchControlExecution__mdt WHERE DeveloperName = 'ECPE_DeleteOI_EB_Days'];
        ibc_days = listIBDays != null && listIBDays.size() > 0 ? listIBDays[0] : new IREP_BatchControlExecution__mdt(Label = '30', IREP_BatchSize__c = 30);
        Integer days = Integer.valueOf(ibc_days.IREP_BatchSize__c);
        
        String queryEB = 'SELECT Id FROM ONTAP__EmptyBalance__c WHERE RecordType.Name IN (\'IREP_EmptyBalance_PE\', \'IREP_EmptyBalance_EC\', \'IREP_EmptyBalance_PA\') AND IREP_Country__c IN (\'PE\', \'EC\', \'PA\') AND LastModifiedDate < LAST_N_DAYS:' + days;
        String queryOI = 'SELECT Id FROM ONTAP__OpenItem__c WHERE RecordType.Name IN (\'IREP_OpenItem_PE\', \'IREP_OpenItem_EC\', \'IREP_OpenItem_PA\') AND IREP_Country__c IN (\'PE\', \'EC\', \'PA\') AND LastModifiedDate < LAST_N_DAYS:' + days;
        System.debug('queryEB: ' + queryEB);
        System.debug('queryOI: ' + queryOI);
        Database.executebatch( new ECPE_DeleteOpenItemsEmptiesBalanceBatch( queryEB, 'ONTAP__EmptyBalance__c'),200);
        Database.executebatch( new ECPE_DeleteOpenItemsEmptiesBalanceBatch( queryOI, 'ONTAP__OpenItem__c'),200);
        
    }
}