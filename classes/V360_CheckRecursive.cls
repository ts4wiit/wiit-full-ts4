/* ----------------------------------------------------------------------------
* AB InBev :: 360 View
* ----------------------------------------------------------------------------
* Clase: V360_CheckRecursive.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 18/12/2018     Gerardo Martinez        Creation of methods.
*/
public class V360_CheckRecursive {
    private static boolean run = true;
    private static Set<String> accounts = new Set<String>();
    /**
    * Returns true the first time that is called in a single transaction, the second time returns false
    * in that way we can ensure that a method is not recursive
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public static boolean runOnce(){
        if(run){
            run=false;
            return true;
        }else{
            return run;
        }
    }
    
    
    /**
    * Reformulates the previous method by relying on a set of external keys, instead of a single flag. 
    * Set False if at least one External key is repeated.
    * Created By: d.hernandez@ts4.mx
    * @param void
    * @return void
    */
    public static boolean preventRecursiveAccount(List<Account> listAccount){
        for(Account acc: listAccount){
            if(accounts.contains(acc.ONTAP__ExternalKey__c)){
                run = false;
                break;
            }else{
                run = true;
                accounts.add(acc.ONTAP__ExternalKey__c);
            }
        }
        return run;
    }
}