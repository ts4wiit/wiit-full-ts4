/* ----------------------------------------------------------------------------
* AB InBev :: Oncall
* ----------------------------------------------------------------------------
* Clase: ONCALL_orderToJSON_Test.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 16/01/2019     Carlos Leal            Creation of methods.
*/

@isTest
public class ONCALL_orderToJSON_Test {
    
    /**
    * Test method for create the Order for a Json
    * Created By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    @istest
    static void setupOrderJsonTest_Case1()
    {
        //ONCALL_orderToJSON orderTo = new ONCALL_orderToJSON();
 
        Test.setMock(HttpCalloutMock.class, new MockHTTPResponseGeneratorgenOrder());
      	
        //Order_Types__mdt Order_Record_Type = [SELECT Id, Atr_4__c, Order_Record_Type__c FROM Order_Types__mdt LIMIT 1];
        
        Account acc = new Account(Name = 'Test Acc',ONTAP__SalesOgId__c='IS');
        insert acc;
        
        //String RT = [SELECT id FROM recordType where sobjecttype = 'ONTAP__Order__c' AND DeveloperName =: Order_Record_Type.Order_Record_Type__c  LIMIT 1].id;
        Set<Id> orderSet = new Set<Id>();
        ONCALL_orderToJSON orderTo = new ONCALL_orderToJSON();
        
        ONTAP__Order__c order = new ONTAP__Order__c(ONTAP__OrderAccount__c = acc.id);
        order.ONCALL__SAP_Order_Item_Counter__c=1;
        order.ONCALL__Total_Order_Item_Quantity__c=1;
        order.ONTAP__DocumentationType__c='test';
        order.ISSM_PaymentMethod__c='Cash';
        order.ONTAP__DeliveryDate__c=System.today();
        order.ONCALL__SAP_Order_Number__c = '00121231283';
        insert order;  
        
        ONTAP__Product__c product= new ONTAP__Product__c();
        insert product;
        
        ONTAP__Order_Item__c orderItem = new ONTAP__Order_Item__c(ONTAP__CustomerOrder__c =order.Id, ONTAP__Cond_Applied__c = 'Deal');
        insert orderItem;
        ONTAP__Order_Item__c orderItem2 = new ONTAP__Order_Item__c(ONTAP__CustomerOrder__c =order.Id, ONTAP__ItemProduct__c = product.id);
        insert orderItem2;
        
		
        test.startTest(); 
        orderSet.add(order.id);
        orderTo.createJson(orderSet);
        ONCALL_orderToJSON.createJsonOnCall(orderSet);
        test.stopTest();
        
    }
    
    @istest
    static void setupOrderJsonTest_Case2()
    {
        //ONCALL_orderToJSON orderTo = new ONCALL_orderToJSON();
 
        Test.setMock(HttpCalloutMock.class, new MockHTTPResponseGeneratorgenOrder());
      	
        //Order_Types__mdt Order_Record_Type = [SELECT Id, Atr_4__c, Order_Record_Type__c FROM Order_Types__mdt LIMIT 1];
        
        Account acc = new Account(Name = 'Test Acc',ONTAP__SalesOgId__c='IS',ONTAP__ExternalKey__c = 'SV0123');
        insert acc;
        
        //String RT = [SELECT id FROM recordType where sobjecttype = 'ONTAP__Order__c' AND DeveloperName =: Order_Record_Type.Order_Record_Type__c  LIMIT 1].id;
        Set<Id> orderSet = new Set<Id>();
        ONCALL_orderToJSON orderTo = new ONCALL_orderToJSON();
        
        ONTAP__Order__c order = new ONTAP__Order__c(ONTAP__OrderAccount__c = acc.id);
        order.ONCALL__SAP_Order_Item_Counter__c=1;
        order.ONCALL__Total_Order_Item_Quantity__c=1;
        order.ONTAP__DocumentationType__c='test';
        order.ISSM_PaymentMethod__c='Cash';
        order.ONTAP__DeliveryDate__c=System.today();
        order.ONCALL__SAP_Order_Number__c = '00121231283';
        insert order;  
        
        ONTAP__Product__c product= new ONTAP__Product__c();
        insert product;
        
        ONTAP__Order_Item__c orderItem = new ONTAP__Order_Item__c(ONTAP__CustomerOrder__c =order.Id, ONTAP__Cond_Applied__c = 'Deal');
        insert orderItem;
        ONTAP__Order_Item__c orderItem2 = new ONTAP__Order_Item__c(ONTAP__CustomerOrder__c =order.Id, ONTAP__ItemProduct__c = product.id);
        insert orderItem2;
        
		
        test.startTest(); 
        orderSet.add(order.id);
        orderTo.createJson(orderSet);
        ONCALL_orderToJSON.createJsonOnCall_HONES(orderSet, '');
        test.stopTest();
        
    }
   
     
    
    
    
    
}