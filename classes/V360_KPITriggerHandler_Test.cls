/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_KPITriggerHandler_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 19/12/2018     Gerardo Martinez        Creation of methods.
 */
@isTest public class V360_KPITriggerHandler_Test {
	
    /**
    * Test Method for make the ralationship between KPIs and account
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    @isTest static void testRelationship(){
        Account acc =  New Account(Name='Test',ONTAP__SAP_Number__c='TEST');
        insert acc;
        ONTAP__KPI__c ob = New ONTAP__KPI__c(Account_Sap_Id__c='TEST',ONTAP__Categorie__c='Coverage');
        insert ob;
        update ob;
    }
}