/**
 * Created by Dejair.Junior on 10/17/2019.
 */

public with sharing class DRAccountPublisher {

    public static final String DR_ACCOUNT_RECORD_TYPE = 'Account_DO';
    private Map<Id,Account> newAccountMap = new Map<Id,Account>();
    private Map<Id,Account> oldAccountMap = new Map<Id,Account>();
    private Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get( DR_ACCOUNT_RECORD_TYPE ).getRecordTypeId();
    private String integrationId;
    private List<DRAccountOutboundMessage__e> accountOutboundMessages = new List<DRAccountOutboundMessage__e>();

    public DRAccountPublisher( Map<Id,Account> newAccountMap, Map<Id,Account> oldAccountMap ){
        this.newAccountMap = newAccountMap;
        this.oldAccountMap = oldAccountMap;
        this.integrationId = DRUserSelector.getIntegrationId();
    }

    public void run(){
        filterAccounts();
        publish();
    }

    private void filterAccounts(){
        for( Id accountId : newAccountMap.keySet() ){
            if( newAccountMap.get( accountId ).RecordTypeId == accountRecordTypeId && integrationId != newAccountMap.get( accountId ).LastModifiedById ){
                if( checkAccount( accountId ) ){
                    addAccount( newAccountMap.get( accountId ) );
                }
            }
        }
    }

    private Boolean checkAccount( Id accountId ){
        if ( checkAddress( accountId ) ) return true;
        if ( checkEmail( accountId ) ) return true;
        if ( checkDistrict( accountId ) ) return true;
        if ( checkLatitude( accountId ) ) return true;
        if ( checkLongitude( accountId ) ) return true;
        if ( checkNeighbourhood( accountId )) return true;
        if ( checkPhone( accountId ) ) return true;
        if ( checkProvince( accountId ) ) return true;
        if ( checkSecondaryPhone( accountId ) ) return true;
        return false;
    }

    private Boolean checkEmail( Id accountId ){
        return newAccountMap.get( accountId ).ONTAP__Email__c == oldAccountMap.get( accountId ).ONTAP__Email__c  ? false : true;
    }

    private Boolean checkDistrict( Id accountId ){
        return newAccountMap.get( accountId ).ONTAP__District__c == oldAccountMap.get( accountId ).ONTAP__District__c  ? false : true;
    }

    private Boolean checkLatitude( Id accountId ){
        return newAccountMap.get( accountId ).ONTAP__Latitude__c == oldAccountMap.get( accountId ).ONTAP__Latitude__c  ? false : true;
    }

    private Boolean checkLongitude( Id accountId ){
        return newAccountMap.get( accountId ).ONTAP__Longitude__c == oldAccountMap.get( accountId ).ONTAP__Longitude__c  ? false : true;
    }

    private Boolean checkNeighbourhood( Id accountId ){
        return newAccountMap.get( accountId ).ONTAP__Neighborhood__c == oldAccountMap.get( accountId ).ONTAP__Neighborhood__c  ? false : true;
    }

    private Boolean checkPhone( Id accountId ){
        return newAccountMap.get( accountId ).Phone == oldAccountMap.get( accountId ).Phone  ? false : true;
    }

    private Boolean checkProvince( Id accountId ){
        return newAccountMap.get( accountId ).ONTAP__Province__c == oldAccountMap.get( accountId ).ONTAP__Province__c  ? false : true;
    }

    private Boolean checkSecondaryPhone( Id accountId ){
        return newAccountMap.get( accountId ).ONTAP__Secondary_Phone__c == oldAccountMap.get( accountId ).ONTAP__Secondary_Phone__c  ? false : true;
    }

    private Boolean checkAddress( Id accountId ){
        return newAccountMap.get( accountId ).ShippingAddress == oldAccountMap.get( accountId ).ShippingAddress  ? false : true;
    }

    private void addAccount( Account account ){


        DRAccountOutboundMessage__e outboundMessage = new DRAccountOutboundMessage__e(
            District__c = account.ONTAP__District__c,
            Email__c = account.ONTAP__Email__c,
            Latitude__c = account.ONTAP__Latitude__c,
            Longitude__c = account.ONTAP__Longitude__c,
            Neighborhood__c = account.ONTAP__Neighborhood__c,
            Phone__c = account.Phone,
            Province__c = account.ONTAP__Province__c,
            SecondaryPhone__c = account.ONTAP__Secondary_Phone__c,
            ShippingAddress__c =  account.ShippingStreet,
            SAPCustomerId__c = account.ONTAP__SAPCustomerId__c
        );
        accountOutboundMessages.add( outboundMessage );
    }

    private void publish(){
        System.debug( accountOutboundMessages );
        List<Database.SaveResult> accountResults = EventBus.publish( accountOutboundMessages );
    }

}