({
	GetDataCase: function(cmp, pIdCase)
    {
        var oDetailcase;
        var action = cmp.get("c.GetDetailCase");
        action.setParams({idCase: pIdCase});
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                oDetailcase = response.getReturnValue();                
                if(oDetailcase.ISSM_TypificationLevel4__c == null || oDetailcase.ISSM_TypificationLevel4__c == '')
                {
                    if(oDetailcase.ISSM_TypificationLevel3__c != null &&  oDetailcase.ISSM_TypificationLevel3__c != '')
                        oDetailcase.ISSM_TypificationLevel4__c = oDetailcase.ISSM_TypificationLevel3__c;
                    else if (oDetailcase.ISSM_TypificationLevel2__c != null &&  oDetailcase.ISSM_TypificationLevel2__c != '')
                        oDetailcase.ISSM_TypificationLevel4__c = oDetailcase.ISSM_TypificationLevel2__c;
                    else if (oDetailcase.ISSM_TypificationLevel1__c != null &&  oDetailcase.ISSM_TypificationLevel1__c != '')
                        oDetailcase.ISSM_TypificationLevel4__c = oDetailcase.ISSM_TypificationLevel1__c;
                }
                cmp.set("v.DetailCase", oDetailcase);
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else 
                {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    GetAccountName: function(cmp, pIdCase)
    {
        var action = cmp.get("c.GetDetailAccount");
        action.setParams({idCase: pIdCase});
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                cmp.set("v.AccountName", response.getReturnValue());
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else 
                {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    GetQuestions: function(cmp, pIdCase)
    {
        var action = cmp.get("c.GetQuestionCloseCase");
        action.setParams({idCase: pIdCase});
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                cmp.set("v.CloseQuestion", response.getReturnValue());
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else 
                {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    GetOptionsMasterSolution: function(cmp)
    {
        var action = cmp.get("c.GetSolutionMasterOptions");
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                cmp.set("v.lstOptions", response.getReturnValue());
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else 
                {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    CloseCase: function(cmp, pIdCase, pIdCaseForce, pDescription, pSolutionMaster)
    {
        var recType = cmp.get("v.recType");
        var action = cmp.get("c.CloseCase");
        action.setParams({idCase:pIdCase, idCaseForce:pIdCaseForce, solutionDescription:pDescription, optionMasterSolution:pSolutionMaster, recType:recType});
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                var closeCase = response.getReturnValue();
                if(closeCase === true)
                {
                    sforce.one.navigateToURL('/' + pIdCase, true);
                }
                else
                {
                   cmp.set("v.tituloModal", $A.get("$Label.c.CS_Titulo_Error"));
                   cmp.set("v.mensajesValidacion",  $A.get("$Label.c.CS_Caso_No_Cerrado"));
                   cmp.set("v.showModal", true);
                }
                cmp.set("v.showSpinner", false);
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else 
                {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
})