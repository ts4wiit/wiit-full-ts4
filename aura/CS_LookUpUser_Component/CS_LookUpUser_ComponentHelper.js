({
	searchHelper : function(cmp, event, getInputkeyWord) 
    {
        var action = cmp.get("c.fetchLookUpValues");
        action.setParams({ 'searchKeyWord': getInputkeyWord,'ObjectName' : cmp.get("v.objectAPIName") });       
        action.setCallback(this, function(response) 
        {
            $A.util.removeClass(cmp.find("mySpinner"), "slds-show");
            var state = response.getState();
            
            if (state === "SUCCESS") 
            {
                var storeResponse = response.getReturnValue();
                if (storeResponse.length == 0) 
                    cmp.set("v.Message", 'No Result Found...');
                else 
                    cmp.set("v.Message", '');
                cmp.set("v.listOfSearchRecords", storeResponse);
            }
        });

        $A.enqueueAction(action);
	},
})