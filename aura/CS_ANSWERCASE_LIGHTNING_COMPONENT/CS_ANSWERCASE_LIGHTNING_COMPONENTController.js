({	
    doInit : function(component, event, helper)
    {
        //Get Lists of Questions and Answers
        helper.getQuestionsAnswersFunc(component, event);        
    },    
    
    saveAnsw : function(component, event, helper)
    {
        //Save List of answers
        helper.saveFunc(component, event);
    },
    
    collapseSeccion : function(component, event, helper) 
    {
       helper.collapseFunc(component,event,'expand');
    },
    
    functionPhone : function(component, event, helper)
    {
        helper.formatPhone(component, event);
	},
    
    functionNumber : function(component, event, helper){
        helper.formatNumber(component, event);
    }
})