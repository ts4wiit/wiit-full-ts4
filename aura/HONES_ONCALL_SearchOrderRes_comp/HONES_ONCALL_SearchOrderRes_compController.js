({
    doInit : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        //helper.getpicklist(component,recordId);
    },
    
    selectRecord : function(component, event, helper){      
        // get the selected record from list selectRecord         
        var getSelectRecord = component.get("v.oRecord");
        
        getSelectRecord.quantity = component.get("v.quantity");
        if (getSelectRecord.quantity == null || !Number.isInteger(getSelectRecord.quantity) || getSelectRecord.quantity<1 ){            
            alert($A.get("$Label.c.You_must_enter_an_amount"));            
        }else{  
            component.set("v.isLoading", true);
            component.get("v.oRecord.lista").forEach(function(item){
                if (item == component.get("v.selectedValue")){
                    getSelectRecord.productType=component.get("v.selectedValue"); 
                    console.log(':::sObject-->', JSON.stringify(getSelectRecord));
                    getSelectRecord.selectedByUser=component.get("v.clickedbyUser");
                    getSelectRecord.reasonCode = component.get("v.reasonSelected");
                    getSelectRecord.reasonDescription = component.get("v.reasonDescription");
                    getSelectRecord.measureCode = component.get('v.UnitSelected');
                    
                    console.log("getSelectRecord.selectedByUser-"+getSelectRecord.selectedByUser);                    
                    console.log("etse_-"+component.get("v.selectedValue"));
                    console.log("the product --"+getSelectRecord.productType);                        
                    // call the event   
                    
                    var compEvent = component.getEvent("oSelectedRecordEvent");
                    
                    console.log("inevent-"+JSON.stringify(getSelectRecord));
                    
                    compEvent.setParams({"recordByEvent" : getSelectRecord});  
                    // fire the event  
                    compEvent.fire();
                    component.find("inputbar").set("v.value", "");
                    
                    var searchEvent = component.getEvent("oSearchRecordEvent");
                    searchEvent.setParams({"cleanAndFocus" : true});  
                    // fire the event  
                    searchEvent.fire();
                    component.set("v.isLoading", false);
                }
            });
            
        }                        
    },
    
    clear :function(component,event,heplper){        
        var pillTarget = component.find("lookup-pill");
        var lookUpTarget = component.find("lookupField");  
        
        $A.util.addClass(pillTarget, 'slds-hide');
        $A.util.removeClass(pillTarget, 'slds-show');
        
        $A.util.addClass(lookUpTarget, 'slds-show');
        $A.util.removeClass(lookUpTarget, 'slds-hide');
        component.set("v.SearchKeyWord",null);
        component.set("v.listOfSearchRecords", null );
        component.set("v.selectedRecord", {} );
        component.set("v.search", "slds-input__icon slds-show");                
    },
    
    onChangeProductType: function(component,event,heplper){
        console.log('Enter to change', component.get('v.selectedValue'));
        var myMap = [];
        var myUnitMap = [];
        console.log(':::onChangeProductType map',component.get('v.initVariables.lstReasonsProductType'));
        for(var key in component.get('v.initVariables.lstReasonsProductType')){
            console.log('for--> key ', key);
            console.log('for--> Value ', component.get('v.initVariables.lstReasonsProductType')[key]);
            console.log('v.selectedValue ', component.get('v.selectedValue'));
            if(component.get('v.selectedValue')==key){
                
                console.log(':::key-->',key);
            console.log(':::values-->,',component.get('v.initVariables.lstReasonsProductType')[key]);
            component.set('v.lstReasons',component.get('v.initVariables.lstReasonsProductType')[key]);
                
                (component.get('v.initVariables.lstUnitsProductype')[key]).forEach(function(element,index) {
                    var item = element.split('-');
                    myUnitMap.push({value:item[1], key:item[0]});
                    if(index==0){
                     console.log('Unit index-->',index);
                     component.set('v.UnitSelected',item[0]);    
                     //component.set('v.reasonDescription',item[1]);  
                    }
                });
                
                (component.get('v.initVariables.lstReasonsProductType')[key]).forEach(function(element,index) {
                    var item = element.split('-');
                    myMap.push({value:item[1], key:item[0]});
                    if(index==0){
                     console.log('Reason index-->',index);
                     component.set('v.reasonSelected',item[0]);    
                     component.set('v.reasonDescription',item[1]);  
                    }
                });
                
                break;
            } else {
                component.set('v.UnitSelected',null);
                component.set('v.reasonSelected',null);    
                component.set('v.reasonDescription',null); 
            }
       }
        component.set('v.mapUnits',myUnitMap);
        component.set('v.mapReasons',myMap);
    },
    
    onchangeReason:function(component,event,helper){
         console.log('onchangeReason');
        
        for(var key in component.get('v.initVariables.lstReasonsProductType')){
            if(component.get('v.selectedValue')==key){
                console.log(':::key-->',key);
            console.log(':::values-->,',component.get('v.initVariables.lstReasonsProductType')[key]);
            component.set('v.lstReasons',component.get('v.initVariables.lstReasonsProductType')[key]);
                (component.get('v.initVariables.lstReasonsProductType')[key]).forEach(function(element) {
                    var item = element.split('-');
                    if(item[0]==component.get('v.reasonSelected')){
                        component.set('v.reasonDescription',item[1]);
                       }
                });
                break;
            }
       }	

	}
    
})