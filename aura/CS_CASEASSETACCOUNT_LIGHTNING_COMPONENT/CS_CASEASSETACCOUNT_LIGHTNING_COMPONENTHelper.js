({    
    getSerialNumber: function(component)
    {
        var action = component.get("c.getSerialNumber");
        var idCase = component.get("v.RecordId");
        action.setParams({idCase: idCase});
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                component.set("v.SerialNumbers", response.getReturnValue());
                if(response.getReturnValue().length > 1)
                {
                    component.set("v.listSerialNumber", 'True');
                }
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors)
                {
                    if (errors[0] && errors[0].message)
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                }
                else
                {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    },
        
    getAssetInfo:function(component, SerialNumber)
    {
        var action = component.get("c.getAssetInfo");
        var idCase = component.get("v.RecordId");
        action.setParams({idCase:idCase, serialNumber:SerialNumber});
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {   
                var conts = response.getReturnValue();
                component.set("v.selectedBrand", conts.ONTAP__Brand__c);
                component.set("v.selectedModel", conts.HONES_Asset_Model__c);
                component.set("v.Description", conts.ONTAP__Asset_Description__c);
                component.set("v.selectedEquipmentNumber", conts.HONES_EquipmentNumber__c);
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors)
                {
                    if (errors[0] && errors[0].message)
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                }
                else
                {
                    console.log("Unknown error");
                }
            } 
        });
        
        $A.enqueueAction(action);        
    },
    
    setAssetCaseInformation: function(component, idCase)
    {
        var idCase = component.get("v.RecordId");
        var action = component.get("c.SetAssetCaseInfo");
        action.setParams({idCase: idCase});
        
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                var conts = response.getReturnValue();
                if(conts == null)
                {
                    console.log("Wihtout Asset Info");
                }
                else
                {
                    component.set("v.caseUpdated",conts);
                    component.set("v.quantity",conts[0].ISSM_CountRetiro__c);
                    component.set("v.selectedBrand",conts[1].ONTAP__Brand__c);
                    component.set("v.selectedModel",conts[1].HONES_Asset_Model__c);
                    component.set("v.Description",conts[1].ONTAP__Asset_Description__c);
                    component.set("v.selectedEquipmentNumber",conts[1].HONES_EquipmentNumber__c);
                    component.set("v.selectedSerialNumber",conts[1].ONTAP__Serial_Number__c);
                    component.set("v.listSerialNumber",'False');
                }
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors)
                {
                    if (errors[0] && errors[0].message)
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else
                {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    
      updateAndCallout: function(component, event, helper)
    {
        var idCase = component.get("v.RecordId");
        console.log('updateAndCallout.idCase:'+idCase);
        var SerialNumber = component.get("v.selectedSerialNumber");
        var quantity = component.get("v.quantity");
        var onlyUpdate = false;
        var action = component.get("c.UpdateCase");
        action.setParams({idCase:idCase, serialNumber:SerialNumber, quantity:quantity,onlyUpdate:onlyUpdate});
        
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            console.log('CalloutStatus:'+response);
            if(state === "SUCCESS")
            {
                var isUpdatedAndSend = response.getReturnValue();
                console.log('CaseAssetResponse: '+response);
                if(isUpdatedAndSend)
                {
                    component.set("v.Error", false);
                    component.set("v.showSpinner", false);
                    component.set("v.caseUpdated",true);
                    component.set("v.mensajeEnvio",$A.get("$Label.c.CS_Caso_Enviado"));
                    component.set("v.buttonDisabled",true);
                    component.set("v.Edit", false);
                }
                else
                {
                    component.set("v.showSpinner", false);
                    component.set("v.mensajeEnvio",$A.get("$Label.c.CS_Caso_Error"));
                    component.set("v.buttonDisable",true);
                    component.set("v.Edit",false);
                    console.log('ErrorCallout'+$A.get("$Label.c.CS_Caso_Error"));
                }
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors)
                {
                    if (errors[0] && errors[0].message)
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else
                {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action); 
    },

    //SM
    updateOnly: function(component, event, helper)
    {
        var idCase = component.get("v.RecordId");
        console.log('updateAndCallout.idCase:'+idCase);
        var SerialNumber = component.get("v.selectedSerialNumber");
        var quantity = component.get("v.quantity");
        var onlyUpdate = true;
        var action = component.get("c.UpdateCase");
        action.setParams({idCase:idCase, serialNumber:SerialNumber, quantity:quantity, onlyUpdate:onlyUpdate});
        
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            console.log('CalloutStatus:'+response);
            if(state === "SUCCESS")
            {
                var isUpdatedAndSend = response.getReturnValue();
                console.log('CaseAssetResponse: '+response);
                if(isUpdatedAndSend)
                {
                    component.set("v.Error", false);
                    component.set("v.showSpinner", false);
                    component.set("v.caseUpdated",true);
                    component.set("v.mensajeEnvio",$A.get("$Label.c.CS_AssetInfoSavedCorrectly"));
                    component.set("v.buttonDisabled",true);
                    component.set("v.Edit", false);
                }
                else
                {
                    component.set("v.showSpinner", false);
                    component.set("v.mensajeEnvio",$A.get("$Label.c.CS_AssetInfoNotSaved"));
                    component.set("v.buttonDisable",true);
                    component.set("v.Edit",false);
                }
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors)
                {
                    if (errors[0] && errors[0].message)
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else
                {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action); 
    },
    
    updateAsset: function(component)
    {
        var idCase = component.get("v.RecordId");
        var serialNumber = component.get("v.SerialNumber"); 
        console.log('este es el serial ' + serialNumber);       
        //var action = component.get("c.UpdateCaseAsset");
        //var action = component.get("c.sendAvisoMantenimiento");
        //SM - Se cambió el método al que se llama en la clase
        var action = component.get("c.sendAvisoMantenimientoCS");
        action.setParams({idCase:idCase, SNumber:serialNumber});
        
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            console.log('stateAvisoMtto: '+state);
            if(state === "SUCCESS")
            {
                var customResponse = response.getReturnValue();
                console.log(customResponse);
                if(customResponse.success)
                {
                    component.set("v.showSpinner", false);
                    component.set("v.caseUpdated",true);
                    component.set("v.mensajeEnvio",$A.get("$Label.c.CS_Caso_Reparacion"));
                    component.set("v.buttonDisabled",true);
                    console.log('Detail message: '+customResponse.message);
                    //location.reload();
                }
                else
                {
                    component.set("v.showSpinner", false);
                    component.set("v.mensajeError",$A.get("$Label.c.CS_Caso_Error"));
                    console.log('Detail message: '+customResponse.message);
                    //component.set("v.mensajeEnvio",$A.get("$Label.c.CS_Caso_Error") + customResponse.message);
                    component.set("v.buttonDisable",true);
                }
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors)
                {
                    if (errors[0] && errors[0].message)
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else
                {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action); 
    }

})