({
    doInit : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        helper.getDataById(component,recordId);
        helper.getDataDifferentId(component,recordId);
    },
 	sectionOne : function(component, event, helper) {
       helper.helperFun(component,event,'articleOne');
    },
})