({
	    doInit : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        helper.getDataLimit(component,recordId);
        helper.getDataDisponible(component,recordId);
        helper.getDataOutOfDate(component,recordId);
        helper.getDataOutOfDateLiquid(component,recordId);    
        helper.getDataOutOfDateContainer(component,recordId);
        helper.getDataOfDate(component,recordId);
        helper.getDataOfDateTotal(component,recordId);
        helper.getDataOnTimeDateLiquid(component,recordId);    
        helper.getDataOnTimeDateContainer(component,recordId);
           
        }
})